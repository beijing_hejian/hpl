/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.modle

import android.text.TextUtils


import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.Utils.FileUtils

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.modle
 * @class describe
 * @anthor admin
 * @time 2021/8/21 15:07
 * @change
 * @chang time
 * @class describe
 */

class ConstantViewMolde {
    companion object {
        var isLogin: Boolean? = null
        var User: UserModle? = null
        fun IsLogin(): Boolean {
            GetUser();
            if (User == null) {
                isLogin = false
            } else {
                if (User!!.id == null) {
                    isLogin = false
                } else {
                    isLogin = true
                }
            }
            return isLogin as Boolean
        }

        fun GetUser(): UserModle? {
            if (User != null) {
                return User
            } else {
                User = FileUtils.getObject(Myapplication.mcontext, "LoginUser") as UserModle?
            }
            return User
        }

        fun SetUser(userModles: UserModle?) {
            FileUtils.saveObject(Myapplication.mcontext, "LoginUser", userModles)
            User = userModles
        }

        fun clearUser() {
            FileUtils.saveObject(Myapplication.mcontext, "LoginUser", null)
            Myapplication.editor.putBoolean("isLogin",false).commit()
            User = null
        }

        fun getToken(): String {
            if (IsLogin()) {
                var token: String = Myapplication.sp.getString("access_token", "")
                return token;
            } else {
                return "";
            }
        }

        fun setToken(token: String) {
            if (!TextUtils.isEmpty(token)) {
                Myapplication.editor.putString("access_token", token).commit()
            } else {
                Myapplication.editor.putString("access_token", "").commit()
            }
        }
    }


}