/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Activity

import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import com.hyphenate.EMCallBack
import com.hyphenate.chat.EMClient
import com.peaceclient.hospitaldoctor.com.Base.BaseUrlUtil
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.Hy.DemoHelper
import com.peaceclient.hospitaldoctor.com.InterFace.CallBackUtils
import com.peaceclient.hospitaldoctor.com.InterFace.ILaunchManagerService
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.FileUtils
import com.peaceclient.hospitaldoctor.com.Utils.Utils
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.TokenBean
import com.peaceclient.hospitaldoctor.com.modle.UserModle
import com.vondear.rxtools.view.RxToast
import com.vondear.rxtools.view.dialog.RxDialogLoading
import kotlinx.android.synthetic.main.regis_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/8/18 15:13
 * @change
 * @chang time
 * @class describe 登陆页面（旧版废弃）
 */

class LoginAct() : HoleBaseActivity(), Parcelable {
    var inten: Intent? = null
    var ser: ILaunchManagerService? = null
    var eyestatus: Boolean = false

    constructor(parcel: Parcel) : this() {
        inten = parcel.readParcelable(Intent::class.java.classLoader)
        eyestatus = parcel.readByte() != 0.toByte()
    }

    //有参次构造器
    constructor(name: Intent, age: ILaunchManagerService) : this() {
        this.inten = name
        this.ser = age
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.regis_act)
        arrow_back.setOnClickListener {
            finish()
        }

        val spannable = SpannableStringBuilder("同意和平里医院《用户协议》和《隐私政策》")
        //设置文字的前景色，2、4分别表示可以点击文字的起始和结束位置。
        spannable.setSpan(ForegroundColorSpan(resources.getColor(R.color.home_green)), 7, 13, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannable.setSpan(ForegroundColorSpan(resources.getColor(R.color.home_green)), 14, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        //这个一定要记得设置，不然点击不生效
        agreement.setMovementMethod(LinkMovementMethod.getInstance())
        spannable.setSpan(TextClick(), 7, 13, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannable.setSpan(TextClickS(), 14, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        agreement.setText(spannable)
        password_eye.setOnClickListener {
            // 不可见密码
            if (!eyestatus) {
                password_eye.setImageResource(R.drawable.passord_open)
                passwords.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                passwords.setSelection(passwords.text.length)
            } else
            // 可见的密码
            {
                password_eye.setImageResource(R.drawable.password_close)
                passwords.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD or InputType.TYPE_CLASS_TEXT)
                passwords.setSelection(passwords.text.length)
            }
            eyestatus = !eyestatus
        }
        /***
         * 跳转忘记密码页面
         */
        forget_.setOnClickListener {
            startActivity(Intent(this, ForgetPassAct::class.java))

        }
        /****
         *  传输名字和密码，请求token
         */
        login.setOnClickListener {
            if (postAction()) {
                GetToken(account.text.toString(), passwords.text.toString())
            }
        }
        /****
         * 注册点击链接
         */
        regist.setOnClickListener {
            startActivity(Intent(this, RegisDact::class.java))
        }
    }

    /***
     * yon用户协议页面
     */
    private inner class TextClick() : ClickableSpan() {
        override fun onClick(widget: View) {
            //在此处理点击事件
            startActivity(Intent(this@LoginAct, AgreeMentAct::class.java))
        }

        override fun updateDrawState(ds: TextPaint) {
            ds.isUnderlineText = false
            //文字的颜色 ds.setUnderlineText(true); //是否设置下划线，true表示设置。
        }

    }

    /***
     * 隐私政策页面
     */
    private inner class TextClickS() : ClickableSpan() {
        override fun onClick(widget: View) {
            //在此处理点击事件
            startActivity(Intent(this@LoginAct, PrivacyAct::class.java))
        }

        override fun updateDrawState(ds: TextPaint) {
            ds.isUnderlineText = false
            //文字的颜色 ds.setUnderlineText(true); //是否设置下划线，true表示设置。
        }
    }

    /***
     * 提交之前的验证
     * @return Boolean 返回true 则通过验证，返回false 则数据验证失败
     */
    private fun postAction(): Boolean {
        if (TextUtils.isEmpty(passwords.text)) {
            RxToast.normal("请输入密码")
            return false;
        } else {
            if (Utils.isPasswordss(passwords.text!!.toString())) {
                return true
            } else {
                RxToast.normal("密码格式不正确")
                return false
            }
        }
        return true;
    }

    /***
     *  请求token接口
     * @param account String
     * @param password String
     */
    fun GetToken(account: String, password: String) {
        var loading: RxDialogLoading = RxDialogLoading(this)
        loading.show()
        var baseUrlUtil: BaseUrlUtil = BaseUrlUtil()
        baseUrlUtil.getInstance().Token("residentApp", "B262DC5E-5562-43FA-9", account, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<HoleResponse<TokenBean>> {
                    override fun onError(e: Throwable?) {
                        loading.dismiss()
                    }

                    override fun onNext(t: HoleResponse<TokenBean>?) {
                        when (t?.code) {
                            0 -> {
                                ConstantViewMolde.setToken(t?.data?.access_token ?: "")
                                // 获取用户信息
                                baseUrlUtil.getInstance().getUser(t?.data?.access_token
                                        ?: "").subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(object : Observer<HoleResponse<UserModle>> {
                                            override fun onError(e: Throwable?) {
                                                loading?.dismiss()
                                            }

                                            override fun onNext(t1: HoleResponse<UserModle>?) {
                                                if (t1?.data != null) {
                                                    if (TextUtils.isEmpty(t1?.data?.imAccount)) {
                                                        Myapplication.editor.putBoolean("isLogin", true).commit()
                                                        FileUtils.saveObject(this@LoginAct, "LoginUser", t1?.data);
                                                        ConstantViewMolde.SetUser(t1?.data)
                                                        handleLoginSuccess3();

                                                    } else {
                                                        // 登录环信

                                                        login(t1.data!!)

                                                    }
                                                }else{
                                                    RxToast.normal(t?.msg?:"")
                                                }

                                            }

                                            override fun onCompleted() {
                                                loading?.dismiss()
                                            }
                                        })


                            }
                            1 -> RxToast.normal(t?.msg!!)
                            else -> RxToast.normal(t?.msg!!)
                        }
                    }

                    override fun onCompleted() {
                    }
                })
    }

    /***
     * 环信登录接口
     * @param user UserModle
     */
    private fun login(user: UserModle) {
        try {
            if (!TextUtils.isEmpty(user?.imAccount)) {
                EMClient.getInstance().login(user.imAccount,user.imSecret, object : EMCallBack {
                    override fun onSuccess() {
                        DemoHelper.getInstance().setCurrentUserName(user.account as String)
                        EMClient.getInstance().groupManager().loadAllGroups()
                        EMClient.getInstance().chatManager().loadAllConversations()
                        ConstantViewMolde.SetUser(user)
                        Myapplication.editor.putBoolean("isLogin", true).commit()
                        FileUtils.saveObject(this@LoginAct, "LoginUser", user)
                        runOnUiThread {
                            handleLoginSuccess3();
                        }
                        // 登录成功后，如果后端云没有缓存用户信息，则新增一个用户
                        // UserWebManager.createUser(userId, nickName, avatarUrl);
                        // 自动加martin为好友，并自动发送一条消       息
                        // ** manually load all local groups and conversation
                        //推送的昵称
                        if (!TextUtils.isEmpty(user.imAccount)) {
                            val nick = user.imAccount as String
                        }
                        println("环信登录回调")
                        val updatenick = EMClient.getInstance().pushManager().updatePushNickname(if (user.account === "") "" else user.account)
                        if (!updatenick) {
                            Log.e("LoginActivity", "update current user nick fail")
                        }
                        DemoHelper.getInstance().getUserProfileManager().asyncGetCurrentUserInfo()

                    }

                    override fun onError(i: Int, s: String) {
                        runOnUiThread {
                            println(i.toString() +"错误码"+s)
                            RxToast.normal("登录失败,请稍后重试")
                        }

                    }

                    override fun onProgress(i: Int, s: String) {

                    }

                })
            } else {
//                if (dialog1 != null || dialog1.isShowing()) {
//                    dialog1.dismiss()
//                }

            }
        } catch (e: Exception) {
            println(e.toString())
        }

    }

    /***
     * 全局跳转路径拦截
     */
    fun handleLoginSuccess3() {
        val bundle = intent.getBundleExtra(Myapplication.SHARED_NAME)
        val className = bundle.getString(Myapplication.CLASSNAME)
        if (className.equals("com.peaceclient.com.Activity.LoginAct")) {
            CallBackUtils.LoginSuccess(0)
            CallBackUtils.ChangeState(0)
        } else {
            intent.component = ComponentName(this@LoginAct, className)
            bundle.remove(Myapplication.CLASSNAME)
            intent.putExtra(Myapplication.SHARED_NAME, bundle)
            CallBackUtils.LoginSuccess(intent)
        }
        finish()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(inten, flags)
        parcel.writeByte(if (eyestatus) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LoginAct> {
        override fun createFromParcel(parcel: Parcel): LoginAct {
            return LoginAct(parcel)
        }

        override fun newArray(size: Int): Array<LoginAct?> {
            return arrayOfNulls(size)
        }
    }


}