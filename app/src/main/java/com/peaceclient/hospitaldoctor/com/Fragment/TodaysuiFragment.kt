package com.peaceclient.hospitaldoctor.com.Fragment

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.TextUtils
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.InterFace.CallBackUtils
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.ClientModle
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.GhModle
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.vondear.rxtools.view.dialog.RxDialog
import com.vondear.rxtools.view.dialog.RxDialogSureCancel
import rx.Observer
import rx.android.schedulers.AndroidSchedulers

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.Fragment
 * @class describe
 * @anthor admin
 * @time 2021/9/15 14:34
 * @change
 * @chang time
 * @class describe   待接诊页面
 */

class TodaysuiFragment() : BaseLazyFragment() {


    private var rootView: View? = null
    private var kong: View? = null
    private var adapter: CheckHisAdapter? = null
    private var messageList: ArrayList<GhModle> = arrayListOf()
    private var visit_recycle: RecyclerView? = null
    private var visitsmart: SmartRefreshLayout? = null
    private var page: Int = 0;
    override fun lazyLoad() {
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = layoutInflater.inflate(R.layout.today_sui_fragment, null)
        kong = View.inflate(Myapplication.mcontext, R.layout.empty_view, null)
        visit_recycle = rootView?.findViewById<RecyclerView>(R.id.sui_recycle)
        visitsmart = rootView?.findViewById<SmartRefreshLayout>(R.id.suismart)
        val manager =
            LinearLayoutManager(Myapplication.mcontext, LinearLayoutManager.VERTICAL, false)
        visit_recycle?.setLayoutManager(manager)
        adapter = CheckHisAdapter(R.layout.today_sui_layout, messageList)
        adapter?.bindToRecyclerView(visit_recycle)
        adapter?.setEmptyView(kong)
        adapter?.isUseEmpty(true)
        visit_recycle?.setAdapter(adapter)
        adapter?.notifyDataSetChanged()
        visitsmart?.autoRefresh()
        visitsmart?.setOnRefreshListener {
            messageList?.clear()
            getList()
        }
        adapter?.setOnItemChildClickListener { adapter, view, position ->
            when (view) {
                adapter.getViewByPosition(visit_recycle, position, R.id.call) -> {
                    if (TextUtils.isEmpty(messageList.get(position).lxdh)) {

                    } else {
                        dialogShows(messageList.get(position).lxdh);
                    }

                }
            }

        }
        return rootView
    }

    internal inner class CheckHisAdapter(layoutResId: Int, data: List<GhModle>?) :
        BaseQuickAdapter<GhModle, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: GhModle) {
            helper.setText(R.id.name, item?.hzxm ?: "")
                .setText(R.id.time, item?.sfrq ?: "")
                .setText(R.id.phone, item?.lxdh ?: "")
                .addOnClickListener(R.id.call)

        }
    }
    fun getList() {
        IpUrl.getInstance()!!.suifangList(
            ConstantViewMolde.getToken(), true, ConstantViewMolde.GetUser()?.user?.id
                ?: ""
        )
            .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<HoleResponse<ArrayList<GhModle>>> {
                override fun onError(e: Throwable?) {
                    visitsmart?.finishRefresh()
                }

                override fun onNext(t: HoleResponse<ArrayList<GhModle>>?) {
                    if (t?.code == 0) {
                        if (t?.data != null) {
                            messageList.addAll(t?.data!!)
                            adapter?.notifyDataSetChanged()

                        }
                    }
                }

                override fun onCompleted() {
                    visitsmart?.finishRefresh()
                }

            })
    }

    fun dialogShows(phon: String) {
        var dialog: RxDialogSureCancel = RxDialogSureCancel(activity)
        dialog.setTitle("提示")
        dialog.setContent("是否拨打$phon?")
        dialog.contentView.gravity = Gravity.LEFT
        dialog.contentView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.contentView.setTextColor(Color.parseColor("#050505"))
        dialog.sureView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0f)
        dialog.cancelView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0f)
        dialog.contentView.gravity = Gravity.CENTER
        dialog.sureView.setTextColor(Color.parseColor("#376FE9"))
        dialog.cancelView.setTextColor(Color.parseColor("#376FE9"))
        dialog.titleView.setTextColor(Color.parseColor("#050505"))
        dialog.titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.setSure("否")
        dialog.setCancel("是")
        dialog.show()
        dialog.setSureListener {
            dialog.dismiss()
        }
        dialog.setCancelListener {
            CallPhone(phon)
            dialog.dismiss()
        }
    }

    fun CallPhone(s: String) {
        val intent = Intent() // 意图对象：动作 + 数据
        intent.action = Intent.ACTION_CALL // 设置动作
        val data = Uri.parse("tel:$s") // 设置数据
        intent.data = data
        startActivity(intent) // 激活Activity组件
    }

    fun getAsk(id: String) {
        var rxdia: RxDialog = RxDialog(activity)
        rxdia.setContentView(R.layout.dialog_loading)
        rxdia.show()
        IpUrl.getInstance()!!.Twask(ConstantViewMolde.getToken(), id)
            .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<HoleResponse<ClientModle>> {
                override fun onError(e: Throwable?) {
                    rxdia?.dismiss()
                }

                override fun onNext(t: HoleResponse<ClientModle>?) {
                    if (t?.code == 0) {
                        rxdia?.dismiss()
                        getList();
                        CallBackUtils.LoginSuccess(1)
                    }
                }

                override fun onCompleted() {
                    rxdia?.dismiss()
                }
            })
    }

    override fun onVisible() {
        super.onVisible()
        messageList?.clear()
        visitsmart?.autoRefresh()
    }

}