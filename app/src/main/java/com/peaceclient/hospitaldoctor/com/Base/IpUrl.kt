package com.peaceclient.hospitaldoctor.com.Base


import com.peaceclient.hospitaldoctor.com.InterFace.RetrofitUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Base
 * @class describe
 * @anthor admin
 * @time 2021/7/12 15:00
 * @change
 * @chang time
 * @class describe retrofit基础请求框架
 */

public class IpUrl {
    private var retrofitService: RetrofitUrl? = null
    private val BaseUrl = "http://api.app.hplyy.com/"
    val TestUrls = "http://192.168.1.69:9200/"

    var Urlhead = "http://www.j1dc.com/sss-web"
    var newlogin = "/user/userLogin"
    var picture = "/download?filePath="

    companion object ins {
        var retrofitService: RetrofitUrl? = null
              //  private val BaseString = "http://api.app.hplyy.com/"
      val BaseStrings = "http://192.168.1.31:9200/"
    private val BaseString = "https://api.app.hplyy.com/"
    val TestsUrl = "https://v29047i063.oicp.vip/"
    var wxString: String = "https://api.weixin.qq.com/sns/oauth2/"
    fun getBase(): String {
            return IpUrl.BaseStrings

    }

    fun getInstance(): RetrofitUrl? {
        if (retrofitService == null) {
            synchronized(RetrofitUrl::class.java) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val client = OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .addInterceptor(interceptor)
                        .retryOnConnectionFailure(true)
                        .build()
                val retrofit = Retrofit.Builder() //设置数据解析器
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .client(client) //设置网络请求的Url地址
                        .baseUrl(getBase())
                        .build() // 创建网络请求接口的实例
                retrofitService = retrofit.create(RetrofitUrl::class.java!!)
            }
        }
        return retrofitService
    }

    fun getInstanceWx(): RetrofitUrl? {
        if (retrofitService == null) {
            synchronized(RetrofitUrl::class.java) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val client = OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .addInterceptor(interceptor)
                        .retryOnConnectionFailure(true)
                        .build()
                val retrofit = Retrofit.Builder() //设置数据解析器
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .client(client) //设置网络请求的Url地址
                        .baseUrl(wxString)
                        .build() // 创建网络请求接口的实例
                retrofitService = retrofit.create(RetrofitUrl::class.java!!)
            }
        }
        return retrofitService
    }
}
}