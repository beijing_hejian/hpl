package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.Gravity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Spinner
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.GhModle
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.RecipeModlex
import com.scwang.smartrefresh.layout.listener.OnRefreshListener
import com.vondear.rxtools.view.RxToast
import com.vondear.rxtools.view.dialog.RxDialog
import kotlinx.android.synthetic.main.medicinehis_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor222
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/10/13 17:31
 * @change
 * @chang time
 * @class describe 远程接诊列表
 */



class RemoteHisAct : HoleBaseActivity() {
    private var messageList: ArrayList<GhModle> = arrayListOf()
    private var kong: View? = null
    private var page = 1
    var name: String = ""
    var week: String = "1"
    var isFirst: Boolean = false
    var JlAdapter: ArrayAdapter<String>? = null
    private var adapter: PeopleAdapter? = null
    var riqiList = arrayListOf<RecipeModlex.Others>()
    var riqiNameList = arrayListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.remotehis_act)
        search_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (TextUtils.isEmpty(s)) {
                    name = ""
                    messageList?.clear()

                    if (isFirst) {
                        prosmarts.autoRefresh()
                    }
                } else {
                    messageList?.clear()
                    name = s.toString()
                    if (isFirst) {
                        prosmarts.autoRefresh()
                    }
                    //messageList?.clear()
                    // GetCheckHis(prosmarts)
                }
            }
        })

        initJlSpinner(spinner)
        getRiqi()
        arrow_back.setOnClickListener {
            finish()
        }

        spinner.prompt = "请选择时间"
        var spinnerAdapter: ArrayAdapter<CharSequence> = ArrayAdapter.createFromResource(this@RemoteHisAct, com.peaceclient.hospitaldoctor.com.R.array.week, R.layout.simple_spinner_item)
        spinnerAdapter.setDropDownViewResource(R.layout.drop_item_layout)
        spinner.adapter = spinnerAdapter
        spinnerAdapter.notifyDataSetChanged()
        var selec: MyItemSelect = MyItemSelect()
        spinner.onItemSelectedListener = selec

        //将spinnertext添加到OnTouchListener对内容选项触屏事件处理

        kong = View.inflate(Myapplication.mcontext, R.layout.empty_view, null)
        val manager = LinearLayoutManager(Myapplication.mcontext, LinearLayoutManager.VERTICAL, false)
        medic_recycle.setLayoutManager(manager)
        adapter = PeopleAdapter(R.layout.medicinez_item, messageList)
        adapter?.bindToRecyclerView(medic_recycle)
        adapter?.setEmptyView(kong)
        adapter?.isUseEmpty(true)
        medic_recycle.setAdapter(adapter)
        adapter?.notifyDataSetChanged()

        prosmarts.setOnRefreshListener(OnRefreshListener {
            messageList?.clear()
            getRemoHis()
        })
        adapter?.setOnItemChildClickListener { dapter, view, position ->
            when (view) {
                adapter?.getViewByPosition(medic_recycle, position, R.id.transport) -> {
                    var inte: Intent = Intent(this@RemoteHisAct, AddSuiActivity::class.java)
                    inte.putExtra("modle", messageList.get(position))
                    startActivity(inte)
                }
                adapter?.getViewByPosition(medic_recycle, position, R.id.transports) -> {
                    var intent: Intent = Intent(this@RemoteHisAct, DiagonsisDetialAct::class.java)
                    intent.putExtra("appointid", "")
                    intent.putExtra("id", messageList.get(position).billid)
                    startActivity(intent)
                }
            }

        }
    }

    private fun initJlSpinner(view: Spinner) {
        view!!.prompt = "请选择日期"
        JlAdapter = ArrayAdapter(this@RemoteHisAct, R.layout.spinner_select, riqiNameList)
        JlAdapter!!.setDropDownViewResource(R.layout.item_drop_layout)
        //view.dropDownHorizontalOffset = resources.getDimensionPixelOffset(R.dimen.dp_44)
        view.dropDownVerticalOffset = resources.getDimensionPixelOffset(R.dimen.dp_30)
        var parm: LinearLayout.LayoutParams = la.layoutParams as LinearLayout.LayoutParams
        view!!.dropDownHorizontalOffset = ((parm.width - spinner.layoutParams.width) / 2).toInt()
        spinner.gravity = Gravity.CENTER
        view!!.adapter = JlAdapter
        spinner.setSelection(0)
        JlAdapter!!.notifyDataSetChanged()
        var selecS: MyItemSelect = MyItemSelect()
        view?.onItemSelectedListener = selecS
    }

    internal inner class MyItemSelect : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            if(riqiList.size>0){
               // name = riqiList.get(position).name
                week = riqiList.get(position).num
                spinner.setSelection(position)
            }
            if (isFirst) {
                prosmarts.autoRefresh()
            }
        }

    }

    inner class PeopleAdapter(layoutResId: Int, data: ArrayList<GhModle>?) : com.chad.library.adapter.base.BaseQuickAdapter<GhModle, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: GhModle) {
            helper.setText(R.id.name, item.uname)
                    .setText(R.id.time, item.createtime)
            helper.addOnClickListener(R.id.transport)
                    .addOnClickListener(R.id.transports)
        }
    }

    override fun onResume() {
        super.onResume()
        messageList?.clear()
        prosmarts?.autoRefresh()
    }

    /**
     * 远程接诊历史列表
     */
    fun getRemoHis() {
        isFirst = true
        IpUrl.getInstance()!!.RemoHis(ConstantViewMolde?.getToken(), name, week)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<GhModle>>> {
                    override fun onError(e: Throwable?) {
                        prosmarts?.finishRefresh()
                    }
                    override fun onNext(t: HoleResponse<ArrayList<GhModle>>?) {
                        if (t != null) {
                            messageList?.addAll(t?.data!!)
                            adapter?.notifyDataSetChanged()
                        }
                    }
                    override fun onCompleted() {
                        prosmarts?.finishRefresh()
                    }
                })
    }

    /***
     *  获取日期
     *  一周内
     *  半年内
     *  等数据
     */
    fun getRiqi() {
        var rxdia: RxDialog = RxDialog(this@RemoteHisAct)
        rxdia.setContentView(R.layout.dialog_loading)
        rxdia.show()
        IpUrl.getInstance()!!.riqi(ConstantViewMolde.getToken())
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<RecipeModlex.Others>>> {
                    override fun onError(e: Throwable?) {
                        rxdia?.dismiss()
                    }

                    override fun onNext(t: HoleResponse<ArrayList<RecipeModlex.Others>>?) {
                        if (t?.code == 0) {
                            rxdia?.dismiss()
                            riqiList.addAll(t?.data!!)
//                            yypclist.addAll(t?.data!!)
                            for (o in 0..riqiList.size - 1) {
                                riqiNameList.add(riqiList.get(o).name)
                                JlAdapter!!.notifyDataSetChanged()
                            }
                        } else {
                            RxToast.normal(t?.msg ?: "")
                        }
                    }

                    override fun onCompleted() {
                        rxdia?.dismiss()
                    }
                })
    }
}