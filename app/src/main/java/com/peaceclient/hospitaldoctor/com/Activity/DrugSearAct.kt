package com.peaceclient.hospitaldoctor.com.Activity

import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.widget.RelativeLayout
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.android.CaptureActivity
import com.peaceclient.hospitaldoctor.com.common.Constant
import com.peaceclient.hospitaldoctor.com.modle.JzCardModle
import kotlinx.android.synthetic.main.drugsear_act.*

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor222
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/10/12 20:00
 * @change
 * @chang time
 * @class describe 药品盘点页面（废弃）
 */

class DrugSearAct : HoleBaseActivity() {
    var adapter: PeopleAdapter? = null
    var list: ArrayList<JzCardModle> = arrayListOf()
    var currentId: String = "0"
    var currLayout: RelativeLayout? = null
    var kong:View? =null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.drugsear_act)
        kong = View.inflate(Myapplication.mcontext, R.layout.empty_view, null)
        var manager: LinearLayoutManager = LinearLayoutManager(this)
        recycle.layoutManager = manager
        adapter = PeopleAdapter(R.layout.drug_search_item, list)
        adapter?.bindToRecyclerView(recycle)
        adapter?.setEmptyView(kong)
        recycle.adapter = adapter
        smart.autoRefresh()
        smart.setOnRefreshListener {
            list.clear()
            initDate()
        }
        scan.setOnClickListener {
            var intent: Intent = Intent(this@DrugSearAct, CaptureActivity::class.java)
            startActivityForResult(intent, 1000)
        }
        radio.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.first -> {
                    currentId = "0"
                    smart.autoRefresh()
                }
                R.id.secend -> {
                    currentId = "1"

                    smart.autoRefresh()
                }
                R.id.third -> {
                    currentId = "2"

                    smart.autoRefresh()
                }
            }
            adapter?.notifyDataSetChanged()
        }
        search_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (!TextUtils.isEmpty(s)){
                    smart.autoRefresh()
                }else{
                    smart.autoRefresh()
                }

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

    }

    private fun initDate() {
        for (z in 0..1) {
            var jzCardModle: JzCardModle = JzCardModle()
            jzCardModle.fullName = z.toString() + "..."
            list.add(jzCardModle)
        }
        adapter?.notifyDataSetChanged()
        smart.finishRefresh()
    }


    inner class PeopleAdapter(layoutResId: Int, data: ArrayList<JzCardModle>?) : com.chad.library.adapter.base.BaseQuickAdapter<JzCardModle, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: JzCardModle) {
            var firstLayout = helper.getView<RelativeLayout>(R.id.first_layout);
            var secendLayout = helper.getView<RelativeLayout>(R.id.secend_layout);
            var thirdLayout = helper.getView<RelativeLayout>(R.id.third_layout);
            when (currentId) {
                "0" -> {
                    currLayout = firstLayout
                }
                "1" -> {
                    currLayout = secendLayout
                }
                "2" -> {
                    currLayout = thirdLayout
                }
            }
            firstLayout.visibility = View.GONE
            secendLayout.visibility = View.GONE
            thirdLayout.visibility = View.GONE
            currLayout?.visibility = View.VISIBLE
           // helper.getView<TextView>(R.id.name).setText(item?.fullName ?: "")
//            helper.addOnClickListener(R.id.cancle)
//                    .addOnClickListener(R.id.tijiao)

        }

        override fun getItemCount(): Int {
            return 1
        }

    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1000) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                var resu: String = data?.getStringExtra(Constant.CODED_CONTENT)!!
                search_text.setText(resu ?: "")
            }
        }
    }

}