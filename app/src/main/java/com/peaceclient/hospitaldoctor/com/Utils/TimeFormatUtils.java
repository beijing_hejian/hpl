/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @packname com.homeclient.com.Utils
 * @filename TimeFormatUtils
 * @date on 2018/8/13 13:41
 *****/
public class TimeFormatUtils {

      //毫秒转换未时间
      public static String ms2Date(long _ms) {
            Date date = new Date(_ms);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return format.format(date);
      }
      //毫秒转换未时间
      public static String ms2daojishi(long _ms) {
            Date date = new Date(_ms);
            SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
            return format.format(date);
      }
      public static String ms2DIAN(long _ms) {
            Date date = new Date(_ms);
            SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd");
            return format.format(date);
      }

      public static String ms2DateOnlyDay(long _ms) {
            Date date = new Date(_ms);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            return format.format(date);
      }
      public static String ms2DateYear(long _ms) {
            Date date = new Date(_ms);
            SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd");
            return format.format(date);
      }

      public static String datetime(long ms, long _ms) {
            long time = (ms - _ms) / 1000;
            if (time <= 60) {
                  return "刚刚";
            }
            if (time > 60 && time <= 3600) {
                  return time / 60 + "分钟前";
            }
            if (time > 3600 && time <= 3600 * 24) {
                  return time / 3600 + "小时前";
            }
            if (time > 3600 * 24 && time < 3600 * 24 * 2) {
                  return "昨天";
            }
            if (time > 3600 * 24 * 2) {
                  return TimeFormatUtils.ms2DIAN(_ms);
            }

            return time + "";
      }

      public static long formatestring(String s) throws ParseException {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = formatter.parse(s);
            return date.getTime();
      }
      public static long formatestringval(String s,String val) throws ParseException {
            SimpleDateFormat formatter = new SimpleDateFormat(val);
            Date date = formatter.parse(s);
            return date.getTime();
      }
      public static String ms2nian(long _ms) {
            Date date = new Date(_ms);
            SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日-HH:mm");
            return format.format(date);
      }


}