package com.peaceclient.hospitaldoctor.com.Activity

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import com.chad.library.adapter.base.BaseViewHolder
import com.jakewharton.rxbinding4.view.clicks
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.RecipeModlex
import com.vondear.rxtools.view.dialog.RxDialog
import kotlinx.android.synthetic.main.diag_shen_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor222
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/11/6 16:29
 * @change
 * @chang time
 * @class describe 处方审核页面
 */

class DiagonsisShenAct : HoleBaseActivity() {
    private var list = arrayListOf<String>()
    var id: String? = null
    var shenStatus: String = "1"
    private var medicineList = arrayListOf<RecipeModlex.BillDetail>()
    private var adapter: PeopleAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.diag_shen_act)
        var manager: LinearLayoutManager = LinearLayoutManager(this)
        id = intent.getStringExtra("id")
        recycle.layoutManager = manager
        adapter = PeopleAdapter(R.layout.diagshen_item, medicineList)
        recycle.adapter = adapter
        adapter?.notifyDataSetChanged()
        getRecipList()
        arrow_back.setOnClickListener {
            finish()
        }
        content.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
            override fun afterTextChanged(s: Editable?) {
                zishu.setText(((s?.length.toString() ?: 0).toString()) + "/30")

            }
        })

        tongguo_layout.setOnClickListener {
            noguo.isChecked = false
            tongguo.isChecked = true
            shenStatus = "1"
        }
        noguo_layout.setOnClickListener {
            tongguo.isChecked = false
            noguo.isChecked = true
            shenStatus = "2"
        }
        save.clicks().throttleFirst(2000, TimeUnit.MICROSECONDS).subscribe {
            shenhe();
        }
    }

    inner class PeopleAdapter(layoutResId: Int, data: ArrayList<RecipeModlex.BillDetail>?) : com.chad.library.adapter.base.BaseQuickAdapter<RecipeModlex.BillDetail, BaseViewHolder>(layoutResId, data) {

        override fun convert(helper: BaseViewHolder, item: RecipeModlex.BillDetail) {
//            var yao :String = (item.ypmc?:"")+(item.ypgg?:""+ ":" ) + (item.kfsm?:"")
//            helper.setText(R.id.text, yao)

            helper.setText(R.id.yptext, item.name)
                    .setText(R.id.ggtext, item.spec)
                    .setText(R.id.dwtext, item.unit)
                    .setText(R.id.yytext, item.freqname)
                    .setText(R.id.sltext, item.unitcount)
                    .setText(R.id.yftext,item.gytjname)
                    .setText(R.id.yltext,item.percount)

        }
    }

    /***
     * 获取处方详情请求
     */
    fun getRecipList() {
        var rx: RxDialog = RxDialog(this@DiagonsisShenAct)
        rx.setContentView(R.layout.dialog_loading)
        rx.show()
        IpUrl.getInstance()!!.RecipDetial(ConstantViewMolde.getToken(), id!!)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<RecipeModlex>> {
                    override fun onError(e: Throwable?) {
                        rx.dismiss()
                    }

                    override fun onNext(t: HoleResponse<RecipeModlex>?) {
                        if (t?.code == 0) {
                            medicineList.addAll(t?.data?.bills?.billDetails!!)
                            println(medicineList.toString() + "medicine")
                            adapter?.notifyDataSetChanged()
                            //result.setText(t?.data?.zdjg)
                            time.setText(t?.data?.bills?.billtime)
                            if (t.data!!.diags == null) {
                                zdType.setText("")
                                result.setText("")
                            } else {
                                when (t.data!!.diags?.prdiagtype) {
//                           1、西医诊断；2、中医病症；3、中医证候
                                    "1" -> {
                                        zdType.setText("西医诊断")
                                    }
                                    "2" -> {
                                        zdType.setText("中医诊断")
                                    }

                                    "" -> {
                                        zdType.setText("")
                                    }
                                }
                                var str: String = t.data!!.diags!!.name ?: ""
                                if (t.data?.others == null || t.data?.others?.size == 0){

                                }else{

                                    for (i in 0..t?.data?.others?.size!!-1){
                                       str + "、" + t?.data?.others!![i].name;
                                        str = str + "、" + t?.data?.others!![i].name;
                                    }
                                }
                                result.setText(str)

                            }
                        }
                    }

                    override fun onCompleted() {
                        rx.dismiss()
                    }
                })
    }

    /***
     * 审核提交接口请求
     */
    fun shenhe() {
        var rx: RxDialog = RxDialog(this@DiagonsisShenAct)
        rx.setContentView(R.layout.dialog_loading)
        rx.show()
        IpUrl.getInstance()!!.Shenhe(ConstantViewMolde.getToken(), id!!, shenStatus, content.text.toString())
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<Any>> {
                    override fun onError(e: Throwable?) {
                        rx.dismiss()
                    }

                    override fun onNext(t: HoleResponse<Any>?) {
                        if (t?.code == 0) {
                            finish()
                        }
                    }

                    override fun onCompleted() {
                        rx.dismiss()
                    }
                })
    }
}