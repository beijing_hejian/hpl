package com.peaceclient.hospitaldoctor.com.View;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.RadioButton;

import com.peaceclient.hospitaldoctor.com.R;


/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name project
 * @class name：com.homeclientz.com.View
 * @class describe
 * @anthor admin
 * @time 2019/10/16 19:12
 * @change
 * @chang time
 * @class describe
 */


@SuppressLint("AppCompatCustomView")
public class MyRadioButton extends RadioButton {
      private Drawable drawable;
      public MyRadioButton(Context context) {
            super(context);
      }
      public MyRadioButton(Context context, AttributeSet attrs) {
            super(context, attrs);
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MyRadioButton);//获取我们定义的属性
            drawable = typedArray.getDrawable(R.styleable.MyRadioButton_drawableTop);
            int h = drawable.getIntrinsicHeight();
            int w = drawable.getIntrinsicWidth();
            drawable.setBounds( 0, 0, (int) (w/0.7), (int) (h/0.7));
           // drawable.setBounds(-4, -4, 60, 60);
            setCompoundDrawables(null,drawable, null, null);

      }
}
