/**
 * Copyright (C) 2016 Hyphenate Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.peaceclient.hospitaldoctor.com.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.peaceclient.hospitaldoctor.com.R;
import com.peaceclient.hospitaldoctor.com.adapter.NewFriendsMsgAdapter;
import com.peaceclient.hospitaldoctor.com.db.InviteMessgeDao;
import com.peaceclient.hospitaldoctor.com.domain.InviteMessage;

import java.util.Collections;
import java.util.List;

/**
 * Application and notification
 *
 */
public class NewFriendsMsgActivity extends BaseActivity {

	private NewFriendsMsgAdapter adapter;
	private List<InviteMessage> msgs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.em_activity_new_friends_msg);
		ListView listView = (ListView) findViewById(R.id.list);
		ImageView delete = (ImageView) findViewById(R.id.delete);
		final InviteMessgeDao dao = new InviteMessgeDao(this);
		msgs = dao.getMessagesList();
		Collections.reverse(msgs);
		adapter = new NewFriendsMsgAdapter(this, 1, msgs);
		listView.setAdapter(adapter);
		dao.saveUnreadMessageCount(0);
		delete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				msgs.clear();
				adapter.notifyDataSetChanged();
				//DemoDBManager ds = DemoDBManager.getInstance();
			}
		});
		
	}

	public void back(View view) {
		finish();
	}
}
