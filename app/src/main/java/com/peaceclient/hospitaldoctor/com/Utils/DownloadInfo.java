package com.peaceclient.hospitaldoctor.com.Utils;

/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Utils
 * @class describe
 * @anthor admin
 * @time 2021/7/8 16:28
 * @change
 * @chang time
 * @class describe
 */

class DownloadInfo {

      /* 存储位置 */
      private String savePath;
      /* 文件总长度 */
      private long contentLength;
      /* 下载长度 */
      private long readLength;
      /* 下载该文件的url */
      private String url;
//      private RetrofitService service;
//
//
//      public RetrofitService getService() {
//            return service;
//      }
//
//      public void setService(RetrofitService service) {
//            this.service = service;
//      }

      public String getUrl() {
            return url;
      }

      public void setUrl(String url) {
            this.url = url;
      }

      public String getSavePath() {
            return savePath;
      }

      public void setSavePath(String savePath) {
            this.savePath = savePath;
      }

      public long getContentLength() {
            return contentLength;
      }

      public void setContentLength(long contentLength) {
            this.contentLength = contentLength;
      }

      public long getReadLength() {
            return readLength;
      }

      public void setReadLength(long readLength) {
            this.readLength = readLength;
      }

      @Override
      public String toString() {
            return "DownloadInfo{" +
                      "savePath='" + savePath + '\'' +
                      ", contentLength=" + contentLength +
                      ", readLength=" + readLength +
                      ", url='" + url + '\'' +
                      '}';
      }
}
