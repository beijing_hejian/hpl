/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.TypedValue
import android.view.Gravity
import android.widget.CheckBox
import android.widget.TextView
import com.chad.library.adapter.base.BaseViewHolder
import com.jakewharton.rxbinding4.view.clicks

import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.Utils
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.JzCardModle
import com.vondear.rxtools.view.RxToast
import com.vondear.rxtools.view.dialog.RxDialog
import com.vondear.rxtools.view.dialog.RxDialogSureCancel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers.mainThread
import kotlinx.android.synthetic.main.elecard_act.*
import kotlinx.android.synthetic.main.news_detail_act.arrow_back
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/8/2 10:29
 * @change
 * @chang time
 * @class describe 首页电子卡就诊页面
 */

class PeopleCardAct : HoleBaseActivity() {
    private var list: ArrayList<JzCardModle> = arrayListOf()
    private var adapter: PeopleAdapter? = null
    private var  dialog:RxDialog? = null
    private var con: String = "1、保证患者就医安全，按国家网络安全要求，所有患者必须进行实名认证。\n" +
            "2、未实名认证的患者不能使用本院线上医疗服务。\n" +
            "3、实名认证: 身份证建卡绑卡:上传身份证照片，实名认证通过后，可在线建卡或绑定已有就诊卡。\n" +
            "4、限于北京市医保政策要求，暂不支持在线医保身份认证。\n" +
            "5、为保护患者权益，严禁未经允许绑定他人就诊卡，否则医院有权禁用您的账户，产生的一切不良后果由用户本人承担。"
    var TempModle: JzCardModle = JzCardModle()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.peoplecard_act)
        arrow_back.setOnClickListener {
            finish()
        }
        val managers = LinearLayoutManager(this)
        elec_recycle.setLayoutManager(managers)
        adapter = PeopleAdapter(R.layout.elecard_item, list)
        elec_recycle.adapter = adapter
        adapter?.notifyDataSetChanged()
        add_people.clicks().throttleFirst(1500, TimeUnit.MICROSECONDS).subscribeOn(mainThread())
                .subscribe {
                    dialogShow();
                }
         dialog = RxDialog(this@PeopleCardAct, R.layout.dialog_loading)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setCanceledOnTouchOutside(false)
        adapter?.setOnItemClickListener { adapter, view, position ->
            dialog?.show()
            IpUrl.getInstance()!!.SetDefultJz(ConstantViewMolde?.getToken(),list.get(position).id.toString())
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<String>> {
                        override fun onError(e: Throwable?) {
                            dialog?.dismiss()
                        }
                        override fun onNext(t: HoleResponse<String>?) {
                            dialog?.dismiss()
                            if (t?.code == 0){
                                TempModle.defaultCard =false
                                list.get(position).defaultCard = true
                                TempModle = list.get(position)
                                adapter.notifyDataSetChanged()
                            }
                        }
                        override fun onCompleted() {
                        }
                    })



        }
        adapter?.setOnItemChildClickListener { adapter, view, position ->
            when (view) {
                adapter.getViewByPosition(elec_recycle, position, R.id.cancle) -> {
                    //解绑
                    dialog?.show()
                    Delete(list.get(position).id ?: "")

                }
                adapter.getViewByPosition(elec_recycle, position, R.id.sure) -> {
                    // 激活
                }
            }
        }
        getZIXUN()
        // initDate()
    }

    private fun dialogShow() {
        var dialog: RxDialogSureCancel = RxDialogSureCancel(this@PeopleCardAct)
        dialog.setTitle("绑卡须知")
        dialog.setContent(con)
        dialog.contentView.gravity = Gravity.LEFT
        dialog.contentView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12.0f)
        dialog.contentView.setTextColor(Color.parseColor("#050505"))
        dialog.sureView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.cancelView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.sureView.setTextColor(Color.parseColor("#376FE9"))
        dialog.cancelView.setTextColor(Color.parseColor("#376FE9"))
        dialog.titleView.setTextColor(Color.parseColor("#050505"))
        dialog.titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.setCancel("同意")
        dialog.setSure("取消")
        dialog.show()
        dialog.setCancelListener({
            dialog.dismiss()
            //startActivity(Intent(this@PeopleCardAct, CreatCardAct::class.java))
        })
        dialog.setSureListener({
            dialog.dismiss()

        })
    }
//
//    private fun initDate() {
//        for (z in 0..7) {
//            var peopleModle: PeopleModle = PeopleModle()
//            peopleModle.id = z
//            peopleModle.idcard = "130431199110100010"
//            peopleModle.isSelect = false
//            peopleModle.name = "z" + z
//            peopleModle.phone = "17611481698"
//            list.add(peopleModle)
//            adapter?.notifyDataSetChanged()
//        }
//    }

    inner class PeopleAdapter(layoutResId: Int, data: ArrayList<JzCardModle>?) : com.chad.library.adapter.base.BaseQuickAdapter<JzCardModle, BaseViewHolder>(layoutResId, data) {

        override fun convert(helper: BaseViewHolder, item: JzCardModle) {
            var checkBox: CheckBox = helper.getView<CheckBox>(R.id.icon)
            helper.addOnClickListener(R.id.cancle).addOnClickListener(R.id.sure)
            helper.getView<TextView>(R.id.idcard).setText(Utils.MakeCardID(item?.idCard ?: ""))
            helper.getView<TextView>(R.id.phone).setText(item?.phone ?: "")
            helper.getView<TextView>(R.id.name).setText(item?.fullName ?: "")

            if (item?.defaultCard!!) {
                checkBox.isChecked = true
            } else {
                checkBox.isChecked = false
            }

        }
    }

    fun getZIXUN() {
        dialog?.show()
        IpUrl.getInstance()!!.SearchJZ(ConstantViewMolde?.getToken())
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<JzCardModle>>> {
                    override fun onError(e: Throwable?) {
                        dialog?.dismiss()
                    }

                    override fun onNext(t: HoleResponse<ArrayList<JzCardModle>>?) {
                        dialog?.dismiss()
                        if (t != null) {
                            list?.addAll(t?.data!!)
                            for (i in 0 until list?.size - 1) {
                                if (list.get(i).defaultCard == true) {
                                    TempModle = list.get(i)
                                }
                            }
                            adapter?.notifyDataSetChanged()
                        }
                    }

                    override fun onCompleted() {
                    }
                })
    }



    fun Delete(string: String) {
        IpUrl.getInstance()!!.DeleteJZ(ConstantViewMolde?.getToken(), string)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<String>> {
                    override fun onError(e: Throwable?) {
                        dialog?.dismiss()
                    }

                    override fun onNext(t: HoleResponse<String>?) {
                        dialog?.dismiss()
                        if (t != null) {
                            if (t.code == 0) {
                                list.clear()
                                getZIXUN()
                            } else if (t.code == 1) {
                                RxToast.normal(t?.msg ?: "".toString())
                            }

                            // println(list?.size.toString()+"size")
                            //adapter?.notifyDataSetChanged()
                        }
                    }

                    override fun onCompleted() {
                    }
                })
    }
}