package com.peaceclient.hospitaldoctor.com.Activity

import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.InterFace.MyGridItemDecorations
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.View.TypeImageView
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.GhModle
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import kotlinx.android.synthetic.main.doc_onlin.arrow_back
import kotlinx.android.synthetic.main.evaluelist_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor222
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/10/13 14:58
 * @change
 * @chang time
 * @class describe 评价列表页面
 */

class EvalueListAct  :HoleBaseActivity(){
    private var adapter: DoctorAdapter? = null
    private var doctorList: ArrayList<GhModle>? = arrayListOf()
    private var heartList: ArrayList<Int>? = arrayListOf()
    private var id :String? = null
    var kong :View? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.evaluelist_act)
        arrow_back.setOnClickListener {
            finish()
        }
        id = intent.getStringExtra("type");
        when(id){
            "1"->{
               titles.setText("患者评价")
            }
            "2"->{
                titles.setText("上级评价")
            }

        }
        kong = View.inflate(this@EvalueListAct,R.layout.empty_view,null)
        var linerlay: LinearLayoutManager = LinearLayoutManager(this)
        elec_recycle.layoutManager = linerlay as RecyclerView.LayoutManager?
        adapter = DoctorAdapter(R.layout.doctordetail_item, doctorList)
        adapter?.setEmptyView(kong)
        adapter?.bindToRecyclerView(elec_recycle)
        elec_recycle.adapter = adapter
        adapter?.notifyDataSetChanged()
        for (i in 1..5) {
            heartList!!.add(i)
        }
        smart.autoRefresh()
        smart.setOnRefreshListener {
            doctorList?.clear()
            when(id){
                "1"->{
                    getvalue()
                }
                "2"->{
                    getvalues()
                }

            }

        }

    }

    internal inner class DoctorAdapter(layoutResId: Int, data: ArrayList<GhModle>?) : com.chad.library.adapter.base.BaseQuickAdapter<GhModle, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: GhModle) {

                  helper  .setText(R.id.doctor_introd, item?.evaluation ?: "")
                    if(id == "1"){
                        helper.setText(R.id.doctor_name, item?.hzxm ?: "")
                        helper .setText(R.id.time, item.createTime!!)
                    }else{
                        helper.setText(R.id.doctor_name, item?.docName ?: "")
                        helper .setText(R.id.time, (item?.nd ?: "")+"年度")
                    }
            Glide.with(this@EvalueListAct).load(item.headImgUrl?:"").placeholder(R.drawable.evalue_icon).error(R.drawable.evalue_icon).into(helper.getView<TypeImageView>(R.id.doctor_header))
            var linerlay: GridLayoutManager = GridLayoutManager(this@EvalueListAct, item?.level?:1, LinearLayoutManager.VERTICAL, true)
            var heart: RecyclerView = helper.getView(R.id.heart) as RecyclerView
            var itemdeco: MyGridItemDecorations = MyGridItemDecorations()
            heart.layoutManager = linerlay
            var doctoradapters: DoctorAdapters = DoctorAdapters(R.layout.image_item, heartList, item?.level)
            heart.addItemDecoration(itemdeco)
            heart.adapter = doctoradapters
            doctoradapters.notifyDataSetChanged()

        }
    }

    internal inner class DoctorAdapters(layoutResId: Int, data: ArrayList<Int>?, var count: Int? = null) : com.chad.library.adapter.base.BaseQuickAdapter<Int, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: Int) {
        }
        override fun getItemCount(): Int {
            return count!!
        }
    }

    /***
     * 患者评价列表请求
     */
    fun getvalue(){
                IpUrl.getInstance()!!.getHzValue(ConstantViewMolde.getToken())
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<GhModle>>> {
                    override fun onError(e: Throwable?) {
                        // rxdia?.dismiss()
                        smart.finishRefresh()
                    }

                    override fun onNext(t: HoleResponse<ArrayList<GhModle>>) {
                        doctorList?.addAll(t?.data!!)
                       // doctorList?.addAll(t?.data!!)
                        adapter?.notifyDataSetChanged()

                    }

                    override fun onCompleted() {
                        //  rxdia?.dismiss()
                        smart.finishRefresh()
                    }
                })
    }

    /****
     *  获取上级评价列表请求
     */
    fun getvalues(){
        IpUrl.getInstance()!!.getSJValue(ConstantViewMolde.getToken(), ConstantViewMolde.GetUser()?.user?.id + "")
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<GhModle>>> {
                    override fun onError(e: Throwable?) {
                        // rxdia?.dismiss()
                        smart.finishRefresh()
                    }

                    override fun onNext(t: HoleResponse<ArrayList<GhModle>>) {
                        doctorList?.addAll(t?.data!!)
                        adapter?.notifyDataSetChanged()

                    }

                    override fun onCompleted() {
                        //  rxdia?.dismiss()
                        smart.finishRefresh()
                    }
                })
    }

}