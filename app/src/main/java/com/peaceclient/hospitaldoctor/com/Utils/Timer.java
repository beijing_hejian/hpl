/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Utils;

import android.content.Context;
import android.os.CountDownTimer;
import android.widget.TextView;


public class Timer extends CountDownTimer {
    private TextView v;
    private Context context;

    public Timer(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
    }

    public Timer(long millisInFuture, long countDownInterval, TextView v, Context context) {
        super(millisInFuture, countDownInterval);
        this.v = v;
        this.context = context;
    }

    @Override
    public void onFinish() {//计时完毕时触发
        v.setText("发送验证码");
        v.setClickable(true);
    }

    @Override
    public void onTick(long millisUntilFinished) {//计时过程显示
        v.setClickable(false);
        v.setText(millisUntilFinished / 1000 + "s");
    }
}
