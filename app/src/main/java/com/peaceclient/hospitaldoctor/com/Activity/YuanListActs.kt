/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.TextUtils
import android.widget.TextView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseViewHolder
import com.jakewharton.rxbinding4.view.clicks

import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.*
import kotlinx.android.synthetic.main.doctorlists_activity.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/7/15 18:02
 * @change
 * @chang time
 * @class describe
 * 图文问诊医生列表
 */

class YuanListActs : HoleBaseActivity() {
    private var list: ArrayList<TimeBean>? = arrayListOf()
    private var lists: ArrayList<DepartModle>? = arrayListOf()
    private var DepartLList: ArrayList<DepartmentModle>? = arrayListOf()
    private var doctorList: ArrayList<GhModle>? = arrayListOf()
    private var doctoradapter: DoctorAdapter? = null
    private var departAdapter: DepartAdapter? = null
    private var departId: String = ""
    private  var nd :String = ""
    private var type: String = ""
    var DepartTemp: DepartModle = DepartModle();
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.doctorlists_activity)
        var departModle: DepartModle = DepartModle()
        nd =  intent.getStringExtra("s");
        departModle.isSelect = true
        departModle.depName = "全部科室"
        DepartTemp = departModle
        lists?.add(departModle)
        var linerma: LinearLayoutManager = LinearLayoutManager(this)
        depart_recycle.layoutManager = linerma
        departAdapter = DepartAdapter(R.layout.depart_item, lists)
        depart_recycle.adapter = departAdapter
        departAdapter?.notifyDataSetChanged()
        arrow_back.clicks().throttleFirst(2000, TimeUnit.MICROSECONDS).subscribeOn(io.reactivex.rxjava3.android.schedulers.AndroidSchedulers.mainThread())
                .subscribe {
                    finish()
                }
        var linerlay: LinearLayoutManager = LinearLayoutManager(this)
        doctor_recycle.layoutManager = linerlay
        doctoradapter = DoctorAdapter(R.layout.yuan_item, doctorList)
        doctor_recycle.adapter = doctoradapter
        doctoradapter?.notifyDataSetChanged()
        getZIXUN()
        departAdapter?.setOnItemClickListener { adapter, view, position ->
            DepartTemp?.isSelect = false
            lists?.get(position)?.isSelect = true
            DepartTemp = lists!!.get(position)
            departAdapter?.notifyDataSetChanged()
            doctorList?.clear()
            getDoct(lists?.get(position)?.id ?: "", nd)
            if (position == 0) {
                departId = ""
            } else {
                departId = lists?.get(position)?.id ?: ""
            }
        }
        departAdapter?.notifyDataSetChanged()
       doctoradapter?.setOnItemChildClickListener { adapter, view, position ->
           if (view == adapter.getViewByPosition(doctor_recycle,position,R.id.judge)){
               if (TextUtils.isEmpty(doctorList?.get(position)?.pingjia)){

                   var intent = Intent(this@YuanListActs,EvaluateAct::class.java)
                   intent.putExtra("modle",doctorList?.get(position))
                   intent.putExtra("nd",nd)
                   startActivity(intent)
                  // startActivity(Intent(this@YuanListActs,EvaluateAct::class.java))
               }else{
                   var intent = Intent(this@YuanListActs,EvaluateActs::class.java)
                   intent.putExtra("modle",doctorList?.get(position))
                   startActivity(intent)
                  // startActivity(Intent(this@YuanListActs,EvaluateActs::class.java))
               }
           }


       }
    }

    /**
     * 请求数据源，填充页面数据
     */
    internal inner class DepartAdapter(layoutResId: Int, data: ArrayList<DepartModle>?) : com.chad.library.adapter.base.BaseQuickAdapter<DepartModle, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: DepartModle) {
            helper.setText(R.id.depart_text, item?.depName ?: "")

            if (item?.isSelect!!) {
                helper.setBackgroundColor(R.id.depart_layout, resources.getColor(R.color.home_green))
                helper.setTextColor(R.id.depart_text, resources.getColor(R.color.white))
            } else {
                helper.setBackgroundColor(R.id.depart_layout, resources.getColor(R.color.hole_background_gree))
                helper.setTextColor(R.id.depart_text, resources.getColor(R.color.sss))

            }
        }
    }

    internal inner class DoctorAdapter(layoutResId: Int, data: ArrayList<GhModle>?) : com.chad.library.adapter.base.BaseQuickAdapter<GhModle, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: GhModle) {
            helper.setText(R.id.name, item?.docName ?: "")    .addOnClickListener(R.id.judge)
            Glide.with(this@YuanListActs).load(item.headImgUrl ) .placeholder(R.drawable.user_header).into(helper.getView(R.id.doctor_header))
            var jug  = helper.getView<TextView>(R.id.judge)
            if (TextUtils.isEmpty(item.pingjia)){
                jug.isSelected = true
                jug.setText("未评价")
            }else{
                jug.isSelected = false
                jug.setText("已评价")
            }


        }
    }

    fun getZIXUN() {

        IpUrl.getInstance()!!.getDepartList()
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<DepartModle>>> {
                    override fun onError(e: Throwable?) {}
                    override fun onNext(t: HoleResponse<ArrayList<DepartModle>>?) {
                        if (t != null) {
                            lists?.addAll(t?.data!!)
                            departAdapter?.notifyDataSetChanged()
                        }
                    }
                    override fun onCompleted() {
                    }
                })
    }

    fun getDoct(depId: String, nd: String) {
        doctorList?.clear()
        IpUrl.getInstance()!!.getYgValue(ConstantViewMolde.getToken(), depId, nd)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<GhModle>>> {
                    override fun onError(e: Throwable?) {}
                    override fun onNext(t: HoleResponse<ArrayList<GhModle>>?) {
                        if (t != null) {
                            doctorList?.addAll(t?.data!!)
                            doctoradapter?.notifyDataSetChanged()
                        }
                    }

                    override fun onCompleted() {
                    }
                })
    }

    override fun onResume() {
        super.onResume()

        getDoct(departId,nd)
    }
}