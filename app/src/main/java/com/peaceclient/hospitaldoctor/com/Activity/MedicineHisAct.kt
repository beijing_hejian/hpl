package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.Gravity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Spinner
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.PeisongBean
import com.peaceclient.hospitaldoctor.com.modle.RecipeModlex
import com.scwang.smartrefresh.layout.listener.OnRefreshListener
import com.vondear.rxtools.view.RxToast
import com.vondear.rxtools.view.dialog.RxDialog
import kotlinx.android.synthetic.main.medicinehis_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*


/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/9/16 9:45
 * @change
 * @chang time
 * @class describe 药品配送记录页面
 */

class MedicineHisAct : HoleBaseActivity() {
    private var messageList: ArrayList<PeisongBean> = arrayListOf()
    private var kong: View? = null
    private var page = 1
    var name: String = ""
    var time: String = "1"
    var JlAdapter :ArrayAdapter<String>? = null
    var riqiList = arrayListOf<RecipeModlex.Others>()
    var riqiNameList = arrayListOf<String>()
    var isFirst: Boolean = false
    private var adapter: PeopleAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.medicinehis_act)
        search_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (TextUtils.isEmpty(s)) {
                    name = ""
                    messageList?.clear()
                } else {
                    name = s.toString()
                    messageList?.clear()
                }
                prosmarts.autoRefresh()
            }
        })
        initJlSpinner(spinner)
        getRiqi()
        arrow_back.setOnClickListener {
            finish()
        }
        //将spinnertext添加到OnTouchListener对内容选项触屏事件处理
        kong = View.inflate(Myapplication.mcontext, R.layout.empty_view, null)
        val manager = LinearLayoutManager(Myapplication.mcontext, LinearLayoutManager.VERTICAL, false)
        medic_recycle.setLayoutManager(manager)
        adapter = PeopleAdapter(R.layout.medicines_item, messageList)
        adapter?.bindToRecyclerView(medic_recycle)
        adapter?.setEmptyView(kong)
        adapter?.isUseEmpty(true)
        medic_recycle.setAdapter(adapter)
        adapter?.notifyDataSetChanged()
        prosmarts.setOnRefreshListener(OnRefreshListener {
            messageList?.clear()
            getRecipList()
        })
        adapter?.setOnItemChildClickListener { adapter, view, position ->
            when (view) {
                adapter.getViewByPosition(medic_recycle, position, R.id.transport) -> {
                    var intent: Intent = Intent(this@MedicineHisAct, MedicManagerActs::class.java)
                    intent.putExtra("modle", messageList.get(position).id)
                    startActivity(intent)
                }
            }

        }
    }

    /***
     * 下拉框spinner 设置数据
     * @param view Spinner
     */
    private fun initJlSpinner(view: Spinner) {
        view!!.prompt = "请选择日期"
         JlAdapter = ArrayAdapter(this@MedicineHisAct, R.layout.spinner_select, riqiNameList)
        JlAdapter!!.setDropDownViewResource(R.layout.item_drop_layout)
        //view.dropDownHorizontalOffset = resources.getDimensionPixelOffset(R.dimen.dp_44)
        view.dropDownVerticalOffset = resources.getDimensionPixelOffset(R.dimen.dp_30)
        var parm: LinearLayout.LayoutParams = la.layoutParams as LinearLayout.LayoutParams
        view!!.dropDownHorizontalOffset = ((parm.width - spinner.layoutParams.width) / 2).toInt()
        spinner.gravity = Gravity.CENTER
        view!!.adapter = JlAdapter
        spinner.setSelection(0)
        JlAdapter!!.notifyDataSetChanged()
        var selecS: MyItemSelects = MyItemSelects()
        view?.onItemSelectedListener = selecS
    }


    internal inner class MyItemSelects() : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            // messageList.clear()
            name = riqiList.get(position).name
            time = riqiList.get(position).num
            spinner.setSelection(position)
            if (isFirst) {
                prosmarts.autoRefresh()
            }

        }
    }
    internal inner class MyItemSelect : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            time = "" + position + 1
            if (isFirst) {
                prosmarts.autoRefresh()
            }

        }

    }

    inner class PeopleAdapter(layoutResId: Int, data: ArrayList<PeisongBean>?) : com.chad.library.adapter.base.BaseQuickAdapter<PeisongBean, BaseViewHolder>(layoutResId, data) {

        override fun convert(helper: BaseViewHolder, item: PeisongBean) {

            helper.addOnClickListener(R.id.transport).setText(R.id.name, item.uname ?: "")
                    .setText(R.id.time, item.billtime ?: "")


        }
    }

    /***
     * 未配送药品记录
     */
    fun getRecipList() {
        isFirst = true
        IpUrl.getInstance()!!.weipeisongList(ConstantViewMolde.getToken(), "2", name, time)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<HoleResponse<ArrayList<PeisongBean>>> {
                    override fun onError(e: Throwable?) {
                        prosmarts.finishRefresh()
                    }

                    override fun onNext(t: HoleResponse<ArrayList<PeisongBean>>) {
                        if (t?.code == 0) {
                            messageList.addAll(t?.data!!)
                            adapter?.notifyDataSetChanged()
                        }
                    }

                    override fun onCompleted() {
                        prosmarts.finishRefresh()
                    }
                })
    }

    override fun onResume() {
        super.onResume()
        prosmarts.autoRefresh()
    }

    /***
     * 请求日期时间端
     */
    fun getRiqi() {
        var rxdia: RxDialog = RxDialog(this@MedicineHisAct)
        rxdia.setContentView(R.layout.dialog_loading)
        rxdia.show()
        IpUrl.getInstance()!!.riqi(ConstantViewMolde.getToken())
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<RecipeModlex.Others>>> {
                    override fun onError(e: Throwable?) {
                        rxdia?.dismiss()
                    }

                    override fun onNext(t: HoleResponse<ArrayList<RecipeModlex.Others>>?) {
                        if (t?.code == 0) {
                            rxdia?.dismiss()
                            riqiList.addAll(t?.data!!)
//                            yypclist.addAll(t?.data!!)
                            for (o in 0..riqiList.size-1) {
                                riqiNameList.add(riqiList.get(o).name)
                                JlAdapter!!.notifyDataSetChanged()
                            }
                        } else {
                            RxToast.normal(t?.msg ?: "")
                        }
                    }

                    override fun onCompleted() {
                        rxdia?.dismiss()
                    }
                })
    }
}