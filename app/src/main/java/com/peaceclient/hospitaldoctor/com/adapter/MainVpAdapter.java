package com.peaceclient.hospitaldoctor.com.adapter;



import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;


public class MainVpAdapter extends FragmentPagerAdapter {
      public ArrayList<Fragment> Fragments;

      public MainVpAdapter(FragmentManager fm, ArrayList<Fragment> fragments) {
            super(fm);
           Fragments = fragments;
      }
      public MainVpAdapter(FragmentManager fm) {
            super(fm);
      }

      @Override
      public Fragment getItem(int i) {
            return Fragments.get(i);
      }
//
//      @NonNull
//      @Override
//      public Object instantiateItem(@NonNull ViewGroup container, int position) {
//
//
//            return Fragments.get(position);
//      }

//      @Override
//      public Fragment getItem(int i) {
//            return   Fragments.get(i);
//      }
      @Override
      public int getCount() {
            return Fragments.size() > 0 ? Fragments.size() : 0;
      }

      public void Update() {
            notifyDataSetChanged();
      }
}