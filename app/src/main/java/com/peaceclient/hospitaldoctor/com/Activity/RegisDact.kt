package com.peaceclient.hospitaldoctor.com.Activity



import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Vibrator
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import android.view.animation.AnimationUtils
import com.peaceclient.hospitaldoctor.com.Base.BaseUrlUtil
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.Timer
import com.peaceclient.hospitaldoctor.com.Utils.Utils
import com.peaceclient.hospitaldoctor.com.View.CustomDialog
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.vondear.rxtools.view.RxToast
import com.vondear.rxtools.view.dialog.RxDialog
import kotlinx.android.synthetic.main.regis_acts.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/9/16 15:45
 * @change
 * @chang time
 * @class describe 医生端注册
 */

class RegisDact : HoleBaseActivity() {
    private var dialog: CustomDialog? = null
    var eyestatus: Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.regis_acts)
        val spannable = SpannableStringBuilder("同意和平里医院《用户协议》和《隐私政策》")
        //设置文字的前景色，2、4分别表示可以点击文字的起始和结束位置。
        spannable.setSpan(ForegroundColorSpan(resources.getColor(R.color.home_green)), 7, 13, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannable.setSpan(ForegroundColorSpan(resources.getColor(R.color.home_green)), 14, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        //这个一定要记得设置，不然点击不生效
        agreement.setMovementMethod(LinkMovementMethod.getInstance())
        spannable.setSpan(TextClick(AgreeMentAct::class.java), 7, 13, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannable.setSpan(TextClickS(PrivacyAct::class.java), 14, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        agreement.setText(spannable)
        password_eye.setOnClickListener {
            // 不可见密码
            if (!eyestatus) {
                password_eye.setImageResource(R.drawable.passord_open)
                passwords.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                passwords.setSelection(passwords.text.length)
            } else
            // 可见的密码
            {
                password_eye.setImageResource(R.drawable.password_close)
                passwords.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD or InputType.TYPE_CLASS_TEXT)
                passwords.setSelection(passwords.text.length)
            }
            eyestatus = !eyestatus
        }
        regist.setOnClickListener {
            if (indefy()) {
                Regist(name.text.toString(), idcard.text.toString(), phone.text.toString(),passwords.text.toString(),vercode.text.toString())
            }
        }
        sms.setOnClickListener {
            if (Utils.isPhone(phone.getText().toString())) {
//                val timer: Timer = Timer(60000, 1000, sms, this)//倒计时
//                timer.start()
                try {
                    getSmscode(phone.getText().toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                RxToast.normal("请输入正确手机号")

            }
        }
    }

    /***
     *  获取验证码
     * @param toString String
     */
    private fun getSmscode(toString: String) {
        var baseUrlUtil: BaseUrlUtil = BaseUrlUtil()
        baseUrlUtil.getInstance().getVcodes(toString, "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<HoleResponse<Int>> {
                    override fun onError(e: Throwable?) {

                    }

                    override fun onNext(t: HoleResponse<Int>?) {

                        when (t!!.code) {
                            0 -> {
                                val timer: Timer = Timer(120000, 1000, sms, this@RegisDact)//倒计时
                                timer.start()
                            }
                            1 -> RxToast.normal(t!!.msg!!)
                            else -> RxToast.normal(t!!.msg!!)
                        }
                    }

                    override fun onCompleted() {
                    }

                })
    }


    private inner class TextClick(clazz: Class<AgreeMentAct>) : ClickableSpan() {
        override fun onClick(widget: View) {
            //在此处理点击事件
            startActivity(Intent(this@RegisDact, AgreeMentAct::class.java))

        }

        override fun updateDrawState(ds: TextPaint) {
            ds.isUnderlineText = false
            //文字的颜色 ds.setUnderlineText(true); //是否设置下划线，true表示设置。
        }

    }

    private inner class TextClickS(clazz: Class<PrivacyAct>) : ClickableSpan() {
        override fun onClick(widget: View) {
            //在此处理点击事件
            startActivity(Intent(this@RegisDact, PrivacyAct::class.java))
        }
        override fun updateDrawState(ds: TextPaint) {
            ds.isUnderlineText = false
            //文字的颜色 ds.setUnderlineText(true); //是否设置下划线，true表示设置。
        }
    }

    fun indefy(): Boolean {
        if (TextUtils.isEmpty(phone?.text)) {
            RxToast.normal("请输入手机号")
            return false
        }
        if (TextUtils.isEmpty(vercode?.text)) {
            RxToast.normal("请输入验证码")
            return false
        }
        if (TextUtils.isEmpty(passwords?.text)) {
            RxToast.normal("请输入密码")
            return false
        } else {
            if (Utils.isPasswordss(passwords?.text.toString())) {
            }else{
                RxToast.normal("密码格式不正确")
                return false
            }
        }
        if (TextUtils.isEmpty(name?.text)) {
            RxToast.normal("请输入真实姓名")
            return false
        }
        if (TextUtils.isEmpty(idcard?.text)) {
            RxToast.normal("请输入身份证号")
            return false
        } else {
            if (!Utils.isCardID(idcard?.text.toString())) {
                RxToast.normal("请输入正确的身份证")
                return false
            }
        }
        if (checkbox.isChecked){
            return true
        }else{
            var ani = AnimationUtils.loadAnimation(this, R.anim.shake_anim)
            bottom_layout.startAnimation(ani)
            val systemService: Vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            systemService.vibrate(300)

            RxToast.normal("请同意用户协议和隐私政策")
            return false
        }
        return true
    }

//var page: Int = intent.extras.get("currentPage") as Int
//        var rxDialogLoading: RxDialogLoading = RxDialogLoading(this)
//        rxDialogLoading.show()
//        dialog = CustomDialog(this@LoginAct, R.style.customDialog, R.layout.update_layout)

//
//        login.setOnClickListener {
//            var intent = Intent()
////            intent.putExtra("currentPage", 1)
////            setResult(1000, intent)
////            Myapplication.editor.putBoolean("isLogin", true).commit()
////            finish()
//            dialog!!.show()
//            var imageView: MyFitimage = dialog!!.findViewById<MyFitimage>(R.id.top_image)
//            var int: Int = imageView.measuredWidth
//            var linearLayout: LinearLayout = dialog!!.findViewById<LinearLayout>(R.id.content_layout)
//            var parms: ViewGroup.LayoutParams = linearLayout.layoutParams
//            println(int .toString()+"int")
//            parms.width = int
//            parms.height = parms.height
// linearLayout.layoutParams = parms
//println(parms.height.toString() +"234"+ parms.width.toString())


    override fun onBackPressed() {
        finish()
    }

    fun Regist(name: String, idcard: String, account: String,password :String,vcode:String) {
        var loading: RxDialog = RxDialog(this)
        loading.setContentView(R.layout.dialog_loading)
        loading.show()
        var baseUrlUtil: BaseUrlUtil = BaseUrlUtil()
        baseUrlUtil.getInstance().Regist(name, idcard, account,password,vcode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<HoleResponse<Int>> {
                    override fun onError(e: Throwable?) {
                        loading.dismiss()
                    }
                    override fun onNext(t: HoleResponse<Int>?) {
                        when (t!!.code) {
                            0 -> {
                                RxToast.normal(t!!.msg!!)
                                finish()
                            }
                            1
                            -> RxToast.normal(t!!.msg!!)
                            else -> RxToast.normal(t!!.msg!!)
                        }
                    }
                    override fun onCompleted() {
                        loading.dismiss()
                    }

                })

    }

    private fun handleLoginSuccess3() {
        val bundle = intent.getBundleExtra(Myapplication.SHARED_NAME)
        val className = bundle.getString(Myapplication.CLASSNAME)
        if (TextUtils.isEmpty(className)) {
            return
        }
        val intent = Intent()
        intent.component = ComponentName(this@RegisDact, className)
        bundle.remove(Myapplication.CLASSNAME)
        intent.putExtra(Myapplication.SHARED_NAME, bundle)
        startActivity(intent)
        finish()
    }

    fun Login() {
        handleLoginSuccess3();
//    var baseUrlUtil: BaseUrlUtil = BaseUrlUtil()
//    baseUrlUtil.getInstance().getVcodes("182", 0)
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribeOn(Schedulers.io())
//            .subscribe(object : Observer<HoleResponse<UpdateModle>> {
//                override fun onNext(t: HoleResponse<UpdateModle>?) {
//                    println(t.toString())
//                    var int: Int = packageManager.getPackageInfo(packageName, 0)!!.versionCode
//                    if (int < t!!.data!!.get(0)!!.versionCode!!.toInt()) {
//                        dialog!!.show()
//                        var imageView: MyFitimage = dialog!!.findViewById<MyFitimage>(com.peaceclient.com.R.id.top_image)
//                        var int: Int = imageView.measuredWidth
//                        var linearLayout: LinearLayout = dialog!!.findViewById<LinearLayout>(com.peaceclient.com.R.id.content_layout)
//                        var parms: ViewGroup.LayoutParams = linearLayout.layoutParams
//                        parms.width = int
//                        linearLayout.layoutParams = parms
//                        if (t!!.data!!.get(0)!!.enable == 0) {
//                            dialog!!.findViewById<Button>(com.peaceclient.com.R.id.cancle).visibility = View.GONE
//                        } else {
//                            dialog!!.findViewById<Button>(com.peaceclient.com.R.id.cancle).visibility = View.VISIBLE
//                        }
//                        dialog!!.findViewById<TextView>(com.peaceclient.com.R.id.update_content).setText(t!!.data!!.get(0)!!.content!!)
//                        dialog!!.findViewById<TextView>(com.peaceclient.com.R.id.title).setText("发现新版本" + t!!.data!!.get(0)!!.versionNo!!)
//
//                    } else {
//                        RxToast.showToast("t")
//                    }

    }
//                    override fun onNext(t: HoleResponse<UpdateModle>?) {
//                        // enable 0,强制更新 1，不强制更新
//                        // versionCode 比较本地versionCode 是否更新
//                        t!!.data!!.enable = 1
//                        var int: Int = packageManager.getPackageInfo(packageName, 0)!!.versionCode
//                        if (int < t!!.data!!.versionCode!!.toInt()) {
//
//                            if (t!!.data!!.enable == 0) {
//                                dialog!!.findViewById<Button>(R.id.cancle).visibility = View.GONE
//                            } else {
//                                dialog!!.findViewById<Button>(R.id.cancle).visibility = View.VISIBLE
//                            }
//                            dialog!!.findViewById<TextView>(R.id.update_content).setText(t!!.data!!.content!!)
//                            dialog!!.findViewById<TextView>(R.id.title).setText("发现新版本"+t!!.data!!.versionNo!!)
//                            dialog!!.show()
//                        } else {
//                                RxToast.showToast("t")
//                        }
//
//
//                    }

    /* override fun onCompleted() {

     }

     override fun onError(e: Throwable) {
         println(e.printStackTrace())
     }


 })*/
}