package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.widget.TextView
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.PeisongBean
import com.peaceclient.hospitaldoctor.com.modle.PeopleModle
import kotlinx.android.synthetic.main.medicinelist_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/9/15 18:00
 * @change
 * @chang time
 * @class describe 药品配送列表页面
 */

class MedicineListAct : HoleBaseActivity() {
    private var kong: View? = null
    private var list: ArrayList<PeisongBean> = arrayListOf()
    private var PeopleTem: PeopleModle = PeopleModle()
    private var adapter: PeopleAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.medicinelist_act)
        kong = View.inflate(this@MedicineListAct, R.layout.empty_view, null)
        var manager: LinearLayoutManager = LinearLayoutManager(this)
        medic_recycle.layoutManager = manager
        adapter = PeopleAdapter(R.layout.medicine_item, list)
        medic_recycle.adapter = adapter
        adapter?.bindToRecyclerView(medic_recycle)
        adapter?.setEmptyView(kong)
        adapter?.isUseEmpty(true)
        medismart.setOnRefreshListener {
            list?.clear()
            getRecipList()
        }
        arrow_back.setOnClickListener {
            finish()
        }
        /***
         * 药品配送管理详情页面
         */
        adapter?.setOnItemChildClickListener { adapter, view, position ->
            when (view) {
                adapter.getViewByPosition(medic_recycle, position, R.id.transport) -> {
                    var intent: Intent = Intent(this@MedicineListAct, MedicManagerAct::class.java)
                    intent.putExtra("modle", list.get(position).id)
                    startActivity(intent)
                }
            }

        }
    }


    inner class PeopleAdapter(layoutResId: Int, data: ArrayList<PeisongBean>?) : com.chad.library.adapter.base.BaseQuickAdapter<PeisongBean, BaseViewHolder>(layoutResId, data) {

        override fun convert(helper: BaseViewHolder, item: PeisongBean) {
            helper.getView<TextView>(R.id.name).setText(item?.uname?:"")
            helper.getView<TextView>(R.id.time).setText(item?.billtime ?: "")
            helper.addOnClickListener(R.id.transport)
        }


    }

    /***
     * 药品配送列表数据页面
     */
    fun getRecipList() {

        IpUrl.getInstance()!!.weipeisongList(ConstantViewMolde.getToken(),"1","","")
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<HoleResponse<ArrayList<PeisongBean>>> {
                    override fun onError(e: Throwable?) {
                        medismart.finishRefresh()
                    }

                    override fun onNext(t: HoleResponse<ArrayList<PeisongBean>>) {
                        if (t?.code == 0) {
                            list.addAll(t?.data!!)
                            adapter?.notifyDataSetChanged()
                        }
                    }

                    override fun onCompleted() {
                        medismart.finishRefresh()
                    }
                })
    }

    override fun onResume() {
        super.onResume()
        medismart.autoRefresh()
    }
}