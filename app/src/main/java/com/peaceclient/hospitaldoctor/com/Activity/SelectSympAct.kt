package com.peaceclient.hospitaldoctor.com.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.widget.TextView
import com.chad.library.adapter.base.BaseViewHolder
import com.jakewharton.rxbinding4.view.clicks
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.KeyboardUtils
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.RecipeModlex
import kotlinx.android.synthetic.main.select_symptom.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor222
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2022/4/20 10:46
 * @change
 * @chang time
 * @class describe 选择症状页面
 */
class SelectSympAct : HoleBaseActivity() {
    var kong: View? = null
    var header: View? = null
    var type:String =""
    var adapter: MedicineAdapter? = null
    var list: ArrayList<RecipeModlex.Others> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.select_symptom)
        kong = View.inflate(this@SelectSympAct, R.layout.empty_view, null)
        header = View.inflate(this@SelectSympAct, R.layout.medicine_header, null);
        var manager: LinearLayoutManager = LinearLayoutManager(this)
        recycle.layoutManager = manager
        adapter = MedicineAdapter(R.layout.symptom_item, list)
        recycle.adapter = adapter
        adapter?.bindToRecyclerView(recycle)
        adapter?.setEmptyView(kong)
        adapter?.addHeaderView(header)
        adapter?.isUseEmpty(true)
        type = intent.getStringExtra("type")
        println("sele" + type)
        symSmart.setOnRefreshListener {
            list?.clear()
            getRecipList()
        }
        arrow_back.setOnClickListener {
            finish()
        }
        search.clicks().throttleFirst(1000, TimeUnit.MICROSECONDS).subscribe {
            KeyboardUtils.hideKeyboard(search)
            symSmart.autoRefresh()
        }
        adapter?.setOnItemChildClickListener { adapter, view, position ->
            when (view) {
                adapter.getViewByPosition(recycle, position + 1, R.id.sym_select) -> {
                    intent.putExtra("zz", list.get(position))
                    setResult(AppCompatActivity.RESULT_OK, intent)
                    finish()
                }
            }
        }
    }

    inner class MedicineAdapter(layoutResId: Int, data: ArrayList<RecipeModlex.Others>?) : com.chad.library.adapter.base.BaseQuickAdapter<RecipeModlex.Others, BaseViewHolder>(layoutResId, data) {

        override fun convert(helper: BaseViewHolder, item: RecipeModlex.Others) {
            helper.getView<TextView>(R.id.sym_content).setText(item?.name ?: "")
            helper.addOnClickListener(R.id.sym_select)
        }
    }

    /***
     * 症状列表请求
     */
    fun getRecipList() {

        IpUrl.getInstance()!!.SympthomList(ConstantViewMolde.getToken(), type, search_text.text.toString())
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<HoleResponse<ArrayList<RecipeModlex.Others>>> {
                    override fun onError(e: Throwable?) {
                        symSmart.finishRefresh()
                    }
                    override fun onNext(t: HoleResponse<ArrayList<RecipeModlex.Others>>) {
                        if (t?.code == 0) {
                            list.addAll(t?.data!!)
                            adapter?.notifyDataSetChanged()
                        }
                    }

                    override fun onCompleted() {
                        symSmart.finishRefresh()
                    }
                })
    }

    override fun onResume() {
        super.onResume()
        symSmart.autoRefresh()
    }

}
