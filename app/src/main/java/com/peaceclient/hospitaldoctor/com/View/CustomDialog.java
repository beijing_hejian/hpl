package com.peaceclient.hospitaldoctor.com.View;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name tags
 * @class name：com.denet.nei.com.View
 * @class describe
 * @anthor admin
 * @time 2021/2/4 10:12
 * @change
 * @chang time
 * @class describe
 */

public class CustomDialog extends Dialog {
      private Context context;
      private int resId;
      public CustomDialog(Context context, int resLayout) {
            this(context,0,0);
      }
      public CustomDialog(Context context, int themeResId, int resLayout) {
            super(context, themeResId);
            this.context = context;
            this.resId = resLayout;
      }
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            this.setContentView(resId);
            super.onCreate(savedInstanceState);
      }

      @Override
      public void dismiss() {

            super.dismiss();
      }
}