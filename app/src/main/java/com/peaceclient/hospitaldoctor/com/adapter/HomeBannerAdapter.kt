/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.adapter


/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.adapter
 * @class describe
 * @anthor admin
 * @time 2021/7/13 15:10
 * @change
 * @chang time
 * @class describe
 */


import android.content.Context
import androidx.viewpager.widget.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide

import com.peaceclient.hospitaldoctor.com.InterFace.OnBannerClicker
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.View.AutoImageView
import com.peaceclient.hospitaldoctor.com.modle.KnowleModle

/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name project
 * @class name：com.homeclientz.com.Adapter
 * @class describe
 * @anthor admin
 * @time 2019/11/6 17:59
 * @change
 * @chang time
 * @class describe
 */

class HomeBannerAdapter : PagerAdapter, View.OnClickListener {
    private var mList: MutableList<KnowleModle>?;
    private var context: Context

    constructor(arr: MutableList<KnowleModle>?, con: Context) {
        this.mList = arr
        this.context = con
    }

    private var listener: OnBannerClicker? = null




    override fun getCount(): Int {
        return Int.MAX_VALUE
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(context).inflate(R.layout.view_banner, null)
        val myFitimage = view?.findViewById(R.id.fitimage) as AutoImageView
        myFitimage.setScaleType(ImageView.ScaleType.FIT_XY)
        if (mList?.size == 0){
        }else{
            Glide.with(context).load(mList?.get(position % mList?.size!! )?.imgUrl).error(R.drawable.home_banner).into(myFitimage)
            view.setTag(position % mList?.size!!)
        }
        container.addView(view)
        view.setOnClickListener(this)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

   public fun setListener(listener: OnBannerClicker) {
        this.listener = listener
    }

    override fun onClick(v: View) {
        if (listener != null) {
            listener?.onImageclick(v, v.tag as Int)
        }
    }
}
