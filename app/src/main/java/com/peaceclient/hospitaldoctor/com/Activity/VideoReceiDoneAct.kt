package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.TimeFormatUtils
import com.peaceclient.hospitaldoctor.com.modle.ClientModle
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import kotlinx.android.synthetic.main.videodone_act.*
import kotlinx.android.synthetic.main.videorece_act.arrow_back
import kotlinx.android.synthetic.main.videorece_act.name
import kotlinx.android.synthetic.main.videorece_act.paihao
import kotlinx.android.synthetic.main.videorece_act.recycle
import kotlinx.android.synthetic.main.videorece_act.status
import rx.Observer
import rx.android.schedulers.AndroidSchedulers

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/9/18 10:31
 * @change
 * @chang time
 * @class describe 远程接诊结束（废弃）
 */

class VideoReceiDoneAct  :HoleBaseActivity(){
    private var toChatUsername: String? = null
    private var startTime: String? = null
    private var endTime: String? = null
    private var adapters:DoctorAdapters? = null
    private var imagelist: ArrayList<ClientModle.urlMo> = arrayListOf()
    private var id: String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.videodone_act)
      id =   intent.getStringExtra("id")
        var linerlay: GridLayoutManager = GridLayoutManager(this@VideoReceiDoneAct, 3, LinearLayoutManager.VERTICAL, true)
        recycle.layoutManager = linerlay
        adapters = DoctorAdapters(R.layout.images_item, imagelist)
        recycle.adapter = adapters
        adapters?.notifyDataSetChanged()
        getList()
        arrow_back.setOnClickListener { finish() }
//        diag_layout.setOnClickListener {
//            var  inten :Intent = Intent(this@VideoReceiDoneAct,DiagnosisActs::class.java)
//            inten.putExtra("id",id)
//            startActivity(inten)
//        }
        adapters?.setOnItemClickListener { adapter, view, position ->
         var  inte :Intent = Intent(this@VideoReceiDoneAct,MyImageActivity::class.java)
            inte.putExtra("url",imagelist.get(position).url)
            startActivity(inte)


        }
    }


    internal inner class DoctorAdapters(layoutResId: Int, data: ArrayList<ClientModle.urlMo>) : com.chad.library.adapter.base.BaseQuickAdapter<ClientModle.urlMo, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder?, item: ClientModle.urlMo?) {
            var ima: ImageView = helper?.getView(R.id.tupian) as ImageView
            Glide.with(this@VideoReceiDoneAct).load(item?.url
                    ?: "").placeholder(R.drawable.user_header)
                    .error(R.drawable.user_header)
                    .into(ima)
        }
    }

    fun getList() {
        IpUrl.getInstance()!!.spDetial(ConstantViewMolde.getToken(), id ?: "")
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ClientModle>> {
                    override fun onError(e: Throwable?) {}
                    override fun onNext(t: HoleResponse<ClientModle>?) {
                        if (t?.code == 0) {
                            if (t?.data != null) {
                                var  pm:String = ""
                                if(t?.data?.makeAppointmentPm == "0"){
                                    pm = "上午"
                                }else{
                                    pm = "下午"
                               }
                                textname .text = (t?.data?.makeAppointmentDate ?: "") +"\r\r"+ pm
                                name.text = t?.data?.resName ?: ""
                                paihao.setText(t?.data?.makeAppointmentNo ?: "")
                                toChatUsername = t?.data?.resImAccount ?: ""
                                startTime = TimeFormatUtils.ms2Date(System.currentTimeMillis())
                                endTime = t?.data?.endTime
                                when (t?.data?.orderStatus) {
                                    "1" -> {
                                        status.setText("待接诊")

                                    }
                                    "2" -> {
                                        status.setText("接诊中")

                                    }
                                    "3" -> {
                                        status.setText("接诊待响应")

//                                        if (!TextUtils.isEmpty(startTime) && !TextUtils.isEmpty(endTime)) {
//                                            if (endTime!!.compareTo(TimeFormatUtils.ms2Date(System.currentTimeMillis())) < 0) {
//                                            } else {
//                                                dillTime(startTime!!, endTime!!, shengyu_time, 0)
//                                            }
//                                        } else {
//                                        }
                                    }
                                    "4" -> {
                                        status.setText("已响应")

                                    }
                                    "5" -> {
                                        status.setText("已结束")
                                    }
                                    "8" -> {
                                        status.setText("已停诊")

                                    }
                                    "7" -> {
                                        status.setText("未响应")

                                    }
                                }
                                if (t?.data?.urls != null) {
                                    imagelist.addAll(t?.data?.urls!!)
                                    adapters?.notifyDataSetChanged()
                                }
                            }
                        }
                    }

                    override fun onCompleted() {}
                })
    }
}