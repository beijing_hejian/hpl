package com.peaceclient.hospitaldoctor.com.Base;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.hyphenate.easeui.EaseUI;
import com.hyphenate.push.EMPushHelper;
import com.hyphenate.push.EMPushType;
import com.hyphenate.push.PushListener;
import com.peaceclient.hospitaldoctor.com.Hy.DemoHelper;
import com.peaceclient.hospitaldoctor.com.View.CustomDialog;
import com.vondear.rxtools.RxTool;

import cn.jpush.android.api.JPushInterface;


/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name tags
 * @class name：com.denet.nei.com.Base
 * @class describe
 * @anthor admin
 * @time 2021/1/7 10:38
 * @change
 * @chang time
 * @class describe
 */

public class Myapplication extends Application {
      public static Context mcontext;
      public static final String SHARED_NAME = "SPS";
      public static final String CLASSNAME = "classnameS";
      public static final String KEY_MSG = "MESSAGE";
      public static SharedPreferences sp;
      public static SharedPreferences.Editor editor;
      private static CustomDialog dialog;
      private PackageInfo packageInfo;
      public static CustomDialog customDialog;

      @Override
      public void onCreate() {
            super.onCreate();
            // 全局上下文，特殊情况使用
            mcontext = getApplicationContext();
            // 全局存储小文件使用shareprefence
            sp = getSharedPreferences(SHARED_NAME, MODE_PRIVATE);
            // sp的编辑者
            editor = sp.edit();
            RxTool.init(this);
            JPushInterface.init(this);
            JPushInterface.setDebugMode(true);
            DemoHelper.getInstance().init(mcontext);
            // 获取包名一些信息，例如包名，版本号,版本名称，有关应用的根部信息
            try {
                  packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                  e.printStackTrace();
            }
            RxTool.init(this);
//            RxRetrofitApp.init(this, BuildConfig.DEBUG);
            if (EaseUI.getInstance().isMainProcess(this)) {
                  // 初始化华为 HMS 推送服务, 需要在SDK初始化后执行
                  // HMSPushHelper.getInstance().initHMSAgent(instance);
                  EMPushHelper.getInstance().setPushListener(new PushListener() {
                        @Override
                        public void onError(EMPushType pushType, long errorCode) {
                              // TODO: 返回的errorCode仅9xx为环信内部错误，可从EMError中查询，其他错误请根据pushType去相应第三方推送网站查询。
                              //EMLog.e("PushClient", "Push client occur a error: " + pushType + " - " + errorCode);
                        }
                  });
            }
      }


}
