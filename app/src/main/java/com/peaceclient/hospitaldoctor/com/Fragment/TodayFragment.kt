package com.peaceclient.hospitaldoctor.com.Fragment

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Activity.VideoReceiAct
import com.peaceclient.hospitaldoctor.com.Activity.VideoReceiDoneAct
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.KeyboardUtils
import com.peaceclient.hospitaldoctor.com.cache.UserCacheManager
import com.peaceclient.hospitaldoctor.com.modle.ClientModle
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.TwModle
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.vondear.rxtools.view.dialog.RxDialog
import rx.Observer
import rx.android.schedulers.AndroidSchedulers

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.Fragment
 * @class describe
 * @anthor admin
 * @time 2021/9/15 14:34
 * @change
 * @chang time
 * @class describe   待接诊页面
 */

class TodayFragment : BaseLazyFragment() {
    private var rootView: View? = null
    private var kong: View? = null
    private var adapter: CheckHisAdapter? = null
    private var messageList: ArrayList<TwModle> = arrayListOf()
    private var visit_recycle: RecyclerView? = null
    private var visitsmart: SmartRefreshLayout? = null
    override fun lazyLoad() {
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = layoutInflater.inflate(R.layout.visit_fragment, null)
        kong = View.inflate(Myapplication.mcontext, R.layout.empty_view, null)
        visit_recycle = rootView?.findViewById<RecyclerView>(R.id.visit_recycle)
        visitsmart = rootView?.findViewById<SmartRefreshLayout>(R.id.visitsmart)
        val manager = LinearLayoutManager(Myapplication.mcontext, LinearLayoutManager.VERTICAL, false)
        visit_recycle?.setLayoutManager(manager)
        adapter = CheckHisAdapter(R.layout.today_item_layout, messageList)
        adapter?.bindToRecyclerView(visit_recycle)
        adapter?.setEmptyView(kong)
        adapter?.isUseEmpty(true)
        visit_recycle?.setAdapter(adapter)
        adapter?.notifyDataSetChanged()
        visitsmart?.isEnableLoadmore = false
        visitsmart?.autoRefresh()
        visitsmart?.setOnRefreshListener {
            messageList?.clear()
            getList()

        }
        adapter?.setOnItemChildClickListener { adapter, view, position ->
            when (view) {
                adapter.getViewByPosition(visit_recycle, position, R.id.cancle_visit) -> {

                    var dialog: RxDialog = RxDialog(activity)
                    dialog.setContentView(R.layout.stop_layout)
                    var edi = dialog.findViewById<EditText>(R.id.content);
                    dialog.show()
                    dialog.findViewById<TextView>(R.id.cancle).setOnClickListener {
                        dialog?.dismiss()
                    }
                    dialog.findViewById<TextView>(R.id.sure).setOnClickListener {
                        KeyboardUtils.hideKeyboard(dialog.findViewById<TextView>(R.id.sure))
                        getStop(messageList?.get(position)?.id ?: "", (edi?.text ?: "").toString())
                        dialog?.dismiss()
                    }
                    dialog.findViewById<EditText>(R.id.content).addTextChangedListener(object : TextWatcher {
                        override fun afterTextChanged(s: Editable?) {
                            dialog.findViewById<TextView>(R.id.zishu).setText((s?.length.toString()
                                    ?: 0.toString()) + "/50")
                        }
                        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                        }
                        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        }
                    })
                }
                adapter.getViewByPosition(visit_recycle, position, R.id.sure_visit) -> {
                    change(messageList?.get(position)?.id ?: "")
                    var inten: Intent = Intent(activity, VideoReceiAct::class.java)
                    inten.putExtra("id", messageList?.get(position)?.id)
                    startActivity(inten)
                }
                adapter.getViewByPosition(visit_recycle, position, R.id.chakan) -> {
                    var inten: Intent = Intent(activity, VideoReceiDoneAct::class.java)
                    inten.putExtra("id", messageList?.get(position)?.id)
                    startActivity(inten)
                }

            }

        }
        return rootView
    }

    internal inner class CheckHisAdapter(layoutResId: Int, data: List<TwModle>?) : BaseQuickAdapter<TwModle, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: TwModle) {
            helper.setText(R.id.name, item?.resName ?: "")
                    .setText(R.id.visit_time, item?.createTime ?: "")
                    .addOnClickListener(R.id.sure_visit)
                    .addOnClickListener(R.id.cancle_visit)
                    .addOnClickListener(R.id.chakan)
            var box: ImageView = helper.getView<ImageView>(R.id.pm_am)
            var paidui: TextView = helper.getView<TextView>(R.id.number)
            var status: TextView = helper.getView<TextView>(R.id.status)
            var layoutwei: LinearLayout = helper.getView(R.id.lay)
            var layoutdone: LinearLayout = helper.getView(R.id.lay2)
            paidui.setText(item?.makeAppointmentNo)
            if (item.makeAppointmentPm == 0) {
                when (item.orderStatus) {
                    "1" -> {
                        box.setImageResource(R.drawable.moringblue)
                        layoutdone.visibility = View.GONE
                        layoutwei.visibility = View.VISIBLE
                        paidui.isSelected = true
                        status.isSelected = true
                        status.setText("待就诊")
                    }
                    "5" -> {
                        box.setImageResource(R.drawable.morninggray)
                        layoutdone.visibility = View.VISIBLE
                        layoutwei.visibility = View.GONE
                        paidui.isSelected = false
                        status.isSelected = false
                        status.setText("已结束")
                    }
                    "8" -> {
                        box.setImageResource(R.drawable.morninggray)
                        layoutdone.visibility = View.VISIBLE
                        layoutwei.visibility = View.GONE
                        paidui.isSelected = false
                        status.isSelected = false
                        status.setText("已停诊")
                    }
                    else -> {
                        box.setImageResource(R.drawable.moringblue)
                        layoutdone.visibility = View.GONE
                        layoutwei.visibility = View.VISIBLE
                        paidui.isSelected = true
                        status.isSelected = true
                        status.setText("接诊中")
                    }
                }


            } else {
                when (item.orderStatus) {
                    "1" -> {
                        box.setImageResource(R.drawable.afternoon_gray)
                        layoutdone.visibility = View.GONE
                        layoutwei.visibility = View.VISIBLE
                        paidui.isSelected = true
                        status.isSelected = true
                        status.setText("待就诊")
                    }
                    "5" -> {
                        box.setImageResource(R.drawable.afternoongray)
                        layoutdone.visibility = View.VISIBLE
                        layoutwei.visibility = View.GONE
                        paidui.isSelected = false
                        status.isSelected = false
                        status.setText("已结束")
                    }
                    "8" -> {
                        box.setImageResource(R.drawable.afternoongray)
                        layoutdone.visibility = View.VISIBLE
                        layoutwei.visibility = View.GONE
                        paidui.isSelected = false
                        status.isSelected = false
                        status.setText("已停诊")
                    }
                    else -> {
                        box.setImageResource(R.drawable.afternoon_gray)
                        layoutdone.visibility = View.GONE
                        layoutwei.visibility = View.VISIBLE
                        paidui.isSelected = true
                        status.isSelected = true
                        status.setText("接诊中")
                    }
                }


            }

        }
    }
    fun getList() {
        IpUrl.getInstance()!!.getVdlist(ConstantViewMolde.getToken(), "")
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<TwModle>>> {
                    override fun onError(e: Throwable?) {
                        visitsmart?.finishRefresh()
                    }

                    override fun onNext(t: HoleResponse<ArrayList<TwModle>>?) {
                        if (t?.code == 0) {
                            if (t?.data != null) {
                                messageList.addAll(t?.data!!)
                                adapter?.notifyDataSetChanged()
                                for (z in 0..messageList.size) {
                                    UserCacheManager.get(messageList.get(z).resImAccount)
                                }
                            }
                        }
                    }

                    override fun onCompleted() {
                        visitsmart?.finishRefresh()
                    }

                })
    }

    fun getStop(id: String, content: String) {
        var rxdia: RxDialog = RxDialog(activity)
        rxdia.setContentView(R.layout.dialog_loading)
        rxdia.show()
        IpUrl.getInstance()!!.stop(ConstantViewMolde.getToken(), id, content)
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ClientModle>> {
                    override fun onError(e: Throwable?) {
                        rxdia?.dismiss()
                    }

                    override fun onNext(t: HoleResponse<ClientModle>?) {
                        if (t?.code == 0) {
                            messageList.clear()
                            getList()
                            rxdia?.dismiss()
                        }
                    }

                    override fun onCompleted() {
                        rxdia?.dismiss()
                    }
                })
    }

    fun change(id: String) {
        IpUrl.getInstance()!!.changeStatus(ConstantViewMolde.getToken(), "2", id)
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ClientModle>> {
                    override fun onError(e: Throwable?) {

                    }

                    override fun onNext(t: HoleResponse<ClientModle>?) {

                    }

                    override fun onCompleted() {

                    }

                })
    }
//    fun getStop(id: String,content:String) {
//        var rxdia: RxDialog = RxDialog(activity)
//        rxdia.setContentView(R.layout.dialog_loading)
//        rxdia.show()
//        IpUrl.getInstance()!!.Spfinish(ConstantViewMolde.getToken(), id)
//                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<Int>> {
//                    override fun onError(e: Throwable?) {
//                        rxdia?.dismiss()
//                    }
//                    override fun onNext(t: HoleResponse<Int>?) {
//                        if (t?.code == 0) {
//                            messageList.clear()
//                            getList()
//                            rxdia?.dismiss()
//                        }
//                    }
//                    override fun onCompleted() {
//                        rxdia?.dismiss()
//                    }
//                })
//    }

    override fun onVisible() {
        super.onVisible()

        visitsmart?.autoRefresh()
    }
}