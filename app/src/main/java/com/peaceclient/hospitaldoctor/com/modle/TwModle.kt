package com.peaceclient.hospitaldoctor.com.modle

class TwModle {
    var resName: String? = null
    var createTime: String? = null
    var endTime: String? = null
    var docImAccount: String? = null
    var orderStatus: String? = null
    var resImAccount: String? = null
    var id: String? = null
    var prescription: String? = null
    var diagnosis: String? = null
    var makeAppointmentDate: String? = null
    var makeAppointmentPm: Int = 0
    var makeAppointmentNo :String =""

}
