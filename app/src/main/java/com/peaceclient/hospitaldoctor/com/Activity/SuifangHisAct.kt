package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle

import android.text.TextUtils
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.GhModle
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.vondear.rxtools.view.dialog.RxDialogSureCancel
import kotlinx.android.synthetic.main.suifang_his_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers


/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor222
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/10/13 16:51
 * @change
 * @chang time
 * @class describe 预约随访历史记录列表页面（随访）
 */

class SuifangHisAct : HoleBaseActivity() {

    private var kong: View? = null
    private var adapter: CheckHisAdapter? = null
    private var messageList: ArrayList<GhModle> = arrayListOf()
    private var visit_recycle: RecyclerView? = null
    private var visitsmart: SmartRefreshLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.suifang_his_act)
        arrow_back.setOnClickListener {
            finish()
        }
        val manager = LinearLayoutManager(Myapplication.mcontext, LinearLayoutManager.VERTICAL, false)
        suifang_recy.setLayoutManager(manager)
        adapter = CheckHisAdapter(R.layout.today_sui_layout, messageList)
        kong = View.inflate(this@SuifangHisAct, R.layout.empty_view, null)
        adapter?.bindToRecyclerView(suifang_recy)
        adapter?.setEmptyView(kong)
        adapter?.isUseEmpty(true)
        suifang_recy?.setAdapter(adapter)
        adapter?.notifyDataSetChanged()
        suifang_smart?.autoRefresh()
        suifang_smart?.setOnRefreshListener {
            messageList?.clear()
            getSuifangList()
        }
        adapter?.setOnItemChildClickListener { adapter, view, position ->
            when (view) {
                adapter.getViewByPosition(suifang_recy, position, R.id.call) -> {
                    if (TextUtils.isEmpty(messageList.get(position).lxdh)){

                    }else{
                        dialogShows(messageList.get(position).lxdh);
                    }

                }
            }

        }
    }



    internal inner class CheckHisAdapter(layoutResId: Int, data: List<GhModle>?) : BaseQuickAdapter<GhModle, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: GhModle) {
            helper.setText(R.id.name, item?.hzxm ?: "")
                    .setText(R.id.time, item?.sfrq ?: "")
                    .setText(R.id.phone,item?.lxdh ?: "" )
                    .addOnClickListener(R.id.call)

        }

        override fun getItemCount(): Int {
            return 1
        }
    }

    fun dialogShows(string: String) {
        var dialog: RxDialogSureCancel = RxDialogSureCancel(this@SuifangHisAct)
        dialog.setTitle("提示")
        dialog.setContent("您是否再次随访？")
        dialog.contentView.gravity = Gravity.LEFT
        dialog.contentView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.contentView.setTextColor(Color.parseColor("#050505"))
        dialog.sureView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0f)
        dialog.cancelView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0f)
        dialog.contentView.gravity = Gravity.CENTER
        dialog.sureView.setTextColor(Color.parseColor("#376FE9"))
        dialog.cancelView.setTextColor(Color.parseColor("#376FE9"))
        dialog.titleView.setTextColor(Color.parseColor("#050505"))
        dialog.titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.setSure("否")
        dialog.setCancel("是")
        dialog.show()
        dialog.setSureListener({
            dialog.dismiss()
        })
        dialog.setCancelListener {
            CallPhone(string)
            dialog.dismiss()
        }
    }

    fun CallPhone(s: String) {
        val intent = Intent() // 意图对象：动作 + 数据
        intent.action = Intent.ACTION_CALL // 设置动作
        val data = Uri.parse("tel:$s") // 设置数据
        intent.data = data
        startActivity(intent) // 激活Activity组件
    }

    /***
     *  预约随访记录
     */
    fun getSuifangList() {
        IpUrl.getInstance()!!.suifangHis(ConstantViewMolde.getToken(),  ConstantViewMolde.GetUser()?.user?.id
                ?: "")
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<GhModle>>> {
                    override fun onError(e: Throwable?) {
                        suifang_smart?.finishRefresh()
                    }

                    override fun onNext(t: HoleResponse<ArrayList<GhModle>>?) {
                        if (t?.code == 0) {
                            if (t?.data != null) {
                                messageList.addAll(t?.data!!)
                                adapter?.notifyDataSetChanged()

                            }
                        }
                    }

                    override fun onCompleted() {
                        suifang_smart?.finishRefresh()
                    }

                })


    }

}