/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.InterFace;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.peaceclient.hospitaldoctor.com.Activity.LoginAct;
import com.peaceclient.hospitaldoctor.com.Base.Myapplication;
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　  ┏┓
 * 　　┏┛┻━━━ ┛┻┓━━
 * 　　┃　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　┃　　　┃
 * 　　　┃　　　┃
 * 　　　┃　　　┗━━━┓
 * 　　　┃　　　　　┣┓
 * 　　　┃　　　　　┏┛
 * 　　　┗┓┓┏━┳┓┏┛
 * 　　　　┃┫┫┃┫┫
 * 　　　　┗┻┛┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.InterFace
 * @class describe
 * @anthor admin
 * @time 2021/8/20 9:31
 * @change
 * @chang time
 * @class describe
 */

public class LaunchInvocationHandler implements InvocationHandler {
      private ILaunchManagerService managerService;
      private Context context;
      public LaunchInvocationHandler(ILaunchManagerService managerService, Context context) {
            this.managerService = managerService;
            this.context = context;
      }
      /***
       * 关掉登录，
       * 让其代理自己操作逻辑
       *
       * */
      @Override
      public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (!ConstantViewMolde.Companion.IsLogin()) {
                  Intent intent = (Intent) args[0];
                  ComponentName component = intent.getComponent();
                  String className = component.getClassName();
                  Bundle bundleExtra = intent.getBundleExtra(Myapplication.SHARED_NAME);
                  bundleExtra.putString(Myapplication.CLASSNAME, className);
                  intent.putExtra(Myapplication.SHARED_NAME, bundleExtra);
                  intent.setClass(context, LoginAct.class);
                  args[0] = intent;
            }
            return method.invoke(managerService, args);
      }
}
