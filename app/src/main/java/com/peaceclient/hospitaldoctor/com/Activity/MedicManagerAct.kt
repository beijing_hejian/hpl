package com.peaceclient.hospitaldoctor.com.Activity

import androidx.appcompat.app.AppCompatActivity
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.Gravity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.chad.library.adapter.base.BaseViewHolder
import com.jakewharton.rxbinding4.view.clicks
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.android.CaptureActivity
import com.peaceclient.hospitaldoctor.com.common.Constant
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.RecipeModlex
import com.vondear.rxtools.view.RxToast
import com.vondear.rxtools.view.dialog.RxDialog
import kotlinx.android.synthetic.main.medicinehis_act.*
import kotlinx.android.synthetic.main.medicmanager_act.*
import kotlinx.android.synthetic.main.medicmanager_act.arrow_back
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/9/17 10:30
 * @change
 * @chang time
 * @class describe  药品管理页面
 */

class MedicManagerAct : HoleBaseActivity() {
    private var messlist: ArrayList<RecipeModlex.BillDetail> = arrayListOf()
    private var adapter: DoctorAdapter? = null
    var modle: String? = null
    var kuai: String? = null
    var exi: RecipeModlex.BillExpress? = null
    var expressName: String? = null
    var JlAdapter: ArrayAdapter<String>? = null
    private var header: View? = null
    var ExpressNamelist: ArrayList<String> = arrayListOf()
    var Express: ArrayList<RecipeModlex.Others> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.medicmanager_act)
        modle = intent.getStringExtra("modle")
        //设置文字的前景色，2、4分别表示可以点击文字的起始和结束位置。
        arrow_back.setOnClickListener { finish() }
        save.clicks().throttleFirst(1500, TimeUnit.MICROSECONDS)
                .subscribe {
                    if (TextUtils.isEmpty(numberlimit.text)) {
                        RxToast.normal("请检查您是否填写快递单号")
                    } else {
                        peisong()
                    }
                }
        scan.setOnClickListener {
            var intent: Intent = Intent(this@MedicManagerAct, CaptureActivity::class.java)
            startActivityForResult(intent, 1000)
        }
        getExpress()
        var linerma: LinearLayoutManager = LinearLayoutManager(this)
        recycle.layoutManager = linerma
        adapter = DoctorAdapter(R.layout.diagshen_item, messlist)
        // adapter?.addHeaderView(header)
        recycle.adapter = adapter
        adapter?.notifyDataSetChanged()
        getRecipList();
        initJlSpinner(kuaidi)
    }

    private fun initJlSpinner(view: Spinner) {
        view!!.prompt = "请选择日期"
        JlAdapter = ArrayAdapter(this@MedicManagerAct, R.layout.spinner_select, ExpressNamelist)
        JlAdapter!!.setDropDownViewResource(R.layout.item_drop_layout)
        view.dropDownVerticalOffset = resources.getDimensionPixelOffset(R.dimen.dp_45)
        kuaidi.gravity = Gravity.CENTER
        view!!.adapter = JlAdapter
        kuaidi.setSelection(0)
        JlAdapter!!.notifyDataSetChanged()
        var selecS: MyItemSelects = MyItemSelects()
        view?.onItemSelectedListener = selecS
    }

    fun getRecipList() {
        var rx: RxDialog = RxDialog(this@MedicManagerAct)
        rx.setContentView(R.layout.dialog_loading)
        rx.show()
        IpUrl.getInstance()!!.PeisongDetial(ConstantViewMolde.getToken(), modle!!)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<RecipeModlex>> {
                    override fun onError(e: Throwable?) {
                        rx.dismiss()
                    }

                    override fun onNext(t: HoleResponse<RecipeModlex>?) {
                        if (t?.code == 0) {
                            messlist.addAll(t?.data?.bills?.billDetails!!)
                            adapter?.notifyDataSetChanged()
                            //result.setText(t?.data?.zdjg)
                            time.setText(t?.data?.bills?.billtime)
                            name.setText(t?.data?.bills?.uname)
                            doctor.setText(t?.data?.bills?.docname)
                            sfys.setText(t?.data?.bills?.checkdocname)
                            var p: String = ""
                            when (t?.data?.bills?.paystatus) {
                                "0" -> {
                                    p = "待支付"
                                }
                                "1" -> {
                                    p = "已支付"
                                }
                                "2" -> {
                                    p = "已退款"
                                }
                            }
                            jfzt.setText(p)


                            if (t?.data?.billexpress != null) {
                                exi = t?.data?.billexpress
                                if (!TextUtils.isEmpty(t?.data?.billexpress!!.express)) {
                                    for (i in 0..Express.size - 1) {
                                        if (t?.data?.billexpress!!.express.equals(Express[i].name))
                                            spinner.setSelection(i)
                                    }
                                }
                                kuai = t?.data?.billexpress!!.expressid
                                //  kuaidi.setText(t?.data?.billexpress!!.express)
                                numberlimit.setText(t?.data?.billexpress!!.expressnum)
                                if (!TextUtils.isEmpty(t?.data?.billexpress!!.address)) {
                                    val spannable = SpannableStringBuilder(t?.data?.billexpress!!.address + "点击复制")
                                    //设置文字的前景色，2、4分别表示可以点击文字的起始和结束位置。
                                    spannable.setSpan(ForegroundColorSpan(resources.getColor(R.color.c96)), 0, spannable.length - 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                                    spannable.setSpan(ForegroundColorSpan(resources.getColor(R.color.blue_texst)), spannable.length - 4, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                                    //这个一定要记得设置，不然点击不生效
                                    addresss.setMovementMethod(LinkMovementMethod.getInstance())
                                    spannable.setSpan(TextClick(spannable.toString()), spannable.length - 4, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                                    addresss.setText(spannable)
                                }
                                if (TextUtils.isEmpty(t?.data?.billexpress!!.phone)) {
                                    fuzhi.visibility = View.GONE
                                } else {
                                    fuzhi.visibility = View.VISIBLE
                                }
                                fuzhi.setOnClickListener {
                                    var clipe: ClipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                                    clipe.text = phone.text.toString()
                                    RxToast.normal("复制成功")
                                }
                            }

                            if (t.data!!.diags == null) {
                                zdType.setText("")
                                result.setText("")
                            } else {
                                when (t.data!!.diags?.prdiagtype) {
//                           1、西医诊断；2、中医病症；3、中医证候
                                    "1" -> {
                                        zdType.setText("西医诊断")
                                    }
                                    "2" -> {
                                        zdType.setText("中医诊断")
                                    }

                                    "" -> {
                                        zdType.setText("")
                                    }
                                }

                                var str: String = t.data?.diags?.name ?: ""
                                if (t.data?.others == null || t.data?.others?.size == 0) {
                                } else {

                                    for (i in 0..t?.data?.others?.size!! - 1) {
                                        str + "、" + t?.data?.others!![i].name;
                                        str = str + "、" + t?.data?.others!![i].name;
                                    }
                                }
                                result.setText(str)
                            }
                        }
                    }

                    override fun onCompleted() {
                        rx.dismiss()
                    }
                })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1000) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                var resu: String = data?.getStringExtra(Constant.CODED_CONTENT)!!
                numberlimit.setText(resu ?: "")
            }

        }
    }

    private inner class TextClick(texts: String) : ClickableSpan() {
        override fun onClick(widget: View) {
            //在此处理点击事件
            var clipe: ClipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            clipe.text = addresss.text.toString().subSequence(0, addresss.text.length - 4)
            RxToast.normal("复制成功")
        }

        override fun updateDrawState(ds: TextPaint) {
            ds.isUnderlineText = false
            //文字的颜色 ds.setUnderlineText(true); //是否设置下划线，true表示设置。
        }

    }

    internal inner class DoctorAdapter(layoutResId: Int, data: ArrayList<RecipeModlex.BillDetail>?) : com.chad.library.adapter.base.BaseQuickAdapter<RecipeModlex.BillDetail, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: RecipeModlex.BillDetail) {
            helper.setText(R.id.yptext, item.name)
                    .setText(R.id.ggtext, item.spec)
                    .setText(R.id.dwtext, item.unit)
                    .setText(R.id.yytext, item.freqname)
                    .setText(R.id.sltext, item.unitcount)
                    .setText(R.id.yftext, item.gytjname)
                    .setText(R.id.yltext, item.percount)

        }
    }

    /***
     * 更新配送管理页面的数据
     */
    fun peisong() {
        var rxdia: RxDialog = RxDialog(this@MedicManagerAct)
        rxdia.setContentView(R.layout.dialog_loading)
        rxdia.show()
        var bod = exi
        bod?.express = expressName!!
        bod?.expressnum = numberlimit.text.toString()
        IpUrl.getInstance()!!.peisong(ConstantViewMolde.getToken(), bod!!)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<HoleResponse<ArrayList<RecipeModlex>>> {
                    override fun onError(e: Throwable?) {
                        rxdia.dismiss()
                    }

                    override fun onNext(t: HoleResponse<ArrayList<RecipeModlex>>) {
                        if (t?.code == 0) {
                            finish()
                            RxToast.normal(t.msg ?: "")

                        }
                    }

                    override fun onCompleted() {
                        rxdia.dismiss()
                    }
                })
    }

    internal inner class MyItemSelects() : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            // messageList.clear()
            expressName = Express.get(position).name
            kuaidi.setSelection(position)
//            if (isFirst) {
//                prosmarts.autoRefresh()
//            }

//            list.get(positions).howtouse = code
//            list.get(positions).freqname = name


        }
    }

    /***
     * 获取快递公司列表
     */
    fun getExpress() {
        var rxdia: RxDialog = RxDialog(this@MedicManagerAct)
        rxdia.setContentView(R.layout.dialog_loading)
        rxdia.show()
        IpUrl.getInstance()!!.GetExpress(ConstantViewMolde.getToken())
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<java.util.ArrayList<RecipeModlex.Others>>> {
                    override fun onError(e: Throwable?) {
                        rxdia?.dismiss()
                    }

                    override fun onNext(t: HoleResponse<java.util.ArrayList<RecipeModlex.Others>>?) {
                        if (t?.code == 0) {
                            rxdia?.dismiss()
                            Express.addAll(t?.data!!)
//                            yypclist.addAll(t?.data!!)
                            for (o in 0..Express.size - 1) {
                                ExpressNamelist.add(Express.get(o).name)
                                JlAdapter!!.notifyDataSetChanged()
                            }
                        } else {
                            RxToast.normal(t?.msg ?: "")
                        }
                    }

                    override fun onCompleted() {
                        rxdia?.dismiss()
                    }
                })
    }

}