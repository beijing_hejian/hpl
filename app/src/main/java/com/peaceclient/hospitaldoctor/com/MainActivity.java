package com.peaceclient.hospitaldoctor.com;


import android.Manifest;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import androidx.core.content.FileProvider;

import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.text.TextUtils;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hyphenate.EMClientListener;
import com.hyphenate.EMConnectionListener;
import com.hyphenate.EMContactListener;
import com.hyphenate.EMMessageListener;
import com.hyphenate.EMMultiDeviceListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.utils.EaseCommonUtils;
import com.peaceclient.hospitaldoctor.com.Base.BaseUrlUtil;
import com.peaceclient.hospitaldoctor.com.Base.Myapplication;
import com.peaceclient.hospitaldoctor.com.Fragment.HomeFragemnt;
import com.peaceclient.hospitaldoctor.com.Fragment.HomeFragemnts;
import com.peaceclient.hospitaldoctor.com.Fragment.InteFragment;
import com.peaceclient.hospitaldoctor.com.Fragment.MessageFragment;
import com.peaceclient.hospitaldoctor.com.Fragment.MyFragment;
import com.peaceclient.hospitaldoctor.com.Hy.Constant;
import com.peaceclient.hospitaldoctor.com.InterFace.CancleBack;
import com.peaceclient.hospitaldoctor.com.InterFace.ILaunchManagerService;
import com.peaceclient.hospitaldoctor.com.InterFace.LaunchInvocationHandler;
import com.peaceclient.hospitaldoctor.com.Receiver.DownLoadReceiver;
import com.peaceclient.hospitaldoctor.com.Utils.ExampleUtil;
import com.peaceclient.hospitaldoctor.com.Utils.StatusBarUtil;
import com.peaceclient.hospitaldoctor.com.Utils.TimeFormatUtils;
import com.peaceclient.hospitaldoctor.com.View.AutoImageView;
import com.peaceclient.hospitaldoctor.com.View.CustomDialog;
import com.peaceclient.hospitaldoctor.com.View.MyRadioButton;
import com.peaceclient.hospitaldoctor.com.View.MyViewPager;
import com.peaceclient.hospitaldoctor.com.View.StatusBarHeightView;
import com.peaceclient.hospitaldoctor.com.adapter.MainVpAdapter;
import com.peaceclient.hospitaldoctor.com.db.InviteMessgeDao;
import com.peaceclient.hospitaldoctor.com.db.UserDao;
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde;
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse;
import com.peaceclient.hospitaldoctor.com.modle.ServiceBean;
import com.peaceclient.hospitaldoctor.com.modle.UpdateModle;
import com.peaceclient.hospitaldoctor.com.ui.ChatActivity;
import com.peaceclient.hospitaldoctor.com.ui.GroupsActivity;
import com.permissionx.guolindev.PermissionX;
import com.permissionx.guolindev.callback.ExplainReasonCallback;
import com.permissionx.guolindev.callback.ForwardToSettingsCallback;
import com.permissionx.guolindev.callback.RequestCallback;
import com.permissionx.guolindev.request.ExplainScope;
import com.permissionx.guolindev.request.ForwardScope;
import com.permissionx.guolindev.request.RationaleDialog;

import java.io.File;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.jpush.android.api.JPushInterface;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class MainActivity extends AppCompatActivity implements CancleBack, EMConnectionListener, ILaunchManagerService {

    public static final String MESSAGE_RECEIVED_ACTION = "com.example.jpushdemo.MESSAGE_RECEIVED_ACTION";
    @BindView(R.id.viewpager)
    MyViewPager viewpager;
    @BindView(R.id.home_banner)
    AutoImageView homeBanner;
    @BindView(R.id.header)
    StatusBarHeightView header;
    @BindView(R.id.rbreport)
    MyRadioButton rbreport;
    @BindView(R.id.rbShop)
    MyRadioButton rbShop;
    @BindView(R.id.rbHome)
    MyRadioButton rbHome;
    @BindView(R.id.rbMessage)
    MyRadioButton rbMessage;
    @BindView(R.id.rgTools)
    RadioGroup rgTools;
    private ArrayList<Fragment> fragments;
    private HomeFragemnts workfragment;
    private InteFragment workfragment1;
    private MessageFragment workfragment2;
    private MyFragment workfragment3;
    private MainVpAdapter vpAdapter;
    private int currentPage;
    private CustomDialog dialog;
    private TextView viewById;
    private ILaunchManagerService ilaunchmanagerservice = null;
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_EXTRAS = "extras";
    private MessageReceiver mMessageReceiver;
    private InviteMessgeDao inviteMessgeDao;
    private LocalBroadcastManager broadcastManager;
    private BroadcastReceiver broadcastReceiver;
    private DownLoadReceiver broadcastReceivers;
    private String fileName;
    private String filename;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setRootViewFitsSystemWindows(this, false);
        StatusBarUtil.setTranslucentStatus(this);
        setContentView(R.layout.activity_mains);
        ButterKnife.bind(this);
        PermissionX.init(this).permissions(Manifest.permission.CAMERA, Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.CALL_PHONE, Manifest.permission.RECORD_AUDIO)
                .onExplainRequestReason(new ExplainReasonCallback() {
                                            @Override
                                            public void onExplainReason(ExplainScope scope, List<String> deniedList) {
                                                scope.showRequestReasonDialog(deniedList, "即将申请的权限是程序必须依赖的权限", "我已明白");
                                            }
                                        }
                ).onForwardToSettings(new ForwardToSettingsCallback() {
                    @Override
                    public void onForwardToSettings(ForwardScope scope, List<String> deniedList) {
                    }
                }).request(new RequestCallback() {
                    @Override
                    public void onResult(boolean allGranted, List<String> grantedList, List<String> deniedList) {
                        if (allGranted) {
                            // Toast.makeText(MainActivity.this, "所有申请的权限都已通过", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MainActivity.this, "您拒绝了如下权限：" + deniedList, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        ilaunchmanagerservice = (ILaunchManagerService) Proxy.newProxyInstance(this.getClassLoader(), new Class[]{ILaunchManagerService.class}, new LaunchInvocationHandler(this, this));
        dialog = new CustomDialog(this, R.style.customDialog, R.layout.update_layout);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        fragments = new ArrayList<>();
        initFragments();
        vpAdapter = new MainVpAdapter(getSupportFragmentManager(), fragments);
        viewpager.setAdapter(vpAdapter);
        vpAdapter.notifyDataSetChanged();
        viewpager.setOffscreenPageLimit(1);
        viewpager.setCurrentItem(0);
        Service();
        JPushInterface.setAlias(this, 0, ConstantViewMolde.Companion.GetUser().getAccount());
        inviteMessgeDao = new InviteMessgeDao(this);
        UserDao userDao = new UserDao(this);
        rgTools.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbreport:
                        currentPage = 0;
                        viewpager.setCurrentItem(0);
                        // Myapplication.editor.putBoolean("isLogin", false).commit();
                        break;
//
                    case R.id.rbHome:
                        currentPage = 2;
                        viewpager.setCurrentItem(2);
                        break;
                    case R.id.rbMessage:
                        currentPage = 3;
                        viewpager.setCurrentItem(3);
                        break;
                }
            }
        });
        initbr();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == 1000) {
            int currentPage = data.getIntExtra("currentPage", 0);
            switch (currentPage) {
                case 0:
                    System.out.println("rbreport");
                    rbreport.setChecked(true);
                    break;
//                        case 1:
//                              rbShop.setChecked(true);
//                              break;
                case 2:
                    System.out.println("rbHome");
                    rbHome.setChecked(true);
                    break;
                case 3:
                    System.out.println("rbMessage");
                    rbMessage.setChecked(true);
                    break;
            }
            vpAdapter.notifyDataSetChanged();
            viewpager.setCurrentItem(currentPage);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initFragments() {
        workfragment = new HomeFragemnts();
        workfragment1 = new InteFragment();
        workfragment2 = new MessageFragment();
        workfragment3 = new MyFragment();
        fragments.add(workfragment);
        fragments.add(workfragment1);
        fragments.add(workfragment2);
        fragments.add(workfragment3);
    }

    @Override
    public void StateChange(int s) {

    }

    @Override
    public void loginsuccess(Intent intent) {

    }

    @Override
    public void onConnected() {

    }

    @Override
    public void onDisconnected(int i) {

    }

    class cusDia extends RationaleDialog {
        public cusDia(@NonNull Context context) {
            super(context);
        }

        @NonNull
        @Override
        public View getPositiveButton() {
            return null;
        }

        @Nullable
        @Override
        public View getNegativeButton() {
            return null;
        }

        @NonNull
        @Override
        public List<String> getPermissionsToRequest() {
            return null;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        listener();
        viewpager.setCurrentItem(currentPage);
        switch (currentPage) {
            case 0:
                System.out.println("rbreport");
                rbreport.setChecked(true);
                break;
            case 1:
                rbShop.setChecked(true);
                break;
            case 2:
                System.out.println("rbHome");
                rbHome.setChecked(true);
                break;
            case 3:
                System.out.println("rbMessage");
                rbMessage.setChecked(true);
                break;
        }

        registerMessageReceiver();
        updateUnreadLabel();
        updateUnreadAddressLable();
        registerBroadcastReceiver();
        EMClient.getInstance().chatManager().addMessageListener(messageListener);
    }

    public void registerMessageReceiver() {
        mMessageReceiver = new MessageReceiver();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction(MESSAGE_RECEIVED_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, filter);
    }

    private void refreshUIWithMessage() {
        runOnUiThread(new Runnable() {
            public void run() {
                updateUnreadLabel();
            }
        });
    }


    public void updateUnreadLabel() {
        int count = getUnreadMsgCountTotal();
        if (count > 0) {
        } else {
        }
    }

    public int getUnreadMsgCountTotal() {
        return EMClient.getInstance().chatManager().getUnreadMsgsCount();
    }

    /**
     * get unread event notification count, including application, accepted, etc
     *
     * @return
     */
    public int getUnreadAddressCountTotal() {
        int unreadAddressCountTotal = 0;
        unreadAddressCountTotal = inviteMessgeDao.getUnreadMessagesCount();
        return unreadAddressCountTotal;
    }

    EMMessageListener messageListener = new EMMessageListener() {

        @Override
        public void onMessageReceived(List<EMMessage> messages) {

            refreshUIWithMessage();
        }

        @Override
        public void onCmdMessageReceived(List<EMMessage> messages) {
            refreshUIWithMessage();
        }

        @Override
        public void onMessageRead(List<EMMessage> messages) {
        }

        @Override
        public void onMessageDelivered(List<EMMessage> message) {
        }

        @Override
        public void onMessageRecalled(List<EMMessage> messages) {
            refreshUIWithMessage();
        }

        @Override
        public void onMessageChanged(EMMessage message, Object change) {
        }
    };

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    public class MessageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (MESSAGE_RECEIVED_ACTION.equals(intent.getAction())) {
                    String messge = intent.getStringExtra(KEY_MESSAGE);
                    String extras = intent.getStringExtra(KEY_EXTRAS);
                    StringBuilder showMsg = new StringBuilder();
                    showMsg.append(KEY_MESSAGE + " : " + messge + "\n");
                    System.out.println(KEY_MESSAGE + " : " + messge + "\n");
                    if (!ExampleUtil.isEmpty(extras)) {
                        showMsg.append(KEY_EXTRAS + " : " + extras + "\n");
                    }

                }
            } catch (Exception e) {
            }
        }
    }

    private void initbr() {
        registerBroadcastReceiver();
        EMClient.getInstance().contactManager().setContactListener(new MyContactListener());
        EMClient.getInstance().addClientListener(clientListener);
        EMClient.getInstance().addMultiDeviceListener(new MyMultiDeviceListener());
    }

    private void registerBroadcastReceiver() {
        broadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constant.ACTION_CONTACT_CHANAGED);
        intentFilter.addAction(Constant.ACTION_GROUP_CHANAGED);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                updateUnreadLabel();
                updateUnreadAddressLable();
                String action = intent.getAction();
                if (action.equals(Constant.ACTION_GROUP_CHANAGED)) {
                    if (EaseCommonUtils.getTopActivity(MainActivity.this).equals(GroupsActivity.class.getName())) {
                        GroupsActivity.instance.onResume();
                    }
                }
            }
        };
        broadcastManager.registerReceiver(broadcastReceiver, intentFilter);
    }

    public class MyContactListener implements EMContactListener {
        @Override
        public void onContactAdded(String username) {
        }

        @Override
        public void onContactDeleted(final String username) {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (ChatActivity.activityInstance != null && ChatActivity.activityInstance.getToChatUsername() != null &&
                            username.equals(ChatActivity.activityInstance.getToChatUsername())) {
                        String st10 = getResources().getString(R.string.have_you_removed);
                        ChatActivity.activityInstance.finish();
                    }
                }
            });
            updateUnreadAddressLable();
        }

        @Override
        public void onContactInvited(String username, String reason) {
        }

        @Override
        public void onFriendRequestAccepted(String username) {
        }

        @Override
        public void onFriendRequestDeclined(String username) {
        }
    }

    public class MyMultiDeviceListener implements EMMultiDeviceListener {

        @Override
        public void onContactEvent(int event, String target, String ext) {

        }

        @Override
        public void onGroupEvent(int event, String target, final List<String> username) {
            switch (event) {
                case EMMultiDeviceListener.GROUP_LEAVE:
                    ChatActivity.activityInstance.finish();
                    break;
                default:
                    break;
            }
        }
    }


    EMClientListener clientListener = new EMClientListener() {
        @Override
        public void onMigrate2x(boolean success) {
            Toast.makeText(MainActivity.this, "onUpgradeFrom 2.x to 3.x " + (success ? "success" : "fail"), Toast.LENGTH_LONG).show();
            if (success) {
                refreshUIWithMessage();
            }
        }
    };

    /**
     * update the total unread count
     */
    public void updateUnreadAddressLable() {
        runOnUiThread(new Runnable() {
            public void run() {
                int count = getUnreadAddressCountTotal();
                if (count > 0) {

                    //    mShapeBadgeItem.show();
                    //  unreadAddressLable.setVisibility(View.VISIBLE);
                } else {

                    //              mShapeBadgeItem.hide();
//                              unreadAddressLable.setVisibility(View.INVISIBLE);
                }
            }
        });

    }

    private void CallPermission(String apkPath) {
        if (Build.VERSION.SDK_INT >= 24) {//判读版本是否在7.0以上
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {//判断是否是8.0或以上
                boolean haveInstallPermission = getPackageManager().canRequestPackageInstalls();
                if (!haveInstallPermission) {
                    Uri packageURI = Uri.parse("package:" + getPackageName());
                    Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES, packageURI);
                    startActivityForResult(intent, 10001);
                } else {
                    Uri apkUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".fileprovider", new File(apkPath));
                    Intent install = new Intent(Intent.ACTION_VIEW);
                    install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    install.setDataAndType(apkUri, "application/vnd.android.package-archive");
                    startActivity(install);
                }
            } else {
                Uri apkUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".fileprovider", new File(apkPath));
                Intent install = new Intent(Intent.ACTION_VIEW);
                install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                install.setDataAndType(apkUri, "application/vnd.android.package-archive");
                startActivity(install);
            }
        } else {
            Intent install = new Intent(Intent.ACTION_VIEW);
            install.setDataAndType(Uri.fromFile(new File(apkPath)), "application/vnd.android.package-archive");
            install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(install);
        }

    }

    private void listener() {
        // 注册广播监听系统的下载完成事件。
        // IntentFilter intentFilter = ;
        broadcastReceivers = new DownLoadReceiver() {
            @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
            @Override
            public void onReceive(Context context, Intent intent) {
                DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                // 这里是通过下面这个方法获取下载的id，
                SharedPreferences sPreferences = context.getSharedPreferences("downloadplato", 0);
                long refernece = sPreferences.getLong("plato", 0);
                long ID = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                // 这里把传递的id和广播中获取的id进行对比是不是我们下载apk的那个id，如果是的话，就开始获取这个下载的路径
                if (ID == refernece) {
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(refernece);
                    Cursor cursor = manager.query(query);
                    if (cursor.moveToFirst()) {
                        // 获取文件下载路径
                        fileName = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                        // 如果文件名不为空，说明文件已存在,则进行自动安装apk
                        int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                        switch (status) {
                            case DownloadManager.STATUS_SUCCESSFUL:
                                // File apkFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), mBuilder.getFileName());
                                if (fileName != null) {
                                    filename = fileName;
                                    CallPermission(fileName);
                                    // apkNeedInstall(fileName);
                                }
                                break;
                        }

                    }
                    cursor.close();
                }
            }
        };
        registerReceiver(broadcastReceivers, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    public long downLoadApk(Context context, String title, String url) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
        request.setDestinationInExternalFilesDir(context, Environment.DIRECTORY_DOWNLOADS, "和医医生端.apk");
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setTitle(title);
        request.setDescription("下载完成后请点击打开");
        request.setVisibleInDownloadsUi(true);
        request.allowScanningByMediaScanner();
        request.setMimeType("application/vnd.android.package-archive");
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);
        // 实例化DownloadManager 对象
        DownloadManager downloadManager = (DownloadManager) Myapplication.mcontext.getSystemService(Context.DOWNLOAD_SERVICE);
        final long refrence = downloadManager.enqueue(request);
        SharedPreferences sPreferences = getSharedPreferences("downloadplato", 0);
        sPreferences.edit().putLong("plato", refrence).commit();
        return refrence;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (broadcastReceivers != null) {
            unregisterReceiver(broadcastReceivers);
            broadcastReceivers = null;
        }
    }

    public void Service() {
        BaseUrlUtil baseUrlUtil = new BaseUrlUtil();
        baseUrlUtil.getInstance().ServiceFun(0, 1).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribeOn(Schedulers.io())
                .subscribe(new Observer<HoleResponse<ServiceBean>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Update();
                    }

                    @Override
                    public void onNext(HoleResponse<ServiceBean> code) {
                        if (code != null) {
                            if (code.getData() != null) {
                                // endtime 小于当前 过
                                // endtime 大于或等于当前 显示
                                if (!TextUtils.isEmpty(code.getData().getEndTime())) {
                                    if (code.getData().getEndTime().compareTo(TimeFormatUtils.ms2Date(System.currentTimeMillis())) >= 0) {
                                        if (dialog != null) {
                                            dialog.show();
                                            ((TextView) dialog.findViewById(R.id.title)).setText("服务暂停访问");
                                            ((TextView) dialog.findViewById(R.id.update_content)).setText(code.getData().getContent() == null ? "" : code.getData().getContent());
                                            viewById = ((TextView) dialog.findViewById(R.id.update));
                                            viewById.setText("退出");
                                            viewById.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    System.exit(0);
                                                }
                                            });
                                            ((TextView) dialog.findViewById(R.id.cancle)).setVisibility(View.GONE);
                                        }
                                    } else {
                                        Update();
                                    }
                                } else {
                                    Update();
                                }
                            } else {
                                Update();
                            }
                        }
                    }
                });
    }

    public void kaiping() {
        BaseUrlUtil baseUrlUtil = new BaseUrlUtil();
        baseUrlUtil.getInstance().kaiping(0, 1).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribeOn(Schedulers.io())
                .subscribe(new Observer<HoleResponse<ServiceBean>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(HoleResponse<ServiceBean> code) {
                        int versioncode = 0;
                        try {
                            versioncode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                        if (code != null) {
                            if (code.getData() != null) {
                                if (!TextUtils.isEmpty(code.getData().getContent())) {
                                    if (dialog != null) {
                                        dialog.show();
                                        ((TextView) dialog.findViewById(R.id.title)).setText("温馨提示");
                                        ((TextView) dialog.findViewById(R.id.update_content)).setText(code.getData().getContent() == null ? "" : code.getData().getContent());
                                        viewById = ((TextView) dialog.findViewById(R.id.update));
                                        viewById.setText("我知道了");
                                        viewById.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();
                                            }
                                        });
                                        ((TextView) dialog.findViewById(R.id.cancle)).setVisibility(View.GONE);
                                    }
                                } else {
                                }
                            }
                        }
                    }
                });
    }

    public void Update() {
        BaseUrlUtil baseUrlUtil = new BaseUrlUtil();
        baseUrlUtil.getInstance().UpdateCode(2, 3).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribeOn(Schedulers.io())
                .subscribe(new Observer<HoleResponse<UpdateModle>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(HoleResponse<UpdateModle> code) {
                        int versioncode = 0;
                        try {
                            versioncode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                        if (code != null) {
                            if (code.getData() != null) {
                                if (code.getData() != null) {
                                    if (!TextUtils.isEmpty(code.getData().getVersionCode())) {
                                        int Scode = Integer.parseInt(code.getData().getVersionCode());
                                        if (versioncode < Scode) {
                                            if (dialog != null) {
                                                dialog.show();
                                                ((TextView) dialog.findViewById(R.id.title)).setText("新版本更新");
                                                ((TextView) dialog.findViewById(R.id.update_content)).setText(code.getData().getContent() == null ? "" : code.getData().getContent());
                                                viewById = ((TextView) dialog.findViewById(R.id.update));
                                                viewById.setText("立即更新");
                                                viewById.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        //下载新版本，更新应用，获取下载地址下载安装
                                                        downLoadApk(Myapplication.mcontext, "和平里医院", code.getData().getUrl());
                                                        dialog.dismiss();
                                                    }
                                                });
                                                if (code.getData().getEnable() == 0) {
                                                    ((TextView) dialog.findViewById(R.id.cancle)).setVisibility(View.GONE);
                                                } else {
                                                    ((TextView) dialog.findViewById(R.id.cancle)).setVisibility(View.VISIBLE);
                                                    ((TextView) dialog.findViewById(R.id.cancle)).setText("忽略该版本");
                                                    ((TextView) dialog.findViewById(R.id.cancle)).setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            dialog.dismiss();
                                                            kaiping();
                                                        }
                                                    });
                                                }
                                            }
                                        } else {
                                            // kaiping();
                                        }
                                    }
                                }
                            }
                        }

                    }
                });
    }

}



