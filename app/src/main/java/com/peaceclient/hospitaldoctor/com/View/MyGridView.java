/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.View;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name tags
 * @class name：com.denet.nei.com.View
 * @class describe
 * @anthor admin
 * @time 2021/1/26 18:08
 * @change
 * @chang time
 * @class describe
 */

public class MyGridView extends GridView {
      public MyGridView(Context context, AttributeSet attrs) {
            super(context, attrs);
      }

      public MyGridView(Context context) {
            super(context);
      }

      public MyGridView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
      }

      @Override
      public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

            int expandSpec = MeasureSpec.makeMeasureSpec(
                      Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);

            super.onMeasure(widthMeasureSpec, expandSpec);
      }


}
