package com.peaceclient.hospitaldoctor.com.cache;

import android.content.Context;

import com.peaceclient.hospitaldoctor.com.Base.IpUrl;
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde;
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse;
import com.peaceclient.hospitaldoctor.com.modle.NickModle;
import com.peaceclient.hospitaldoctor.com.modle.UserWebInfo;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * 管理web服务器上的昵称头像
 * Created by Martin on 2017/4/26.
 */

public class UserWebManager {

      //  private static NickAvterResponse nickAvterResponse;

      /**
       * 配置Web数据存储服务
       *
       * @param appId  数据存储服务的AppID
       * @param appKey 数据存储服务的AppKey
       */
      public static void config(Context context, String appId, String appKey) {

//        AVObject.registerSubclass(UserWebInfo.class);
//
//        // 后端云存储
//        AVOSCloud.initialize(context, appId, appKey);
      }

      /**
       * 获取用户信息
       *
       * @param userid
       * @param callback
       * @parm accesstoken
       */
      public static void getUserInfoAync(final String userid, final UserCallback callback) {

            final UserWebInfo info = new UserWebInfo();
            info.setOpenID(userid);
            IpUrl.ins.getInstance().getNick(ConstantViewMolde.Companion.getToken(),userid,ConstantViewMolde.Companion.getUser().getImAccount())
                      .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<HoleResponse<NickModle>>() {
                  @Override
                  public void onCompleted() {
                  }
                  @Override
                  public void onError(Throwable e) {
                        info.nickName = "";
                        callback.onCompleted(info);
                  }
                  @Override
                  public void onNext(HoleResponse<NickModle> nick) {
                        final UserWebInfo info = new UserWebInfo();
                        info.setOpenID(userid);
                           if (nick.getData()!= null)  {
                                 info.nickName = nick.getData().getResName() ==null ? "":nick.getData().getResName();
                                 info.remarkName = nick.getData().getResName() ==null ? "":nick.getData().getResName();
                           }else {
                           }
                        callback.onCompleted(info);
                  }
            });
            /****
             *  请求个人备注信息，昵称等
             */
//            if (userid != "") {
//                  if (Myapplication.sp.getBoolean("islogin", false)) {
//                        AccessTokenUtils.IfShouldRequestAccesstoken(System.currentTimeMillis());
//                        String accesstoken = Myapplication.sp.getString("accesstoken", "");
//
//                        NetBaseUtil.getInstance().getImageNick(accesstoken, userid).observeOn(AndroidSchedulers.mainThread())
//                                  .subscribeOn(Schedulers.io()).subscribe(new Observer<NickImageBean>() {
//                              @Override
//                              public void onCompleted() {
//
//                              }
//
//                              @Override
//                              public void onError(Throwable e) {
//
//                              }
//
//                              @Override
//                              public void onNext(NickImageBean nickImageBean) {
//                                    final UserWebInfo info = new UserWebInfo();
//                                    if (nickImageBean.getResp_code() == 0 && null != nickImageBean.getDatas()) {
//                                          info.setOpenID(userid);
//                                          info.setNickName(nickImageBean.getDatas().getNickname() == null ? userid : nickImageBean.getDatas().getNickname());
//                                          if (!TextUtils.isEmpty(nickImageBean.getDatas().getImg())) {
//                                                if (nickImageBean.getDatas().getImg().contains("http")) {
//                                                      info.setAvatarUrl(nickImageBean.getDatas().getImg());
//                                                } else if (nickImageBean.getDatas().getImg().contains("download")) {
//                                                      info.setAvatarUrl(NetBaseUtil.Urlhead + nickImageBean.getDatas().getImg());
//                                                } else {
//                                                      info.setAvatarUrl(NetBaseUtil.Urlhead + NetBaseUtil.picture + nickImageBean.getDatas().getImg());
//                                                }
//                                          }
//                                          info.setRemarkName("");
////                                          info.setOpenID(userid);
////                                          info.setNickName(nickImageBean.getDatas().getNickname() == null ? userid : nickImageBean.getDatas().getNickname());
////                                          if (!TextUtils.isEmpty(nickImageBean.getDatas().getImg())) {
////                                                if (nickImageBean.getDatas().getImg().contains("http")) {
////                                                      info.setAvatarUrl(nickImageBean.getDatas().getImg());
////                                                } else if (nickImageBean.getDatas().getImg().contains("download")) {
////                                                      info.setAvatarUrl(NetBaseUtil.Urlhead + nickImageBean.getDatas().getImg());
////                                                } else {
////                                                      info.setAvatarUrl(NetBaseUtil.Urlhead + NetBaseUtil.picture + nickImageBean.getDatas().getImg());
////                                                }
////                                          }
//
//                                          NetBaseUtil.getInstance().getRemark( userid, EMClient.getInstance().getCurrentUser()).observeOn(AndroidSchedulers.mainThread())
//                                                    .subscribeOn(Schedulers.io()).subscribe(new Observer<FriendRemark>() {
//                                                @Override
//                                                public void onCompleted() {
//
//                                                }
//
//                                                @Override
//                                                public void onError(Throwable e) {
//                                                      callback.onCompleted(info);
//                                                }
//
//                                                @Override
//                                                public void onNext(FriendRemark friendRemark) {
//                                                      if (friendRemark.getResp_code() == 0 && !TextUtils.isEmpty(friendRemark.getDatas())) {
//                                                            info.setRemarkName(friendRemark.getDatas());
//                                                            callback.onCompleted(info);
//                                                      }else {
//                                                            info.setRemarkName("");
//                                                            callback.onCompleted(info);
//                                                      }
//                                                }
//                                          });
//
//                                    }else {
//                                          info.setNickName(userid);
//                                          info.setAvatarUrl("");
//                                          info.setRemarkName("");
//                                          callback.onCompleted(info);
//                                    }
//                              }
//                        });
//
////                        OkHttpUtils.get(NetUrl.Url + NetUrl.GETNICK).params("easemobPhone", userid).execute(new StringCallback() {
////                              @Override
////                              public void onSuccess(String s, Call call, Response response) {
////                                    if (response.code() == 200) {
////                                          final UserWebInfo info = new UserWebInfo();
////                                          nickAvterResponse = JsonUtil.parseJsonToBean(s, NickAvterResponse.class);
////                                          if (null != nickAvterResponse.getContent()) {
////                                                info.setOpenID(userid);
////                                                info.setNickName(nickAvterResponse.getContent().getNickname() == null ? userid : nickAvterResponse.getContent().getNickname());
////                                                if (!TextUtils.isEmpty(nickAvterResponse.getContent().getImg())) {
////                                                      if (nickAvterResponse.getContent().getImg().contains("http")) {
////                                                            info.setAvatarUrl(nickAvterResponse.getContent().getImg());
////                                                      } else if (nickAvterResponse.getContent().getImg().contains("download")) {
////                                                            info.setAvatarUrl(NetUrl.Urlhead + nickAvterResponse.getContent().getImg());
////                                                      } else {
////                                                            info.setAvatarUrl(NetUrl.Urlhead + NetUrl.PICTURE + nickAvterResponse.getContent().getImg());
////                                                      }
////                                                }
////                                                info.setOpenID(userid);
////                                                info.setNickName(nickAvterResponse.getContent().getNickname() == null ? userid : nickAvterResponse.getContent().getNickname());
////                                                if (!TextUtils.isEmpty(nickAvterResponse.getContent().getImg())) {
////                                                      if (nickAvterResponse.getContent().getImg().contains("http")) {
////                                                            info.setAvatarUrl(nickAvterResponse.getContent().getImg());
////                                                      } else if (nickAvterResponse.getContent().getImg().contains("download")) {
////                                                            info.setAvatarUrl(NetUrl.Urlhead + nickAvterResponse.getContent().getImg());
////                                                      } else {
////                                                            info.setAvatarUrl(NetUrl.Urlhead + NetUrl.PICTURE + nickAvterResponse.getContent().getImg());
////                                                      }
////                                                }
////                                                OkHttpUtils.post(NetUrl.Url + NetUrl.GETBEIZHU).upJson(JsonUtil.tojson(bean)).execute(new StringCallback() {
////                                                      @Override
////                                                      public void onSuccess(String s, Call call, Response response) {
////                                                            if (response.code() == 200) {
////                                                                  BeizhuBeanResponse beizhuBean = JsonUtil.parseJsonToBean(s, BeizhuBeanResponse.class);
////                                                                  if (beizhuBean.getCode().equals("0")){
////                                                                        if (null != beizhuBean.getContent()) {
////                                                                              if (!TextUtils.isEmpty(beizhuBean.getContent().getRemarkName())) {
////                                                                                    info.setRemarkName(beizhuBean.getContent().getRemarkName());
////                                                                              }
////                                                                        }
////                                                                        callback.onCompleted(info);
////                                                                  }
////                                                            }else {
////                                                                  callback.onCompleted(info);
////                                                            }
////
////                                                      }
////
////                                                });
////
////                                          }
////
////
////                                    }else {
////
////                                    }
////
////
////                              }
////
////                        });
//
//                  }
//            }
      }


      /**
       * 创建用户
       *
       * @param userId    环信ID
       * @param nickName  昵称
       * @param avatarUrl 头像Url
       */
      public static void createUser(final String userId, final String nickName, final String avatarUrl, final String remarkName) {
            UserWebInfo user = new UserWebInfo();
            user.setOpenID(userId);
            user.setNickName(nickName);
            user.setAvatarUrl(avatarUrl);
            user.setRemarkName(remarkName);

//        OkHttpUtils.post(GolableApplication.getUrl(GolableApplication.mContext.getString(R.string.CREATE))).upJson(JsonUtil.tojson(user)).execute(new StringCallback() {
//            @Override
//            public void onSuccess(String s, Call call, Response response) {
//                if (s.contains("添加环信数据成功") || s.contains("更新环信数据成功")) {
//                    LogUtil.e("环信数据", "exccuse");
//                }
//            }
//        });
      }


      /**
       * 保存用户信息（如果已存在，则更新）
       *
       * @param
       */
      public static void saveInfo(String userId) {
            UserWebManager.getUserInfoAync(userId, new UserCallback() {
                  @Override
                  public void onCompleted(UserWebInfo info) {
                        if (info == null) {
                              info = new UserWebInfo();
                        }
//                info.setNickName(nickName);
////                info.setAvatarUrl(avatarUrl);
//                info.saveEventually();
                  }

//         @Override
//            public void onCompleted(UserWebInfo info) {
//                if (info == null) {
//                    info = new UserWebInfo();
//                }
//
//                info.setNickName(nickName);
//                info.setAvatarUrl(avatarUrl);
//
//                // 如果用户目前尚未接入网络，saveEventually会缓存设备中的数据，并在网络连接恢复后上传
//             info.saveEventually();
//            }
            });
//        OkHttpUtils.get(Myapplication.getUrl(GolableApplication.mContext.getString(R.string.QUERY))).params("openID",Long.parseLong(userId)).execute(new StringCallback() {
//            @Override
//            public void onSuccess(String s, Call call, Response response) {
//                Huanxin bean = JsonUtil.parseJsonToBean(s, Huanxin.class);
//                if (bean.getContent() != null) {
//
//                }
//
//            }
//        });
//
//
//    }
      }

      public interface UserCallback {
            /**
             * 数据获取完成后
             *
             * @param info
             */
            void onCompleted(UserWebInfo info);
      }

}


