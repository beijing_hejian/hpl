package com.peaceclient.hospitaldoctor.com.modle

import java.io.Serializable

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.modle
 * @class describe
 * @anthor admin
 * @time 2021/9/8 16:51
 * @change
 * @chang time
 * @class describe
 */

class DoctModle  :Serializable{
    var id :String?= ""
    var title :String = ""
    var alias : String =""
    var docName :String =""
    var depId:String =""
    var depCode:String =""
    var depName:String =""
    var costGraphic:String =""
    var openGraphic:Boolean =false
    var visitorsGraphic:String =""
    var costRemote:String =""
    var goodAt:String ="无"
    var introduction:String ="无"
    var headImgUrl:String =""
    var isScore :Boolean = false
}