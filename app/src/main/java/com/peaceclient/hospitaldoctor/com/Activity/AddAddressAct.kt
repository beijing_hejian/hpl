package com.peaceclient.hospitaldoctor.com.Activity

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.View
import com.bigkoo.pickerview.builder.OptionsPickerBuilder
import com.bigkoo.pickerview.listener.OnOptionsSelectListener
import com.google.gson.Gson


import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.FileUtils
import com.peaceclient.hospitaldoctor.com.modle.ProvinceBean
import org.json.JSONArray


/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/9/3 17:38
 * @change
 * @chang time
 * @class describe 添加地址页面
 */

class AddAddressAct : HoleBaseActivity() {
    var options1Items: ArrayList<ProvinceBean> = arrayListOf()
    var options2Items: ArrayList<ArrayList<String?>> = arrayListOf()
    var options3Items: ArrayList<ArrayList<ArrayList<String?>>> = arrayListOf()
    private val MSG_LOAD_DATA = 0x0001
    private val MSG_LOAD_SUCCESS = 0x0002
    private val MSG_LOAD_FAILED = 0x0003
    private var thread: Thread? = null
    private var isLoaded = false
    private var mHandler: Handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message?) {
            when (msg?.what) {
                MSG_LOAD_DATA -> {
                    if (thread == null) {
                        thread = Thread(Runnable {
                            initJsonData();
                        })
                    }
                    thread?.start();
                }
                MSG_LOAD_SUCCESS -> {
                    isLoaded = true;
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.addaddress_act)
        mHandler.sendEmptyMessage(MSG_LOAD_DATA);
    }
    fun showopti() {
        var pvOptions = OptionsPickerBuilder(this@AddAddressAct, object : OnOptionsSelectListener {
            override fun onOptionsSelect(options1: Int, options2: Int, options3: Int, v: View?) {
                var opt1 = options1Items?.get(options1)?.pickerViewText ?: ""
                var opt2 = options2Items?.get(options1).get(options2) ?: ""
                var opt3 = options3Items?.get(options1)?.get(options2)?.get(options3) ?: ""
            }
        })
                .setTitleText("城市选择")
                .setDividerColor(Color.BLACK)
                .setTextColorCenter(resources.getColor(R.color.home_green))
                .setContentTextSize(20)
                .build<Any?>()
//        pvOptions.setPicker(options1Items, options2Items, options3Items);//三级选择器
//        pvOptions.show();
    }

    fun initJsonData() {
        val JsonData = FileUtils.getJson(this, "province.json")//获取assets目录下的json文件数据
        val provinceBean = parseData(JsonData)//用Gson 转成实体
        options1Items = provinceBean;
        for (i in 0 until provinceBean.size) {//遍历省份
            val cityList = arrayListOf<String?>()//该省的城市列表（第二级）
            val province_AreaList = arrayListOf<ArrayList<String?>>()//该省的所有地区列表（第三极）

            for (c in 0 until provinceBean.get(i).city?.size!!) {//遍历该省份的所有城市
                val cityName = provinceBean.get(i).city?.get(c)?.name
                cityList.add(cityName)//添加城市
                val city_AreaList: ArrayList<String?> = arrayListOf()//该城市的所有地区列表

                //如果无地区数据，建议添加空字符串，防止数据为null 导致三个选项长度不匹配造成崩溃
                /*if (jsonBean.get(i).getCityList().get(c).getArea() == null
                        || jsonBean.get(i).getCityList().get(c).getArea().size() == 0) {
                    city_AreaList.add("");
                } else {
                    city_AreaList.addAll(jsonBean.get(i).getCityList().get(c).getArea());
                }*/
                city_AreaList.addAll(provinceBean.get(i).city?.get(c)?.area!!)

                province_AreaList.add(city_AreaList)//添加该省所有地区数据
            }

            /**
             * 添加城市数据
             */
            options2Items.add(cityList)

            /**
             * 添加地区数据
             */
            options3Items.add(province_AreaList)
        }
    }

    fun parseData(result: String): ArrayList<ProvinceBean> {//Gson 解析
        val detail = arrayListOf<ProvinceBean>()
        try {
            val data = JSONArray(result)
            val gson = Gson()
            for (i in 0 until data.length()) {
                val entity = gson.fromJson<ProvinceBean>(data.optJSONObject(i).toString(), ProvinceBean::class.java)
                detail.add(entity)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            mHandler.sendEmptyMessage(MSG_LOAD_FAILED)
        }

        return detail
    }
}