package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager

import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.MainActivity
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.StatusBarUtil
import kotlinx.android.synthetic.main.guide_act.*


/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/7/9 14:27
 * @change
 * @chang time
 * @class describe  引导页面
 */

class GuiderAct : HoleBaseActivity() {
    var list: MutableList<View> = ArrayList<View>()
    private var isPress: Boolean = false//手指是否触摸屏幕
    private var isOpen: Boolean = false//是否打开下一个activity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        StatusBarUtil.setTranslucentStatus(this)
        if (StatusBarUtil.NavigationBarUtil.hasNavigationBar(this)) {
            StatusBarUtil.NavigationBarUtil.initActivity(findViewById(android.R.id.content))
            StatusBarUtil.NavigationBarUtil.hasNavigationBar(this)
        }
        setContentView(R.layout.guide_act)
        val adapter = MyViewpager(list);
        viewpager.adapter = adapter
        val view = View.inflate(this, R.layout.guide_one, null)
        val view2 = View.inflate(this, R.layout.guide_two, null)
        val view3 = View.inflate(this, R.layout.guide_three, null)
        list.add(view)
        list.add(view2)
        list.add(view3)
        adapter.notifyDataSetChanged()
        viewpager.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {
                if (p0 == ViewPager.SCROLL_STATE_DRAGGING) {
                    isPress = true;
                } else {//必须写else，不然的话，倒数第二页就开始自动跳转了
                    isPress = false;
                }
            }
            override fun onPageSelected(p0: Int) {
            }
            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
                if (list!!.size - 1 == p0 && isPress && p2 == 0) {
                    startActivity(Intent(this@GuiderAct, MainActivity::class.java))
                    finish()
                }
            }
        })
    }
    class MyViewpager(list: List<View>) : PagerAdapter() {
        var list: List<View>? = null
        init {
            this.list = list
        }
        override fun isViewFromObject(p0: View, p1: Any): Boolean {
            return p0 == p1
        }
        override fun getCount(): Int {
            return list!!.size
        }
        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            container.addView(list?.get(position))
            return list!!.get(position)
        }
        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View)
        }
    }
}