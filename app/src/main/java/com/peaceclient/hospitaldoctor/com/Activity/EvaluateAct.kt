/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Activity

import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.widget.CheckBox
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseViewHolder
import com.jakewharton.rxbinding4.view.clicks
import com.peaceclient.com.modle.StarModle
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.InterFace.MyGridDecorations
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.GhModle
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.vondear.rxtools.view.RxToast
import com.vondear.rxtools.view.dialog.RxDialog
import kotlinx.android.synthetic.main.evaluate_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit
/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/8/31 10:16
 * @change
 * @chang time
 * @class describe 展现评价页面
 */

class EvaluateAct : HoleBaseActivity() {
    var list: ArrayList<StarModle>? = arrayListOf();
    var id: String? = null
    var nd :String?= null
    var adapter: DoctorAdapter? = null
    var scoret: Int? = 1
    var scorets: String? = "0分"
    var modle: GhModle? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.evaluate_act)
       modle = intent.getSerializableExtra("modle") as GhModle
      nd =   intent.getStringExtra("nd");
       // println(modle.toString())
        doctor_name.setText(modle?.docName ?: "")
        titlet.setText(modle?.title ?: "")
        ksmc.setText(modle?.depName ?: "")
        Glide.with(this@EvaluateAct).load(modle?.headImgUrl).placeholder(R.drawable.user_header).into(user_headers)
        for (i in 0..4) {
            var start: StarModle = StarModle()
            start.Chose = false
            start.scor = 1
            if (i == 0) {
                start.Chose = true
            }
            list?.add(start)
        }
        arrow_back.clicks().throttleFirst(2000, TimeUnit.MICROSECONDS).subscribeOn(io.reactivex.rxjava3.android.schedulers.AndroidSchedulers.mainThread())
                .subscribe {
                    finish()
                }
        var mana: GridLayoutManager = GridLayoutManager(this, 5)
        Reycle.layoutManager = mana
        adapter = DoctorAdapter(R.layout.evalute_item, list)
        var item: MyGridDecorations = MyGridDecorations()
        if (Reycle.itemDecorationCount == 0) {
            Reycle.addItemDecoration(item)
        }
        Reycle.adapter = adapter
        adapter?.notifyDataSetChanged()
        adapter?.setOnItemClickListener { adapter, view, position ->
            for (item in 0..list?.size!! - 1) {
                // 选中状态 ，保留最后一个
                if (list?.get(item)?.Chose!!) {
                    if (item == position && position == 0) {
                        list?.get(item)?.Chose = false
                        list?.get(0)?.Chose = true

                    } else {
                        for (i in position..list?.size!! - 1) {
                            if (i == 0) {
                                list?.get(0)?.Chose = true
                            } else {
                                list?.get(i)?.Chose = false
                            }
                            adapter.notifyDataSetChanged()
                        }
                    }
                } else {
                    //非选中，保留第一个
                    if (position == 0) {
                        break
                    } else {
                        if (item <= position) {
                            list?.get(item)?.Chose = true
                            adapter.notifyDataSetChanged()
                        }
                    }
                }
                adapter.notifyDataSetChanged()
            }
            for (s in 0..list?.size!! - 1) {
                if (list!!.get(s).Chose!!) {
                    scoret = (s + 1)
                }
            }
            when (scoret) {
                0 -> scorets = "0分"
                1 -> scorets = "一分"
                2 -> scorets = "二分"
                3 -> scorets = "三分"
               4 -> scorets = "四分"
                5 -> scorets = "五分"
            }
            score.setText(scorets!!)
        }
        content.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                zishu.setText(s?.length.toString() + "/100")
            }
        })
        cancle.setOnClickListener {
            finish()
        }
        tijiao.setOnClickListener {
            if (TextUtils.isEmpty(content.text)) {
                RxToast.normal("请输入评价")
                return@setOnClickListener
            } else {
                // 提交

               postvalue()
            }

        }

    }

    inner class DoctorAdapter(layoutResId: Int, data: ArrayList<StarModle>?) : com.chad.library.adapter.base.BaseQuickAdapter<StarModle, BaseViewHolder>(layoutResId, data) {

        override fun convert(helper: BaseViewHolder, item: StarModle) {
            var checkBox: CheckBox = helper.getView<CheckBox>(R.id.checkbox)
            if (item?.Chose!!) {
                checkBox.isChecked = true
            } else {
                checkBox.isChecked = false
            }

        }
    }

    fun postvalue() {
        var rxdia: RxDialog = RxDialog(this@EvaluateAct)
        rxdia.setContentView(R.layout.dialog_loading)
        rxdia.show()
        IpUrl.getInstance()!!.YgValue(ConstantViewMolde.getToken(), modle?.id + "",nd?:"", content?.text.toString()
                ?: "", scoret.toString())
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<String>> {
                    override fun onError(e: Throwable?) {
                        rxdia?.dismiss()
                    }
                    override fun onNext(t: HoleResponse<String>?) {
                        when(t?.code ){
                            0->{
                               finish()
                            }
                            1->{
                                RxToast.normal(t?.msg?:"")
                            }
                        }


                    }

                    override fun onCompleted() {
                        rxdia?.dismiss()
                    }
                })
    }

    override fun onResume() {
        super.onResume()

    }
}