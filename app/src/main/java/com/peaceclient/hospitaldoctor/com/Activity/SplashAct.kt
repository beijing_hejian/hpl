package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.View
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.MainActivity
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.StatusBarUtil
import com.peaceclient.hospitaldoctor.com.View.AutoImageView

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/7/8 17:44
 * @change
 * @chang time
 * @class describe
 */

class SplashAct : HoleBaseActivity() {
    val SHOW_TIME: Long = 1000
    var ima: AutoImageView? = null;
    var versionCode: Int = 0
    val mHadler: Handler = object : Handler() {
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            when (msg?.what) {
                0 -> {
                    postDelayed(goToGuideActivity, SHOW_TIME);
                }
            }
        }
    }
    internal var goToGuideActivity: Runnable = Runnable {
        if (!Myapplication.sp.getBoolean("isLogin",false)) {
            startActivity(Intent(this,
                    LoginDact::class.java))
            finish()
        } else {
            if (Myapplication.sp.getInt("isFirsts", 1) != versionCode) {
                this.startActivity(Intent(this,
                        GuiderAct::class.java))
                finish()
                Myapplication.editor.putInt("isFirsts", versionCode).commit()
            } else {
                startActivity(Intent(this,
                        MainActivity::class.java))
                finish()
         }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_act)
        ima = findViewById(R.id.image)
        try {
            var info = packageManager.getPackageInfo(packageName, 0)
            versionCode = info.versionCode
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        StatusBarUtil.setTranslucentStatus(this)
        if (StatusBarUtil.NavigationBarUtil.hasNavigationBar(this)) {
            StatusBarUtil.NavigationBarUtil.initActivity(findViewById<View>(android.R.id.content))
            StatusBarUtil.NavigationBarUtil.hasNavigationBar(this)
        }
        mHadler.sendEmptyMessage(0)
    }

}