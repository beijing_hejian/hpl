/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Activity


import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.CallModle
import kotlinx.android.synthetic.main.doctorselect_act.*
import kotlinx.android.synthetic.main.news_detail_act.arrow_back
import java.util.*

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/8/2 10:29
 * @change
 * @chang time
 * @class describe 首页候诊叫号页面
 */

class WaitingCallAct  : HoleBaseActivity(){
    private var calladapter : CallAdapter? = null
    private  var list:ArrayList<CallModle> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.wating_act)
        arrow_back.setOnClickListener {
            finish()
        }
        var linerma: LinearLayoutManager = LinearLayoutManager(this)
        recycle.layoutManager = linerma
        calladapter = CallAdapter(R.layout.call_item, list)
        recycle.adapter = calladapter
        calladapter?.notifyDataSetChanged()
        calladapter?.setOnItemClickListener { adapter, view, position ->

        }
        initData()
    }
    internal inner class CallAdapter(layoutResId: Int, data: ArrayList<CallModle>?) : com.chad.library.adapter.base.BaseQuickAdapter<CallModle, BaseViewHolder>(layoutResId, data) {

        override fun convert(helper: BaseViewHolder, item: CallModle) {
            helper.setText(R.id.doctor_name, item?.depName ?: "")
                    .setText(R.id.count, item?.count ?: "")
                    .setText(R.id.text, item?.person ?: "")
                    .setText(R.id.time, item?.time ?: "")
                    .addOnClickListener(R.id.sure)

        }
    }
    /**
     * 请求数据源，填充页面数据
     */
    private fun initData() {
        for (i in 0..6) {
            var depart: CallModle = CallModle()
            depart.count = "100"
            depart.depName="眼科"
            depart.person = "张怡"
            depart.time ="2021-08-10 14:23:23"
            list.add(depart)
            calladapter?.notifyDataSetChanged()
        }

    }

}