/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import com.hyphenate.chat.EMClient
import com.peaceclient.hospitaldoctor.com.Hy.DemoHelper
import com.peaceclient.hospitaldoctor.com.R

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Fragment
 * @class describe
 * @anthor admin
 * @time 2021/7/14 14:55
 * @change
 * @chang time
 * @class describe 问诊（废弃）
 */

class ConsultationFragment : BaseLazyFragment() {
    private var rview: View? = null
    private var noSuccessLayout :RelativeLayout? = null
    private var SuccessLayout :RelativeLayout? = null
    override fun lazyLoad() {
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rview = inflater.inflate(R.layout.consulta_fragment, null)
        noSuccessLayout = rview?.findViewById(R.id.layout) as RelativeLayout
        SuccessLayout = rview?.findViewById(R.id.buttons) as RelativeLayout
        var button: Button = rview!!.findViewById(R.id.button) as Button
        button.setOnClickListener {

        }
        return rview
    }

    override fun onVisible() {
        super.onVisible()
        if (DemoHelper.getInstance().isLoggedIn) {
            // 环信登录成功
            SuccessLayout?.visibility = View.VISIBLE
            noSuccessLayout?.visibility = View.GONE
            println(DemoHelper.getInstance().currentUsernName)
        } else {
            // 环信登录失败
            SuccessLayout?.visibility = View.GONE
            noSuccessLayout?.visibility = View.VISIBLE
            println(EMClient.getInstance().isLoggedInBefore .toString()+"")
        }
    }

    override fun onInvisible() {
        super.onInvisible()

    }
}