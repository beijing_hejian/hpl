package com.peaceclient.hospitaldoctor.com.Fragment

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Activity.VideoReceiAct
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.DateUtils
import com.peaceclient.hospitaldoctor.com.Utils.KeyboardUtils
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.TimeBean
import com.peaceclient.hospitaldoctor.com.modle.TwModle
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.vondear.rxtools.view.dialog.RxDialog
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import java.util.*

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.Fragment
 * @class describe
 * @anthor admin
 * @time 2021/9/15 14:34
 * @change
 * @chang time
 * @class describe   待接诊页面
 */

class OtherDayFragment : BaseLazyFragment() {
    private var rootView: View? = null
    private var kong: View? = null
    private var adapter: CheckHisAdapter? = null
    private var messageList: ArrayList<TwModle> = arrayListOf()
    private var visit_recycle: RecyclerView? = null
    private var recycle: RecyclerView? = null
    private var visitsmart: SmartRefreshLayout? = null
    var beanTemp: TimeBean = TimeBean();
    private var list: java.util.ArrayList<TimeBean>? = arrayListOf()
    override fun lazyLoad() {
    }

    private var count: Int = 0;
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = layoutInflater.inflate(R.layout.otherday_fragment, null)
        kong = View.inflate(Myapplication.mcontext, R.layout.empty_view, null)
        visit_recycle = rootView?.findViewById<RecyclerView>(R.id.visit_recycle)
        recycle = rootView?.findViewById<RecyclerView>(R.id.recycle)
        visitsmart = rootView?.findViewById<SmartRefreshLayout>(R.id.visitsmart)
        val manager = LinearLayoutManager(Myapplication.mcontext, LinearLayoutManager.VERTICAL, false)
        visit_recycle?.setLayoutManager(manager)
        adapter = CheckHisAdapter(R.layout.otherday_item_layout, messageList)
        adapter?.bindToRecyclerView(visit_recycle)
        adapter?.setEmptyView(kong)
        adapter?.isUseEmpty(true)
        visit_recycle?.setAdapter(adapter)
        adapter?.notifyDataSetChanged()
      //  visitsmart?.autoRefresh()
        visitsmart?.isEnableLoadmore = false
        visitsmart?.setOnRefreshListener {
            messageList.clear()
            getList()
        }
        adapter?.setOnItemChildClickListener { adapter, view, position ->
            when (view) {
                adapter.getViewByPosition(visit_recycle, position, R.id.look) -> {
                    var innt: Intent = Intent(Myapplication.mcontext, VideoReceiAct::class.java)
                    innt.putExtra("id", messageList.get(position).id)
                    startActivity(innt)
                    count = count + 1
                    if (count == 6) {
                        count = 0
                    }
                }
                adapter.getViewByPosition(visit_recycle, position, R.id.stop) -> {
                    var dialog: RxDialog = RxDialog(activity)
                    dialog.setContentView(R.layout.stop_layout)
                    dialog.show()
                    dialog.findViewById<TextView>(R.id.cancle).setOnClickListener {
                        dialog?.dismiss()
                    }
                    dialog.findViewById<TextView>(R.id.sure).setOnClickListener {
                        KeyboardUtils.hideKeyboard(dialog.findViewById<TextView>(R.id.sure))
                        dialog?.dismiss()
                        messageList?.clear()
                        visitsmart?.autoRefresh()
                    }
                    dialog.findViewById<EditText>(R.id.content).addTextChangedListener(object : TextWatcher {
                        override fun afterTextChanged(s: Editable?) {
                            dialog.findViewById<TextView>(R.id.zishu).setText((s?.length.toString()
                                    ?: 0.toString()) + "/50")
                        }

                        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                        }

                        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        }
                    })
                }
            }

        }
        val c = Calendar.getInstance()
        val date = DateUtils.getAFTERYearDay(c.getTime(), 1, 7)
        for (i in 0..6) {
            val week = DateUtils.getWeekYear(date.get(i))
            var bean: TimeBean = TimeBean();
            bean.setWeek(week)
            bean.setNumber(date.get(i))
            if (i == 0) {
                bean.setSelectMu(true)
                beanTemp = bean
                // year_month.setText(bean.getNumber()!!.subSequence(0, 8))
            } else {
                bean.setSelectMu(false)
            }
            list?.add(bean)
        }
        val managers = GridLayoutManager(Myapplication.mcontext, 7)
        recycle?.setLayoutManager(managers)
        var adapter: Dateadapter = Dateadapter(R.layout.doctor_date_item, list)
        recycle?.adapter = adapter
        adapter.notifyDataSetChanged()
        adapter.setOnItemClickListener { adapter, view, position ->
            beanTemp.setSelectMu(false)
            list?.get(position)?.setSelectMu(true)
            beanTemp = list?.get(position)!!
            adapter.notifyDataSetChanged()
            messageList.clear()
            getList();
        }
        return rootView
    }

    internal inner class CheckHisAdapter(layoutResId: Int, data: List<TwModle>?) : BaseQuickAdapter<TwModle, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: TwModle) {
            helper.setText(R.id.name, item?.resName ?: "")
                    .setText(R.id.visit_time, item?.createTime ?: "")
                    .addOnClickListener(R.id.look)
                    .addOnClickListener(R.id.stop)
            var box: ImageView = helper.getView<ImageView>(R.id.pm_am)
            var status: TextView = helper.getView<TextView>(R.id.status)
            if (item.makeAppointmentPm == 0) {
                box.setImageResource(R.drawable.moringblue)
                status.setText("待就诊")
            } else {
                box.setImageResource(R.drawable.afternoon_gray)
                status.setText("待就诊")
            }
        }
    }

    internal inner class Dateadapter(layoutResId: Int, data: java.util.ArrayList<TimeBean>?) : com.chad.library.adapter.base.BaseQuickAdapter<TimeBean, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: TimeBean) {
            helper.setText(R.id.week, item.getWeek())
                    .setText(R.id.time, item.getNumber()?.subSequence(item.getNumber()?.length!!.minus(2), item.getNumber()?.length!!)).addOnClickListener(R.id.sure)
            if (item.getSelectMu()!!) {
                helper.getView<Button>(R.id.time).isEnabled = true
            } else {
                helper.getView<Button>(R.id.time).isEnabled = false
            }
        }
    }


    fun getList() {
        IpUrl.getInstance()!!.getVdlist(ConstantViewMolde.getToken(), beanTemp?.getNumber() ?: "")
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<TwModle>>> {
                    override fun onError(e: Throwable?) {
                        visitsmart?.finishRefresh()
                    }

                    override fun onNext(t: HoleResponse<ArrayList<TwModle>>?) {
                        if (t?.code == 0) {
                            if (t?.data != null) {
                                messageList.addAll(t?.data!!)
                                adapter?.notifyDataSetChanged()
                            }
                        }
                    }

                    override fun onCompleted() {
                        visitsmart?.finishRefresh()
                    }
                })
    }

    override fun onVisible() {
        super.onVisible()
        visitsmart?.autoRefresh()
    }
}