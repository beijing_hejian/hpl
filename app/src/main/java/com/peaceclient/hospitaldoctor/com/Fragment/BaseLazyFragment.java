/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Fragment;

import android.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;


import com.peaceclient.hospitaldoctor.com.View.CustomDialog;


/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name doctor
 * @class name：com.homedoctorz.com.ui.fragment
 * @class describe
 * @anthor admin
 * @time 2020/8/14 11:17
 * @change
 * @chang time
 * @class describe fragemnt 基类，定义懒加载和主动加载方法
 */

public abstract class BaseLazyFragment extends Fragment {
      // private   ILaunchManagerService launchManagerService;

      /**
       * 判断当前的Fragment是否可见(相对于其他的Fragment)
       */
      protected boolean mIsVisible;
      protected Activity mActivity;
      /**
       * 标志位，标志已经初始化完成
       */
      protected boolean mIsprepared;
      /**
       * 是否已被加载过一次，第二次就不再去请求数据了
       */
      protected boolean mHasLoadedOnce;
      public CustomDialog dialog;

      @Override
      public void setUserVisibleHint(boolean isVisibleToUser) {
            //设置Fragment的可见状态
            super.setUserVisibleHint(isVisibleToUser);
            if (getUserVisibleHint()) {//getUserVisibleHint获取Fragment可见状态
                  mIsVisible = true;
                  onVisible();
            } else {
                  mIsVisible = false;
                  onInvisible();
            }

            if (isResumed()) {
                  onVisibilityChangedToUser(isVisibleToUser);
            }
      }


      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
            mActivity = getActivity();
      }

      /**
       * 可见
       */
      protected void onVisible() {
            lazyLoad();
      }

      @Nullable
      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            return super.onCreateView(inflater, container, savedInstanceState);
            // launchManagerService = Proxy.newProxyInstance(getClass().getClassLoader(), arrayOf<Class<*>>(ILaunchManagerService::class.java), LaunchInvocationHandler(this, Myapplication.mcontext)) as ILaunchManagerService
      }

      /**
       * 不可见
       */
      protected void onInvisible() {
            stopLoad();
      }

      /**
       * 延迟加载
       * 子类必须重写此方法
       */
      protected abstract void lazyLoad();

      /**
       * 当视图已经对用户不可见并且加载过数据，如果需要在切换到其他页面时停止加载数据，可以覆写此方法
       */
      protected void stopLoad() {

      }

      //region 统计Fragement 可见时间
      @Override
      public void onResume() {
            super.onResume();
            if (getUserVisibleHint()) {
                  onVisibilityChangedToUser(true);
            }
      }

      @Override
      public void onPause() {
            super.onPause();
            if (getUserVisibleHint()) {
                  onVisibilityChangedToUser(false);
            }
      }

      /**
       * 当Fragment对用户的可见性发生了改变的时候就会回调此方法
       *
       * @param isVisibleToUser true：用户能看见当前Fragment；false：用户看不见当前Fragment
       */
      public void onVisibilityChangedToUser(boolean isVisibleToUser) {
            if (isVisibleToUser) {
                  visibleToUser();
            } else {
                  inVisibleToUser();
            }
      }

      protected void visibleToUser() {

      }

      protected void inVisibleToUser() {

      }

      protected void ToLogin(Intent intent) {
            startActivity(intent);
      }


}
