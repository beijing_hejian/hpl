/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.modle

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.modle
 * @class describe
 * @anthor admin
 * @time 2021/8/27 14:42
 * @change
 * @chang time
 * @class describe
 */

class MessageModle {
    var content: ArrayList<MessageDetail>? = null
    //总数
    var recordsTotal: Int? = null

    inner class MessageDetail {
        var id: String? = null
        // 消息类型： 1 通知消息 2.处方消息
        var messageType: String? = null
        // 操作系统 1.全部 2、安卓 3.ios
        var os: String? = null
        var client: String? = null
        var title: String? = null
        // 通知范围 1.全部 2、个人
        var noticeType: String? = null
        //点击跳转: 0.不可 1.可以跳转
        var operationParameters: String? = null
        //跳转路径或参数
        var noticeContent: String? = null
        //资讯内容(限定500字)
        var uid: String? = null

        var createTime: String? = null

    }
}