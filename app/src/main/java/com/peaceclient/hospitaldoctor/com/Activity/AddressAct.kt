/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.os.Bundle

import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.BaseViewHolder


import com.peaceclient.hospitaldoctor.com.Activity.AddAddressActivity
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.PeopleModle
import kotlinx.android.synthetic.main.address_act.*
import kotlinx.android.synthetic.main.elecard_act.elec_recycle
import kotlinx.android.synthetic.main.news_detail_act.arrow_back
import java.util.*

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/8/2 10:29
 * @change
 * @chang time
 * @class describe 首页电子卡就诊页面
 */

class AddressAct : HoleBaseActivity() {
    private var list: ArrayList<PeopleModle> = arrayListOf()
    private var adapter: PeopleAdapter? = null
    private var tempAddress: PeopleModle? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.address_act)
        arrow_back.setOnClickListener {
            finish()
        }
        add.setOnClickListener {
            startActivity(Intent(this@AddressAct, AddAddressActivity::class.java))
        }

        val managers = LinearLayoutManager(this)
        elec_recycle.setLayoutManager(managers)
        adapter = PeopleAdapter(R.layout.address_item, list)
        elec_recycle.adapter = adapter
        adapter?.notifyDataSetChanged()
        initDate()
        adapter?.setOnItemClickListener { adapter, view, position ->
            tempAddress?.isSelect = false
            list.get(position).isSelect = true
            tempAddress = list.get(position)
            adapter.notifyDataSetChanged()


        }
    }

    private fun initDate() {
        for (z in 0..7) {
            var peopleModle: PeopleModle = PeopleModle()
            peopleModle.id = z
            peopleModle.idcard = "130431199110100010"
            peopleModle.isSelect = false
            peopleModle.name = "z" + z
            peopleModle.phone = "17611481698"
            if (z == 0) {
                peopleModle.isSelect = true
                tempAddress = peopleModle
            }
            list.add(peopleModle)

            adapter?.notifyDataSetChanged()
        }
    }

    inner class PeopleAdapter(layoutResId: Int, data: ArrayList<PeopleModle>?) : com.chad.library.adapter.base.BaseQuickAdapter<PeopleModle, BaseViewHolder>(layoutResId, data) {

        override fun convert(helper: BaseViewHolder, item: PeopleModle) {
            var checkBox: CheckBox = helper.getView<CheckBox>(R.id.icon)
            helper.getView<TextView>(R.id.address).setText(item?.idcard ?: "")
            helper.getView<TextView>(R.id.phone).setText(item?.phone ?: "")
            helper.getView<TextView>(R.id.name).setText(item?.name ?: "")
            var view = helper.getView<TextView>(R.id.moren)
            if (item?.isSelect!!) {
                checkBox.isChecked = true
                view.setTextColor(resources.getColor(R.color.home_green))
                view.setText("已设为默认")
            } else {
                checkBox.isChecked = false
                view.setTextColor(resources.getColor(R.color.nnn))
                view.setText("设为默认")
            }

        }
    }


}