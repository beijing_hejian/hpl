package com.peaceclient.hospitaldoctor.com.modle

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.modle
 * @class describe
 * @anthor admin
 * @time 2021/9/24 17:06
 * @change
 * @chang time
 * @class describe
 */

class ClientModle {
    var resSid: String? = null
    var docId: String? = null
    var orderStatus: String? = null
    var docTitle: String? = null
    var depCode: String? = null
    var docName: String? = null
    var resName: String? = null
    var docStartOrderTime: String? = null
    var docCloseOrderTime: String? = null
    var id: String? = null
    var resPhone: String? = null
    var resIdCard: String? = null
    var resImAccount: String? = null
    var payStatus: String? = null
    var diagnosis: String? = null
    var prescription: String? = null
    var makeAppointmentNo: String? = null
    var makeAppointmentPm: String? = null
    var makeAppointmentDate: String? = null
    var depName: String? = null
    var depId: String? = null
    var createTime: String? = null
    var endTime: String? = null

    var evaluationTime: String? = null

    var updateTime: String? = null
    var category: String? = null
    var urls: ArrayList<urlMo> = arrayListOf()

    inner class urlMo {
        var fid: String? = null
        var url: String? = null
    }
}