/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.View;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

/**
 * Created by 20906 on 2017/6/3.
 */
public class MyFitimage extends AppCompatImageView {
      private float ratio;

      public void setRatio(float ratio) {
            this.ratio = ratio;
      }

      public MyFitimage(Context context) {
            super(context);
      }

      public MyFitimage(Context context, AttributeSet attrs) {
            super(context, attrs);
      }

      public MyFitimage(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
      }

      @Override
      protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

            // 父容器传过来的宽度方向上的模式
            int widthMode = MeasureSpec.getMode(widthMeasureSpec);
            // 父容器传过来的高度方向上的模式
            int heightMode = MeasureSpec.getMode(heightMeasureSpec);

            // 父容器传过来的宽度的值
            int width = MeasureSpec.getSize(widthMeasureSpec) - getPaddingLeft()
                      - getPaddingRight();
            // 父容器传过来的高度的值
            int height = MeasureSpec.getSize(heightMeasureSpec) - getPaddingLeft()
                      - getPaddingRight();

            if (widthMode == MeasureSpec.EXACTLY
                      && heightMode != MeasureSpec.EXACTLY && ratio != 0.0f) {
                  // 判断条件为，宽度模式为Exactly，也就是填充父窗体或者是指定宽度；
                  // 且高度模式不是Exaclty，代表设置的既不是fill_parent也不是具体的值，于是需要具体测量
                  // 且图片的宽高比已经赋值完毕，不再是0.0f
                  // 表示宽度确定，要测量高度
                  height = (int) (width / ratio + 0.5f);
                  heightMeasureSpec = MeasureSpec.makeMeasureSpec(height,
                            MeasureSpec.EXACTLY);
            } else if (widthMode != MeasureSpec.EXACTLY
                      && heightMode == MeasureSpec.EXACTLY && ratio != 0.0f) {
                  // 判断条件跟上面的相反，宽度方向和高度方向的条件互换
                  // 表示高度确定，要测量宽度
                  width = (int) (height * ratio + 0.5f);

                  widthMeasureSpec = MeasureSpec.makeMeasureSpec(width,
                            MeasureSpec.EXACTLY);
            }

            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
      }

}
