/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.modle

import java.io.Serializable

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.modle
 * @class describe
 * @anthor admin
 * @time 2021/7/19 15:11
 * @change
 * @chang time
 * @class describe
 */

class DoctorModle   :Serializable{
    constructor()
    var  name:String? = null
    var  pingjia:Int? = 4
    var skill:String? = null
    var department:String? = null
    var introduction:String? = null
    var boolean:Boolean? = false

    override fun toString(): String {
        return "DoctorModle(name=$name, skill=$skill, department=$department, introduction=$introduction)"
    }

}