package com.peaceclient.hospitaldoctor.com.InterFace;

import android.content.Intent;

/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name project
 * @class name：com.homeclientz.com.CallBack
 * @class describe
 * @anthor admin
 * @time 2019/9/6 16:01
 * @change
 * @chang time
 * @class describe
 */

public class CallBackUtils {
      private static CancleBack cancleBack;
      private static SuccessBack successBack;

      public static void setCancleBack(CancleBack cancleBac) {
            cancleBack = cancleBac;
      }

      public static void setSuccessBack(SuccessBack cancleBac) {
            successBack = cancleBac;
      }


      public static void ChangeState(int pos) {
            cancleBack.StateChange(pos);
      }

      public static void LoginSuccess(int pos) {
            successBack.LoginSuccess(pos);
      }

      public static void LoginSuccess(Intent intent) {
            cancleBack.loginsuccess(intent);
      }

}
