package com.peaceclient.hospitaldoctor.com.View;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.View
 * @class describe
 * @anthor admin
 * @time 2021/7/8 17:26
 * @change
 * @chang time
 * @class describe
 */


      @SuppressLint("AppCompatCustomView")
      public class AutoImageView extends ImageView
      {
            float arg = .0f;

            public AutoImageView(Context context) {
                  super(context);

            }

            public AutoImageView(Context context, AttributeSet attrs)
            {
                  super(context, attrs);
            }

            @Override
            protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
            {
                  int widthSize = MeasureSpec.getSize(widthMeasureSpec);
                  float ft = arg == 0f ? 0.75f : arg;
                  setMeasuredDimension(widthSize, (int) (widthSize * ft));

            }

            @Override
            protected void onFinishInflate()
            {
                  super.onFinishInflate();
                  CharSequence charSequence = getContentDescription();
                  if (!TextUtils.isEmpty(charSequence))
                  {
                        try
                        {
                              arg = Float.parseFloat(charSequence.toString());
                        } catch (Exception e)
                        {
                        }
                  }
            }

            @Override
            public void setImageBitmap(Bitmap bm)
            {
                  if (arg == 0f && bm != null)
                  {
                        arg = 1f * bm.getHeight() / bm.getWidth();
                  }
                  super.setImageBitmap(bm);
            }


}
