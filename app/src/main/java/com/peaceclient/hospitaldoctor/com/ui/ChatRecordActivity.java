package com.peaceclient.hospitaldoctor.com.ui;

import android.content.Intent;
import android.os.Bundle;

import com.hyphenate.easeui.ui.EaseChatFragment;
import com.peaceclient.hospitaldoctor.com.R;


/**
 * chat activity，EaseChatFragment was used
 */
public class ChatRecordActivity extends BaseActivity {
    public static ChatRecordActivity activityInstance;
    private EaseChatFragment chatFragment;
    String toChatUsername;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.em_activity_chat);
        activityInstance = this;
        //get user id or group id
        boolean isvisible = getIntent().getBooleanExtra("isvisible", true);
        toChatUsername = getIntent().getExtras().getString("userId");
        //use EaseChatFratFragment
        chatFragment = new ChatFragments();
        //pass parameters to chat fragment
        Bundle extras = getIntent().getExtras();
        extras.putBoolean("isvisible", isvisible);
        chatFragment.setArguments(extras);
        getSupportFragmentManager().beginTransaction().add(R.id.container, chatFragment).commit();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        activityInstance = null;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // make sure only one chat activity is opened
        String username = intent.getStringExtra("userId");
        if (toChatUsername.equals(username))
            super.onNewIntent(intent);
        else {
            finish();
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        chatFragment.onBackPressed();
        finish();
    }

    public String getToChatUsername() {
        return toChatUsername;
    }


}
