/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.modle

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.modle
 * @class describe
 * @anthor admin
 * @time 2021/7/28 15:45
 * @change
 * @chang time
 * @class describe
 */

class HoleResponse <T>{

companion object{
    val CODE_SUCCESS = 0//成功
    val CODE_FAIL = 1//失败
    val CODE_UNAUTHORIZED =401//未认证（签名错误）
    val CODE_NOUSER = 1001//用户不存在（签名错误）
    val CODE_NOPARAM = 1002//缺失参数（签名错误）
    val CODE_USEREXIST = 1004//手机号已被注册（签名错误）
    val CODE_PHONEEXIST = 1003//用户名已被注册（签名错误）
    val CODE_INTERNAL_SERVER_ERROR = 500//服务器内部错误
}


    var  data :T? = null
    var msg :String? = null
    var timestamp :String? = null
    var  code : Int?  = 0
    override fun toString(): String {
        return "HoleResponse(data=$data, msg=$msg, timestamp=$timestamp, code=$code)"
    }


}