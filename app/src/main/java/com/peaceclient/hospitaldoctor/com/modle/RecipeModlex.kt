package com.peaceclient.hospitaldoctor.com.modle

import java.io.Serializable

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor222
 * @class name：com.peaceclient.hospitaldoctor.com.modle
 * @class describe
 * @anthor admin
 * @time 2022/4/7 10:56
 * @change
 * @chang time
 * @class describe
 * 处方实体
 */

class RecipeModlex : Serializable {
    companion object {
        private const val serialVersionUID = 20180617104500L
    }

    var id: String = "" // 处方表主键
    var appointid: String = ""  // 预约识别码
    var diags: Others? = null
    var others: ArrayList<Others>? = arrayListOf<Others>()
    var bills: Bills? = null
    var isCommon: Int = 0

    //   var onlineBillDiags: OnlineBilldiags? = null
    var billexpress: BillExpress? = null

    class Bills : Serializable {
        companion object {
            private const val serialVersionUID = 20180617104501L
        }

        var billtime: String = "" // 开放时间
        var appointid: String = "" // 开放时间
        var uname: String = "" // 居民姓名
        var checkresult: String = ""//医生审核结果
        var docid: String = ""
        var fee: String = "" // 总金额
        var count: String = "" //草药付数
        var regname: String = ""// 科室名称
        var cfts: String = ""  // 处方贴数
        var getway: String = "" //取药方式
        var ischeck: String = "" // 审核状态
        var payedtime: String = "" // 支付时间
        var checkdocname: String = "" // 审核医生
        var billtype: String = "" // 单据类型
        var checkdocid: String = ""
        var refundtime: String = "" //退款时间
        var paystatus: String = ""  // 支付状态 0：待支付 1：已支付 2：已退款
        var docname: String = ""  // 开方医生
        var billDetails: ArrayList<BillDetail>? = arrayListOf()
    }

    class BillDetail  :Serializable{
        var freqname: String = "" //频次名称
        var ypcd: String = ""  //  厂家编号
        var unitcount: String = "" // 数量
        var totalprice: String = "" //
        var howtouse: String = "" //用药频次编号
        var spec: String = ""// 包装规格
        var unit: String = "" // 包装单位
        var gytjname: String = ""//  用法名称
        var yfgg: String = "" //药房规格
        var price: String = "" // 单价
        var gytj: String = "" //(给药途径)
        var name: String = "" // 药品名称
        var yfdw: String = "" //药房单位
        var percount: String = ""  //每次用量
        var yfbz: String = "" // 药房包装
        var ypxh: String = "" //项目编码
        var type: String = "" //药品类型 1：西药 2： 中成药 3：草药
        var medcode: String = "" //医保编号
        var advunit: String = "" //医嘱单位
        var unittoadvunit: String = "" //换算关系
        var code :String =""
        override fun toString(): String {
            return "BillDetail(freqname='$freqname', ypcd='$ypcd', unitcount='$unitcount', totalprice='$totalprice', howtouse='$howtouse', spec='$spec', unit='$unit', gytjname='$gytjname', yfgg='$yfgg', price='$price', gytj='$gytj', name='$name', yfdw='$yfdw', percount='$percount', yfbz='$yfbz', ypxh='$ypxh', type='$type', medcode='$medcode', advunit='$advunit', unittoadvunit='$unittoadvunit', code='$code')"
        }

    }
    /***
     * 主要症状
     * @property prdiagtype String
     * @property code String
     * @property name String
     * @property id String
     */
    class Diags : Serializable {
        var prdiagtype: String = "" //主诊断类型
        var code: String = ""
        var name: String = ""
        var id: String = ""

    }

//    class OnlineBilldiags : Serializable {
//        companion object {
//            private const val serialVersionUID = 20180617104502L
//        }
//
//        var prdiagtype: String = ""
//        var diagname: String = ""
//        var otherdiags: String = ""
//    }

    class BillExpress : Serializable {
        companion object {
            private const val serialVersionUID = 20180617104503L
        }

        var expressid: String = ""
        var phone: String = "" // 收件人电话
        var senduser: String = "" // 发件人姓名
        var takeuser: String = ""// 收件人姓名
        var address: String = ""// 收件地址
        var express: String = "" // 快递名称
        var expressnum: String = ""// 快递单号

    }

    class Others  :Serializable {
        var prdiagtype: String = "" //主诊断类型
        var code: String = ""
        var name: String = ""
        var num:String =""
        var xmmc :String =""
        var pydm :String =""
        var ypyf :String =""
        var mszd: String = "" // 诊断名称
        var id: String = ""
        var zxlb: String = "" //诊断类型 1：西医 2：中医
        var other: Other? = null //其他症状
        override fun toString(): String {
            return "Others(prdiagtype='$prdiagtype', code='$code', name='$name', num='$num', xmmc='$xmmc', pydm='$pydm', ypyf='$ypyf', mszd='$mszd', id='$id', zxlb='$zxlb', other=$other)"
        }


    }

    class Other: Serializable {
        var id: String = ""
        var name: String = ""
    }


}