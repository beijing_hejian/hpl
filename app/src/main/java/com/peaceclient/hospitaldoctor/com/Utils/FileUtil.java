package com.peaceclient.hospitaldoctor.com.Utils;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Utils
 * @class describe
 * @anthor admin
 * @time 2021/9/6 15:14
 * @change
 * @chang time
 * @class describe
 */

public class FileUtil {
      public static File getSaveFile(Context context) {
            return new File(context.getFilesDir(), "pic.jpg");
      }
      public static boolean copyAsset(Context ctx, String assetName, String destinationPath) throws IOException {
            InputStream in = ctx.getAssets().open(assetName);
            File f = new File(destinationPath);
            f.createNewFile();
            OutputStream out = new FileOutputStream(new File(destinationPath));
            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                  out.write(buffer, 0, read);
            }
            in.close();
            out.close();
            return true;
      }
      public static String extractFileNameFromURL(String url) {
            if (url.contains("agentCardId=")) {
                  int i = url.lastIndexOf("/");
                  int y = url.indexOf("gpEnd=");
                  return url.substring(i+1,y+1);
            }

            return url.substring(url.lastIndexOf('/') + 1);
      }
}
