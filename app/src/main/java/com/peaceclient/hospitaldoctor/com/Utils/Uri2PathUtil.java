/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Utils;

import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

import androidx.loader.content.CursorLoader;


/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Utils
 * @class describe
 * @anthor admin
 * @time 2021/8/24 17:40
 * @change
 * @chang time
 * @class describe
 */

public class Uri2PathUtil {

      //复杂版处理  (适配多种API)
      public static String getRealPathFromUri(Context context, Uri uri) {
            int sdkVersion = Build.VERSION.SDK_INT;
            if (sdkVersion < 11) return getRealPathFromUri_BelowApi11(context, uri);
            if (sdkVersion < 19) return getRealPathFromUri_Api11To18(context, uri);
            else return getRealPathFromUri_AboveApi19(context, uri);
      }

      /**
       * 适配api19以上,根据uri获取图片的绝对路径
       */
      @TargetApi(Build.VERSION_CODES.KITKAT)
      private static String getRealPathFromUri_AboveApi19(Context context, Uri uri) {
            final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                  if (isExternalStorageDocument(uri)) {
                        final String docId = DocumentsContract.getDocumentId(uri);
                        final String[] split = docId.split(":");
                        final String type = split[0];
                        if ("primary".equalsIgnoreCase(type)) {
                              return Environment.getExternalStorageDirectory() + "/" + split[1];
                        }
                  }  else if (isDownloadsDocument(uri)) {
                        final String id = DocumentsContract.getDocumentId(uri);
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O){//判断有没有超过android 8，区分开来，不然崩溃崩溃崩溃崩溃
                              final Uri contentUri = ContentUris.withAppendedId(
                                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                              return getDataColumn(context, contentUri, null, null);
                        }else{
                              final String[] split = id.split(":");
                              if (split.length>=2){
                                    return split[1];
                              }
                        }
                  } else if (isMediaDocument(uri)) {
                        final String docId = DocumentsContract.getDocumentId(uri);
                        final String[] split = docId.split(":");
                        final String type = split[0];

                        Uri contentUri;
                        if ("image".equals(type)) {
                              contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        } else if ("video".equals(type)) {
                              contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                        } else if ("audio".equals(type)) {
                              contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                        } else {
                              contentUri = MediaStore.Files.getContentUri("external");
                        }

                        final String selection = "_id=?";
                        final String[] selectionArgs = new String[]{split[1]};
                        return getDataColumn(context, contentUri, selection, selectionArgs);
                  }


            } else if ("content".equalsIgnoreCase(uri.getScheme())) {
                  if (isHuaWeiUri(uri)) {
                        String uriPath = uri.getPath();
                        //content://com.huawei.hidisk.fileprovider/root/storage/emulated/0/Android/data/com.xxx.xxx/
                        if (uriPath != null && uriPath.startsWith("/root")) {
                              return uriPath.replaceAll("/root", "");
                        }
                  }
                  return getDataColumn(context, uri, null, null);
            } else if ("file".equalsIgnoreCase(uri.getScheme())) {
                  return uri.getPath();
            }

            return null;
      }

      /**
       * 适配api11-api18,根据uri获取图片的绝对路径
       */
      private static String getRealPathFromUri_Api11To18(Context context, Uri uri) {
            String filePath = null;
            String[] projection = {MediaStore.Images.Media.DATA};
            //这个有两个包不知道是哪个。。。。不过这个复杂版一般用不到
            CursorLoader loader = new CursorLoader(context, uri, projection, null, null, null);
            Cursor cursor = loader.loadInBackground();

            if (cursor != null) {
                  cursor.moveToFirst();
                  filePath = cursor.getString(cursor.getColumnIndex(projection[0]));
                  cursor.close();
            }
            return filePath;
      }

      /**
       * 适配api11以下(不包括api11),根据uri获取图片的绝对路径
       */
      private static String getRealPathFromUri_BelowApi11(Context context, Uri uri) {
            String filePath = null;
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                  filePath = cursor.getString(cursor.getColumnIndex(projection[0]));
                  cursor.close();
            }
            return filePath;
      }

      /**
       * Get the value of the data column for this Uri. This is useful for
       * MediaStore Uris, and other file-based ContentProviders.
       *
       * @param context       The context.
       * @param uri           The Uri to query.
       * @param selection     (Optional) Filter used in the query.
       * @param selectionArgs (Optional) Selection arguments used in the query.
       * @return The value of the _data column, which is typically a file path.
       */
      public static String getDataColumn(Context context, Uri uri, String selection,
                                         String[] selectionArgs) {
            Cursor cursor = null;
            String column = MediaStore.MediaColumns.DATA;
            String[] projection = {column};
            try {
                  cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                            null);
                  if (cursor != null && cursor.moveToFirst()) {
                        int column_index = cursor.getColumnIndexOrThrow(column);
                        return cursor.getString(column_index);
                  }
            } catch (Exception e) {
                  e.printStackTrace();
            } finally {
                  if (cursor != null)
                        cursor.close();
            }
            return null;
      }

      /**
       * @param uri The Uri to check.
       * @return Whether the Uri authority is ExternalStorageProvider.
       */
      public static boolean isExternalStorageDocument(Uri uri) {
            return "com.android.externalstorage.documents".equals(uri.getAuthority());
      }

      /**
       * @param uri The Uri to check.
       * @return Whether the Uri authority is DownloadsProvider.
       */
      public static boolean isDownloadsDocument(Uri uri) {
            return "com.android.providers.downloads.documents".equals(uri.getAuthority());
      }

      /**
       * @param uri The Uri to check.
       * @return Whether the Uri authority is MediaProvider.
       */
      public static boolean isMediaDocument(Uri uri) {
            return "com.android.providers.media.documents".equals(uri.getAuthority());
      }

      public static boolean isHuaWeiUri(Uri uri) {
            return "com.huawei.hidisk.fileprovider".equals(uri.getAuthority());
      }
}
