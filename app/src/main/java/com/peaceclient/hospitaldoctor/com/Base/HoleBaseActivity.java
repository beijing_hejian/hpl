package com.peaceclient.hospitaldoctor.com.Base;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.peaceclient.hospitaldoctor.com.InterFace.ILaunchManagerService;
import com.peaceclient.hospitaldoctor.com.Utils.StatusBarUtil;


/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name tags
 * @class name：com.denet.nei.com.Base
 * @class describe
 * @anthor admin
 * @time 2021/1/7 10:47
 * @change
 * @chang time
 * @class describe
 */

public class HoleBaseActivity extends AppCompatActivity {

      public  Intent intents;
      public ILaunchManagerService launchManagerService;
      public HoleBaseActivity(Intent intents, ILaunchManagerService launchManagerService) {
            this.intents = intents;
            this.launchManagerService = launchManagerService;
      }
      public HoleBaseActivity() {
      }
      @Override
      protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            StatusBarUtil.setRootViewFitsSystemWindows(this, false);
            StatusBarUtil.setTranslucentStatus(this);
            if (!StatusBarUtil.setStatusBarDarkTheme(this, true)) {
                  //这样半透明+白=灰, 状态栏的文字能看得清
                  StatusBarUtil.setStatusBarColor(this, 0x000000);
            }
            if (StatusBarUtil.NavigationBarUtil.hasNavigationBar(this)) {
                  StatusBarUtil.NavigationBarUtil.initActivity(findViewById(android.R.id.content));
            }
        // launchManagerService = (ILaunchManagerService) Proxy.newProxyInstance(this.getClassLoader(), new Class[]{ILaunchManagerService.class}, new LaunchInvocationHandler(this, this));
      }
      protected void hideBottomUIMenu() {
            //隐藏虚拟按键，并且全屏
            if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) { // lower api
                  View v = this.getWindow().getDecorView();
                  v.setSystemUiVisibility(View.GONE);
            } else if (Build.VERSION.SDK_INT >= 19) {
                  //for new api versions.
                  View decorView = getWindow().getDecorView();
                  int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
                  decorView.setSystemUiVisibility(uiOptions);
            }
      }

      @Override
      public void onConfigurationChanged(Configuration newConfig) {
            //非默认值
            if (newConfig.fontScale != 1) {
                  getResources();
            }
            super.onConfigurationChanged(newConfig);
      }

      @Override
      public Resources getResources() {//还原字体大小
            Resources res = super.getResources();
            //非默认值
            if (res.getConfiguration().fontScale != 1) {
                  Configuration newConfig = new Configuration();
                  newConfig.setToDefaults();//设置默认
                  res.updateConfiguration(newConfig, res.getDisplayMetrics());
            }
            return res;
      }

      @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
      public static boolean isHaveSoftKey(AppCompatActivity activity) {
            Display d = activity.getWindowManager().getDefaultDisplay();
            DisplayMetrics realDisplayMetrics = new DisplayMetrics();
            d.getRealMetrics(realDisplayMetrics);
            int realHeight = realDisplayMetrics.heightPixels;
            int realWidth = realDisplayMetrics.widthPixels;
            DisplayMetrics displayMetrics = new DisplayMetrics();
            d.getMetrics(displayMetrics);
            int displayHeight = displayMetrics.heightPixels;
            int displayWidth = displayMetrics.widthPixels;
            return (realWidth - displayWidth) > 0 || (realHeight - displayHeight) > 0;
      }

      @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
      public static int getBottomSoftKeysHeight(AppCompatActivity activity) {
            Display d = activity.getWindowManager().getDefaultDisplay();
            DisplayMetrics realDisplayMetrics = new DisplayMetrics();
            d.getRealMetrics(realDisplayMetrics);
            int realHeight = realDisplayMetrics.heightPixels;
            DisplayMetrics displayMetrics = new DisplayMetrics();
            d.getMetrics(displayMetrics);
            int displayHeight = displayMetrics.heightPixels;
            return (realHeight - displayHeight);
      }

      @Override
      protected void onDestroy() {
            Log.i(this.getClass().getName(), "销毁执行");
            super.onDestroy();
      }

//      @Override
//      public void startActivity(Intent intent) {
//            Intent intent1 = new Intent(this, intent.getClass());
//            Bundle bundle = new Bundle();
//            bundle.putString(Myapplication.KEY_MSG, "通过登录传递的产品");
//            intent1.putExtra(Myapplication.SHARED_NAME, bundle);
//            launchManagerService.startActivity(intent1);
//      }
}
