/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.wxapi;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.peaceclient.hospitaldoctor.com.Base.Constants;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

public class WXPayEntryActivity extends AppCompatActivity implements IWXAPIEventHandler {
      private static final String TAG = "MicroMsg.SDKSample.WXPayEntryActivity";
      private IWXAPI api;

      @Override
      public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            api = WXAPIFactory.createWXAPI(this, Constants.Companion.getAPP_ID());
            api.handleIntent(getIntent(), this);
      }

      @Override
      protected void onNewIntent(Intent intent) {
            super.onNewIntent(intent);
            setIntent(intent);
            api.handleIntent(intent, this);
      }

      @Override
      public void onReq(BaseReq req) {

      }

      @Override
      public void onResp(BaseResp resp) {
            Log.d("pay", "onPayFinish, errCode = " + resp.errCode);
            if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
                  switch (resp.errCode) {
                        case 0:
                             /* System.out.println("oop");
                              Intent intent = new Intent(WXPayEntryActivity.this, PaySuccessActivity.class);
                              intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                              startActivity(intent);
                              runOnUiThread(new Runnable() {
                                    //微信支付回调
                                    @Override
                                    public void run() {
//                                          Wxpay(0 + "");
                                    }
                              });
                              finish();*/
                              break;
                        case -1:
                           /*   Intent intent1 = new Intent(WXPayEntryActivity.this, SignCreateActivity.class);
                              intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                              startActivity(intent1);
                              finish();*/
                              break;
                        case -2:
                             /* Intent intent2 = new Intent(WXPayEntryActivity.this, SignCreateActivity.class);
                              intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                              startActivity(intent2);
                              finish();*/
                              break;

                  }


            }
      }

//      void Wxpay(String status) {
//            String s = AccessTokenUtils.IfShouldRequestAccesstoken(System.currentTimeMillis());
//            String accesstoken = Myapplication.sp.getString("accesstoken", "");
//            NetBaseUtil.getInstance().SendSatus(accesstoken, status).
//                      subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Object>() {
//                  @Override
//                  public void onCompleted() {
//                        //  finish();
////                        dialog2.dismiss();
//                        System.out.println("oooooo");
//                  }
//
//                  @Override
//                  public void onError(Throwable e) {
////                        dialog2.dismiss();
//                        System.out.println(e.toString());
//                        //Toast.makeText(SignPayActivity.this, "网络繁忙，请稍后重试", Toast.LENGTH_SHORT).show();
//                  }
//
//                  @Override
//                  public void onNext(Object o) {
//
//                  }
//
//            });
//      }
}