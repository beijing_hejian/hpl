/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseViewHolder
import com.chad.library.adapter.base.util.MultiTypeDelegate
import com.jakewharton.rxbinding4.view.clicks
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.View.AutoImageView
import com.peaceclient.hospitaldoctor.com.View.TypeImageView
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.KnowleModle
import kotlinx.android.synthetic.main.newslist_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/7/20 11:30
 * @change
 * @chang time
 * @class describe 新闻列表页面
 */

class NewsListAct : HoleBaseActivity() {
    var list: ArrayList<KnowleModle> = arrayListOf()
    var type: String? = null
    private var newsadapter: NewsAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.newslist_act)
        var newsManager: LinearLayoutManager = LinearLayoutManager(this)
        newsRecycle.layoutManager = newsManager
        newsadapter = NewsAdapter(list)
        newsRecycle.adapter = newsadapter!!
        newsadapter?.emptyView
        newsadapter?.notifyDataSetChanged()
        type = intent.getStringExtra("type")
        InitData(newsadapter!!)

        newsadapter?.setOnItemClickListener { adapter, view, position ->
            when (list.get(position).contentType) {
                1 -> {
                    var intent: Intent = Intent(this@NewsListAct, NewsDetailAct::class.java)
                    intent.putExtra("id", list.get(position).id.toString())
                    startActivity(intent)
                }
                2-> {
                    var intent: Intent = Intent(this@NewsListAct, VideoPlayActivity::class.java)
                    intent.putExtra("id", list.get(position).id.toString())
                    startActivity(intent)
                }
            }

        }
        arrow_back.clicks().throttleFirst(2000, TimeUnit.MICROSECONDS).subscribeOn(io.reactivex.rxjava3.android.schedulers.AndroidSchedulers.mainThread())
                .subscribe {
                    finish()
                }
    }

    private fun InitData(newsadapter: NewsAdapter) {
        when (type) {
            "3" -> {
                titley.setText("健康资讯")
                getZIXUN();
            }
            // 名义风采
            "4" -> {
                titley.setText("名医风采")
                getDoctor();
            }
            //就诊指南
            "6" -> {
                titley.setText("就诊指南")
                getJiuzhen();
            }
            "5" -> {

                getIntro();
            }
        }


    }

    /***
     * 名义风采列表请求
     */
    fun getDoctor() {
        IpUrl.getInstance()!!.getDoctorFeng()
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<MutableList<KnowleModle>>> {
                    override fun onError(e: Throwable?) {
                        println(e.toString() + "err")
                    }

                    override fun onNext(t: HoleResponse<MutableList<KnowleModle>>?) {

                        if (t?.code == 0) {

                            if (t?.data != null) {
                                list?.addAll(t.data!!)
                                newsadapter?.notifyDataSetChanged()
                            }
                        }
                    }

                    override fun onCompleted() {
                    }

                })
    }

    /**
     * 获取医院介绍
     */
    fun getIntro() {
        IpUrl.getInstance()!!.getHospital()
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<KnowleModle>> {
                    override fun onError(e: Throwable?) {
                        println(e.toString() + "err")
                    }

                    override fun onNext(t: HoleResponse<KnowleModle>?) {
                        if (t?.code == 0) {
                            if (t?.data != null) {
                                list?.add(t.data!!)
                                newsadapter?.notifyDataSetChanged()
                            }
                        }
                    }

                    override fun onCompleted() {
                    }
                })
    }

    /***
     *  就诊指南请求
     */
    fun getJiuzhen() {
        IpUrl.getInstance()!!.getClinic()
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<MutableList<KnowleModle>>> {
                    override fun onError(e: Throwable?) {
                        println(e.toString() + "err")
                    }

                    override fun onNext(t: HoleResponse<MutableList<KnowleModle>>?) {

                        if (t?.code == 0) {

                            if (t?.data != null) {
                                list?.addAll(t.data!!)
                                newsadapter?.notifyDataSetChanged()
//                                var message: Message = Message.obtain()
//                                message.what = 1
//                                message.obj = listZIXUN
//                                mHadler.sendMessage(message)
//

                            }
                        }
                    }

                    override fun onCompleted() {
                    }

                })
    }

    /***
     * 健康资讯列表
     */
    fun getZIXUN() {

        IpUrl.getInstance()!!.getHealth()
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<MutableList<KnowleModle>>> {
                    override fun onError(e: Throwable?) {
                        println(e.toString() + "err")
                    }

                    override fun onNext(t: HoleResponse<MutableList<KnowleModle>>?) {

                        if (t?.code == 0) {

                            if (t?.data != null) {
                                list?.addAll(t.data!!)
                                newsadapter?.notifyDataSetChanged()
//                                var message: Message = Message.obtain()
//                                message.what = 1
//                                message.obj = listZIXUN
//                                mHadler.sendMessage(message)
//

                            }
                        }
                    }

                    override fun onCompleted() {
                    }

                })
    }

    internal inner class NewsAdapter(data: ArrayList<KnowleModle>?) : com.chad.library.adapter.base.BaseQuickAdapter<KnowleModle, BaseViewHolder>(data) {
        init {
            multiTypeDelegate = object : MultiTypeDelegate<KnowleModle>() {
                override fun getItemType(javaBean: KnowleModle): Int {
                    return javaBean.category!!
                }
            }
            // 3 健康资讯 4 ，名义风采， 6，就诊指南，5 医院介绍
            multiTypeDelegate.registerItemType(3, R.layout.newslist_items)
                    .registerItemType(4, R.layout.newslist_item)
                    .registerItemType(6, R.layout.item_jiuzhen_layout)
                    .registerItemType(5, R.layout.newslist_item)


        }

        override fun convert(helper: BaseViewHolder, item: KnowleModle) {
            when (helper.itemViewType) {
                6 -> {
                    helper.setText(R.id.text1, item?.title)
                    var image = helper.getView(R.id.message_chufang) as AutoImageView
                    Glide.with(this@NewsListAct).load(item?.imgUrl).into(image)
                    //  image.setImageDrawable(resources.getDrawable(item.url))
                }
                4 -> {
                    helper.setText(R.id.title, item?.title)
                            .setText(R.id.news_time, item?.publishTime)
                    var image = helper.getView(R.id.news_header) as TypeImageView
                    image.type = TypeImageView.TYPE_ROUND
                    image.roundRadius = 10

                    Glide.with(this@NewsListAct).load(item?.imgUrl).into(image)
                }
                5 -> {
                    helper.setText(R.id.title, item?.title)
                            .setText(R.id.news_time, item?.publishTime)
                    var image = helper.getView(R.id.news_header) as TypeImageView
                    image.type = TypeImageView.TYPE_ROUND
                    image.roundRadius = 10
                    Glide.with(this@NewsListAct).load(item?.imgUrl).into(image)
                }
                3 -> {
                    helper.setText(R.id.title, item?.title)
                            .setText(R.id.news_time, item?.publishTime)
                    var image = helper.getView(R.id.news_header) as TypeImageView
                    image.type = TypeImageView.TYPE_ROUND
                    image.roundRadius = 10
                    Glide.with(this@NewsListAct).load(item?.imgUrl).into(image)
                }
            }

        }


    }


}