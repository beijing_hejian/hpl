package com.peaceclient.hospitaldoctor.com.modle

import java.io.Serializable

class GhModle : Serializable {
    var cjsj: String? = null
    var ghid: String? = null
    var hzxm: String? = null
    var jzdd: String? = null
    var jzfs: String? = null
    var jzrq: String? = null
    var jzsj: String? = null
    var ksbm: String? = null
    var ksid: String? = null
    var ksmc: String? = null
    var ysid: String? = null
    var ysxm: String? = null
    var yszc: String? = null
    var zhid: String? = null
    var pm: String? = null
    var am: String? = null
    var jyjzsj: String? = null
    var jzlx: String? = null
    var yyhm: String? = null
    var yyzt: Boolean = false
    var headima: String? = null
    var dqjh: String? = null
    var wdpdh: String? = null
    var wzztTxt: String? = null
    var wzzt: Int = 0
    var pjid: String? = null
    var zfsj: String? = null
    var xing: Int = 0
    var pingjia: String? = null
    var pjsj: String? = null
    var sfkf: Boolean = false
    var headImgUrl: String = ""
    var nd: String = ""
    var depName: String = ""
    var docName: String = ""
    var title: String = ""
    var id: String = ""
    var sfrq: String = ""
    var lxdh: String = ""
    var imrAccount: String? = null
    var imdAccount: String? = null

    var uid: String? = ""
    var billid: String? = ""
    var patientid: String? = ""
    var uname: String? = ""
    var imraccount: String? = ""
    var doctid: String? = ""
    var doctname: String? = ""
    var regcode: String? = ""
    var imdaccount: String? = ""
    var regname: String? = ""
    var clinicdate: String? = ""
    var type: String? = ""
    var appointid: String? = ""
    var status: String? = ""
    var createtime: String? = ""
    var level: Int? = 0
    var evaluation: String? = ""
    var createTime: String? = ""



}
