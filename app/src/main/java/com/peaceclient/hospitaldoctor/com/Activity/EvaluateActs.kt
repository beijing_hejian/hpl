/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Activity

import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.widget.CheckBox
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseViewHolder
import com.jakewharton.rxbinding4.view.clicks
import com.peaceclient.com.modle.StarModle
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.InterFace.MyGridDecorations
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.GhModle
import com.vondear.rxtools.view.RxToast
import kotlinx.android.synthetic.main.evaluate_acts.*
import java.util.concurrent.TimeUnit

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/8/31 10:16
 * @change
 * @chang time
 * @class describe 查看评价页面
 */

class EvaluateActs : HoleBaseActivity() {
    var list: ArrayList<StarModle>? = arrayListOf();
    var id: String? = null
    var adapter: DoctorAdapter? = null
    var scoret: Int? = 2
    var scorets: String? = "一分"
   var modle: GhModle? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.evaluate_acts)
        modle = intent.getSerializableExtra("modle") as GhModle
        println(modle.toString())
        docname.setText(modle?.docName ?: "")
        doctitle.setText(modle?.title ?: "")
        depname.setText(modle?.depName ?: "")
        content.setText(modle?.pingjia ?: "")
        Glide.with(this@EvaluateActs).load(modle?.headImgUrl?:"").placeholder(R.drawable.user_header).into(user_header)

        for (i in 0..4) {
            var start: StarModle = StarModle()
            start.Chose = false
            start.scor = 1
            for (z in 0..modle?.xing!!-1) {
                if (i == z) {
                    start.Chose = true
                }
            }
            list?.add(start)
        }
        when(modle?.xing){
            1->{
                score.setText("一分")
            }2->{
            score.setText("二分")
            } 3->{
            score.setText("三分")
            } 4->{
            score.setText("四分")
            } 5->{
            score.setText("五分")
            }
        }
        arrow_back.clicks().throttleFirst(2000, TimeUnit.MICROSECONDS).subscribeOn(io.reactivex.rxjava3.android.schedulers.AndroidSchedulers.mainThread())
                .subscribe {
                    finish()
                }
        var mana: GridLayoutManager = GridLayoutManager(this, 5)
        Reycle.layoutManager = mana
        adapter = DoctorAdapter(R.layout.evalute_item, list)
        var item: MyGridDecorations = MyGridDecorations()
        if (Reycle.itemDecorationCount == 0) {
            Reycle.addItemDecoration(item)
        }
        Reycle.adapter = adapter
        adapter?.notifyDataSetChanged()
//        adapter?.setOnItemClickListener { adapter, view, position ->
//            for (item in 0..list?.size!! - 1) {
//                // 选中状态 ，保留最后一个
//                if (list?.get(item)?.Chose!!) {
//                    if (item == position && position == 0) {
//                        list?.get(item)?.Chose = false
//                        list?.get(0)?.Chose = true
//
//                    } else {
//                        for (i in position..list?.size!! - 1) {
//                            if (i == 0) {
//                                list?.get(0)?.Chose = true
//                            } else {
//                                list?.get(i)?.Chose = false
//                            }
//
//                            adapter.notifyDataSetChanged()
//                        }
//                    }
//                } else {
//                    //非选中，保留第一个
//                    if (position == 0) {
//                        break
//                    } else {
//                        if (item <= position) {
//                            list?.get(item)?.Chose = true
//                            adapter.notifyDataSetChanged()
//                        }
//                    }
//                }
//                adapter.notifyDataSetChanged()
//            }
//            for (s in 0..list?.size!! - 1) {
//                if (list!!.get(s).Chose!!) {
//                    scoret = (s + 1) * 2
//                }
//            }
//            when (scoret) {
//                0 -> scorets = "0分"
//                2 -> scorets = "一分"
//                4 -> scorets = "二分"
//                6 -> scorets = "三分"
//                8 -> scorets = "四分"
//                10 -> scorets = "五分"
//
//            }
//            score.setText(scorets!!)
//        }
        content.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                zishu.setText(s?.length.toString() + "/100")
            }
        })
        tijiao.setOnClickListener {
            if (TextUtils.isEmpty(content.text)) {
                RxToast.normal("请输入评价")
                return@setOnClickListener
            } else {
                // 提交
                finish()
              //  postvalue()
            }

        }

//        adapter?.setOnItemChildClickListener { adapter, view, position ->
//          if (view ==  adapter.getViewByPosition(Reycle, position, R.id.layout)) {
//
//              for (item in 0..list?.size!! - 1) {
//                  // 选中状态 ，保留最后一个
//                  if (list?.get(item)?.Chose!!) {
//                      if (position == 0) {
//                   return@setOnItemChildClickListener
//                      }else{
//                          for (i in position.. list?.size!! -1){
//                              list?.get(i)?.Chose =false
//                              adapter.notifyDataSetChanged()
//                          }
//                      }
//
//                  } else {
//                      //非选中，保留第一个
//                      if (position == 0) {
//                          return@setOnItemChildClickListener
//                      } else {
//                          if (item <= position) {
//                              list?.get(item)?.Chose = true
//                              adapter.notifyDataSetChanged()
//                          }
//
//                      }
//
//
//                  }
//
//              }
//              println(list.toString())
//          }
//        }
    }

    inner class DoctorAdapter(layoutResId: Int, data: ArrayList<StarModle>?) : com.chad.library.adapter.base.BaseQuickAdapter<StarModle, BaseViewHolder>(layoutResId, data) {

        override fun convert(helper: BaseViewHolder, item: StarModle) {
            var checkBox: CheckBox = helper.getView<CheckBox>(R.id.checkbox)
            if (item?.Chose!!) {
                checkBox.isChecked = true
            } else {
                checkBox.isChecked = false
            }

        }
    }

//    fun postvalue() {
//        var rxdia: RxDialog = RxDialog(this@EvaluateActs)
//        rxdia.setContentView(R.layout.dialog_loading)
//        rxdia.show()
//        IpUrl.getInstance()!!.postValue(ConstantViewMolde.getToken(), id + "", scoret.toString(), content?.toString()
//                ?: "")
//                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<Int>> {
//                    override fun onError(e: Throwable?) {
//                        rxdia?.dismiss()
//                    }
//
//                    override fun onNext(t: HoleResponse<Int>?) {
//                        finish()
////                        loading?.dismiss()
////                        resultBody.imageUrl = t?.data?.url
////                        var mess = Message.obtain();
////                        mess.obj = resultBody;
////                        mhandler.sendMessage(mess)
////                        Glide.with(this@CreatCardAct).load(t?.data?.url).placeholder(R.drawable.create_top).into(card)
//                    }
//
//                    override fun onCompleted() {
//                        rxdia?.dismiss()
//                    }
//                })
//    }
}