package com.peaceclient.hospitaldoctor.com.Utils;


import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//* 封装的是使用Gson解析json的方法
// * @author Administrator
// *
// */
public class JsonUtil {

    /**
     * //	 * 把一个map变成json字符串
     * //	 * @param map
     * //	 * @return
     * ////
     */
    public static String parseMapToJson(Map<?, ?> map) {
        try {
            Gson gson = new Gson();
            return gson.toJson(map);
        } catch (Exception e) {
        }
        return null;
    }

    /**
     * ////	 * 把一个json字符串变成对象
     * ////	 * @param json
     * ////	 * @param cls
     * ////	 * @return
     * ////
     */
    public static <T> T parseJsonToBean(String json, Class<T> cls) {
        Gson gson = new Gson();
        T t = null;
        try {
            t = gson.fromJson(json, cls);
        } catch (Exception e) {
        }
        return t;
    }
    /***
     * json 转换成list
     * */
    public static List<Object> getList4Json(String jsonString, Class<?> pojoClass) {
        Gson gson = new Gson();
        List<Object> list = new ArrayList<Object>();
        JsonArray jsonArray = gson.fromJson(jsonString, JsonArray.class);
        for (int i = 0; i < jsonArray.size(); i++) {
                list.add(gson.fromJson(jsonArray.get(i), pojoClass));
          }
        return list;
    }
    ////	/**
////	 * 把json字符串变成map
////	 * @param json
////	 * @return
////	 */
    public static HashMap<String, Object> parseJsonToMap(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, Object>>() {
        }.getType();
        HashMap<String, Object> map = null;
        try {
            map = gson.fromJson(json, type);
        } catch (Exception e) {
        }
        return map;
    }

    /**
     * 把json字符串变成集合
     * params: new TypeToken<List<yourbean>>(){}.getType(),
     *
     * @param json
     * @param
     * @return
     */
    public static <T>List<T> parseJson2List(String json,Class<T> modle ) {
        Gson gson = new Gson();
        List<T> lists = new ArrayList<T>();
        JsonArray jsonArray = gson.fromJson(json, JsonArray.class);
        for (int i = 0; i < jsonArray.size(); i++) {
            lists.add(gson.fromJson(jsonArray.get(i),modle));
        }
        return lists;
    }

    /**
     * 获取json串中某个字段的值，注意，只能获取同一层级的value
     *
     * @param json
     * @param key
     * @return
     */
    public static String getFieldValue(String json, String key) {
        if (TextUtils.isEmpty(json))
            return null;
        if (!json.contains(key))
            return "";
        JSONObject jsonObject = null;
        String value = null;
        try {
            jsonObject = new JSONObject(json);
            value = jsonObject.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return value;
    }

    public static String getjson(Object object) {
        Gson gson = new Gson();
        String s = gson.toJson(object.toString()).toString();
        return s;
    }

    public static String tojson(Object object) {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        String s = gson.toJson(object);
        return s;
    }
//    public static <T> List<T> getStringToList(String jsonStr, Class<T> model)
//    {
//        List<T> object = (List<T>) JSONArray.parseArray(jsonStr, model);
//        return object;
//        // String输出: "aa","bb","cc"
//        // userModel输出: lmx,jqy
//    }

    public static <T> List<T> parseJsonToList(String jsonString, Class<T> cls) {
        List<T> list = new ArrayList<T>();
        try {
            Gson gson = new Gson() ;
            list = gson.fromJson(jsonString, new TypeToken<List<T>>() {
            }.getType());

            //  list=(List<T>)JSONObject.toBean(jsonString,cls);
        } catch (Exception e) {
        }
        return list;
    }

    /* String path = getClass().getClassLoader().getResource("menu.json").toString();
      path = path.replace("\\", "/");
      if (path.contains(":")) {
          path = path.replace("file:/", "");
      }*/
//    ClassPathResource resource = new ClassPathResource("menu.json");
//    File filePath = resource.getFile();
//    JSONArray btnArray = null;
//
//    //读取文件
//    String input = FileUtils.readFileToString(filePath, "UTF-8");
//    //将读取的数据转换为JSONObject
//    JSONObject jsonObject = JSONObject.fromObject(input);
//    if (jsonObject != null) {
//        //取出按钮权限的数据
//        btnArray = jsonObject.getJSONArray("btnList");
//    }
//    Map<String, List<MenuVo>> btnMap = new HashMap<>();
//    Iterator<Object> num = btnArray.iterator();
//    //遍历JSONArray，转换格式。按按钮集合按模块（name）放入map中
//    while (num.hasNext()) {
//        JSONObject btn = (JSONObject) num.next();
//        btnMap.put((String) btn.get("name"), JSONArray.toList((JSONArray) btn.get("children"), new MenuVo(), new JsonConfig()));
//    }

}
