package com.peaceclient.hospitaldoctor.com.Activity

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import android.view.ViewGroup
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Fragment.VisitFragment
import com.peaceclient.hospitaldoctor.com.Fragment.VisitedFragment
import com.peaceclient.hospitaldoctor.com.Fragment.VisitingFragment
import com.peaceclient.hospitaldoctor.com.InterFace.CallBackUtils
import com.peaceclient.hospitaldoctor.com.InterFace.SuccessBack
import com.peaceclient.hospitaldoctor.com.R
import kotlinx.android.synthetic.main.twlist_act.*
import java.util.*

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/9/15 14:23
 * @change
 * @chang time
 * @class describe 图文列表页面（废弃）
 */

class TwListAct  :HoleBaseActivity(),SuccessBack{
    override fun LoginSuccess(s: Int) {
        health_viewpager?.setCurrentItem(s)
        println("222")
    }
    private var listTitles:ArrayList<String> = arrayListOf("待接诊","接诊中","待开方")
    private var fragmentList: ArrayList<Fragment>? = arrayListOf()
    private var adapter:MyPagerAdapter?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.twlist_act)
        initda()
        arrow_back.setOnClickListener {
            finish()
        }

        CallBackUtils.setSuccessBack(this)
    }


    private fun initda() {
        for (i in 0..listTitles.size) {
            when (i) {
                0 -> fragmentList?.add(VisitFragment())
                1 -> fragmentList?.add(VisitingFragment())
                2 -> fragmentList?.add(VisitedFragment())
            }
        }
        adapter = MyPagerAdapter(getSupportFragmentManager())
        adapter?.setFragments(fragmentList!!)
        health_viewpager?.setAdapter(adapter)
        health_tab.setupWithViewPager(health_viewpager)
        adapter?.notifyDataSetChanged()
    }

    inner class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        private var mFragmentList: ArrayList<Fragment>? = null

        fun setFragments(fragments: ArrayList<Fragment>) {
            mFragmentList = fragments
        }

        override fun getItem(position: Int): Fragment {
            return mFragmentList!![position]
        }

        override fun getCount(): Int {
            return mFragmentList?.size ?:0
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return listTitles.get(position).toString()
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {}


    }
}