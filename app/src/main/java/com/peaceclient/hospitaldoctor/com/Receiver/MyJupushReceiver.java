/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Receiver;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.core.app.NotificationCompat;

import com.peaceclient.hospitaldoctor.com.R;

import cn.jpush.android.api.JPushInterface;

/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Receiver
 * @class describe
 * @anthor admin
 * @time 2021/8/25 15:29
 * @change
 * @chang time
 * @class describe
 */

public class MyJupushReceiver extends BroadcastReceiver {
      private String TAG = "MyJupushReceiver";
      @Override
      public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
           // System.out.println("消息type =" + bundle.getString(JPushInterface.EXTRA_EXTRA));
            if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {
                  String regId = bundle.getString(JPushInterface.EXTRA_REGISTRATION_ID);
                 // Log.d(TAG, "[MyReceiver] 接收 Registration Id : " + regId);
            } else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {
                 // Log.d(TAG, "收到了自定义消息。消息内容是：" + bundle.getString(JPushInterface.EXTRA_MESSAGE));
                  // 自定义消息不会展示在通知栏，完全要开发者写代码去处理
                  processCustomMessages(context,intent.getExtras());
            } else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent.getAction())) {
                  //Log.d(TAG, "收到了通知");
                  // 在这里可以做些统计，或者做些其他工作
                  processCustomMessages(context,intent.getExtras());
            } else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
                  //Log.d(TAG, "用户点击打开了通知");
                  // 在这里可以自己写代码去定义用户点击后的行为
                //  processCustomMessage(context,intent.getExtras());
                  String string = intent.getExtras().getString(JPushInterface.EXTRA_EXTRA);
//                  if (!TextUtils.isEmpty(string)) {
//                  } else {
//                  }
            } else {
                  //Log.d(TAG, "Unhandled intent - " + intent.getAction());
            }
      }
      private void processCustomMessages(Context context, Bundle bundle) {

            NotificationCompat.Builder notification = new NotificationCompat.Builder(context);
            //这一步必须要有而且setSmallIcon也必须要，没有就会设置自定义声音不成功
            notification.setAutoCancel(true).setSmallIcon(R.mipmap.ic_launcher);
//                  builder.notificationFlags = Notification.FLAG_AUTO_CANCEL
//                            | Notification.FLAG_SHOW_LIGHTS;  //设置为自动消失和呼吸灯闪烁
//                  builder.notificationDefaults = Notification.DEFAULT_SOUND
//                            | Notification.DEFAULT_VIBRATE
//                            | Notification.DEFAULT_LIGHTS;
            notification.setVibrate(new long[]{10, 5});
            notification.setLights(1,0,1);
            String alert = bundle.getString(JPushInterface.EXTRA_ALERT);
            if (alert != null && !alert.equals("")) {
//                        notification.setSound(
//                                  Uri.parse("android.resource://" + context.getPackageName() + "/" +R.raw.notification));
                  notification.setSound(
                            Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.beep));
            }
            //最后刷新notification是必须的
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(1, notification.build());

      }
}
