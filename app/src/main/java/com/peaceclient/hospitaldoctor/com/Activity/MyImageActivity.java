package com.peaceclient.hospitaldoctor.com.Activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity;
import com.peaceclient.hospitaldoctor.com.R;
import com.peaceclient.hospitaldoctor.com.View.StatusBarHeightView;
import com.peaceclient.hospitaldoctor.com.widgt.ZoomImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name project
 * @class name：com.homeclientz.com.Activity
 * @class describe
 * @anthor admin
 * @time 2020/9/20 18:00
 * @change
 * @chang time
 * @class describe 已废弃
 */

public class MyImageActivity extends HoleBaseActivity implements View.OnClickListener {

      @BindView(R.id.imageview_head_big)
      ZoomImageView imageviewHeadBig;
      @BindView(R.id.arrow_back)
      ImageView arrowBack;
      @BindView(R.id.title)
      TextView title;
      @BindView(R.id.add_person)
      TextView addPerson;
      @BindView(R.id.rel)
      RelativeLayout rel;
      @BindView(R.id.top)
      StatusBarHeightView top;

      @Override
      protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.imagedialogview);
            ButterKnife.bind(this);
            String url = getIntent().getStringExtra("url");
         arrowBack.setOnClickListener(this);
            Glide.with(this).load(url).asBitmap().into(imageviewHeadBig);
            imageviewHeadBig.setOnClickListener(this);
            String[] PERMISSIONS = {
                      "android.permission.READ_EXTERNAL_STORAGE",
                      "android.permission.WRITE_EXTERNAL_STORAGE"};
            //检测是否有写的权限
            int permission = ContextCompat.checkSelfPermission(this,
                      "android.permission.WRITE_EXTERNAL_STORAGE");
            if (permission != PackageManager.PERMISSION_GRANTED) {
                  // 没有写的权限，去申请写的权限，会弹出对话框
                  ActivityCompat.requestPermissions(this, PERMISSIONS, 1);
            }

      }

      @Override
      public void onClick(View v) {
            switch (v.getId()) {
                  case R.id.imageview_head_big:
                        this.finish();
                        break;
                  case R.id.arrow_back:
                        finish();
                        break;
            }
      }


      // 保存文件的方法：
      public void SaveBitmapFromView(View view) {
            int w = view.getWidth();
            int h = view.getHeight();
            Bitmap bmp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(bmp);
            view.layout(0, 0, w, h);
            view.draw(c);
            // 缩小图片
            Matrix matrix = new Matrix();
            matrix.postScale(0.5f, 0.5f); //长和宽放大缩小的比例
            bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
            DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            saveBitmap(bmp, format.format(new Date()) + ".JPEG");
      }

      /*
       * 保存文件，文件名为当前日期
       */
      public void saveBitmap(Bitmap bitmap, String bitName) {
            String fileName;
            File file;
            if (Build.BRAND.equals("Xiaomi")) { // 小米手机
                  fileName = Environment.getExternalStorageDirectory().getPath() + "/DCIM/Camera/" + bitName;
            } else {  // Meizu 、Oppo
                  fileName = Environment.getExternalStorageDirectory().getPath() + "/DCIM/" + bitName;
            }
            file = new File(fileName);

            if (file.exists()) {
                  file.delete();
            }
            FileOutputStream out;
            try {
                  out = new FileOutputStream(file);
                  // 格式为 JPEG，照相机拍出的图片为JPEG格式的，PNG格式的不能显示在相册中
                  if (bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)) {
                        out.flush();
                        out.close();
// 插入图库
                        MediaStore.Images.Media.insertImage(this.getContentResolver(), file.getAbsolutePath(), bitName, null);

                  }
            } catch (FileNotFoundException e) {
                  e.printStackTrace();
            } catch (IOException e) {
                  e.printStackTrace();

            }
            // 发送广播，通知刷新图库的显示
            this.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + fileName)));

      }
}
