package com.peaceclient.hospitaldoctor.com.Fragment

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.Hy.Constant
import com.peaceclient.hospitaldoctor.com.InterFace.OnFinishListener
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.TimeFormatUtils
import com.peaceclient.hospitaldoctor.com.View.RushTime
import com.peaceclient.hospitaldoctor.com.modle.ClientModle
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.TwModle
import com.peaceclient.hospitaldoctor.com.ui.ChatActivity
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.vondear.rxtools.view.dialog.RxDialog
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import java.text.SimpleDateFormat

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.Fragment
 * @class describe
 * @anthor admin
 * @time 2021/9/15 14:34
 * @change
 * @chang time
 * @class describe   接诊中页面
 */

class VisitingFragment : BaseLazyFragment() {
    private var rootView: View? = null
    private var kong: View? = null
    private var adapter: CheckHisAdapter? = null
    private var messageList: ArrayList<TwModle> = arrayListOf()
    private  var visit_recycle : RecyclerView? = null
    private  var visitsmart : SmartRefreshLayout? = null
    override fun lazyLoad() {
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = layoutInflater.inflate(R.layout.visit_fragment, null)
        kong = View.inflate(Myapplication.mcontext, R.layout.empty_view, null)
        visit_recycle = rootView?.findViewById<RecyclerView>(R.id.visit_recycle)
        visitsmart = rootView?.findViewById<SmartRefreshLayout>(R.id.visitsmart)
        val manager = LinearLayoutManager(Myapplication.mcontext, LinearLayoutManager.VERTICAL, false)
        visit_recycle?.setLayoutManager(manager)
        adapter = CheckHisAdapter(R.layout.visiting_item_layout, messageList)
        adapter?.bindToRecyclerView(visit_recycle)
        adapter?.setEmptyView(kong)
        adapter?.isUseEmpty(true)
        visit_recycle?.setAdapter(adapter)
        adapter?.notifyDataSetChanged()
        visitsmart?.autoRefresh()
        visitsmart?.isEnableLoadmore =false
        visitsmart?.setOnRefreshListener {
            messageList?.clear()
          getList()
        }
        adapter?.setOnItemChildClickListener { adapter, view, position ->
            when(view){
                adapter.getViewByPosition(visit_recycle,position,R.id.cancle_visit) -> {
                    getAsk(messageList.get(position).id ?:"".toString())
                }
                adapter.getViewByPosition(visit_recycle,position,R.id.sure_visit) -> {
                    var  intent:Intent = Intent(activity,ChatActivity::class.java)
                    intent.putExtra(Constant.EXTRA_USER_ID, messageList?.get(position)?.docImAccount)
                    intent.putExtra(Constant.EXTRA_CHAT_TYPE, Constant.CHATTYPE_SINGLE)
                    intent.putExtra("userId",messageList.get(position).resImAccount)
                    startActivity(intent)
                }
            }
        }
        return rootView
    }
    internal inner class CheckHisAdapter(layoutResId: Int, data: List<TwModle>?) : BaseQuickAdapter<TwModle, BaseViewHolder>(layoutResId, data),OnFinishListener {
        override fun onFinish(pos: Int) {
            notifyItemRemoved(pos)
        }
        override fun convert(helper: BaseViewHolder, item: TwModle) {
            var string: String = TimeFormatUtils.ms2Date(System.currentTimeMillis())
            helper.setText(R.id.name, item?.resName ?: "")
                    .setText(R.id.visit_time, item?.createTime ?: "")
                    .addOnClickListener(R.id.cancle_visit)
                    .addOnClickListener(R.id.sure_visit)

            if (!TextUtils.isEmpty(item.endTime)) {
                if ((item?.endTime ?: "")!!.compareTo(string) < 0) {
                    helper.getView<LinearLayout>(R.id.lay).visibility = View.GONE
                } else {
                    helper.getView<LinearLayout>(R.id.lay).visibility = View.VISIBLE
                    var timev: RushTime = helper.getView<RushTime>(R.id.shengyu_time)
                    dillTime(item?.createTime!!, item?.endTime!!, timev, helper.position)
                    if (timev.isStop) {
                        notifyItemRemoved(helper.position)
                        helper.getView<LinearLayout>(R.id.lay).visibility = View.GONE
                    } else {
                        timev.setListener(this)
                    }
                }
            }
        }
    }

    fun dillTime(startTime: String, endTime: String, timerView: RushTime, int: Int) {
        var days: Int = 0
        val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val d1 = df.parse(startTime)
        val d2 = df.parse(endTime)
        val diff = d2.getTime() - d1.getTime() // mslong days = diff / (1000 * 60 * 60 * 24);
        var hours = (diff - days * (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
        val minutes = (diff - days * (1000 * 60 * 60 * 24) - hours * (1000 * 60 * 60)) / (1000 * 60)
        val second = diff / 1000 - days * 24 * 60 * 60 - hours * 60 * 60 - minutes * 60
        hours += days * 24
        timerView.setTime(hours.toString() + "", minutes.toString() + "", second.toString() + "")
        timerView.start(int)
    }

    fun getList() {
        IpUrl.getInstance()!!.getTwlist(ConstantViewMolde.getToken(), "2")
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<TwModle>>> {
                    override fun onError(e: Throwable?) {
                        visitsmart?.finishRefresh()
                    }
                    override fun onNext(t: HoleResponse<ArrayList<TwModle>>?) {
                        if (t?.code == 0) {
                            if (t?.data != null) {
                                messageList.addAll(t?.data!!)
                                adapter?.notifyDataSetChanged()
                            }
                        }
                    }
                    override fun onCompleted() {
                        visitsmart?.finishRefresh()
                    }

                })
    }
    fun getAsk(id: String) {
        var rxdia: RxDialog = RxDialog(activity)
        rxdia.setContentView(R.layout.dialog_loading)
        rxdia.show()
        IpUrl.getInstance()!!.Twfinish(ConstantViewMolde.getToken(), id)
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ClientModle>> {
                    override fun onError(e: Throwable?) {
                        rxdia?.dismiss()
                    }
                    override fun onNext(t: HoleResponse<ClientModle>?) {
                        if (t?.code == 0) {
                            rxdia?.dismiss()
                            messageList.clear()
                            getList();
                        }
                    }
                    override fun onCompleted() {
                        rxdia?.dismiss()
                    }
                })
    }
    override fun onVisible() {
        super.onVisible()
        messageList?.clear()
        visitsmart?.autoRefresh()
    }

}