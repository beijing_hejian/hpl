/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.wxapi;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.peaceclient.hospitaldoctor.com.Base.Constants;
import com.peaceclient.hospitaldoctor.com.Base.IpUrl;
import com.peaceclient.hospitaldoctor.com.InterFace.ILaunchManagerService;
import com.peaceclient.hospitaldoctor.com.R;
import com.peaceclient.hospitaldoctor.com.View.MyDialog;
import com.peaceclient.hospitaldoctor.com.modle.WxToken;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.vondear.rxtools.view.RxToast;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * @packname com.homeclient.com.wxapi
 * @filename WXEntryActivity
 * @date on 2018/9/3 11:28
 *****/
public class WXEntryActivity extends AppCompatActivity implements IWXAPIEventHandler  {
      private static final int RETURN_MSG_TYPE_LOGIN = 1;
      private static final int RETURN_MSG_TYPE_SHARE = 2;
      private String access_token;
      private String openid;
      private String unionid;
      // public static LoginActivtiy.loginin loginin;
      private boolean istrue;
      private String name;
      private String refresh_token;

      private long expiresIn1;
      private String accessToken;
      private String refreshToken;
      private String openID;
      private long expires_in;
//      private CustomDialog dialog1;
      private WxToken wxToken;
      private IWXAPI api;
      private MyDialog dialog1;
      private ILaunchManagerService launchManagerService;
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            //如果没回调onResp，八成是这句没有写
            api = WXAPIFactory.createWXAPI(this, Constants.Companion.getAPP_ID());
            api.handleIntent(getIntent(), this);
           // launchManagerService = (ILaunchManagerService) Proxy.newProxyInstance(getClassLoader(), ILaunchManagerService.class.getInterfaces() , new LaunchInvocationHandler(this,this));

            // Myapplication.mWxApi.handleIntent(getIntent(), this);
      }
      @Override
      protected void onNewIntent(Intent intent) {
            super.onNewIntent(intent);
            setIntent(intent);
            api.handleIntent(intent, this);
      }
      //      public static void setOnclickBa(LoginActivtiy.loginin onclickBa) {
//            LoginActivtiy.loginin = onclickBa;
//      }
      // 微信发送请求到第三方应用时，会回调到该方法
      @Override
      public void onReq(BaseReq req) {
            switch (req.getType()) {
                  case RETURN_MSG_TYPE_LOGIN:
                        finish();
                        break;
                  case ConstantsAPI.COMMAND_PAY_BY_WX:
                        Log.d("wxEntry", "onPayFinish,errCode=");
            }
      }

      // 第三方应用发送到微信的请求处理后的响应结果，会回调到该方法
      //app发送消息给微信，处理返回消息的回调
      @Override
      public void onResp(BaseResp resp) {
            System.out.println(resp.getType() + resp.errStr);
            switch (resp.errCode) {
                  case BaseResp.ErrCode.ERR_AUTH_DENIED:
                              finish();
                        break;
                  case BaseResp.ErrCode.ERR_USER_CANCEL:
                        if (RETURN_MSG_TYPE_SHARE == resp.getType()) {
                              RxToast.normal("分享失败");

//                              ToastUtil.getInstance("分享失败");
                        } else if (RETURN_MSG_TYPE_LOGIN == resp.getType()) {
//                              ToastUtil.getInstance("登录失败");
                              RxToast.normal("登录失败");
                        }
                        finish();
                        break;
                  case BaseResp.ErrCode.ERR_OK:
                        switch (resp.getType()) {
                              case RETURN_MSG_TYPE_LOGIN:
                                    //根据code请求token
                                    //拿到了微信返回的code,立马再去请求access_token
                                    String code = ((SendAuth.Resp) resp).code;
                                    showiosDialog();
                                    IpUrl.ins.getInstanceWx().GetWxtoken(Constants.Companion.getAPP_ID(),Constants.Companion.getAPP_SECRET_WX(),code,"authorization_code")
                                              . subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<WxToken>() {
                                          @Override
                                          public void onCompleted() {

                                          }

                                          @Override
                                          public void onError(Throwable e) {
                                                if (dialog1!= null){
                                                      dialog1.dismiss();
                                                }
                                                RxToast.normal("网络繁忙，请稍后重试");
                                          }

                                          @Override
                                          public void onNext(WxToken wxoken) {
                                                      if (TextUtils.isEmpty(wxoken.getUnionid())){

                                                      }
                                          }
                                    });
//                                    NetBaseUtil.getInstanceWX().getWxtoken(Constant.APP_ID, Constant.APP_SECRET_WX, ((SendAuth.Resp) resp).code, "authorization_code").
//                                              subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<WxToken>() {
//                                          @Override
//                                          public void onCompleted() {
//                                                //  finish();
//                                          }
//
//                                          @Override
//                                          public void onError(Throwable e) {
//                                                if (dialog1.isShowing()) {
//                                                      dialog1.dismiss();
//                                                }
//                                                Toast.makeText(WXEntryActivity.this, "网络繁忙，请稍后重试", Toast.LENGTH_SHORT).show();
//                                          }
//
//                                          @Override
//                                          public void onNext(final WxToken wxToken) {
//                                                // Toast.makeText(Myapplication.mContext, wxToken.toString(), Toast.LENGTH_SHORT).show();
//                                                //获取到 微信的unionid
//                                                if (!TextUtils.isEmpty(wxToken.getUnionid())) {
//                                                      if (Myapplication.istrue) {
//                                                            NetBaseUtil.getInstance().GetWxToken("0", wxToken.getUnionid(), "androidApp", "androidApp")
//                                                                      .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
//                                                                      .subscribe(new Observer<AccessTokenResponse>() {
//                                                                            @Override
//                                                                            public void onCompleted() {
//                                                                            }
//
//                                                                            @Override
//                                                                            public void onError(Throwable e) {
//                                                                                  if (dialog1 != null) {
//                                                                                        if (dialog1.isShowing()) {
//                                                                                              dialog1.dismiss();
//                                                                                        }
//                                                                                  }
//                                                                                  Toast.makeText(WXEntryActivity.this, "网络繁忙，请稍后重试", Toast.LENGTH_SHORT).show();
//                                                                            }
//
//                                                                            @Override
//                                                                            public void onNext(AccessTokenResponse accessTokenResponse) {
//                                                                                  //获取到token，
//                                                                                  if (accessTokenResponse.getResp_code() == 0) {
//                                                                                        // 此处请求详情，登录环信
//                                                                                        // Toast.makeText(WXEntryActivity.this, "GetWxToken"+accessTokenResponse.getRsp_msg(), Toast.LENGTH_SHORT).show();
//                                                                                        AccessTokenUtils.SaveAccesstoken(accessTokenResponse.getDatas().getAccess_token(), System.currentTimeMillis() + (accessTokenResponse.getDatas().getExpires_in() * 1000));
//                                                                                        getUserInfo(accessTokenResponse.getDatas().getAccess_token(), wxToken);
//                                                                                  } else {
//                                                                                        if (dialog1 != null) {
//                                                                                              dialog1.dismiss();
//                                                                                        }
//                                                                                        ToastUtil.getInstance(accessTokenResponse.getRsp_msg());
////                                                            runOnUiThread(new Runnable() {
////                                                                @Override
////                                                                public void run() {
////                                                                    if (dialog1.isShowing()) {
////                                                                        dialog1.dismiss();
////                                                                    }
////                                                                }
////                                                            });
//                                                                                  }
//                                                                            }
//                                                                      });
//                                                      } else {
//                                                            BindWechat(wxToken.getUnionid());
//                                                      }
//                                                      unionid = wxToken.getUnionid();
//                                                      finish();
//                                                } else {
//                                                      finish();
//                                                }
//                                                Intent intent = new Intent("dismiss");
//                                                sendBroadcast(intent);
//                                          }
//                                    });
                                    break;
                              case RETURN_MSG_TYPE_SHARE:
                                    RxToast.normal("微信分享成功");
                                    finish();
                                    break;

                        }
                        break;

            }

      }

      private void showiosDialog() {
            dialog1 = new MyDialog(WXEntryActivity.this, R.layout.dialog_loading,false,false);
            if (dialog1 != null) {
                  dialog1.show();
            }
      }

      private void getUserInfo(String access_token, WxToken S) {
/*            NetBaseUtil.getInstance().GetAccount(access_token, "0").subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).
                      subscribe(new Observer<LoginUser>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {


                            }

                            @Override
                            public void onNext(LoginUser loginUser) {
                                  if (loginUser.getResp_code() == 0) {
                                        if (TextUtils.isEmpty(loginUser.getDatas().getPhone())) {
                                              //前去绑定手机号码
                                              startActivity(new Intent(WXEntryActivity.this, BindPhoneActivity.class));
                                              finish();
                                        } else {
                                              login(loginUser.getDatas());
                                        }
                                        Myapplication.editor.putString("unid", S.getUnionid()).commit();
                                  } else {
                                        Toast.makeText(Myapplication.mContext, loginUser.getResp_msg(), Toast.LENGTH_SHORT).show();
                                        // Toast.makeText(Myapplication.mContext, "getaccount", Toast.LENGTH_SHORT).show();
                                        finish();
                                        Intent intent = new Intent("dismiss");
                                        sendBroadcast(intent);
                                  }
                            }
                      });*/
      }

//      private void login(LoginUser.DatasBean user) {
//            try {
//                  if (!TextUtils.isEmpty(user.getPhone())) {
//                        EMClient.getInstance().login(user.getPhone() + "", user.getPhone() + "123456", new EMCallBack() {
//                              @Override
//                              public void onSuccess() {
//                                    DemoHelper.getInstance().setCurrentUserName((String) user.getAccount());
//                                    EMClient.getInstance().groupManager().loadAllGroups();
//                                    EMClient.getInstance().chatManager().loadAllConversations();
//                                    Myapplication.editor.putBoolean("islogin", true).commit();
//                                    FileUtils.saveObject(WXEntryActivity.this, "loginUser", user);
//                                    // 登录成功，将用户的昵称头像缓存在本地
//                                    Intent intent = new Intent("login");
//                                    intent.putExtra("name", true);
//                                    sendBroadcast(intent);
//                                    runOnUiThread(new Runnable() {
//                                          @Override
//                                          public void run() {
//                                                if (XiaoxiFragment.getInstance() != null) {
//                                                      XiaoxiFragment.getInstance().ondatechange(true);
//                                                }
//                                                // ToastUtil.getInstance("微信登录成功");
//                                          }
//                                    });
//
//                                    boolean updatenick = EMClient.getInstance().pushManager().updatePushNickname(user.getAccount() == "" ? "" : user.getAccount());
//                                    if (!updatenick) {
//                                          Log.e("LoginActivity", "update current user nick fail");
//                                    }
//                                    DemoHelper.getInstance().getUserProfileManager().asyncGetCurrentUserInfo();
//                                    if (dialog1 != null && dialog1.isShowing()) {
//                                          dialog1.dismiss();
//                                    }
//                                    Intent intent3 = new Intent(WXEntryActivity.this, MainActivity.class);
//                                    intent3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//关掉所要到的界面中间的 activity
//                                    intent3.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);//设置不要刷新将要跳转的界面
//                                    startActivity(intent3);
//                                    finish();
//                              }
//
//                              @Override
//                              public void onError(int i, String s) {
//                                    if (Myapplication.istrue) {
//                                          ToastUtil.getInstance("微信登录失败");
//                                    } else {
//                                          ToastUtil.getInstance("微信绑定失败");
//                                    }
//                                    Intent intent = new Intent("dismiss");
//                                    sendBroadcast(intent);
//                                    finish();
//                              }
//
//                              @Override
//                              public void onProgress(int i, String s) {
//
//                              }
//                        });
//                  } else {
//                        if (dialog1 != null || dialog1.isShowing()) {
//                              dialog1.dismiss();
//                        }
//                        startActivity(new Intent(WXEntryActivity.this, BindPhoneActivity.class));
//                        finish();
//                  }
//            } catch (Exception e) {
//
//            }
//      }

/*      public void BindWechat(String unionid) {
            final LoginUser.DatasBean user = (LoginUser.DatasBean) FileUtils.getObject(WXEntryActivity.this, "loginUser");

            AccessTokenUtils.IfShouldRequestAccesstoken(System.currentTimeMillis());
            String accesstoken = Myapplication.sp.getString("accesstoken", "");
            NetBaseUtil.getInstance().wxBind(accesstoken, unionid).observeOn(AndroidSchedulers.mainThread())
                      .subscribeOn(Schedulers.io()).subscribe(new Observer<WxBindBean>() {
                  @Override
                  public void onCompleted() {

                  }

                  @Override
                  public void onError(Throwable e) {
                        ToastUtil.getInstance("网络错误，稍后重试");
                  }

                  @Override
                  public void onNext(WxBindBean wxBindBean) {
                        if (wxBindBean.getResp_code() == 0) {
                              ToastUtil.getInstance("微信绑定成功");
                              user.setUnionId(wxBindBean.getDatas().getUnionId());
                              FileUtils.saveObject(WXEntryActivity.this, "loginUser", user);
                              Intent intent = new Intent("login");
                              intent.putExtra("name", true);
                              sendBroadcast(intent);
                        } else {
                              ToastUtil.getInstance("微信绑定失败");
                        }

                  }
            });
      }*/

      @Override
      protected void onDestroy() {
            super.onDestroy();
            if (dialog1 != null) {
                  dialog1.dismiss();
                  dialog1 = null;
            }

      }


}

