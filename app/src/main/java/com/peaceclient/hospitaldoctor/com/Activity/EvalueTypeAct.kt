package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.os.Bundle
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.R
import kotlinx.android.synthetic.main.evaluetype_act.*

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor222
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/10/13 14:42
 * @change
 * @chang time
 * @class describe 评价页面
 */

class EvalueTypeAct  :HoleBaseActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.evaluetype_act)
        arrow_back.setOnClickListener {
            finish()
        }

        report1.setOnClickListener {
            // 患者评价
            var  intent:Intent = Intent(this@EvalueTypeAct,EvalueListAct::class.java)
            intent.putExtra("type","1")
            startActivity(intent)
        }
        report2.setOnClickListener {
            //上级评价
            var  intent:Intent = Intent(this@EvalueTypeAct,EvalueListAct::class.java)
            intent.putExtra("type","2")
            startActivity(intent)

        }
        report3.setOnClickListener {
            // 员工考评
//            var  intent:Intent = Intent(this@EvalueTypeAct,EvalueListAct::class.java)
//            intent.putExtra("type","3")
//            startActivity(intent)
            var  intent:Intent = Intent(this@EvalueTypeAct,OtherEvalueAct::class.java)

            startActivity(intent)
        }
    }
}