/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.InterFace

import androidx.annotation.Nullable
import com.peaceclient.hospitaldoctor.com.modle.*
import okhttp3.MultipartBody
import retrofit2.http.*
import rx.Observable
import java.util.concurrent.atomic.AtomicInteger

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.InterFace
 * @class describe
 * @anthor admin
 * @time 2021/7/12 15:02
 * @change
 * @chang time
 * @class describe
 */

public interface RetrofitUrl {
    @POST("/login")
    abstract fun login(
        @Query("username") name: String,
        @Query("password") password: String
        //                ,@Query("imei") String imei
    ): rx.Observable<TimeBean>

    /****
     *  登录请求
     * @param name String 登录用户名
     * @param password String
     * @return Observable<Objects>
     */
//    @POST("/login")
//    abstract fun login(
//            @Query("username") name: String,
//            @Query("password") password: String
//    ): Observable

    /***
     * 版本更新请求
     *
     * @param sys AtomicInteger 1.全部 2.android 3.ios
     * @param client AtomicInteger  客户端 1.全部 2.居民端 3.医生端
     */
    @GET(" /api-desktop/pub/app/version")
    abstract fun UpdateCode(
        @Query("sys") sys: Int,
        @Query("client") client: Int
    ): Observable<HoleResponse<UpdateModle>>

    /****
     *  就诊指南接口
     * @param integer AtomicInteger 客户端 1.全部 2.Android 3.ios
     */
    @GET("/api/clinicguide/findClinicguide")
    abstract fun ClinentGuide(
        @Query("client") integer: AtomicInteger
    ): Observable<TimeBean>

    /****
     *  咨询管理接口 无参数
     */
    @GET("/api/information/findInformation")
    abstract fun Information(): Observable<TimeBean>

    /***
     * 哦通知接口
     * @param clientNotice AtomicInteger 客户端 1.全部 2.居民端 3.医生端
     * @param messageType AtomicInteger 消息类型：1.通知消息 2、处方消息
     * @param operationSystem AtomicInteger 操作系统 1.全部 2.Android 3. ios
     * @param starttime String 上次查询的时间格式要求（yyyy-mm-ddhh：mm：ss）
     */

    @GET("/api/noticemessage/messagequery")
    abstract fun NoticeList(
        @Query("clientNotice") clientNotice: AtomicInteger,
        @Query("messageType") messageType: AtomicInteger,
        @Query("operationSystem") operationSystem: AtomicInteger,
        @Query("starttime") starttime: String

    ): Observable<TimeBean>


    /****
     *  获取验证码接口 fake
     * @param phone String
     * @param Authorization String
     * @return Observable<TimeBean>
     */
    @POST("/api-sms/sms/code")
    fun getVcodes(
        @Query("phone") phone: String,
        @Header("Authorization") Authorization: String
    ): Observable<HoleResponse<Int>>

    /****
     *  获取验证码接口
     * @param phone String
     * @param Authorization String
     * @return Observable<TimeBean>
     */
    @GET("/api-sms/sms/code")
    abstract fun getVcode(
        @Query("phone") phone: String,
        @Header("Authorization") Authorization: String
    ): Observable<TimeBean>

    /****
     * 用户注册接口
     * @param account String
     * @param password String
     * @param vcode String
     * @return Observable<HoleResponse<Int>>
     */
    @POST("/api-user/users/app/pub/register/doctor")
    fun Regist(
        @Query("docName") docName: String,
        @Query("idCard") idCard: String,
        @Query("account") account: String,
        @Query("password") password: String,
        @Query("vcode") vcode: String

    ): Observable<HoleResponse<Int>>

    /****
     *  手机号码密码获取token
     * @param client_id String
     * @param client_secret String
     * @param account String
     * @param password String
     * @return Observable<HoleResponse<Int>>
     */
    @POST("/api-auth/oauth/app/doctor/user/token")
    fun Token(
        @Header("client_id") client_id: String,
        @Header("client_secret") client_secret: String,
        @Query("account") account: String,
        @Query("password") password: String
    ): Observable<HoleResponse<TokenBean>>

    /****
     *
     * @param access_token String
     * @return Observable<HoleResponse<UserModle>>
     */

    @GET("/api-auth/oauth/user/info")
    fun getUser(
        @Query("access_token") access_token: String
    ): Observable<HoleResponse<UserModle>>


    @GET("/api-basis/doctor/details")
    fun getModle(
        @Query("access_token") access_token: String,
        @Query("id") id: String
    ): Observable<HoleResponse<UserModle>>

    /***
     * 忘记密码
     * @param access_token String
     * @return Observable<HoleResponse<UserModle>>
     */
    @POST("/api-user/users/app/pub/forget")
    fun Forget(
        @Query("account") account: String,
        @Query("password") password: String,
        @Query("vcode") vcode: String
    ): Observable<HoleResponse<Int>>

    /****
     * 修改密码接口
     * @param account String
     * @param password String
     * @param vcode String
     * @return Observable<HoleResponse<Int>>
     */
    @POST("/api-user/users/app/password/reset")
    fun changePaw(
        @Query("password") password: String,
        @Query("oldPassword") oldPassword: String
    ): Observable<HoleResponse<Int>>

    /****
     *  获取wx token接口 以及openid和unionid
     * @param appid String
     * @param secret String
     * @param code String
     * @param grant_type String
     * @return Observable<WxToken>
     */
    @POST("access_token")
    fun GetWxtoken(
        @Query("appid") appid: String,
        @Query("secret") secret: String,
        @Query("code") code: String,
        @Query("grant_type") grant_type: String
    ): Observable<WxToken>

    /***
     *  获取服务信息
     * @param client String
     * @param sys String
     * @return Observable<ServiceBean>
     */
    @GET("/api-desktop/pub/pause")
    fun ServiceFun(
        @Query("client") client: Int,
        @Query("sys") sys: Int
    ): Observable<HoleResponse<ServiceBean>>


    /***
     * 开屏信息
     * @param client String 0 医生端 1.用户端
     * @param sys String 0，苹果 1.android
     * @return Observable<ServiceBean>
     */
    @GET("/api-desktop/pub/open/message")
    fun kaiping(
        @Query("client") client: Int,
        @Query("sys") sys: Int
    ): Observable<HoleResponse<ServiceBean>>


    /****
     * 文件上传
     * @param client String
     * @param sys String
     * @return Observable<ServiceBean>
     */
    @Multipart
    @POST("/api-file/files-app")
    fun Upload(
        @Query("access_token") access_token: String,
        @Part file: MultipartBody.Part
    ): Observable<HoleResponse<FileResopnse>>


    /****
     * 上传头像接口
     * @param access_token String
     * @param file MultipartBody
     * @return Observable<ServiceBean>
     */
    @POST("/api-user/users/app/headimg/reset")
    fun PostImage(
        @Query("access_token") access_token: String,
        @Query("headImgUrl") headImgUrl: String
    ): Observable<HoleResponse<String>>

    /****
     * 主页资讯滚动列表
     * @return Observable<HoleResponse<ArrayList<KnowleModle>>>
     */
    @GET("/api-desktop/pub/info/announcement")
    fun getZixun(
    ): Observable<HoleResponse<MutableList<KnowleModle>>>

    /****
     *主页Banner
     * @return Observable<HoleResponse<ArrayList<KnowleModle>>>
     */
    @GET("/api-desktop/pub/info/banner")
    fun getBanner(
    ): Observable<HoleResponse<MutableList<KnowleModle>>>

    /****
     * 获取所有详情
     * @return Observable<HoleResponse<ArrayList<KnowleModle>>>
     */
    @GET("/api-desktop/pub/info/details")
    fun getDetail(
        @Query("id") id: String
    ): Observable<HoleResponse<KnowleModle>>

    /***
     * 健康资讯列表
     * @return Observable<HoleResponse<ArrayList<KnowleModle>>>
     */
    @GET("/api-desktop/pub/info/messages")
    fun getHealth(

    ): Observable<HoleResponse<MutableList<KnowleModle>>>

    /***
     * 名医风采
     * @return Observable<HoleResponse<ArrayList<KnowleModle>>>
     */
    @GET("/api-desktop/pub/info/doctor")
    fun getDoctorFeng(
    ): Observable<HoleResponse<MutableList<KnowleModle>>>

    /***
     * 医院介绍
     * @return Observable<HoleResponse<ArrayList<KnowleModle>>>
     */
    @GET(" /api-desktop/pub/info/introduction")
    fun getHospital(
    ): Observable<HoleResponse<KnowleModle>>

    /****
     * 就诊指南
     * @return Observable<HoleResponse<ArrayList<KnowleModle>>>
     */
    @GET("/api-desktop/pub/info/clinic_guide")
    fun getClinic(
    ): Observable<HoleResponse<MutableList<KnowleModle>>>

    @GET("/api-desktop/pub/notices/messagequery")
    fun getNotification(
    ): Observable<HoleResponse<MessageModle>>

    /*****
     *   科室列表含有详情条目接口
     * @return Observable<HoleResponse<ArrayList<DepartModle>>>
     */
    @GET("/api-basis/pub/department/tree")
    fun getDepartTree(
    ): Observable<HoleResponse<ArrayList<DepartModle>>>

    /*****
     *  科室列表不含有子条目数据接口
     * @return Observable<HoleResponse<ArrayList<DepartModle>>>
     */
    @GET("/api-basis/pub/department/list")
    fun getDepartList(
    ): Observable<HoleResponse<ArrayList<DepartModle>>>

    /***
     *
     * @param os String 1.全部 2.安卓 3.苹果
     * @param client String 1.全部 2.居民端 3.医生端
     * @param uid String 用户id
     * @param starttime String 上次查询时间
     * @return Observable<HoleResponse<MessageModle>>
     */
    @GET("/api-desktop/pub/notices/messagequery")
    fun getMessage(
        @Query("os") os: String,
        @Query("client") client: String,
        @Nullable
        @Query("uid") uid: String,
        @Nullable
        @Query("starttime") starttime: String
    ): Observable<HoleResponse<MessageModle>>

    /****
     * 查询就诊卡接口
     * @param access_token String
     */
    @GET("/api-basis/see/doctor/card")
    fun SearchJZ(
        @Query("access_token") access_token: String
    ): Observable<HoleResponse<ArrayList<JzCardModle>>>


    /****
     * 查询就诊卡接口
     * @param access_token String
     */
    @GET("/api-basis/see/doctor/card/default")
    fun SearchDefultJZ(
        @Query("access_token") access_token: String
    ): Observable<HoleResponse<JzCardModle>>

    /****
     *  添加就诊卡 接口
     * @param access_token String
     * @param fullName String
     * @param idCard String
     * @param idCardImgUrl String
     * @param phone String
     * @param vcode String
     */
    @POST("/api-basis/see/doctor/card")
    fun AddJZ(
        @Query("access_token") access_token: String,
        @Query("fullName") fullName: String,
        @Query("idCard") idCard: String,
        @Query("idCardImgUrl") idCardImgUrl: String,
        @Query("phone") phone: String,
        @Query("vcode") vcode: String

    ): Observable<HoleResponse<String>>

    /***
     *  删除就诊卡
     * @param access_token String
     * @param id String
     * @return Observable<HoleResponse<MessageModle>>
     */
    @DELETE("/api-basis/see/doctor/card")
    fun DeleteJZ(
        @Query("access_token") access_token: String,
        @Query("id") id: String
    ): Observable<HoleResponse<String>>


    @POST("/api-basis/see/doctor/card/default")
    fun SetDefultJz(
        @Query("access_token") access_token: String,
        @Query("id") id: String
    ): Observable<HoleResponse<String>>


    /****
     *  图文列表 接口 需要登陆
     * @param access_token String
     * @param id String
     * @param orderStatus String   1 待就诊 2接诊中 5待开方
     * @return Observable<HoleResponse<ArrayList<DoctModle>>>
     */
    @GET("/api-basis/graphic/accepts/record")
    fun getTwlist(
        @Query("access_token") access_token: String,
        @Query("orderStatus") orderStatus: String
    ): Observable<HoleResponse<ArrayList<TwModle>>>


    @GET("/api-basis/remote/accepts/record")
    fun getVdlist(
        @Query("access_token") access_token: String,
        @Query("makeAppointmentDate") makeAppointmentDate: String
    ): Observable<HoleResponse<ArrayList<TwModle>>>

    /****
     * 图文接诊记录
     * @param access_token String
     * @param orderStatus String
     * @return Observable<HoleResponse<ArrayList<TwModle>>>
     */
    @GET("/api-basis/graphic/recordall")
    fun getTwHis(
        @Query("access_token") access_token: String,
        @Query("resName") resName: String,
        @Query("weeks") weeks: String
    ): Observable<HoleResponse<ArrayList<TwModle>>>

    /****
     *  视频接诊记录
     * @param access_token String
     * @param resName String
     * @param weeks String
     * @return Observable<HoleResponse<ArrayList<TwModle>>>
     */
    @GET("/api-basis/remote/recordall")
    fun getSpHis(
        @Query("access_token") access_token: String,
        @Query("resName") resName: String,
        @Query("weeks") weeks: String
    ): Observable<HoleResponse<ArrayList<TwModle>>>

    /****
     * 查询诊断结果接口
     * @param access_token String
     * @param id String
     * @return Observable<HoleResponse<ArrayList<TwModle>>>
     */
    @GET("/api-basis/graphic/diag")
    fun getDalogis(
        @Query("access_token") access_token: String,
        @Query("id") id: String

    ): Observable<HoleResponse<TwModle>>

    /****
     * 图文接诊中接诊接口
     * @param access_token String
     * @param id String
     * @return Observable<HoleResponse<Int>>
     */
    @POST("/api-basis/graphic/ensure")
    fun Twask(
        @Query("access_token") access_token: String,
        @Query("id") id: String
    ): Observable<HoleResponse<ClientModle>>

    /****
     * 图文接诊中医生停诊
     * @param access_token String
     * @param id String
     * @return Observable<HoleResponse<Int>>
     */
    @POST("/api-basis/graphic/refuse")
    fun stop(
        @Query("access_token") access_token: String,
        @Query("id") id: String,
        @Query("remark") remark: String
    ): Observable<HoleResponse<ClientModle>>


    /****
     *  修改视频接诊的状态
     * @param access_token String
     * @param orderStatus String
     * @param id String
     * @return Observable<HoleResponse<Int>>
     */
    @POST("/api-basis/remote/confirm")
    fun changeStatus(
        @Query("access_token") access_token: String,
        @Query("orderStatus") orderStatus: String,
        @Query("id") id: String
    ): Observable<HoleResponse<ClientModle>>

    /****
     *  结束图文问诊
     * @param access_token String
     * @param id String
     * @return Observable<HoleResponse<Int>>
     */
    @POST("/api-basis/graphic/close")
    fun Twfinish(
        @Query("access_token") access_token: String,
        @Query("id") id: String
    ): Observable<HoleResponse<ClientModle>>

    /***
     *
     * @param access_token String
     * @param id String
     * @return Observable<HoleResponse<Int>>
     */
    @GET("/api-basis/remote/details")
    fun spDetial(
        @Query("access_token") access_token: String,
        @Query("id") id: String
    ): Observable<HoleResponse<ClientModle>>

    /****
     * 视频接诊结束
     * @param access_token String
     * @param id String
     * @return Observable<HoleResponse<Int>>
     */
    @POST("/api-basis/remote/close")
    fun Spfinish(
        @Query("access_token") access_token: String,
        @Query("id") id: String
    ): Observable<HoleResponse<ClientModle>>

    /*****
     * 诊断录入
     * @param access_token String
     * @param id String
     * @param diagnosis String
     * @param prescription String
     * @return Observable<HoleResponse<Int>>
     */
    @POST("/api-basis/graphic/diagnosis")
    fun diagnosis(
        @Query("access_token") access_token: String,
        @Query("id") id: String,
        @Query("diagnosis") diagnosis: String,
        @Query("prescription") prescription: String
    ): Observable<HoleResponse<Int>>


    @GET(" /api-basis/doctor/details")
    fun getVideoDetial(
        @Query("access_token") access_token: String,
        @Query("id") id: String,
        @Query("diagnosis") diagnosis: String,
        @Query("prescription") prescription: String
    ): Observable<HoleResponse<Int>>

    @Multipart
    @POST("/api-file/files-app")
    fun UploadPic(
        @Query("access_token") access_token: String,
        @Part file: MultipartBody.Part
    ): Observable<HoleResponse<FileResopnse>>

    /****
     * 在线获取昵称
     * @param access_token String
     * @param resImAccount String
     * @param docImAccount String
     * @return Observable<HoleResponse<NickModle>>
     */
    @GET(" /api-basis/graphic/nikename")
    fun getNick(
        @Query("access_token") access_token: String,
        @Query("resImAccount") resImAccount: String,
        @Query("docImAccount") docImAccount: String

    ): Observable<HoleResponse<NickModle>>


    @GET("/api-basis/remote/refresh")
    fun Refresh(
        @Query("access_token") access_token: String,
        @Query("id") id: String

    ): Observable<HoleResponse<PaiModle>>

    /***
     * 远程问诊首页列表
     * @param access_token String
     * @param zhid String
     * @return Observable<HoleResponse<ArrayList<GhModle>>>
     */
    @GET("/api-basis/remote/doctor/record")
    fun getRemoteList(
        @Query("access_token") access_token: String
    ): Observable<HoleResponse<ArrayList<GhModle>>>

    //医生接诊
    @GET("/api-basis/remote/doctor/reply")
    fun getRemote(
        @Query("access_token") access_token: String,
        @Query("appointid") appointid: String//挂号主键
    ): Observable<HoleResponse<ArrayList<GhModle>>>

    @GET("/api-basis/menzhen/yisheng/pingjia")
    fun getValue(
        @Query("access_token") access_token: String,
        @Query("ysid") zhid: String//患者id

    ): Observable<HoleResponse<ArrayList<GhModle>>>

    /***
     *  患者评价列表
     * @param access_token String
     * @param zhid String
     * @return Observable<HoleResponse<ArrayList<GhModle>>>
     */
    @GET("/api-basis/evaluation/listByDoc")
    fun getHzValue(
        @Query("access_token") access_token: String
    ): Observable<HoleResponse<ArrayList<GhModle>>>

    /***
     * 上级评价列表
     * @param access_token String
     * @param id String
     * @return Observable<HoleResponse<ArrayList<GhModle>>>
     */
    @GET("/api-basis/menzhen/v2/kaoping/sjpj")
    fun getSJValue(
        @Query("access_token") access_token: String,
        @Query("id") id: String//患者id

    ): Observable<HoleResponse<ArrayList<GhModle>>>

    /****
     * 员工评价列表
     *
     * @param access_token String
     * @param depId String
     * @param nd String
     * @return Observable<HoleResponse<ArrayList<GhModle>>>
     */
    @GET("/api-basis/menzhen/v2/kaoping/cxys")
    fun getYgValue(
        @Query("access_token") access_token: String,
        @Query("depId") depId: String,//社区id
        @Query("nd") nd: String//年度

    ): Observable<HoleResponse<ArrayList<GhModle>>>

    // 远程问诊停诊
    @GET("/api-basis/remote/doctor/suspend")
    fun tingzhen(
        @Query("access_token") access_token: String,
        @Query("appointid") zhid: String

    ): Observable<HoleResponse<GhModle>>

    /*****
     * 处方结算列表
     * @param access_token String
     * @param zhid String
     * @return Observable<HoleResponse<String>>
     */
    @GET(" /api-basis/remote/doctor/prescriptions")
    fun repclist(
        @Query("access_token") access_token: String,
        @Query("ischeck") ischeck: String

    ): Observable<HoleResponse<ArrayList<PeisongBean>>>

    /*****
     * 处方审核列表
     * @param access_token String
     * @param zhid String
     * @return Observable<HoleResponse<String>>
     */
    @GET("/api-basis/remote/pharmacist/checkingRecord")
    fun Repclist(
        @Query("access_token") access_token: String
    ): Observable<HoleResponse<ArrayList<PeisongBean>>>

    /******
     *  查看处方详情
     * @param access_token String
     * @param zhid String
     * @return Observable<HoleResponse<ArrayList<RecipeModle>>>
     */
    @GET("/api-basis/remote/doctor/presDetails")
    fun recipDetial(
        @Query("access_token") access_token: String,
        @Query("id") id: String
    ): Observable<HoleResponse<RecipeModlex>>

    /****
     *   配送详情
     * @param access_token String
     * @param id String
     * @return Observable<HoleResponse<RecipeModlex>>
     */

    @GET("/api-basis/remote/pharmacist/expressDetails")
    fun PeisongDetial(
        @Query("access_token") access_token: String,
        @Query("id") id: String

    ): Observable<HoleResponse<RecipeModlex>>

    /******
     *  查看处方详情
     * @param access_token String
     * @param zhid String
     * @return Observable<HoleResponse<ArrayList<RecipeModle>>>
     */
    @GET("/api-basis/remote/pharmacist/checkingDetails")
    fun RecipDetial(
        @Query("access_token") access_token: String,
        @Query("id") id: String
    ): Observable<HoleResponse<RecipeModlex>>

    /****
     * 医生处方审核接口
     * @param access_token String
     * @param ghid String 挂号主键
     * @param shjy String 审核建议
     * @param shysid String 审核医生主键
     * @param shzt String 审核状态
     * @return Observable<HoleResponse<RecipeModle>>
     */
    @GET("/api-basis/wenzhen/v2/chufang/shenfang")
    fun shenhe(
        @Query("access_token") access_token: String,
        @Query("ghid") ghid: String,
        @Query("shjy") shjy: String,
        @Query("shysid") shysid: String,
        @Query("shzt") shzt: String


    ): Observable<HoleResponse<RecipeModle>>

    /****
     *  医生开方接口
     * @param access_token String
     * @param zx RecipeModle
     * @return Observable<HoleResponse<RecipeModle>>
     */
    @POST("/api-basis/remote/doctor/diagnosis")
    fun Kaifang(
        @Query("access_token") access_token: String,
        @Body requ: RecipeModlex

    ): Observable<HoleResponse<RecipeModlex>>

    /***
     * 查询药品列表
     * @param access_token String
     * @param medtype String 查询类型：0、全部；1、西药；2、中成药；3、草药
     * @param shrm String 	药品拼音字头
     * @return Observable<HoleResponse<ArrayList<RecipeModle.zdkfm>>>
     */
    @GET("/api-basis/medicine/dictmedquery")
    fun cxyp(
        @Query("access_token") access_token: String,
        @Query("medtype") medtype: String,
        @Query("shrm") shrm: String


    ): Observable<HoleResponse<ArrayList<RecipeModlex.BillDetail>>>

    /***
     * 我的澳品
     * @param access_token String
     * @param shzt String
     * @return Observable<HoleResponse<ArrayList<RecipeModle>>>
     */
    @GET("/api-basis/wenzhen/v2/chufang/wdcf")
    fun mydrug(
        @Query("access_token") access_token: String,
        @Query("shzt") shzt: String,
        @Query("ysid") ysid: String

    ): Observable<HoleResponse<ArrayList<RecipeModle>>>

    /*****
     *  配送提交信息
     * @param access_token String
     * @param expressid String 快递信息主键
     * @param expressnum String 快递单号
     * @param express String 快递名称
     * @return Observable<HoleResponse<ArrayList<RecipeModle>>>
     */
    @POST("/api-basis/remote/pharmacist/sendExpress")
    fun peisong(
        @Query("access_token") access_token: String,
        @Body bod: RecipeModlex.BillExpress

    ): Observable<HoleResponse<ArrayList<RecipeModlex>>>

    /****
     * 个人中心配送历史
     * @param access_token String
     * @return Observable<HoleResponse<ArrayList<RecipeModle>>>
     */
    @GET("/api-basis/wenzhen/v2/chufang/yipeisong")
    fun yipeisongList(
        @Query("access_token") access_token: String
    ): Observable<HoleResponse<ArrayList<RecipeModle>>>


    /****
     *  配送列表接口
     * @param access_token String
     * @param type String 类型为1 是 是待配送 2 是为配送历史
     * @param uname String  姓名
     * @param week String 时间端，在type 为2时传输字段
     * @return Observable<HoleResponse<ArrayList<RecipeModlex>>>
     */
    @GET("/api-basis/remote/pharmacist/expressRecord")
    fun weipeisongList(

        @Query("access_token") access_token: String,
        @Query("type") type: String,
        @Query("uname") uname: String,
        @Query("week") week: String
    ): Observable<HoleResponse<ArrayList<PeisongBean>>>

    /***
     * 员工评价接口
     * @param access_token String
     * @param id String
     * @param nd String
     * @param pingjia String
     * @param xing String
     * @return Observable<HoleResponse<String>>
     */
    @POST("/api-basis/menzhen/v2/kaoping/pjyg")
    fun YgValue(
        @Query("access_token") access_token: String,
        @Query("id") id: String,
        @Query("nd") nd: String,
        @Query("pingjia") pingjia: String,
        @Query("xing") xing: String
    ): Observable<HoleResponse<String>>

    /****
     * 上级评价接口
     * @param access_token String
     * @param id String
     * @param nd String
     * @param pingjia String
     * @param xing String
     * @return Observable<HoleResponse<ArrayList<RecipeModle>>>
     */
    @GET("/api-basis/menzhen/v2/kaoping/sjpj")
    fun sjvalue(
        @Query("access_token") access_token: String,
        @Query("id") id: String,
        @Query("nd") nd: String,
        @Query("pingjia") pingjia: String,
        @Query("xing") xing: String
    ): Observable<HoleResponse<ArrayList<RecipeModle>>>

    /****
     * 员工年度列表
     * @param access_token String
     * @return Observable<HoleResponse<ArrayList<GhModle>>>
     */
    @GET("/api-basis/menzhen/v2/kaoping/cxnd")
    fun ndList(
        @Query("access_token") access_token: String
    ): Observable<HoleResponse<ArrayList<String>>>


    @GET("/api-basis/remote/doctor/clinicRecord")
    fun RemoHis(
        @Query("access_token") access_token: String,
        @Query("uname") uname: String,
        @Query("weeks") weeks: String

    ): Observable<HoleResponse<ArrayList<GhModle>>>

    /****
     * 查询是否验证了身份证
     * @param access_token String
     * @param zhid String
     * @return Observable<HoleResponse<DangAnModle>>
     */
    @GET("/api-basis/dangan/chaxun")
    fun getDanan(
        @Query("access_token") access_token: String,
        @Query("zhid") zhid: String
    ): Observable<HoleResponse<DangAnModle>>

    @POST("/api-basis/wenzhen/suifang/yuyue")
    fun addSui(
        @Query("access_token") access_token: String,
        @Query("ghid") ghid: String,
        @Query("sfrq") sfrq: String,
        @Query("ysid") ysid: String,
        @Query("zhid") zhid: String
    ): Observable<HoleResponse<DangAnModle>>

    /****
     * 结束接诊
     * @param access_token String
     * @param ghid String
     * @param ysid String
     * @return Observable<HoleResponse<Any>>
     */
    @GET("/api-basis/remote/doctor/endclinic")
    fun endWZ(
        @Query("access_token") access_token: String,
        @Query("appointid") ghid: String
    ): Observable<HoleResponse<Any>>

    @GET("/api-basis/wenzhen/suifang/yisheng")
    fun suifangList(
        @Query("access_token") access_token: String,
        @Query("today") today: Boolean,
        @Query("ysid") ysid: String
    ): Observable<HoleResponse<ArrayList<GhModle>>>

    /***
     * 随访历史
     * @param access_token String
     * @param ysid String
     * @return Observable<HoleResponse<ArrayList<GhModle>>>
     */
    @GET("/api-basis/wenzhen/suifang/yisheng/lishi")
    fun suifangHis(
        @Query("access_token") access_token: String,

        @Query("ysid") ysid: String
    ): Observable<HoleResponse<ArrayList<GhModle>>>

    /****
     *  医生修改处方接口
     * @param access_token String
     * @param ysid String
     * @return Observable<HoleResponse<ArrayList<GhModle>>>
     */
    @GET("/api-basis/remote/doctor/modifiyPres")
    fun UpdateMedicine(
        @Query("access_token") access_token: String,

        @Query("ysid") ysid: String
    ): Observable<HoleResponse<ArrayList<GhModle>>>

    /***
     * 处方审核接口
     * @param access_token String
     * @param id String
     * @param ischeck String
     * @param checkresult String
     * @return Observable<HoleResponse<Any>>
     */
    @GET("/api-basis/remote/pharmacist/check")
    fun Shenhe(
        @Query("access_token") access_token: String,

        @Query("id") id: String,
        @Query("ischeck") ischeck: String,
        @Query("checkresult") checkresult: String
    ): Observable<HoleResponse<Any>>

    /***
     * 症状查询
     * @param access_token String
     * @param type String
     * @param shrm String
     * @return Observable<HoleResponse<Any>>
     */
    @GET("/api-basis/medicine/dictdiagquery")
    fun SympthomList(
        @Query("access_token") access_token: String,
        @Query("type") type: String,
        @Query("shrm") shrm: String
    ): Observable<HoleResponse<ArrayList<RecipeModlex.Others>>>

    /***
     * 用药频次接口
     * @param access_token String
     * @return Observable<HoleResponse<ArrayList<OnillModle>>>
     */
    @GET("/api-basis/medicine/dictfreq")
    fun yypc(
        @Query("access_token") access_token: String
    ): Observable<HoleResponse<ArrayList<RecipeModlex.Others>>>

    /***
     * 药品用法
     * @param access_token String
     * @return Observable<HoleResponse<ArrayList<OnillModle>>>
     */
    @GET("/api-basis/medicine/ypyf")
    fun ypyf(
        @Query("access_token") access_token: String
    ): Observable<HoleResponse<ArrayList<RecipeModlex.Others>>>

    /****
     * 修改开方接口（现在是重新开方）
     * @param access_token String
     * @param requ RecipeModlex
     * @return Observable<HoleResponse<RecipeModlex>>
     */
    @POST("/api-basis/remote/doctor/modifiyPres")
    fun UpdateKaifang(
        @Query("access_token") access_token: String,
        @Body requ: RecipeModlex

    ): Observable<HoleResponse<RecipeModlex>>

    /****
     *药品配送中有关时间查询接口
     * @param access_token String
     * @return Observable<HoleResponse<ArrayList<RecipeModlex.Others>>>
     */
    @GET("/api-basis/medicine/dictdays")
    fun riqi(
        @Query("access_token") access_token: String
    ): Observable<HoleResponse<ArrayList<RecipeModlex.Others>>>

    /***
     *获取快递类别
     * @param access_token String
     * @return Observable<HoleResponse<ArrayList<RecipeModlex.Others>>>
     */
    @GET("/api-basis/medicine/expresstype")
    fun GetExpress(
        @Query("access_token") access_token: String
    ): Observable<HoleResponse<ArrayList<RecipeModlex.Others>>>

    /***
     *  根据id 查询消息详情
     * @param id String
     * @return Observable<HoleResponse<MessageModle.MessageDetail>>
     */
    @GET("/api-desktop/pub/notices/getMessageInfo")
    fun GetMess(
        @Query("id") id: String
    ): Observable<HoleResponse<MessageModle.MessageDetail>>


}