package com.peaceclient.hospitaldoctor.com.Activity

import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.Gravity
import android.view.View
import android.widget.*
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jakewharton.rxbinding4.view.clicks
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.InterFace.MyGridDecorations
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.KeyboardUtils
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.RecipeModlex
import com.peaceclient.hospitaldoctor.com.modle.SymptomModle
import com.vondear.rxtools.view.RxToast
import com.vondear.rxtools.view.dialog.RxDialog
import com.vondear.rxtools.view.dialog.RxDialogSure
import io.reactivex.rxjava3.functions.Consumer
import kotlinx.android.synthetic.main.diagnosiss_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.math.BigDecimal
import java.util.concurrent.TimeUnit


/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/9/16 20:17
 * @change
 * @chang time
 * @class describe 修改诊断处方页面
 */


class UpdateDiagnosisAct : HoleBaseActivity() {
    var mhandler: Handler = object : Handler() {
        override fun handleMessage(msg: Message?) {
            when (msg?.what) {
                0 -> {
                    var recipeModlex: RecipeModlex = msg.obj as RecipeModlex
                    list.addAll(recipeModlex?.bills?.billDetails!!)
                    adapter?.notifyDataSetChanged()
                    if (recipeModlex!!.diags == null) {
                        layout.isSelected = false
                    } else {
                        layout.setText((recipeModlex!!.diags?.name ?: "") ?: "选择症状 >")
                        layout.isSelected = true
                        when (recipeModlex!!.diags?.prdiagtype) {
//                           1、西医诊断；2、中医病症；3、中医证候
                            "1" -> {
                                spinner.setSelection(0)
                                //zdType.setText("西医诊断")
                            }
                            "2" -> {
                                spinner.setSelection(1)
                            }
                        }
                        var str: String = recipeModlex!!.diags!!.name ?: ""
                        dia = recipeModlex!!.diags!!
                        if (recipeModlex?.others == null || recipeModlex?.others?.size == 0) {
                        } else {
                            for (i in 0..recipeModlex?.others?.size!! - 1) {
                                billDiagsList.add(recipeModlex?.others!![i])
                                Diaadapter?.notifyDataSetChanged()
                            }
                        }
                    }

                }
            }
        }
    }
    val SELECT_CODE = 1001 // 药品信息状态码
    val SELECT_ZZ = 1002 // 症状得状态码
    val SELECT_OTHER = 1003 // 其他症状得状态码
    var id: String? = null
    var appointid: String? = null
    var list: ArrayList<RecipeModlex.BillDetail> = arrayListOf() // 药品信息
    var billDiagsList: ArrayList<RecipeModlex.Others> = arrayListOf() // 其他症状列表
    var yypcNamelist: ArrayList<String> = arrayListOf()
    var yypclist: ArrayList<RecipeModlex.Others> = arrayListOf()
    var ypyfNamelist: ArrayList<String> = arrayListOf()
    var ypyflist: ArrayList<RecipeModlex.Others> = arrayListOf()
    var adapter: DoctorAdapter? = null
    var Diaadapter: BillDiagsAdapter? = null
    var type: String = "1"
    var code: String = ""
    var name: String = ""
    var isRemote = true
    val max: Int = 9
    var appoinid: String = ""
    var jlspinner: Spinner? = null
    var otherList: ArrayList<SymptomModle> = arrayListOf();
    // 暂定为中医诊断和西医诊断
    var types: ArrayList<String> = arrayListOf("中医诊断", "西医诊断")
    var recipeModlex: RecipeModlex = RecipeModlex()
    var dia: RecipeModlex.Others = RecipeModlex.Others();
    var others: ArrayList<RecipeModlex.Others> = arrayListOf();
    var bills: RecipeModlex.Bills = RecipeModlex.Bills();
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.diagnosiss_act)


        /* var linerma: LinearLayoutManager = LinearLayoutManager(this)
         // 药品列表
         recycle.layoutManager = linerma
         adapter = DoctorAdapter(R.layout.diagnosis_delete_item, list)
         recycle.adapter = adapter
         id = intent.getStringExtra("id")
         appointid = intent.getStringExtra("appointid")
         adapter?.notifyDataSetChanged()
         Diaadapter = BillDiagsAdapter(R.layout.dia_item, billDiagsList)
         var item: MyGridDecorations = MyGridDecorations()
         // 其他症状列表
         recy.adapter = Diaadapter
         var linermas = GridLayoutManager(this@UpdateDiagnosisAct, 3)
         linermas.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
             override fun getSpanSize(p0: Int): Int {
                 return setSpanSize(p0, billDiagsList)
             }
         }

         recy.layoutManager = linermas
         Diaadapter?.notifyDataSetChanged()*/

        getYPYF()
        getYYPC()
        remote.isSelected  = isRemote
        var linerma: LinearLayoutManager = LinearLayoutManager(this)
        // 药品列表
        recycle.layoutManager = linerma
        adapter = DoctorAdapter(R.layout.diagnosis_delete_item, list)
        recycle.adapter = adapter
        id = intent.getStringExtra("id")
        appointid = intent.getStringExtra("appointid")
        adapter?.notifyDataSetChanged()
        Diaadapter = BillDiagsAdapter(R.layout.dia_item, billDiagsList)
        var item: MyGridDecorations = MyGridDecorations()
        // 其他症状列表

        hole2.setOnClickListener {
            remote.isSelected = !isRemote
            isRemote = !isRemote
        }
        recy.adapter = Diaadapter
        var linermas = GridLayoutManager(this@UpdateDiagnosisAct, 3)
        linermas.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(p0: Int): Int {
                return setSpanSize(p0, billDiagsList)
            }
        }

        recy.layoutManager = linermas
        Diaadapter?.notifyDataSetChanged()
        Diaadapter?.setOnItemChildClickListener { adapter, view, position ->
            when (view) {
                adapter.getViewByPosition(recy, position, R.id.delete) -> {
                    billDiagsList.removeAt(position)
                    adapter.notifyItemRemoved(position)
                }
            }
        }

        save.clicks().throttleFirst(1500, TimeUnit.MICROSECONDS).subscribe(Consumer {
            KeyboardUtils.hideKeyboard(content)
            var dialog: RxDialogSure = RxDialogSure(this@UpdateDiagnosisAct)
            dialog.show()
            dialog.setTitle("提示")
            dialog.contentView.textAlignment = TextView.TEXT_ALIGNMENT_TEXT_START
            dialog.contentView.setText("\t1.不能开含有'麻醉、精神类药品和儿童处方药'等药品"+"\n\t2.按照互联网诊疗监管规定，处方尽量不要超过5个，否则会扣分。")
            dialog.setSureListener {
                dialog.dismiss()
                if (list.size == 0) {
                    RxToast.normal("请选择药品")
                } else {
                    if (vertiresult())
                        postKaiFang();

                }
            }
            dialog.setCanceledOnTouchOutside(false)

        })
        //将spinnertext添加到OnTouchListener对内容选项触屏事件处理
        adapter?.setOnItemChildClickListener { adapter, view, position ->
            when (view) {
                adapter.getViewByPosition(recycle, position, R.id.delete) -> {
                    list.removeAt(position)
                    adapter.notifyItemRemoved(position)
                }
//                adapter.getViewByPosition(recycle, position, R.id.jl) -> {
//                  initJlSpinners(view as Spinner, position)
//                }
//                adapter.getViewByPosition(recycle, position, R.id.yf) -> {
//              initJlSpinnery(view as Spinner, position)
//            }
            }

        }
        layout.clicks().throttleFirst(1500, TimeUnit.MICROSECONDS).subscribe(Consumer {
            KeyboardUtils.hideKeyboard(content)
            var intent = Intent(this@UpdateDiagnosisAct, SelectSympAct::class.java)
            intent.putExtra("type", type)
            startActivityForResult(intent, SELECT_ZZ)
        })
        qita.clicks().throttleFirst(1500, TimeUnit.MICROSECONDS).subscribe(Consumer {
            KeyboardUtils.hideKeyboard(content)

            var intent = Intent(this@UpdateDiagnosisAct, SelectSympAct::class.java)
            intent.putExtra("type", type)
            startActivityForResult(intent, SELECT_OTHER)

        })
        add_diag.setOnClickListener {

            startActivityForResult(Intent(this@UpdateDiagnosisAct, SelectMedicineAct::class.java), SELECT_CODE)
        }
        arrow_back.setOnClickListener {
            finish()
        }
        add_diag.clicks().throttleFirst(1500, TimeUnit.MICROSECONDS)
                .subscribe {
                    var intent = Intent(this@UpdateDiagnosisAct, SelectMedicineAct::class.java)
                    intent.putExtra("type", type)
                    startActivityForResult(intent, SELECT_CODE)
                }

        contents.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                zishus.setText((s?.length.toString() ?: 0.toString()) + "/30")
            }
        })

        spinner.prompt = "请选择诊断类型"
        spinner.dropDownVerticalOffset = resources.getDimensionPixelOffset(R.dimen.dp_27)
        spinner.dropDownHorizontalOffset = resources.getDimensionPixelOffset(R.dimen.dp_15)
        var spinnerAdapter: ArrayAdapter<String> = ArrayAdapter(this@UpdateDiagnosisAct, R.layout.spinner_select, types)
        spinnerAdapter.setDropDownViewResource(R.layout.item_drop_layout)
        spinner.adapter = spinnerAdapter
        spinner.setSelection(1)
        spinnerAdapter.notifyDataSetChanged()
        var selec: MyItemSelect = MyItemSelect()
        spinner.onItemSelectedListener = selec
        synchronized(this@UpdateDiagnosisAct) {

            getRecipList()
        }

    }

    private fun vertiresult(): Boolean {
        for (z in 0..list.size - 1) {
            if (list.get(z).unitcount.equals("")) {
                if (list.size > 1) {
                    RxToast.normal("请填写" + list[z].name + "的数量")
                } else {
                    RxToast.normal("请填写药品的数量")
                }
                return false
            } else if (list[z].percount.equals("")) {
                if (list.size > 1) {
                    RxToast.normal("请填写" + list[z].name + "的用量")
                } else {
                    RxToast.normal("请填写药品的用量")
                }

                return false
            }

        }
        return true;
    }


    private fun setSpanSize(position: Int, list: ArrayList<RecipeModlex.Others>): Int {
        var maxWidth = windowManager.defaultDisplay.width
        println(maxWidth.toString() + "宽度")
        var textView = View.inflate(this@UpdateDiagnosisAct, R.layout.dia_item, null).findViewById<TextView>(R.id.text)
        var itemWidth = textView.paint.measureText(billDiagsList.get(position).name)
        //int itemWidth = (int) (textView.getPaint().measureText(favoriteList.get(position).getName()));
        var count = 3
        if ((maxWidth / 2) / 3 > itemWidth) {
            count = 1;
        } else if ((maxWidth / 2) / 3 < itemWidth && maxWidth / 3 > itemWidth) {
            count = 2;
        } else if (maxWidth / 2 > itemWidth && maxWidth / 3 < itemWidth) {
            count = 3;
        }

        return count
    }

    private fun initJlSpinners(view: Spinner, position: Int, item: RecipeModlex.BillDetail) {
        //  view!!.prompt = "请选择剂量"
        var JlAdapter: ArrayAdapter<String> = ArrayAdapter(this@UpdateDiagnosisAct, R.layout.spinner_select, yypcNamelist)
        JlAdapter.setDropDownViewResource(R.layout.item_drop_layout)
        view.dropDownHorizontalOffset = resources.getDimensionPixelOffset(R.dimen.dp_44)
        view!!.adapter = JlAdapter
        JlAdapter.notifyDataSetChanged()
        var selecS: MyItemSelects = MyItemSelects(view, position, JlAdapter)
        view.onItemSelectedListener = selecS;
        if (yypclist.size > 0) {
            for (i in 0..yypclist.size - 1) {
                if (!TextUtils.isEmpty(item.howtouse)) {
                    if (item.howtouse.equals(yypclist[i].code)) {
                        view.setSelection(i)

                    }
                }
            }
        }


    }

    private fun initJlSpinnery(view: Spinner, position: Int, item: RecipeModlex.BillDetail) {
        // view!!.prompt = "请选择用法"
        var JlAdapter: ArrayAdapter<String> = ArrayAdapter(this@UpdateDiagnosisAct, R.layout.spinner_select, ypyfNamelist)
        JlAdapter.setDropDownViewResource(R.layout.item_drop_layout)
        view.dropDownHorizontalOffset = resources.getDimensionPixelOffset(R.dimen.dp_44)
        view!!.adapter = JlAdapter
        JlAdapter.notifyDataSetChanged()
        var selecS: MyItemSelectY = MyItemSelectY(view, position, JlAdapter)
        view.onItemSelectedListener = selecS;
        if (ypyflist.size > 0) {
            for (i in 0..ypyflist.size - 1) {
                if (!TextUtils.isEmpty(item.gytj)) {
                    if (item.gytj.equals(ypyflist[i].code)) {
                        println(item.howtouse + "]]--]" + yypclist[i].code)
                        println(ypyfNamelist[i] + "[[[[[")
                        view.setSelection(i)

                        //                            spinners.setSelection(i)
                    }
                }

            }
        }
    }

    fun vertify(): Boolean {
        if (TextUtils.isEmpty(contents.text)) {
            RxToast.normal("请输入您的诊断结果")
            return false
        }
        return true
    }

    internal inner class MyItemSelect : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            // messageList.clear()
            when (position) {
                0 -> {
                    type = "1"
                    // 中医诊断
                }
                1 -> {
                    type = "2"
                    //西医诊断

                }
            }
        }
    }

    internal inner class MyItemSelects(var spinner: Spinner, var pos: Int, var arrayAdapter: ArrayAdapter<String>) : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            // messageList.clear()
            // println(yypcNamelist[position] + "http")
            spinner.setSelection(position)
            // arrayAdapter.notifyDataSetChanged()
            name = yypclist.get(position).name
            code = yypclist.get(position).code
            list.get(pos).howtouse = code
            list.get(pos).freqname = name


        }
    }

    internal inner class MyItemSelectY(var spinner: Spinner, var pos: Int, var JlAdapter: ArrayAdapter<String>) : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            // messageList.clear()
            // println(yypcNamelist[position] + "https")
            spinner.setSelection(position)
            // JlAdapter.notifyDataSetChanged()
            name = ypyflist.get(position).xmmc
            code = ypyflist.get(position).ypyf
            list.get(pos).gytj = code
            list.get(pos).gytjname = name
            println(list.toString() + "gytj")
            //  println(yypcNamelist[position]+"zzzz")


        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == AppCompatActivity.RESULT_OK) {
            if (requestCode == SELECT_CODE) {
                list.add(data?.getSerializableExtra("date") as RecipeModlex.BillDetail)
                adapter?.notifyDataSetChanged()
            } else if (requestCode == SELECT_ZZ) {
                var sym = data?.getSerializableExtra("zz") as RecipeModlex.Others
                if (TextUtils.isEmpty(sym?.name)) {
                    layout.isSelected = false
                } else {
                    layout.setText(sym?.name + " >")
                    layout.isSelected = true

                }
                dia.name = sym.name
                dia.prdiagtype = type
                dia.code = sym.code
                dia.id = sym.id
            } else if (requestCode == SELECT_OTHER) {
                var sym = data?.getSerializableExtra("zz") as RecipeModlex.Others
                billDiagsList.add(sym)
                Diaadapter?.notifyDataSetChanged()

            }
        }
    }


    /**
     * 用药频次接口
     */
    fun getYYPC() {
        var rxdia: RxDialog = RxDialog(this@UpdateDiagnosisAct)
        rxdia.setContentView(R.layout.dialog_loading)
        rxdia.show()
        IpUrl.getInstance()!!.yypc(ConstantViewMolde.getToken())
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<RecipeModlex.Others>>> {
                    override fun onError(e: Throwable?) {
                        rxdia?.dismiss()
                    }

                    override fun onNext(t: HoleResponse<ArrayList<RecipeModlex.Others>>?) {
                        if (t?.code == 0) {
                            rxdia?.dismiss()
                            yypclist.addAll(t?.data!!)
                            for (o in 0..yypclist.size) {
                                yypcNamelist.add(yypclist.get(o).name)
                            }
//                            Diaadapter?.notifyDataSetChanged()
                            adapter?.notifyDataSetChanged()
                        } else {
                            RxToast.normal(t?.msg ?: "")

                        }

                    }

                    override fun onCompleted() {
//                        mhandler.sendEmptyMessage(0)
                        rxdia?.dismiss()
                    }
                })
    }

    /***
     * 药品用法接口请求
     */
    fun getYPYF() {
        var rxdia: RxDialog = RxDialog(this@UpdateDiagnosisAct)
        rxdia.setContentView(R.layout.dialog_loading)
        rxdia.show()
        IpUrl.getInstance()!!.ypyf(ConstantViewMolde.getToken())
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<RecipeModlex.Others>>> {
                    override fun onError(e: Throwable?) {
                        rxdia?.dismiss()
                    }

                    override fun onNext(t: HoleResponse<ArrayList<RecipeModlex.Others>>?) {
                        if (t?.code == 0) {
                            rxdia?.dismiss()
                            ypyflist.addAll(t?.data!!)
                            for (o in 0..ypyflist.size) {
                                ypyfNamelist.add(ypyflist.get(o).xmmc)
                            }
                            //todo adapter?.notifyDataSetChanged()
                            adapter?.notifyDataSetChanged()
                        } else {

                            RxToast.normal(t?.msg ?: "")
                        }
                    }

                    override fun onCompleted() {
//                        mhandler.sendEmptyMessage(0)
                        rxdia?.dismiss()
                    }
                })
    }

    /**
     * 处方详情接口
     * */
    fun getRecipList() {
        var rx: RxDialog = RxDialog(this@UpdateDiagnosisAct)
        rx.setContentView(R.layout.dialog_loading)
        rx.show()
        IpUrl.getInstance()!!.recipDetial(ConstantViewMolde.getToken(), id!!)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<HoleResponse<RecipeModlex>> {
                    override fun onError(e: Throwable?) {
                        rx.dismiss()
                    }

                    override fun onNext(t: HoleResponse<RecipeModlex>?) {
                        if (t?.code == 0) {
                            list.addAll(t?.data?.bills?.billDetails!!)
                            if (t?.data?.isCommon == 0){
                                isRemote = false
                            }else{
                                isRemote = true
                            }
                            remote.isSelected = isRemote
                            adapter?.notifyDataSetChanged()
                            if (t.data!!.diags == null) {
                                layout.isSelected = false
                            } else {
                                layout.setText((t.data!!.diags?.name ?: "") ?: "选择症状 >")
                                layout.isSelected = true
                                when (t.data!!.diags?.prdiagtype) {
//                           1、西医诊断；2、中医病症；3、中医证候
                                    "1" -> {
                                        spinner.setSelection(0)
                                        //zdType.setText("西医诊断")
                                    }
                                    "2" -> {
                                        spinner.setSelection(1)
                                    }
                                }
                                var str: String = t.data!!.diags!!.name ?: ""
                                dia = t.data!!.diags!!
                                if (t.data?.others == null || t.data?.others?.size == 0) {
                                } else {
                                    for (i in 0..t?.data?.others?.size!! - 1) {
                                        billDiagsList.add(t?.data?.others!![i])
                                        Diaadapter?.notifyDataSetChanged()
                                    }
                                }
                            }
                        }
                    }

                    override fun onCompleted() {
                        rx.dismiss()
                    }
                })
    }

    inner class DoctorAdapter(layoutResId: Int, data: ArrayList<RecipeModlex.BillDetail>?) : com.chad.library.adapter.base.BaseQuickAdapter<RecipeModlex.BillDetail, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: RecipeModlex.BillDetail) {
            helper.setText(R.id.result, item.name + item.spec + item.unit)
            var spinners = helper.getView<Spinner>(R.id.jl)
            var spinnery = helper.getView<Spinner>(R.id.yf)
            var zishu = helper.getView<EditText>(R.id.zishu)
            var yl = helper.getView<EditText>(R.id.yl)

            // 复现数量字段
            zishu.setText(item.unitcount ?: "")
            // 复现剂量字段
            yl.setText(item.percount ?: "")

//
            zishu.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    item.unitcount = s.toString();
                }
            })
            yl.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    item.percount = s.toString();
                }
            })
            //  var alyout = helper.getView<LinearLayout>(R.id.layout)

            helper.addOnClickListener(R.id.delete)
            spinners.dropDownVerticalOffset = resources.getDimensionPixelOffset(R.dimen.dp_32)
            spinners.gravity = Gravity.CENTER
            spinnery.dropDownVerticalOffset = resources.getDimensionPixelOffset(R.dimen.dp_32)
            spinnery.gravity = Gravity.CENTER
            initJlSpinners(spinners, helper.adapterPosition, item)
            initJlSpinnery(spinnery, helper.adapterPosition, item)
            //println(yypclist.toString() + "aa" + yypcNamelist.toString())
//            if (yypclist.size > 0) {
//                for (i in 0..yypclist.size - 1) {
//                    if (!TextUtils.isEmpty(item.howtouse)) {
//                        if (item.howtouse.equals(yypclist[i].code)) {
//                            println(item.howtouse + "]]]]]" + yypclist[i].code)
//                            spinners.prompt = yypcNamelist[i]
//                        }
//                    }
//                }
//            }
//            if (ypyflist.size > 0) {
//                for (i in 0..ypyflist.size - 1) {
//                    if (!TextUtils.isEmpty(item.gytj)) {
//                        if (item.gytj.equals(ypyflist[i].code)) {
//                            //println(item.howtouse + "]]]]]" + yypclist[i].code)
//                            spinnery.prompt = ypyfNamelist[i]
//                        }
//                    }
//
//                }
//            }
        }
    }

    inner class BillDiagsAdapter(layoutResId: Int, data: ArrayList<RecipeModlex.Others>?) : BaseQuickAdapter<RecipeModlex.Others, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder?, item: RecipeModlex.Others?) {
            helper?.setText(R.id.text, item?.name)
            helper?.addOnClickListener(R.id.delete)
        }
    }

    /***
     * 修改处方
     */
    fun postKaiFang() {
        var rx: RxDialog = RxDialog(this@UpdateDiagnosisAct)
        rx.setContentView(R.layout.dialog_loading)
        rx.show()
        recipeModlex.appointid = appointid ?: ""
        bills.appointid = appointid ?: ""
        if (isRemote){
            recipeModlex.isCommon = 1
        }else{
            recipeModlex.isCommon = 0
        }

        bills.billtype = "1"
        bills.billDetails = list
        for (i in 0..billDiagsList.size - 1) {
            billDiagsList[i].zxlb = type
        }
        recipeModlex.others = billDiagsList
        recipeModlex.diags = dia
        var fee: BigDecimal = BigDecimal(0)
        for (i in 0..list.size - 1) {
            fee = fee.plus(((list[i].price).toBigDecimal()) * ((list[i].unitcount).toBigDecimal()))
        }
        bills.fee = fee.toString()
        recipeModlex.bills = bills
        IpUrl.getInstance()!!.UpdateKaifang(ConstantViewMolde.getToken(), recipeModlex)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<HoleResponse<RecipeModlex>> {
                    override fun onError(e: Throwable?) {
                        rx.dismiss()
                    }

                    override fun onNext(t: HoleResponse<RecipeModlex>?) {
                        if (t?.code == 0) {
                            //RxToast.normal("开方成功")
                            setResult(100)
                            finish()
//                            list.addAll(t?.data?.bills?.billDetails!!)
//                            adapter?.notifyDataSetChanged()
//                            if (t.data!!.diags == null) {
//                            } else {
//                                layout.setText(t.data!!.diags?.name ?: "")
//                                layout.isSelected = false
//                                when (t.data!!.diags?.prdiagtype) {
////                           1、西医诊断；2、中医病症；3、中医证候
//                                    "1" -> {
//                                        spinner.setSelection(0)
//                                        //zdType.setText("西医诊断")
//                                    }
//
//
//                                    "2" -> {
//                                        spinner.setSelection(1)
//                                        //zdType.setText("中医病症")
//                                    }
//                                }
//                                var str: String = t.data!!.diags!!.name ?: ""
//                                if (t.data?.others == null || t.data?.others?.size == 0) {
//                                } else {
//                                    for (i in 0..t?.data?.others?.size!!) {
//                                        billDiagsList.add(t?.data?.others!![i])
//                                        Diaadapter?.notifyDataSetChanged()
//                                    }
//                                }
////                                var str: String = t.data!!.onlineBillDiags!!.diagname ?: ""
////                                if (!TextUtils.isEmpty(t.data!!.onlineBillDiags?.otherdiags)) {
////                                    var list: List<OnillModle> = JsonUtil.parseJson2List(t.data!!.onlineBillDiags?.otherdiags, OnillModle::class.java)
////                                    billDiagsList.addAll(list)
////                                    Diaadapter?.notifyDataSetChanged()
////                                }
//                            }
                        }
                    }


                    override fun onCompleted() {
                        rx.dismiss()
                    }
                })
    }

}


