/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.View;

import android.content.Context;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;

import com.vondear.rxtools.view.dialog.RxDialog;

/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.View
 * @class describe
 * @anthor admin
 * @time 2021/8/23 15:14
 * @change
 * @chang time
 * @class describe
 */

public class MyDialog extends RxDialog {
      public MyDialog(Context context, @LayoutRes int layoutResID, @Nullable Boolean cancelable,@Nullable Boolean touchOut) {
            super(context);
            setContentView(layoutResID);
            if (cancelable == null){
                  setCancelable(false);
            }else {
                  setCancelable(cancelable);
            }
            if (touchOut == null){
                  setCanceledOnTouchOutside(false);
            }else {
                  setCanceledOnTouchOutside(touchOut);
            }

      }

      public MyDialog(Context context) {
            super(context);
      }


}
