package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.SystemClock
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.TypedValue
import android.view.Gravity
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.JzCardModle
import com.vondear.rxtools.view.dialog.RxDialog
import com.vondear.rxtools.view.dialog.RxDialogSureCancel
import kotlinx.android.synthetic.main.drug_act.*



/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor222
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/10/12 18:05
 * @change
 * @chang time
 * @class describe 药品盘点
 */

class DruginventoryAct  :HoleBaseActivity(){
    var adapter :PeopleAdapter ?=null
    var list:ArrayList<JzCardModle> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.drug_act)
        arrow_back.setOnClickListener {
            finish()
        }
        var manager: LinearLayoutManager = LinearLayoutManager(this)
        recycle.layoutManager = manager
        adapter = PeopleAdapter(R.layout.drug_item, list)
        recycle.adapter = adapter
        drug_smart.autoRefresh()
        drug_smart.setOnRefreshListener {
            list.clear()
            initDate()
        }
        adapter?.setOnItemChildClickListener { adapter, view, position ->
            when(view){
                adapter.getViewByPosition(recycle,position,R.id.pandian) ->{
                    startActivity(Intent(this@DruginventoryAct,DrugSearAct::class.java))

                }
                adapter.getViewByPosition(recycle,position,R.id.btnDele) ->{
                   var RxDialog :RxDialog = RxDialog(this@DruginventoryAct)
                    RxDialog.setContentView(R.layout.dialog_loading)
                    RxDialog.show()
                    SystemClock.sleep(1000)
                    RxDialog.dismiss()
                    drug_smart.autoRefresh()
                }
            }

        }
        bottom_mask.setOnClickListener {
            dialogShows()
        }

    }
    private fun dialogShows() {
        var dialog: RxDialogSureCancel = RxDialogSureCancel(this@DruginventoryAct)
        dialog.setTitle("提示")
        dialog.setContent("您是否要穿件一份盘点清单？")
        dialog.contentView.gravity = Gravity.LEFT
        dialog.contentView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.contentView.setTextColor(Color.parseColor("#050505"))
        dialog.sureView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0f)
        dialog.cancelView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0f)
        dialog.contentView.gravity = Gravity.CENTER
        dialog.sureView.setTextColor(Color.parseColor("#376FE9"))
        dialog.cancelView.setTextColor(Color.parseColor("#376FE9"))
        dialog.titleView.setTextColor(Color.parseColor("#050505"))
        dialog.titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.setSure("否")
        dialog.setCancel("是")
        dialog.show()
        dialog.setSureListener({
            dialog.dismiss()
        })
        dialog.setCancelListener {
            drug_smart.autoRefresh()
            dialog.dismiss()
        }
    }
    private fun initDate() {
            for ( z in 0..1){
                var  jzCardModle:JzCardModle = JzCardModle()
                jzCardModle.fullName = z.toString() +"..."
                list.add(jzCardModle)
            }
            adapter?.notifyDataSetChanged()
            drug_smart.finishRefresh()
    }
    inner class PeopleAdapter(layoutResId: Int, data: ArrayList<JzCardModle>?) : com.chad.library.adapter.base.BaseQuickAdapter<JzCardModle, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: JzCardModle) {

            helper.addOnClickListener(R.id.btnDele).addOnClickListener(R.id.pandian)
        }

        override fun getItemCount(): Int {
            return 1
        }
    }

}