package com.peaceclient.hospitaldoctor.com.modle

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.modle
 * @class describe
 * @anthor admin
 * @time 2021/9/15 14:53
 * @change
 * @chang time
 * @class describe
 */

class VisitModle {

    private var count: Int = 0
    private var code: Int = 0
    private var data: List<DataBean>? = null

    fun getCount(): Int {
        return count
    }

    fun setCount(count: Int) {
        this.count = count
    }

    fun getCode(): Int {
        return code
    }

    fun setCode(code: Int) {
        this.code = code
    }

    fun getData(): List<DataBean>? {
        return data
    }

    fun setData(data: List<DataBean>) {
        this.data = data
    }

    class DataBean {
        /**
         * id : 1
         * attendanceId : 9
         * attendanceName : zhang
         * processId : 253
         * processName : 项目管理
         * coordinate : null
         * address : null
         * distance : null
         * image : null
         * time : null
         * page : null
         * limit : null
         */

        var id: Int = 0
        var attendanceId: Int = 0
        var attendanceName: String? = null
        var processId: Int = 0
        var processName: String? = null
        var coordinate: String? = null
        var address: String? = null
        var companyId: String? = null
        var companyName: String? = null
        var distance: Any? = null
        var image: String? = null
        var time: String? = null
        var page: Any? = null
        var limit: Any? = null
        var isDone: Boolean = false
        var timeZ: String?= null
        fun setDistance(distance: String) {
            this.distance = distance
        }
    }
}