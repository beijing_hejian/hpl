package com.peaceclient.hospitaldoctor.com.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog.Builder;

import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.easeui.utils.EaseUserUtils;
import com.peaceclient.hospitaldoctor.com.Hy.DemoHelper;
import com.peaceclient.hospitaldoctor.com.R;
import com.peaceclient.hospitaldoctor.com.View.CustomDialog;
import com.peaceclient.hospitaldoctor.com.cache.UserCacheManager;

import java.io.ByteArrayOutputStream;

public class UserProfileActivity extends BaseActivity implements OnClickListener {

      private static final int REQUESTCODE_PICK = 1;
      private static final int REQUESTCODE_CUTTING = 2;
      private ImageView headAvatar;
      private ImageView headPhotoUpdate;
      private ImageView iconRightArrow;
      private TextView tvNickName;
      private TextView txttitle;
      private TextView tvRemarkname;
      private TextView tvUsername;
      private ProgressDialog dialog;
      private RelativeLayout rlNickName;
      private RelativeLayout rlRemarkName;
      private RelativeLayout rlRecord;
      private CustomDialog dialog4;
      private CustomDialog dialog2;
      private String friend;

      private EMConversation conversation;


      @Override
      protected void onCreate(Bundle arg0) {
            super.onCreate(arg0);

            setContentView(R.layout.em_activity_user_profile);
            initView();
            initListener();

      }

      private void initView() {
            txttitle = (TextView) findViewById(R.id.txt_title);
            rlRemarkName = (RelativeLayout) findViewById(R.id.rl_remark);
            rlRecord = (RelativeLayout) findViewById(R.id.rl_record);
            headAvatar = (ImageView) findViewById(R.id.user_head_avatar);
            headPhotoUpdate = (ImageView) findViewById(R.id.user_head_headphoto_update);
            tvUsername = (TextView) findViewById(R.id.user_username);
            tvNickName = (TextView) findViewById(R.id.user_nickname);
            tvRemarkname = (TextView) findViewById(R.id.user_remark);
            rlNickName = (RelativeLayout) findViewById(R.id.rl_nickname);
            iconRightArrow = (ImageView) findViewById(R.id.ic_right_arrow);
      }

      private void initListener() {
            Intent intent = getIntent();
            String username = intent.getStringExtra("username");
            boolean isvisible = intent.getBooleanExtra("isvisible",true);
            System.out.println("isvisible"+isvisible);
            boolean enableUpdate = intent.getBooleanExtra("setting", false);
            if (enableUpdate) {
                  headPhotoUpdate.setVisibility(View.VISIBLE);
                  iconRightArrow.setVisibility(View.VISIBLE);
                  rlNickName.setOnClickListener(this);
                  headAvatar.setOnClickListener(this);
            } else {
                  headPhotoUpdate.setVisibility(View.GONE);
                  iconRightArrow.setVisibility(View.INVISIBLE);
            }

            if (username != null) {
                  if (username.equals(EMClient.getInstance().getCurrentUser())) {
                        rlRemarkName.setVisibility(View.GONE);
                        rlRecord.setVisibility(View.GONE);
                        tvUsername.setText(EMClient.getInstance().getCurrentUser());
                        EaseUserUtils.setUserNick(username, tvNickName);
                        EaseUserUtils.setUserAvatar(this, username, headAvatar);
                  } else {
                        friend = username;
                        rlRemarkName.setVisibility(View.VISIBLE);
                        rlRecord.setVisibility(View.GONE);
                        tvUsername.setText(username);
                        EaseUserUtils.setUserNick(username, tvNickName);
                        EaseUserUtils.setremark(username, tvRemarkname);
                        EaseUserUtils.setUserAvatar(this, username, headAvatar);
                        asyncFetchUserInfo(username);
                  }

            }
            if (isvisible){
                  rlRemarkName.setVisibility(View.VISIBLE);
            }else {
                  rlRemarkName.setVisibility(View.GONE);
            }
            // rlNickName.setOnClickListener(this);
            rlRemarkName.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        showiosDialog();
                  }
            });
            rlRecord.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        // todo
                        showiosDialog1();
                  }
            });
      }
//      protected void emptyHistory() {
//            conversation = EMClient.getInstance().chatManager().getConversation(toChatUsername, EaseCommonUtils.getConversationType(chatType), true);
//            String msg = getResources().getString(com.hyphenate.easeui.R.string.Whether_to_empty_all_chats);
//            new EaseAlertDialog(this, null, msg, null, new EaseAlertDialog.AlertDialogUser() {
//
//                  @Override
//                  public void onResult(boolean confirmed, Bundle bundle) {
//                        if (confirmed) {
//                              if (conversation != null) {
//                                    conversation.clearAllMessages();
//                              }
//                              messageList.refresh();
//                              haveMoreData = true;
//                        }
//                  }
//            }, true).show();


      private void showiosDialog1() {
//            if (dialog2 != null && dialog2.isShowing()) {
//                  dialog2.dismiss();
//            }
//            dialog2.show();
//            TextView tvsure = (TextView) dialog2.findViewById(R.id.tv_ok);
//            TextView tvcancle = (TextView) dialog2.findViewById(R.id.tv_cancle);
//            tvsure.setOnClickListener(new OnClickListener() {
//                  @Override
//                  public void onClick(View view) {
//                        //   CallBackUtils.ClearRecord();
//                        dialog2.dismiss();
//                  }
//            });
//            tvcancle.setOnClickListener(new OnClickListener() {
//                  @Override
//                  public void onClick(View view) {
//                        dialog2.dismiss();
//                  }
//            });
//
//
//            dialog2.show();
      }


      @Override
      public void onClick(View v) {
            switch (v.getId()) {
                  case R.id.user_head_avatar:
                        uploadHeadPhoto();
                        break;
                  case R.id.rl_nickname:
                        final EditText editText = new EditText(this);
                        new Builder(this).setTitle(R.string.setting_nickname).setIcon(android.R.drawable.ic_dialog_info).setView(editText)
                                  .setPositiveButton(R.string.dl_ok, new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                              String nickString = editText.getText().toString();
                                              if (TextUtils.isEmpty(nickString)) {
                                                    Toast.makeText(UserProfileActivity.this, getString(R.string.toast_nick_not_isnull), Toast.LENGTH_SHORT).show();
                                                    return;
                                              }
                                              updateRemoteNick(nickString);
                                        }
                                  }).setNegativeButton(R.string.dl_cancel, new DialogInterface.OnClickListener() {
                              @Override
                              public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                              }
                        }).show();
                        break;
                  default:
                        break;
            }

      }

      public void asyncFetchUserInfo(String username) {
            //  DemoHelper.getInstance().getUserProfileManager().asyncGetUserInfo(username, new EMValueCallBack<EaseUser>() {
            UserCacheManager.gets(username);
            //  @Override
            // public void onSuccess(EaseUser user) {
//                        if (user != null) {
//                              UserCacheManager.saves(user.getUsername(), user.getNick(), user.getAvatar(), user.getRemarkName());
//                              DemoHelper.getInstance().saveContact(user);
//                              if (isFinishing()) {
//                                    return;
//                              }
//                              if (!TextUtils.isEmpty(user.getRemarkName())) {
//                                    //rlRemarkName.setVisibility(View.VISIBLE);
//                                    tvRemarkname.setText(user.getRemarkName());
//                              } else {
//                                    tvNickName.setText(user.getNick());
//                                    tvRemarkname.setText(user.getRemarkName());
//                              }
//                              if (!TextUtils.isEmpty(user.getAvatar())) {
//                                    Glide.with(UserProfileActivity.this).load(user.getAvatar()).placeholder(R.drawable.em_default_avatar).into(headAvatar);
//                              } else {
//                                    Glide.with(UserProfileActivity.this).load(R.drawable.em_default_avatar).into(headAvatar);
//                              }
//                        }
            //  }

//                  @Override
//                  public void onError(int error, String errorMsg) {
//                  }
//            });
      }


      private void showiosDialog() {
//            if (dialog4 != null && dialog4.isShowing()) {
//                  dialog4.dismiss();
//            }
//            dialog4.show();
//            TextView tvOk = (TextView) dialog4.findViewById(R.id.tv_bind);
//            TextView tvbiaoti = (TextView) dialog4.findViewById(R.id.text);
//            TextView tvtext = (TextView) dialog4.findViewById(R.id.tv_cancl);
//            final TextView signs = (TextView) dialog4.findViewById(R.id.textsign);
//            tvtext.setText("取消");
//            tvbiaoti.setText("修改备注信息");
//            signs.setHint("修改备注信息");
//            signs.setHintTextColor(getResources().getColor(R.color.nnn));
//            tvOk.setOnClickListener(new OnClickListener() {
//                  @Override
//                  public void onClick(View view) {
//                        if (TextUtils.isEmpty(signs.getText().toString())) {
//                              Toast.makeText(UserProfileActivity.this, "备注不能为空", Toast.LENGTH_SHORT).show();
//                        } else {
//                              postDate(signs.getText().toString());
//                              dialog4.dismiss();
//                        }
//
//                  }
//            });
//            tvtext.setOnClickListener(new OnClickListener() {
//                  @Override
//                  public void onClick(View v) {
//                        dialog4.dismiss();
//                  }
//            });
//            dialog4.show();
//      }
//
//      private void postDate(final String s2) {
//            NetBaseUtil.getInstance().updateRemark( friend, EMClient.getInstance().getCurrentUser(), s2).subscribeOn(Schedulers.io())
//                      .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<FriendRemark>() {
//                  @Override
//                  public void onCompleted() {
//
//                  }
//
//                  @Override
//                  public void onError(Throwable e) {
//                        Toast.makeText(UserProfileActivity.this, "修改备注失败", Toast.LENGTH_SHORT).show();
//
//                  }
//
//                  @Override
//                  public void onNext(FriendRemark friendRemark) {
//                        if (friendRemark.getResp_code() == 0) {
//                              Toast.makeText(UserProfileActivity.this, "修改备注成功", Toast.LENGTH_SHORT).show();
//                              tvRemarkname.setText(s2);
//                              txttitle.setText(s2);
//                              asyncFetchUserInfo(friend);
//                              CallBackUtils.changeTitle(s2);
//                        }else{
//                              Toast.makeText(UserProfileActivity.this, "修改备注失败", Toast.LENGTH_SHORT).show();
//                        }
//                  }
//            });
//
//
////            BeizhuBean ben = new BeizhuBean();
////            ben.setPossessPhone(EMClient.getInstance().getCurrentUser());
////            ben.setFriendPhone(friend);
////            ben.setRemarkName(s2);
////            OkHttpUtils.post(NetUrl.Url + NetUrl.XIUGAIBEIZHU).upJson(JsonUtil.tojson(ben)).execute(new StringCallback() {
////                  @Override
////                  public void onSuccess(String s, Call call, Response response) {
////                        if (response.code() == 200) {
////                              NickAvterResponse nickAvterResponse = JsonUtil.parseJsonToBean(s, NickAvterResponse.class);
////                              if (nickAvterResponse.getCode().equals("0")) {
////                                    Toast.makeText(UserProfileActivity.this, "修改备注成功", Toast.LENGTH_SHORT).show();
////                                    tvRemarkname.setText(s2);
////                                    txttitle.setText(s2);
////                                    asyncFetchUserInfo(friend);
////                                   CallBackUtils.changeTitle(s2);
////
////                              }
////                        }
////                  }
////            });

      }

      private void uploadHeadPhoto() {
            Builder builder = new Builder(this);
            builder.setTitle(R.string.dl_title_upload_photo);
            builder.setItems(new String[]{getString(R.string.dl_msg_take_photo), getString(R.string.dl_msg_local_upload)},
                      new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                  dialog.dismiss();
                                  switch (which) {
                                        case 0:
                                              Toast.makeText(UserProfileActivity.this, getString(R.string.toast_no_support),
                                                        Toast.LENGTH_SHORT).show();
                                              break;
                                        case 1:
                                              Intent pickIntent = new Intent(Intent.ACTION_PICK, null);
                                              pickIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
                                              startActivityForResult(pickIntent, REQUESTCODE_PICK);
                                              break;
                                        default:
                                              break;
                                  }
                            }
                      });
            builder.create().show();
      }


      private void updateRemoteNick(final String nickName) {
            dialog = ProgressDialog.show(this, getString(R.string.dl_update_nick), getString(R.string.dl_waiting));
            new Thread(new Runnable() {

                  @Override
                  public void run() {
                        boolean updatenick = DemoHelper.getInstance().getUserProfileManager().updateCurrentUserNickName(nickName);
                        if (UserProfileActivity.this.isFinishing()) {
                              return;
                        }
                        if (!updatenick) {
                              runOnUiThread(new Runnable() {
                                    public void run() {
                                          Toast.makeText(UserProfileActivity.this, getString(R.string.toast_updatenick_fail), Toast.LENGTH_SHORT)
                                                    .show();
                                          dialog.dismiss();
                                    }
                              });
                        } else {
                              runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                          dialog.dismiss();
                                          Toast.makeText(UserProfileActivity.this, getString(R.string.toast_updatenick_success), Toast.LENGTH_SHORT)
                                                    .show();
                                          tvNickName.setText(nickName);
                                    }
                              });
                        }
                  }
            }).start();
      }

      @Override
      protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            switch (requestCode) {
                  case REQUESTCODE_PICK:
                        if (data == null || data.getData() == null) {
                              return;
                        }
                        startPhotoZoom(data.getData());
                        break;
                  case REQUESTCODE_CUTTING:
                        if (data != null) {
                              setPicToView(data);
                        }
                        break;
                  default:
                        break;
            }
            super.onActivityResult(requestCode, resultCode, data);
      }

      public void startPhotoZoom(Uri uri) {
            Intent intent = new Intent("com.android.camera.action.CROP");
            intent.setDataAndType(uri, "image/*");
            intent.putExtra("crop", true);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("outputX", 300);
            intent.putExtra("outputY", 300);
            intent.putExtra("return-data", true);
            intent.putExtra("noFaceDetection", true);
            startActivityForResult(intent, REQUESTCODE_CUTTING);
      }

      /**
       * save the picture data
       *
       * @param picdata
       */
      private void setPicToView(Intent picdata) {
            Bundle extras = picdata.getExtras();
            if (extras != null) {
                  Bitmap photo = extras.getParcelable("data");
                  Drawable drawable = new BitmapDrawable(getResources(), photo);
                  headAvatar.setImageDrawable(drawable);
                  uploadUserAvatar(Bitmap2Bytes(photo));
            }

      }

      private void uploadUserAvatar(final byte[] data) {
            dialog = ProgressDialog.show(this, getString(R.string.dl_update_photo), getString(R.string.dl_waiting));
            new Thread(new Runnable() {

                  @Override
                  public void run() {
                        final String avatarUrl = DemoHelper.getInstance().getUserProfileManager().uploadUserAvatar(data);
                        runOnUiThread(new Runnable() {
                              @Override
                              public void run() {
                                    dialog.dismiss();
                                    if (avatarUrl != null) {
                                          Toast.makeText(UserProfileActivity.this, getString(R.string.toast_updatephoto_success),
                                                    Toast.LENGTH_SHORT).show();
                                    } else {
                                          Toast.makeText(UserProfileActivity.this, getString(R.string.toast_updatephoto_fail),
                                                    Toast.LENGTH_SHORT).show();
                                    }

                              }
                        });

                  }
            }).start();

            dialog.show();
      }


      public byte[] Bitmap2Bytes(Bitmap bm) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
            return baos.toByteArray();
      }


}
