/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.modle

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.modle
 * @class describe
 * @anthor admin
 * @time 2021/8/25 16:11
 * @change
 * @chang time
 * @class describe
 */

class KnowleModle {
    val id: Int? = null
    var category: Int? = null
    var position: Int? = null
    var contentType: Int? = null
    var status: Int? = null
    var title: String? = null
    var ftitle: String? = null
    var imgUrl: String? = null
    var author: String? = null
    var source: String? = null
    var content: String? = null
    var publishTime: String? = null
    override fun toString(): String {
        return "KnowleModle(id=$id, category=$category, position=$position, contentType=$contentType, status=$status, title=$title, ftitle=$ftitle, imgUrl=$imgUrl, author=$author, source=$source, content=$content, publishTime=$publishTime)"
    }

}