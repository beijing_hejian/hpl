package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import kotlinx.android.synthetic.main.otherevalue_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor222
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/10/13 15:28
 * @change
 * @chang time
 * @class describe 员工考评页面
 */

class OtherEvalueAct  :HoleBaseActivity(){
    private var adapter: DoctorAdapter? = null
    private var doctorList: ArrayList<String>? = arrayListOf()
    private var heartList: ArrayList<Int>? = arrayListOf()
    private var id :String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.otherevalue_act)
        var linerlay: LinearLayoutManager = LinearLayoutManager(this)
        elec_recycle.layoutManager = linerlay as RecyclerView.LayoutManager?
        adapter = DoctorAdapter(R.layout.otherevalue_item, doctorList)
        elec_recycle.adapter = adapter
        adapter?.notifyDataSetChanged()
        smart.autoRefresh()
        smart.setOnRefreshListener {
            doctorList?.clear()
            getValue()
        }
        arrow_back.setOnClickListener {
            finish()
        }
        adapter?.setOnItemClickListener { adapter, view, position ->
          var inte =   Intent(this@OtherEvalueAct,YuanListActs::class.java)
            inte.putExtra("s",doctorList?.get(position))
            startActivity(inte)

        }
    }
    internal inner class DoctorAdapter(layoutResId: Int, data: ArrayList<String>?) : com.chad.library.adapter.base.BaseQuickAdapter<String, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: String) {
            helper.setText(R.id.time, (item?: "")+"年度")
        }
    }

    /***
     * 员工年度列表数据
     */
    fun getValue() {
        IpUrl.getInstance()!!.ndList(ConstantViewMolde.getToken())
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<String>>> {
                    override fun onError(e: Throwable?) {
                        // rxdia?.dismiss()
                        smart.finishRefresh()
                    }
                    override fun onNext(t: HoleResponse<ArrayList<String>>) {
                        doctorList?.addAll(t?.data!!)
                        adapter?.notifyDataSetChanged()
                    }
                    override fun onCompleted() {
                        smart.finishRefresh()
                    }
                })
    }
}