package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle

import android.text.TextUtils
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.BaseViewHolder
import com.jakewharton.rxbinding4.view.clicks
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.RecipeModlex
import com.vondear.rxtools.view.dialog.RxDialog
import kotlinx.android.synthetic.main.diag_detial_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit


/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor222
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/11/6 17:32
 * @change
 * @chang time
 * @class describe 处方详情待审核页面
 */

class DiagonsisDetialAct : HoleBaseActivity() {
    var id: String? = null
    var appointid: String? = null
    private var medicineList = arrayListOf<RecipeModlex.BillDetail>()
    private var adapter: PeopleAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.diag_detial_act)
        arrow_back.setOnClickListener {
            finish()
        }
        var manager: LinearLayoutManager = LinearLayoutManager(this)
        id = intent.getStringExtra("id")
        appointid = intent.getStringExtra("appid")
        recycle.layoutManager = manager
        adapter = PeopleAdapter(R.layout.diagshen_item, medicineList)
        recycle.adapter = adapter
        adapter?.notifyDataSetChanged()
        getRecipList()
        save.clicks().throttleFirst(1500, TimeUnit.MICROSECONDS)
                .subscribe {
                    var intent: Intent = Intent(this@DiagonsisDetialAct, UpdateDiagnosisAct::class.java)
                    intent.putExtra("id", id)
                    intent.putExtra("appointid", appointid)
                    startActivityForResult(intent, 100)
                }

    }

    /***
     * 根据id 获取处方详细信息
     */
    fun getRecipList() {
        var rx: RxDialog = RxDialog(this@DiagonsisDetialAct)
        rx.setContentView(R.layout.dialog_loading)
        rx.show()
        IpUrl.getInstance()!!.recipDetial(ConstantViewMolde.getToken(), id!!)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<HoleResponse<RecipeModlex>> {
                    override fun onError(e: Throwable?) {
                        rx.dismiss()
                    }

                    override fun onNext(t: HoleResponse<RecipeModlex>?) {
                        if (t?.code == 0) {
                            medicineList.addAll(t?.data?.bills?.billDetails!!)
                            adapter?.notifyDataSetChanged()
                            name.setText(t?.data?.bills?.checkdocname ?: "")
                            if (!TextUtils.isEmpty(t?.data?.bills?.billtime)) {
                                time.setText(t?.data?.bills?.billtime)
                            }
                            if (TextUtils.isEmpty(t?.data?.bills?.checkresult)) {
                                zishu.setText("0/30")
                            } else {
                                zishu.setText(t?.data?.bills?.checkresult!!.length.toString() + "/30")
                                notice.setText(t?.data?.bills?.checkresult ?: "")
                            }
                            if (t.data!!.bills?.ischeck == "0") {
                                yijian.setText("待审核")
                                save.visibility = View.GONE
                                yijian.setTextColor(Color.parseColor("#EFCA1C"))
                            } else if (t.data!!.bills?.ischeck == "1") {
                                yijian.setText("已通过")
                                save.visibility = View.GONE
                                yijian.setTextColor(Color.parseColor("#E71D1D"))
                            } else {
                                yijian.setText("未通过")
                                save.visibility = View.VISIBLE
                                yijian.setTextColor(Color.parseColor("#E71D1D"))
                            }
                            if (t.data!!.diags == null) {
                                zdType.setText("")
                                result.setText("")
                            } else {
                                when (t.data!!.diags?.prdiagtype) {
//                           1、西医诊断；2、中医病症；3、中医证候
                                    "1" -> {
                                        zdType.setText("西医诊断")
                                    }
                                    "2" -> {
                                        zdType.setText("中医诊断")
                                    }
                                    "" -> {
                                        zdType.setText("")
                                    }
                                }
//
                                var str: String = t.data?.diags?.name ?: ""
                                if (t.data?.others == null || t.data?.others?.size == 0) {
                                } else {
                                    for (i in 0..t?.data?.others?.size!! - 1) {
                                        str + "、" + t?.data?.others!![i].name;
                                        str = str + "、" + t?.data?.others!![i].name;
                                    }
                                }
                                result.setText(str)
                            }
//
                        }
                    }

                    override fun onCompleted() {
                        rx.dismiss()
                    }
                })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 100) {
            finish()
        }
    }

    inner class PeopleAdapter(layoutResId: Int, data: ArrayList<RecipeModlex.BillDetail>?) : com.chad.library.adapter.base.BaseQuickAdapter<RecipeModlex.BillDetail, BaseViewHolder>(layoutResId, data) {

        override fun convert(helper: BaseViewHolder, item: RecipeModlex.BillDetail) {
//            var yao: String = (item.ypmc ?: "") + (item.ypgg ?: "" + ":") + (item.kfsm ?: "")
//            helper.setText(R.id.text, yao)


            helper.setText(R.id.yptext, item.name)
                    .setText(R.id.ggtext, item.spec)
                    .setText(R.id.dwtext, item.unit)
                    .setText(R.id.yytext, item.freqname)
                    .setText(R.id.sltext, item.unitcount)
                    .setText(R.id.yftext, item.gytjname)
                    .setText(R.id.yltext, item.percount)
        }
    }

}