/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.TwModle
import com.scwang.smartrefresh.layout.listener.OnRefreshListener
import kotlinx.android.synthetic.main.twhis_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import java.util.*

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/9/1 11:22
 * @change
 * @chang time
 * @class describe 图文接诊记录（废弃）
 */

class TwHisAct : HoleBaseActivity() {
    private var messageList: ArrayList<TwModle> = arrayListOf()
    private var kong: View? = null
    private var page = 1
    private var weeks: String = "1"
    private var adapter: PeopleAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.twhis_act)
        search_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (TextUtils.isEmpty(s)) {
                    messageList?.clear()
                    getList()
                    //GetCheckHis(prosmarts)
                } else {
                    messageList?.clear()
                    getList()
                    // GetCheckHis(prosmarts)
                }
            }
        })
        arrow_back.setOnClickListener {
            finish()
        }
        spinner.prompt = "请选择时间"
        var spinnerAdapter: ArrayAdapter<CharSequence> = ArrayAdapter.createFromResource(this@TwHisAct, com.peaceclient.hospitaldoctor.com.R.array.week, R.layout.simple_spinner_item)
        spinnerAdapter.setDropDownViewResource(R.layout.drop_item_layout)
        spinner.adapter = spinnerAdapter
        spinnerAdapter.notifyDataSetChanged()
        var selec: MyItemSelect = MyItemSelect()
        spinner.onItemSelectedListener = selec
        //将spinnertext添加到OnTouchListener对内容选项触屏事件处理
        println(spinner.measuredHeight.toString() + "222" + spinner.measuredWidth)
        kong = View.inflate(Myapplication.mcontext, R.layout.empty_view, null)
        val manager = LinearLayoutManager(Myapplication.mcontext, LinearLayoutManager.VERTICAL, false)
        tw_recycle.setLayoutManager(manager)
        adapter = PeopleAdapter(R.layout.medicine_his_item, messageList)
        adapter?.bindToRecyclerView(tw_recycle)
        adapter?.setEmptyView(kong)
        adapter?.isUseEmpty(true)
        tw_recycle.setAdapter(adapter)
        adapter?.notifyDataSetChanged()
        prosmarts.autoRefresh()
        prosmarts.isEnableLoadmore = false
        prosmarts.setOnRefreshListener(OnRefreshListener {
            page = 1
            messageList?.clear()
            getList();
            //GetCheckHis(prosmarts)
            // initDate()
        })
//        prosmarts.setOnLoadmoreListener {
//            page += 1
//            // LoadgetMessage(prosmarts)
//            initDate()
//        }
        adapter?.setOnItemChildClickListener { adapter, view, position ->
            when (view) {
                adapter.getViewByPosition(tw_recycle, position, R.id.result) -> {
                  /*  var intent: Intent = Intent(this@TwHisAct, DiagnosisActs::class.java)
                    intent.putExtra("id", messageList?.get(position)?.id ?: "")
                    startActivity(intent)*/
                }
                adapter.getViewByPosition(tw_recycle, position, R.id.weista) -> {
                    var innt: Intent = Intent(Myapplication.mcontext, VideoReceiAct::class.java)
                    innt.putExtra("id", messageList.get(position).id)
                    startActivity(innt)
                }
            }
        }
    }

//    private fun initDate() {
//        for (z in 0..7) {
//            var peopleModle: PeopleModle = PeopleModle()
//            peopleModle.id = z
//            peopleModle.idcard = "130431199110100010"
//
//            if (z % 2 == 0) {
//                peopleModle.isSelect = false
//            } else {
//                peopleModle.isSelect = true
//            }
//            peopleModle.name = "z" + z
//            peopleModle.phone = "17611481698"
//            messageList.add(peopleModle)
//            adapter?.notifyDataSetChanged()
//            prosmarts.finishLoadmore()
//            prosmarts.finishRefresh()
//        }
//    }

    internal inner class MyItemSelect : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            messageList.clear()
            when (position) {
                0 -> {
                    weeks = "1"
                    getList()
                }
                1 -> {
                    weeks = "2"
                    getList()
                }
                2 -> {
                    weeks = "3"
                    getList()
                }
                3 -> {
                    weeks = "4"
                    getList()
                }
                4 -> {
                    weeks = "12"
                    getList()
                }
                5 -> {
                    weeks = "24"
                    getList()
                }

            }
        }

    }

    inner class PeopleAdapter(layoutResId: Int, data: java.util.ArrayList<TwModle>?) : com.chad.library.adapter.base.BaseQuickAdapter<TwModle, BaseViewHolder>(layoutResId, data) {

        override fun convert(helper: BaseViewHolder, item: TwModle) {
            helper.getView<TextView>(R.id.name).setText(item?.resName ?: "")
            helper.getView<TextView>(R.id.time).setText(item?.createTime ?: "")
            helper.addOnClickListener(R.id.result)
                    .addOnClickListener(R.id.weista)
            var sta = helper.getView<TextView>(R.id.status);
            var res = helper.getView<TextView>(R.id.result);
            var weista = helper.getView<TextView>(R.id.weista);


            if (item?.orderStatus == "8") {
                sta.visibility = View.GONE
                res.visibility = View.GONE
                weista.visibility = View.VISIBLE

            } else {
                sta.visibility = View.VISIBLE
                res.visibility = View.VISIBLE
                weista.visibility = View.GONE
            }


        }
    }

    fun getList() {
        IpUrl.getInstance()!!.getTwHis(ConstantViewMolde.getToken(), search_name.text.toString(), weeks)
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<TwModle>>> {
                    override fun onError(e: Throwable?) {
                        prosmarts?.finishRefresh()
                    }

                    override fun onNext(t: HoleResponse<ArrayList<TwModle>>?) {
                        if (t?.code == 0) {
                            if (t?.data != null) {
                                messageList.addAll(t?.data!!)
                                adapter?.notifyDataSetChanged()
                            }
                        }
                    }

                    override fun onCompleted() {
                        prosmarts?.finishRefresh()
                    }
                })
    }
}