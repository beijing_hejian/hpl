package com.peaceclient.hospitaldoctor.com.Activity

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.jakewharton.rxbinding4.view.clicks
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.KeyboardUtils
import com.peaceclient.hospitaldoctor.com.Utils.TimeFormatUtils
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import io.reactivex.rxjava3.functions.Consumer
import kotlinx.android.synthetic.main.persongood_act.*

import java.util.concurrent.TimeUnit

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/9/16 11:51
 * @change
 * @chang time
 * @class describe 个人专长页面
 */

class PersonGoodAct  : HoleBaseActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.persongood_act)
        save.clicks().throttleFirst(1500,TimeUnit.MICROSECONDS).subscribe(Consumer {
            KeyboardUtils.hideKeyboard(content)
            println("保存"+TimeFormatUtils.ms2Date(System.currentTimeMillis()))
        })

        arrow_back.setOnClickListener {
            finish()
        }
        content.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
            override fun afterTextChanged(s: Editable?) {
                zishu.setText(((s?.length.toString() ?:0 .toString()))+"/50")
            }
        })
        content.setText(ConstantViewMolde?.GetUser()?.user?.goodAt?:"")

    }
}