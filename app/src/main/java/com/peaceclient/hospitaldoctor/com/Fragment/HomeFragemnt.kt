/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Fragment


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message
import androidx.viewpager.widget.ViewPager

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.ViewFlipper
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.paradoxie.autoscrolltextview.VerticalTextview
import com.peaceclient.hospitaldoctor.com.Activity.*
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.InterFace.*
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.adapter.HomeBannerAdapter
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.KnowleModle
import com.peaceclient.hospitaldoctor.com.modle.LevelBean
import com.vondear.rxtools.view.RxToast
import kotlinx.android.synthetic.main.homes_fragment.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.lang.reflect.Proxy

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/7/12 15:59
 * @change
 * @chang time
 * @class describe 首页（废弃）
 */

class HomeFragemnt : BaseLazyFragment(), ILaunchManagerService, OnBannerClicker, CancleBack {
    override fun StateChange(s: Int) {}
    override fun loginsuccess(intent: Intent?) {
        startActivity(intent)
    }

    override fun onImageclick(view: View?, position: Int) {
        var intent: Intent = Intent(Myapplication.mcontext, NewsDetailAct::class.java)
        intent.putExtra("id", listBanner?.get(position)?.id?.toString() ?: "")
        startActivity(intent)
    }

    private var rootview: View? = null
    private var viewFlipper: ViewFlipper? = null
    private var homeadapter: HomeBannerAdapter? = null
    private var launchManagerService: ILaunchManagerService? = null
    private var verticalTextview2: VerticalTextview? = null
    private var verticalTextview1: VerticalTextview? = null
    val mHadler: Handler = object : Handler() {
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            when (msg?.what) {
                0 -> {
                    if (home_banner == null) {

                    } else {
                        if (home_banner?.currentItem == Int.MAX_VALUE) {
                            home_banner?.currentItem = 0
                        } else {
                            home_banner?.currentItem = home_banner!!.currentItem + 1
                        }
                    }
                    sendEmptyMessageDelayed(0, 2500)
                }

            }
        }
    }
    var listImage: MutableList<Int>? = mutableListOf()
    var listImages: Array<Int?>? = arrayOfNulls<Int>(5)
    var listBanner: MutableList<KnowleModle>? = mutableListOf()
    var listZIXUN: ArrayList<KnowleModle>? = arrayListOf()
    var arrayes = arrayOf(R.drawable.home_date, R.drawable.chufang_icon, R.drawable.home_evalue)
    //   var array = arrayOf(R.drawable.home_1, R.drawable.home_2, R.drawable.home_3, R.drawable.home_4, R.drawable.home_5, R.drawable.home_6, R.drawable.home_7, R.drawable.home_8)
    var arraye = arrayOf(R.drawable.home_remote, R.drawable.home_video, R.drawable.home_medicine)
    var arrays = arrayOf("预约随访", "我的开方", "患者评价")
    var arrayet = arrayOf("处方审核", "远程接诊", "药品配送")
    val fragment: HomeFragemnt? = null
    override fun lazyLoad() {}
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootview = layoutInflater.inflate(R.layout.homes_fragment, null)
        var list: MutableList<LevelBean>? = mutableListOf()
        var listOne: MutableList<LevelBean>? = mutableListOf()
        var homebanner = rootview?.findViewById(R.id.home_banner) as ViewPager
        var health: RelativeLayout? = rootview?.findViewById<RelativeLayout>(R.id.health_layout)
        var fengcai: RelativeLayout? = rootview?.findViewById<RelativeLayout>(R.id.fengcai_layout)
        launchManagerService = Proxy.newProxyInstance(javaClass.classLoader, arrayOf<Class<*>>(ILaunchManagerService::class.java), LaunchInvocationHandler(this, Myapplication.mcontext)) as ILaunchManagerService
        CallBackUtils.setCancleBack(this)
        for (intz in 0..arrayet.size - 1) {
            var bean: LevelBean = LevelBean()
            bean.iconid = arraye[intz]
            bean.desc = arrayet[intz]
            listOne!!.add(bean)
        }
        for (intz in 0..1) {
            var bean: LevelBean = LevelBean()
            bean.iconid = arrayes[intz + 1]
            bean.desc = arrays[intz + 1]
            list!!.add(bean)
        }
        health!!.setOnClickListener {
            var intent: Intent = Intent(Myapplication.mcontext, NewsListAct::class.java)
            intent.putExtra("type", "3")
            startActivity(intent)
        }
        fengcai!!.setOnClickListener {
            var intent: Intent = Intent(Myapplication.mcontext, NewsListAct::class.java)
            intent.putExtra("type", "4")
            startActivity(intent)
        }

        var manager: GridLayoutManager = GridLayoutManager(Myapplication.mcontext, 3)
        var itemdeco: MyGridItemDecoration = MyGridItemDecoration()
        var recycle = rootview?.findViewById<RecyclerView>(R.id.recycle)
        recycle!!.layoutManager = manager
        recycle!!.addItemDecoration(itemdeco)
        var deadapter: DepartAdapter = DepartAdapter(R.layout.item_home, listOne)
        recycle!!.adapter = deadapter
        deadapter.notifyDataSetChanged()
        deadapter.setOnItemClickListener { adapter, view, position ->
            when (position) {
                0 -> {
                    //医生标志为1


                    print(ConstantViewMolde.GetUser()!!.user?.personType.toString() +"DOC")
                    if (ConstantViewMolde.GetUser()!!.user?.personType == 1) {
                        RxToast.normal("您不是药剂师无法对处方进行审核")
                    } else {
                        startActivity(Intent(Myapplication.mcontext, DiagnosisListAct::class.java))
                    }

                    //startActivity(Intent(Myapplication.mcontext, TestActivity::class.java))
//                    var inten: Intent = Intent(Myapplication.mcontext, DepartSelectAct::class.java)
//                    var bundle: Bundle = Bundle();
//                    inten.putExtra(Myapplication.SHARED_NAME, bundle)
//                    launchManagerService?.startActivity(inten)

                }
                1 -> {
                    if (ConstantViewMolde.GetUser()!!.user?.personType == 2) {
                        RxToast.normal("您不是医生无法对进行远程接诊")
                    } else {
                        startActivity(Intent(Myapplication.mcontext, RemoteCliAct::class.java))
                    }

//                    var inten: Intent = Intent(Myapplication.mcontext, OutpatientPayAct::class.java)
//                    var bundle: Bundle = Bundle();
//                    inten.putExtra(Myapplication.SHARED_NAME, bundle)
//                    launchManagerService?.startActivity(inten)
                }

                2 -> {
                    startActivity(Intent(Myapplication.mcontext, MedicineListAct::class.java))
//                    var inten: Intent = Intent(Myapplication.mcontext, MedicalCloudAct::class.java)
//                    var bundle: Bundle = Bundle();
//                    inten.putExtra(Myapplication.SHARED_NAME, bundle)
//                    launchManagerService?.startActivity(inten)
                }
            }
        }

        var managers: GridLayoutManager = GridLayoutManager(Myapplication.mcontext, 3)
        var itemdecos: MyGridItemDecoration = MyGridItemDecoration()
        var recyclew = rootview?.findViewById<RecyclerView>(R.id.home_grid)
        recyclew!!.layoutManager = managers
        recyclew!!.addItemDecoration(itemdecos)
        var deadapters: DepartAdapter = DepartAdapter(R.layout.item_home, list)
        recyclew!!.adapter = deadapters
        deadapters.notifyDataSetChanged()
        deadapters.setOnItemClickListener { adapter, view, position ->
            when (position) {
                0 -> {
                    //进入我的开方

//                    1-> {
//                        startActivity(Intent(Myapplication.mcontext,SuifangAct::class.java))
//                    }
//                    2 -> {
//                        startActivity(Intent(Myapplication.mcontext,EvalueTypeAct::class.java))
//                        //startActivity(Intent(Myapplication.mcontext,ChartActivity::class.java))
//                    }
//                    3 -> {
//                        //startActivity(Intent(Myapplication.mcontext,EvalueTypeAct::class.java))

//2 为药剂师
                    if (ConstantViewMolde.GetUser()!!.user?.personType == 2) {
                        RxToast.normal("您不是医生无法对处方查看")
                    } else {
                        startActivity(Intent(Myapplication.mcontext, MyDrugAct::class.java))
                    }
                    //startActivity(Intent(Myapplication.mcontext, SuifangAct::class.java))
                    //RxToast.normal("功能开发中~")
//                    var inten: Intent = Intent(Myapplication.mcontext, DepartSelectAct::class.java)
//                    var bundle: Bundle = Bundle();
//                    inten.putExtra(Myapplication.SHARED_NAME, bundle)
//                    launchManagerService?.startActivity(inten)
                }
                1 -> {
                    var intent = Intent(Myapplication.mcontext, EvalueListAct::class.java)
                    intent.putExtra("type", "1")
                    startActivity(intent)

                }

                2 -> {

                    if (ConstantViewMolde.GetUser()!!.user?.personType == 2) {
                        RxToast.normal("您不是医生无法对处方查看")
                    }else{
                        startActivity(Intent(Myapplication.mcontext, MyDrugAct::class.java))
                    }

//                    var inten: Intent = Intent(Myapplication.mcontext, MedicalCloudAct::class.java)
//                    var bundle: Bundle = Bundle();
//                    inten.putExtra(Myapplication.SHARED_NAME, bundle)
//                    launchManagerService?.startActivity(inten)
                }
            }
        }
        homeadapter = HomeBannerAdapter(listBanner, Myapplication.mcontext)
        homebanner.adapter = homeadapter
        homeadapter?.notifyDataSetChanged()
        homebanner.setCurrentItem(Int.MAX_VALUE / 2)
        getBanner()
        homeadapter?.setListener(this)
        return rootview
    }

    /***
     * banner 轮播图请求
     */
    fun getBanner() {
        IpUrl.getInstance()!!.getBanner()
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<MutableList<KnowleModle>>> {
                    override fun onError(e: Throwable?) {
                    }

                    override fun onNext(t: HoleResponse<MutableList<KnowleModle>>?) {
                        if (t?.code == 0) {
                            println()
                            if (t?.data != null) {
                                listBanner?.addAll(t.data!!)
                                homeadapter?.notifyDataSetChanged()
                            }
                        }
                    }

                    override fun onCompleted() {
                    }
                })
    }

    fun getZIXUN() {
        IpUrl.getInstance()!!.getZixun()
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<MutableList<KnowleModle>>> {
                    override fun onError(e: Throwable?) {
                        println(e.toString() + "err")
                    }

                    override fun onNext(t: HoleResponse<MutableList<KnowleModle>>?) {
                        if (t?.code == 0) {
                            if (t?.data != null) {
                                listZIXUN?.addAll(t.data!!)
//                                var message: Message = Message.obtain()
//                                message.what = 1
//                                message.obj = listZIXUN
//                                mHadler.sendMessage(message)
                                for (int in 0..listZIXUN?.size!! - 1) {
                                    val inflate = View.inflate(Myapplication.mcontext, R.layout.one_view, null)
                                    var text1: TextView = inflate.findViewById<TextView>(R.id.text1)
                                    var text2: TextView = inflate.findViewById<TextView>(R.id.text2)
                                    text1.setText(listZIXUN?.get(int)?.title)
                                    if (int + 1 > listZIXUN?.size!! - 1) {
                                        text2.setText(listZIXUN?.get(0)?.title)
                                    } else {
                                        text2.setText(listZIXUN?.get(int + 1)?.title)
                                    }
                                    //  viewFlipper?.addView(inflate)
                                }
                            }
                        }
                    }

                    override fun onCompleted() {
                    }
                })
    }


    override fun onResume() {
        super.onResume()
    }

    override fun onInvisible() {
        super.onInvisible()
        mHadler!!.removeMessages(0)
    }

    override fun onVisible() {
        super.onVisible()
        //  mHadler!!.sendEmptyMessage(0)
    }


    override fun onPause() {
        super.onPause()
    }


    inner internal class DepartAdapter(layoutResId: Int, data: MutableList<LevelBean>?) : BaseQuickAdapter<LevelBean, BaseViewHolder>(layoutResId, data) {
        override fun convert(holder: BaseViewHolder, item: LevelBean) {
            holder.setText(R.id.name, item!!.desc).setImageResource(R.id.image, item!!.iconid)
        }
    }

    override fun visibleToUser() {
        super.visibleToUser()
        mHadler!!.sendEmptyMessage(0)
    }

    override fun inVisibleToUser() {
        super.inVisibleToUser()
        mHadler!!.removeMessages(0)
    }
}