/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Activity


import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import com.jakewharton.rxbinding4.view.clicks
import com.peaceclient.hospitaldoctor.com.Base.BaseUrlUtil
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.Timer
import com.peaceclient.hospitaldoctor.com.Utils.Utils
import com.peaceclient.hospitaldoctor.com.View.CustomDialog
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.vondear.rxtools.view.RxToast
import com.vondear.rxtools.view.dialog.RxDialog
import kotlinx.android.synthetic.main.forget_acts.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/8/21 16:31
 * @change
 * @chang time
 * @class describe 忘记密码页面
 */

class ForgetPassAct : HoleBaseActivity() {
    private var dialog: CustomDialog? = null
    private var eyestatus: Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.forget_acts)

        arrow_back.clicks().throttleFirst(2000, TimeUnit.MICROSECONDS).subscribeOn(io.reactivex.rxjava3.android.schedulers.AndroidSchedulers.mainThread())
                .subscribe {
                    finish()
                }
//        val spannable = SpannableStringBuilder("同意和平里医院《用户协议》和《隐私政策》")
//        //设置文字的前景色，2、4分别表示可以点击文字的起始和结束位置。
//        spannable.setSpan(ForegroundColorSpan(resources.getColor(R.color.home_green)), 7, 13, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
//        spannable.setSpan(ForegroundColorSpan(resources.getColor(R.color.home_green)), 14, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
//        //这个一定要记得设置，不然点击不生效
//        buttom_text.setMovementMethod(LinkMovementMethod.getInstance())
//        spannable.setSpan(TextClick(DocOnlineAct::class.java), 7, 13, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
//        spannable.setSpan(TextClickS(DocOnlineAct::class.java), 14, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        forget_eye.setOnClickListener {
            // 不可见密码
            if (!eyestatus) {
                forget_eye.setImageResource(R.drawable.passord_open)
                password_forget.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                password_forget.setSelection(password_forget.text.length)
            } else
            // 可见的密码
            {
                forget_eye.setImageResource(R.drawable.password_close)
                password_forget.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD or InputType.TYPE_CLASS_TEXT)
                password_forget.setSelection(password_forget.text.length)
            }
            eyestatus = !eyestatus
        }
        forget.setOnClickListener {
            if (indefy()) {
                Regist(phone_forget.text.toString(), password_forget.text.toString(), vcode_forget.text.toString())
            }
        }
        sms_forget.setOnClickListener {
            if (Utils.isPhone(phone_forget.getText().toString())) {

                try {
                    getSmscode(phone_forget.getText().toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                RxToast.normal("请输入正确手机号")

            }
        }
    }

    /***
     *  获取验证码
     * @param toString String
     */
    private fun getSmscode(toString: String) {
        var baseUrlUtil: BaseUrlUtil = BaseUrlUtil()
        baseUrlUtil.getInstance().getVcodes(toString, "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<HoleResponse<Int>> {
                    override fun onError(e: Throwable?) {
                    }

                    override fun onNext(t: HoleResponse<Int>?) {

                        when (t!!.code) {
                            0 -> {
                                val timer: Timer =Timer(60000, 1000, sms_forget, this@ForgetPassAct)//倒计时
                                timer.start()
                            }
                            1 -> RxToast.normal(t!!.msg!!)
                            else -> RxToast.normal(t!!.msg!!)
                        }
                    }

                    override fun onCompleted() {
                    }

                })
    }



    fun indefy(): Boolean {
        if (TextUtils.isEmpty(phone_forget!!.text)) {
            RxToast.normal("请输入手机号")
            return false
        }
        if (TextUtils.isEmpty(vcode_forget!!.text)) {
            RxToast.normal("请输入验证码")
            return false
        }
        if (TextUtils.isEmpty(password_forget!!.text)) {
            RxToast.normal("请输入密码")
            return false
        } else {
            if (!Utils.isPasswordss(password_forget.text.toString())) {
                RxToast.normal("密码格式不正确")
                return false
            }
        }
        return true
    }

//var page: Int = intent.extras.get("currentPage") as Int
//        var rxDialogLoading: RxDialogLoading = RxDialogLoading(this)
//        rxDialogLoading.show()
//        dialog = CustomDialog(this@LoginAct, R.style.customDialog, R.layout.update_layout)

//
//        login.setOnClickListener {
//
//            var intent = Intent()
////            intent.putExtra("currentPage", 1)
////            setResult(1000, intent)
////            Myapplication.editor.putBoolean("isLogin", true).commit()
////            finish()
//            dialog!!.show()
//            var imageView: MyFitimage = dialog!!.findViewById<MyFitimage>(R.id.top_image)
//            var int: Int = imageView.measuredWidth
//            var linearLayout: LinearLayout = dialog!!.findViewById<LinearLayout>(R.id.content_layout)
//            var parms: ViewGroup.LayoutParams = linearLayout.layoutParams
//            println(int .toString()+"int")
//            parms.width = int
//            parms.height = parms.height
// linearLayout.layoutParams = parms
//println(parms.height.toString() +"234"+ parms.width.toString())


    override fun onBackPressed() {
        finish()
    }

    fun Regist(account: String, password: String, vcode: String) {
        var loading: RxDialog = RxDialog(this)
        loading.setContentView(R.layout.dialog_loading)
        loading.show()
        var baseUrlUtil: BaseUrlUtil = BaseUrlUtil()
        baseUrlUtil.getInstance().Forget(account, password, vcode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<HoleResponse<Int>> {
                    override fun onError(e: Throwable?) {
                        loading.dismiss()
                    }
                    override fun onNext(t: HoleResponse<Int>?) {
                        when (t!!.code) {
                            0 -> {
                                RxToast.normal(t!!.msg!!)
                                finish()
                            }
                            1
                            -> RxToast.normal(t!!.msg!!)
                            else -> RxToast.normal(t!!.msg!!)
                        }
                    }
                    override fun onCompleted() {
                        loading.dismiss()
                    }
                })
    }
    private fun handleLoginSuccess3() {
        val bundle = intent.getBundleExtra(Myapplication.SHARED_NAME)
        val className = bundle.getString(Myapplication.CLASSNAME)
        if (TextUtils.isEmpty(className)) {
            return
        }
        val intent = Intent()
        intent.component = ComponentName(this@ForgetPassAct, className)
        bundle.remove(Myapplication.CLASSNAME)
        intent.putExtra(Myapplication.SHARED_NAME, bundle)
        startActivity(intent)
        finish()
    }

    fun Login() {
        handleLoginSuccess3();
//    var baseUrlUtil: BaseUrlUtil = BaseUrlUtil()
//    baseUrlUtil.getInstance().getVcodes("182", 0)
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribeOn(Schedulers.io())
//            .subscribe(object : Observer<HoleResponse<UpdateModle>> {
//                override fun onNext(t: HoleResponse<UpdateModle>?) {
//                    println(t.toString())
//                    var int: Int = packageManager.getPackageInfo(packageName, 0)!!.versionCode
//                    if (int < t!!.data!!.get(0)!!.versionCode!!.toInt()) {
//                        dialog!!.show()
//                        var imageView: MyFitimage = dialog!!.findViewById<MyFitimage>(com.peaceclient.com.R.id.top_image)
//                        var int: Int = imageView.measuredWidth
//                        var linearLayout: LinearLayout = dialog!!.findViewById<LinearLayout>(com.peaceclient.com.R.id.content_layout)
//                        var parms: ViewGroup.LayoutParams = linearLayout.layoutParams
//                        parms.width = int
//                        linearLayout.layoutParams = parms
//                        if (t!!.data!!.get(0)!!.enable == 0) {
//                            dialog!!.findViewById<Button>(com.peaceclient.com.R.id.cancle).visibility = View.GONE
//                        } else {
//                            dialog!!.findViewById<Button>(com.peaceclient.com.R.id.cancle).visibility = View.VISIBLE
//                        }
//                        dialog!!.findViewById<TextView>(com.peaceclient.com.R.id.update_content).setText(t!!.data!!.get(0)!!.content!!)
//                        dialog!!.findViewById<TextView>(com.peaceclient.com.R.id.title).setText("发现新版本" + t!!.data!!.get(0)!!.versionNo!!)
//
//                    } else {
//                        RxToast.showToast("t")
//                    }

    }
//                    override fun onNext(t: HoleResponse<UpdateModle>?) {
//                        // enable 0,强制更新 1，不强制更新
//                        // versionCode 比较本地versionCode 是否更新
//                        t!!.data!!.enable = 1
//                        var int: Int = packageManager.getPackageInfo(packageName, 0)!!.versionCode
//                        if (int < t!!.data!!.versionCode!!.toInt()) {
//
//                            if (t!!.data!!.enable == 0) {
//                                dialog!!.findViewById<Button>(R.id.cancle).visibility = View.GONE
//                            } else {
//                                dialog!!.findViewById<Button>(R.id.cancle).visibility = View.VISIBLE
//                            }
//                            dialog!!.findViewById<TextView>(R.id.update_content).setText(t!!.data!!.content!!)
//                            dialog!!.findViewById<TextView>(R.id.title).setText("发现新版本"+t!!.data!!.versionNo!!)
//                            dialog!!.show()
//                        } else {
//                                RxToast.showToast("t")
//                        }
//
//
//                    }

    /* override fun onCompleted() {

     }

     override fun onError(e: Throwable) {
         println(e.printStackTrace())
     }


 })*/
}