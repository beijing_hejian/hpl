/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name project
 * @class name：com.homeclientz.com.Utils
 * @class describe
 * @anthor admin
 * @time 2019/11/22 15:30
 * @change
 * @chang time
 * @class describe
 */

public class DateUtils {
      private static String mYear; // 当前年
      private static String mMonth; // 月
      private static String mDay;
      private static String mWay;

      /**
       * 获取当前日期几月几号
       */
      public static String getDateString() {
            final Calendar c = Calendar.getInstance();
            c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
            mMonth = String.valueOf(c.get(Calendar.MONTH) + 1);// 获取当前月份
            mDay = String.valueOf(c.get(Calendar.DAY_OF_MONTH));// 获取当前月份的日期号码
            if (Integer.parseInt(mDay) > MaxDayFromDay_OF_MONTH(Integer.parseInt(mYear), (Integer.parseInt(mMonth)))) {
                  mDay = String.valueOf(MaxDayFromDay_OF_MONTH(Integer.parseInt(mYear), (Integer.parseInt(mMonth))));
            }
            return mMonth + "月" + mDay + "日";
      }

      /***
       *
       *
       * */
      public static Date getDateAfter(Date d, int day) {
            Calendar now = Calendar.getInstance();
            now.setTime(d);
            now.set(Calendar.DAY_OF_YEAR, now.get(Calendar.DAY_OF_YEAR) + day);
            return now.getTime();
      }

      public static List<String> getAFTER(Date d, int day, int LON) {
            List<String> list = new ArrayList<>();
            Date dateAfter = getDateAfter(d, 3);
            for (int i = 0; i < 7; i++) {
                  Date dateAfter1 = getDateAfter(dateAfter, i);
                  list.add(TimeFormatUtils.ms2DateOnlyDay(dateAfter1.getTime()));
            }
            return list;
      }
      ;
      public static List<String> getAFTERDay(Date d, int day, int LON) {
            List<String> list = new ArrayList<>();
            Date dateAfter = getDateAfter(d, day);
            for (int i = 0; i < LON; i++) {
                  Date dateAfter1 = getDateAfter(dateAfter, i);
                  list.add(TimeFormatUtils.ms2DateOnlyDay(dateAfter1.getTime()));
            }
            return list;
      }
      public static List<String> getAFTERYearDay(Date d, int day, int LON) {
            List<String> list = new ArrayList<>();
            Date dateAfter = getDateAfter(d, day);
            for (int i = 0; i < LON; i++) {
                  Date dateAfter1 = getDateAfter(dateAfter, i);
                  list.add(TimeFormatUtils.ms2DateOnlyDay(dateAfter1.getTime()));
            }
            return list;
      }
      /**
       * 获取当前年月日
       *  
       *
       * @return
       */
      public static String StringData() {


            final Calendar c = Calendar.getInstance();
            c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
            mYear = String.valueOf(c.get(Calendar.YEAR));// 获取当前年份
            mMonth = String.valueOf(c.get(Calendar.MONTH) + 1);// 获取当前月份
            mDay = String.valueOf(c.get(Calendar.DAY_OF_MONTH));// 获取当前月份的日期号码
            if (Integer.parseInt(mDay) > MaxDayFromDay_OF_MONTH(Integer.parseInt(mYear), (Integer.parseInt(mMonth)))) {
                  mDay = String.valueOf(MaxDayFromDay_OF_MONTH(Integer.parseInt(mYear), (Integer.parseInt(mMonth))));
            }
            return mYear + "-" + (mMonth.length() == 1 ? "0" + mMonth : mMonth) + "-" + (mDay.length() == 1 ? "0" + mDay : mDay);
      }


      /**
       * 根据当前日期获得是星期几
       *  
       *
       * @return
       */
      public static String getWeek(String time) {
            String Week = "";
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            try {
                  c.setTime(format.parse(time));
            } catch (ParseException e) {
                  e.printStackTrace();
            }
            if (c.get(Calendar.DAY_OF_WEEK) == 1) {
                  Week += "日";
            }
            if (c.get(Calendar.DAY_OF_WEEK) == 2) {
                  Week += "一";
            }
            if (c.get(Calendar.DAY_OF_WEEK) == 3) {
                  Week += "二";
            }
            if (c.get(Calendar.DAY_OF_WEEK) == 4) {
                  Week += "三";
            }
            if (c.get(Calendar.DAY_OF_WEEK) == 5) {
                  Week += "四";
            }
            if (c.get(Calendar.DAY_OF_WEEK) == 6) {
                  Week += "五";
            }
            if (c.get(Calendar.DAY_OF_WEEK) == 7) {
                  Week += "六";
            }
            return Week;
      }

      public static String getWeekYear(String time) {
            String Week = "";


            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            try {
                  c.setTime(format.parse(time));
            } catch (ParseException e) {
                  e.printStackTrace();
            }
            if (c.get(Calendar.DAY_OF_WEEK) == 1) {
                  Week += "日";
            }
            if (c.get(Calendar.DAY_OF_WEEK) == 2) {
                  Week += "一";
            }
            if (c.get(Calendar.DAY_OF_WEEK) == 3) {
                  Week += "二";
            }
            if (c.get(Calendar.DAY_OF_WEEK) == 4) {
                  Week += "三";
            }
            if (c.get(Calendar.DAY_OF_WEEK) == 5) {
                  Week += "四";
            }
            if (c.get(Calendar.DAY_OF_WEEK) == 6) {
                  Week += "五";
            }
            if (c.get(Calendar.DAY_OF_WEEK) == 7) {
                  Week += "六";
            }
            return Week;
      }
      /**
       * 获取今天往后一周的日期（几月几号）
       */
      public static List<String> getSevendate() {
            List<String> dates = new ArrayList<String>();
            final Calendar c = Calendar.getInstance();
            c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
            for (int i = 0; i < 7; i++) {
                  mYear = String.valueOf(c.get(Calendar.YEAR));// 获取当前年份
                  mMonth = String.valueOf(c.get(Calendar.MONTH) + 1);// 获取当前月份
                  mDay = String.valueOf(c.get(Calendar.DAY_OF_MONTH) + i);// 获取当前日份的日期号码
                  if (Integer.parseInt(mDay) > MaxDayFromDay_OF_MONTH(Integer.parseInt(mYear), (i + 1))) {
                        mDay = String.valueOf(MaxDayFromDay_OF_MONTH(Integer.parseInt(mYear), (i + 1)));
                  }
                  String date = mMonth + "月" + mDay + "日";
                  dates.add(date);
            }
            return dates;
      }


      /**
       * 获取今天往后一周的集合
       */
      public static List<String> get7week() {
            String week = "";
            List<String> weeksList = new ArrayList<String>();
            List<String> dateList = get7date();
            for (String s : dateList) {
                  if (s.equals(StringData())) {
                        week = "今天";
                  } else {
                        week = getWeek(s);
                  }
                  weeksList.add(week);
            }
            return weeksList;
      }


      public static List<String> get7date() {
            List<String> dates = new ArrayList<String>();
            final Calendar c = Calendar.getInstance();
            c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
            SimpleDateFormat sim = new SimpleDateFormat(
                      "yyyy-MM-dd");
            Date dateAfter = getDateAfter(c.getTime(), 3);
            String date = sim.format(dateAfter);
            dates.add(date);
            for (int i = 0; i < 6; i++) {
                  c.add(Calendar.DAY_OF_MONTH, 1);
                  date = sim.format(date);
                  dates.add(date);
            }
            return dates;
      }

      /**
       * 得到当年当月的最大日期
       **/
      public static int MaxDayFromDay_OF_MONTH(int year, int month) {
            Calendar time = Calendar.getInstance();
            time.clear();
            time.set(Calendar.YEAR, year);
            time.set(Calendar.MONTH, month - 1);//注意,Calendar对象默认一月为0                 
            int day = time.getActualMaximum(Calendar.DAY_OF_MONTH);//本月份的天数
            return day;
      }
}
