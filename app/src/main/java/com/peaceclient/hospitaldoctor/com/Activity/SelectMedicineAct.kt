package com.peaceclient.hospitaldoctor.com.Activity


import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jakewharton.rxbinding4.view.clicks
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.KeyboardUtils
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.RecipeModlex
import kotlinx.android.synthetic.main.medicinehis_act.arrow_back
import kotlinx.android.synthetic.main.search_medicine.*
import kotlinx.android.synthetic.main.search_medicine.medic_recycle
import kotlinx.android.synthetic.main.search_medicine.prosmarts
import kotlinx.android.synthetic.main.search_medicine.search_name
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit


/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor222
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/11/9 15:22
 * @change
 * @chang time
 * @class describe 诊断开方药品页面
 */

class SelectMedicineAct : HoleBaseActivity() {
    private var messageList: ArrayList<RecipeModlex.BillDetail> = arrayListOf()
    private var header: View? = null
    private var kong: View? = null
    private var adapter: DoctorAdapter? = null
    var type: String = "1"
    var types: ArrayList<String> = arrayListOf("全部", "西药", "中成药", "草药")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_medicine)
/*
        search_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (TextUtils.isEmpty(s)) {
                    (Myapplication.mcontext
                            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                            .hideSoftInputFromWindow(this@SelectMedicineAct
                                    .getCurrentFocus().getWindowToken(),
                                    InputMethodManager.HIDE_NOT_ALWAYS)
                    prosmarts.autoRefresh()
                }
            }
        })

        search_name.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                // 先隐藏键盘
                (Myapplication.mcontext
                        .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                        .hideSoftInputFromWindow(this@SelectMedicineAct
                                .getCurrentFocus().getWindowToken(),
                                InputMethodManager.HIDE_NOT_ALWAYS)
                if (search_name.getText().toString().isEmpty()) {
                    prosmarts.autoRefresh()
                } else {
                    //搜索
                    prosmarts.autoRefresh()
                }
                return@OnEditorActionListener true
            }
            false
        })*/

        search.clicks().throttleFirst(1000, TimeUnit.MICROSECONDS).subscribe {
            KeyboardUtils.hideKeyboard(search)
            prosmarts.autoRefresh()
        }

        arrow_back.setOnClickListener {
            finish()
        }
        kong = View.inflate(this@SelectMedicineAct, R.layout.empty_view, null)
        header = View.inflate(this@SelectMedicineAct, com.peaceclient.hospitaldoctor.com.R.layout.medicine_search_header, null);
        var linerma: LinearLayoutManager = LinearLayoutManager(this)
        medic_recycle.layoutManager = linerma
        adapter = DoctorAdapter(com.peaceclient.hospitaldoctor.com.R.layout.medicine_search_item, messageList)
        adapter?.addHeaderView(header)
        adapter?.bindToRecyclerView(medic_recycle)
        adapter?.setEmptyView(kong)
        medic_recycle.adapter = adapter
        adapter?.notifyDataSetChanged()
        prosmarts.autoRefresh()
        prosmarts.setOnRefreshListener {
            messageList.clear()
            GetMedicine()
        }

        adapter?.setOnItemClickListener { adapter, view, position ->
            var intent: Intent = Intent()
            intent.putExtra("date", messageList.get(position))
            setResult(AppCompatActivity.RESULT_OK, intent)
            finish()        }
        spinner.prompt = "请选择诊断类型"
        spinner.dropDownVerticalOffset = resources.getDimensionPixelOffset(R.dimen.dp_44)
        spinner.dropDownHorizontalOffset = resources.getDimensionPixelOffset(R.dimen.dp_20)
        var spinnerAdapter: ArrayAdapter<String> = ArrayAdapter(this@SelectMedicineAct, R.layout.spinner_select, types)
        spinnerAdapter.setDropDownViewResource(R.layout.item_drop_layout)
        spinner.adapter = spinnerAdapter
        spinner.setSelection(1)
        spinnerAdapter.notifyDataSetChanged()
        var selec: MyItemSelect = MyItemSelect()
        spinner.onItemSelectedListener = selec
    }
    internal inner class MyItemSelect : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            // messageList.clear()
            when (position) {
                0 -> {
                    type = "0"
                    // 全部
                    messageList.clear();

                    GetMedicine();
                }
                1 -> {
                    type = "1"
                    //中药
                    messageList.clear();

                    GetMedicine();
                }
                2 -> {
                    type = "2"
                    //中成药
                    messageList.clear();

                    GetMedicine();
                }
                3 -> {
                    type = "3"
                    //艹药
                    messageList.clear();

                    GetMedicine();
                }
            }
        }
    }

    /***
     * 查询药品列表请求
     */
    fun GetMedicine() {
        IpUrl.getInstance()!!.cxyp(ConstantViewMolde.getToken(), type, search_name.text.toString())
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<java.util.ArrayList<RecipeModlex.BillDetail>>> {
                    override fun onError(e: Throwable?) {
                        prosmarts.finishRefresh()
                    }
                    override fun onNext(t: HoleResponse<java.util.ArrayList<RecipeModlex.BillDetail>>?) {
                        if (t != null) {
                            messageList?.addAll(t?.data!!)
                            adapter?.notifyDataSetChanged()
                        }
                    }
                    override fun onCompleted() {
                        prosmarts.finishRefresh()
                    }
                })
    }

    internal inner class DoctorAdapter(layoutResId: Int, data: ArrayList<RecipeModlex.BillDetail>?) : com.chad.library.adapter.base.BaseQuickAdapter<RecipeModlex.BillDetail, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: RecipeModlex.BillDetail) {
            helper.setText(R.id.name, item.name)
                    .setText(R.id.guige, item.spec)
                    .setText(R.id.danwei, item.unit)
            //.setText(com.peaceclient.hospitaldoctor.com.R.id.kucun, item.ypkc)
//            helper.setText(R.id.doctor_name, item?.docName ?: "")
//                    .setText(R.id.doctor_introd, "专长："+item?.introduction ?: "")
//                    .setText(R.id.doctor_pay, "￥"+item?.costGraphic ?: "")
//                    .setText(R.id.doctor_profession, item?.title ?: "")
//                    .addOnClickListener(R.id.sure)
        }
    }
}