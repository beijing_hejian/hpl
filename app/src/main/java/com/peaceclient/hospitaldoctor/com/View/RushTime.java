package com.peaceclient.hospitaldoctor.com.View;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.peaceclient.hospitaldoctor.com.InterFace.OnFinishListener;
import com.peaceclient.hospitaldoctor.com.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.View
 * @class describe
 * @anthor admin
 * @time 2021/9/17 17:46
 * @change
 * @chang time
 * @class describe
 */


public class RushTime extends LinearLayout {

      // 小时，十位
      private TextView tv_hour_decade;
      // 小时，个位
      private TextView tv_hour_unit;
      // 分钟，十位
      private TextView tv_min_decade;
      // 分钟，个位
      private TextView tv_min_unit;
      // 秒，十位
      private TextView tv_sec_decade;
      // 秒，个位
      private TextView tv_sec_unit;

      private Context context;

      private int hour_decade;
      private int hour_unit;
      private int min_decade;
      private int min_unit;
      private int sec_decade;
      private int sec_unit;
      // 计时器
      private Timer timer;
      private OnFinishListener listener;
      private Handler handler = new Handler() {

            public void handleMessage(Message msg) {

                  countDown(msg.what);
            }

            ;
      };

      public void setListener(OnFinishListener listener) {
            this.listener = listener;
      }

      public RushTime(Context context, AttributeSet attrs) {
            super(context, attrs);
            this.context = context;
            LayoutInflater inflater = (LayoutInflater) context
                      .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.view_countdowntimer, this);

            tv_hour_decade = (TextView) view.findViewById(R.id.tv_hour_decade);
            tv_hour_unit = (TextView) view.findViewById(R.id.tv_hour_unit);
            tv_min_decade = (TextView) view.findViewById(R.id.tv_min_decade);
            tv_min_unit = (TextView) view.findViewById(R.id.tv_min_unit);
            tv_sec_decade = (TextView) view.findViewById(R.id.tv_sec_decade);
            tv_sec_unit = (TextView) view.findViewById(R.id.tv_sec_unit);

      }

      /**
       * @param
       * @return void
       * @throws
       * @Description: 开始计时
       */
      public void start( int pos) {
            if (timer == null) {
                  timer = new Timer();
                  timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                              Message mess = Message.obtain();
                                        mess.what = pos;

                              handler.sendMessage(mess);
                        }
                  }, 0, 1000);
            }
      }

      /**
       * @param
       * @return void
       * @throws
       * @Description: 停止计时
       */
      public void stop() {
            if (timer != null) {
                  timer.cancel();
                  timer = null;
                  tv_hour_decade.setText(0 + "");
                  tv_hour_unit.setText(0 + "");
                  tv_min_decade.setText(0 + "");
                  tv_min_unit.setText(0 + "");
                  tv_sec_decade.setText(0 + "");
                  tv_sec_unit.setText(0 + "");
            }

      }

      public boolean isStop() {
            if (tv_hour_decade.getText().equals("0") && tv_hour_unit.getText().equals("0") && tv_min_decade.getText().equals("0") && tv_min_unit.getText().equals("0") && tv_sec_decade.equals("0") && tv_sec_unit.getText().equals("0")) {
                  return true;
            } else {
                  return false;
            }


      }

      /**
       * @param
       * @return void
       * @throws Exception
       * @throws
       * @Description: 设置倒计时的时长
       */
      public void setTime(String hour, String min, String sec) throws  Exception {
            int Hour = Integer.parseInt(hour);
            int Min = Integer.parseInt(min);
            int Second = Integer.parseInt(sec);
            if (Hour >= 120 || Min >= 60 || Second >= 60 || Hour < 0 || Min < 0
                      || Second < 0) {
                  tv_hour_decade.setText(0 + "");
                  tv_hour_unit.setText(0 + "");
                  tv_min_decade.setText(0 + "");
                  tv_min_unit.setText(0 + "");
                  tv_sec_decade.setText(0 + "");
                  tv_sec_unit.setText(0 + "");
                 // throw new RuntimeException("error:check your time");
            }
            hour_decade = Hour / 10;
            hour_unit = Hour - hour_decade * 10;
            min_decade = Min / 10;
            min_unit = Min - min_decade * 10;
            sec_decade = Second / 10;
            sec_unit = Second - sec_decade * 10;
            tv_hour_decade.setText(hour_decade + "");
            tv_hour_unit.setText(hour_unit + "");
            tv_min_decade.setText(min_decade + "");
            tv_min_unit.setText(min_unit + "");
            tv_sec_decade.setText(sec_decade + "");
            tv_sec_unit.setText(sec_unit + "");

      }

      /**
       * @param
       * @return boolean
       * @throws
       * @Description: 倒计时
       */
      private void countDown(int pos) {

            if (isCarry4Unit(tv_sec_unit)) {
                  if (isCarry4Decade(tv_sec_decade)) {

                        if (isCarry4Unit(tv_min_unit)) {
                              if (isCarry4Decade(tv_min_decade)) {

                                    if (isCarry4Unit(tv_hour_unit)) {
                                          if (isCarry4Decade(tv_hour_decade)) {
                                                // ToastUtil.safeShowToast(TLApplication.mContext, "Time limited");
                                                stop();
                                                listener.onFinish(pos);
                                          }
                                    }
                              }
                        }
                  }
            }
      }

      /**
       * @param
       * @return boolean
       * @throws
       * @Description: 变化十位，并判断是否需要进位
       */
      private boolean isCarry4Decade(TextView tv) {

            int time = Integer.valueOf(tv.getText().toString());
            time = time - 1;
            if (time < 0) {
                  time = 5;
                  tv.setText(time + "");
                  return true;
            } else {
                  tv.setText(time + "");
                  return false;
            }

      }

      /**
       * @param
       * @return boolean
       * @throws
       * @Description: 变化个位，并判断是否需要进位
       */
      private boolean isCarry4Unit(TextView tv) {

            int time = Integer.valueOf(tv.getText().toString());
            time = time - 1;
            if (time < 0) {
                  time = 9;
                  tv.setText(time + "");
                  return true;
            } else {
                  tv.setText(time + "");
                  return false;
            }

      }


}
