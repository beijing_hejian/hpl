/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Activity;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity;
import com.peaceclient.hospitaldoctor.com.R;
import com.peaceclient.hospitaldoctor.com.Utils.DensityUtil;
import com.peaceclient.hospitaldoctor.com.Utils.StatusBarUtil;
import com.peaceclient.hospitaldoctor.com.View.StatusBarHeightView;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * @packname com.homeclient.com.Fragment
 * @filename VideoActivity
 * @date on 2018/8/3 15:31
 * @describe 视频播放页面
 *****/
public class VideoPlayActivity extends HoleBaseActivity {
      @BindView(R.id.content)
      LinearLayout content;
      @BindView(R.id.arrow_back)
      ImageView arrowBack;
      @BindView(R.id.arrow_close)
      ImageView arrowClose;
      @BindView(R.id.rel)
      RelativeLayout rel;
      @BindView(R.id.jj)
      StatusBarHeightView jj;
      @BindView(R.id.fl_video)
      FrameLayout flVideo;
      @BindView(R.id.videoweb)
      WebView videoweb;
      @BindView(R.id.video_title)
      TextView videoTitle;
      @BindView(R.id.video_intruction)
      TextView videoIntruction;
      @BindView(R.id.total)
      RelativeLayout total;
      private List<String> list;
      private View nVideoView = null;
      private Boolean islandport = true;//true表示此时是竖屏，false表示此时横屏。
      private View xCustomView;
      private xWebChromeClient xwebchromeclient;

      private WebChromeClient.CustomViewCallback xCustomViewCallback;


      private int orientation;
      private int orientation1;
      private int width;
      private int height1;
      private int height;
      private int width1;
      private int width2;
      private float holewidth;
      private float holeheight;
      private String id;


      @Override
      public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.video_activity);
            ButterKnife.bind(this);
            StatusBarUtil.setRootViewFitsSystemWindows(this, false);
            //设置状态栏透明
            StatusBarUtil.setTranslucentStatus(this);
            //一般的手机的状态栏文字和图标都是白色的, 可如果你的应用也是纯白色的, 或导致状态栏文字看不清
            //所以如果你是这种情况,请使用以下代码, 设置状态使用深色文字图标风格, 否则你可以选择性注释掉这个if内容
            if (!StatusBarUtil.setStatusBarDarkTheme(this, false)) {
                  //这样半透明+白=灰, 状态栏的文字能看得清
                  StatusBarUtil.setStatusBarColor(this, 0x000000);
            }
            arrowBack.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        finish();
                  }
            });
            id = getIntent().getStringExtra("id");
            // txtMainTitle.setVisibility(View.VISIBLE);
            initweb();
//            videoTitle.setText(vide.getTitle());
//            videoIntruction.setText(vide.getIntroduction());
            WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
            DisplayMetrics dm = new DisplayMetrics();
            wm.getDefaultDisplay().getMetrics(dm);
            // 屏幕宽度（像素）
            width2 = dm.widthPixels;
            // 屏幕高度（像素）
            height = dm.heightPixels;
            // holewidth = DensityUtil.pix(this, width2);
            holewidth = DensityUtil.px2dip(this, width2);
            holeheight = DensityUtil.px2dip(this, height);
      }



      private void initweb() {
            initwidget();
            videoweb.addJavascriptInterface(new JsObject(VideoPlayActivity.this), "jscr");

      }

      @Override
      public void onConfigurationChanged(Configuration newConfig) {
            super.onConfigurationChanged(newConfig);
            // 在这里添加屏幕切换后的操作
            if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                  // 横向
                  jj.setVisibility(View.GONE);
                  jj.setBackgroundColor(getResources().getColor(R.color.transparent));
                  total.setBackgroundColor(getResources().getColor(R.color.transparent));
                  StatusBarUtil.setRootViewFitsSystemWindows(this, false);
                  //设置状态栏透明
                  StatusBarUtil.setTranslucentStatus(this);
                  //一般的手机的状态栏文字和图标都是白色的, 可如果你的应用也是纯白色的, 或导致状态栏文字看不清
                  //所以如果你是这种情况,请使用以下代码, 设置状态使用深色文字图标风格, 否则你可以选择性注释掉这个if内容
                  if (!StatusBarUtil.setStatusBarDarkTheme(this, false)) {
                        //这样半透明+白=灰, 状态栏的文字能看得清
                        StatusBarUtil.setStatusBarColor(this, 0x000000);
                  }
                  content.setVisibility(View.GONE);
                  videoweb.setBackgroundColor(Color.DKGRAY);
                  RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) videoweb.getLayoutParams();
                  params.height = width2;
                  params.width = height;
                  videoweb.setLayoutParams(params);
            } else {
                  //竖向
                  jj.setVisibility(View.VISIBLE);
                  total.setBackgroundColor(getResources().getColor(R.color.white));
                  jj.setBackgroundColor(getResources().getColor(R.color.color_bluegreen));
                  StatusBarUtil.setRootViewFitsSystemWindows(this, false);
                  //设置状态栏透明
                  StatusBarUtil.setTranslucentStatus(this);
                  //一般的手机的状态栏文字和图标都是白色的, 可如果你的应用也是纯白色的, 或导致状态栏文字看不清
                  //所以如果你是这种情况,请使用以下代码, 设置状态使用深色文字图标风格, 否则你可以选择性注释掉这个if内容
                  if (!StatusBarUtil.setStatusBarDarkTheme(this, false)) {
                        //这样半透明+白=灰, 状态栏的文字能看得清
                        StatusBarUtil.setStatusBarColor(this, 0x000000);
                  }
                  RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) videoweb.getLayoutParams();
                  params.height = height / 3;
                  params.width = width2;
                  videoweb.setLayoutParams(params);
                  rel.setVisibility(View.VISIBLE);
                  content.setVisibility(View.VISIBLE);

            }
//
      }

      private void initwidget() {
            // TODO Auto-generated method stub
            WebSettings ws = videoweb.getSettings();
            /**
             * setAllowFileAccess 启用或禁止WebView访问文件数据 setBlockNetworkImage 是否显示网络图像
             * setBuiltInZoomControls 设置是否支持缩放 setCacheMode 设置缓冲的模式
             * setDefaultFontSize 设置默认的字体大小 setDefaultTextEncodingName 设置在解码时使用的默认编码
             * setFixedFontFamily 设置固定使用的字体 setJavaSciptEnabled 设置是否支持Javascript
             * setLayoutAlgorithm 设置布局方式 setLightTouchEnabled 设置用鼠标激活被选项
             * setSupportZoom 设置是否支持变焦
             * */
            ws.setRenderPriority(WebSettings.RenderPriority.HIGH);
            ws.setBuiltInZoomControls(true);// 隐藏缩放按钮
            ws.setAllowContentAccess(true);

            ws.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);// 排版适应屏幕
            ws.setUseWideViewPort(false);// 可任意比例缩放
            ws.setLoadWithOverviewMode(true);// setUseWideViewPort方法设置webview推荐使用的窗口。setLoadWithOverviewMode方法是设置webview加载的页面的模式。
            ws.setSavePassword(true);
            ws.setSaveFormData(true);// 保存表单数据
            ws.setJavaScriptEnabled(true);
            ws.setGeolocationEnabled(true);// 启用地理定位
            ws.setUseWideViewPort(false);
            ws.setLoadWithOverviewMode(false);
            ws.setDomStorageEnabled(true);
//
            ws.setCacheMode(WebSettings.LOAD_NO_CACHE); // 不加载缓存内容
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                  ws.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            }
            xwebchromeclient = new xWebChromeClient();
            videoweb.setWebChromeClient(xwebchromeclient);
            videoweb.setWebViewClient(new xWebViewClientent());
            trustAllHosts();
           // kaiping();
            // videoweb.loadUrl(vide.getAddress());
      }


      private static void trustAllHosts() {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {

                  public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[]{};
                  }

                  public void checkClientTrusted(X509Certificate[] chain, String authType) {
                  }

                  public void checkServerTrusted(X509Certificate[] chain, String authType) {
                  }
            }};

            try {
                  SSLContext sc = SSLContext.getInstance("TLS");
                  sc.init(null, trustAllCerts, new SecureRandom());
                  HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (Exception e) {
                  e.printStackTrace();
            }

      }

      @Override
      public boolean onKeyDown(int keyCode, KeyEvent event) {
            AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {//音量+
                  mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume + 1, 1);
            }
            if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {//音量-
                  mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume - 1, 1);
            }


            if (keyCode == KeyEvent.KEYCODE_BACK) {
                  if (inCustomView()) {
                        hideCustomView();
                        return true;
                  } else {
                        videoweb.loadUrl("about:blank");
//     mTestWebView.loadData("", "text/html; charset=UTF-8", null);
                        VideoPlayActivity.this.finish();
                        //  Log.i("testwebview", "===>>>2");
                  }
            }
            return true;
      }

      /**
       * 判断是否是全屏
       *
       * @return
       */
      public boolean inCustomView() {
            return (xCustomView != null);
      }

      /**
       * 全屏时按返加键执行退出全屏方法
       */
      public void hideCustomView() {
            xwebchromeclient.onHideCustomView();
      }

      /**
       * 处理Javascript的对话框、网站图标、网站标题以及网页加载进度等
       *
       * @author
       */
      public class xWebChromeClient extends WebChromeClient {
            private Bitmap xdefaltvideo;
            private View xprogressvideo;

            @Override
            //播放网络视频时全屏会被调用的方法
            public void onShowCustomView(View view, CustomViewCallback callback) {
                  if (islandport) {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                  } else {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                  }
                  // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                  flVideo.setVisibility(View.GONE);
                  //如果一个视图已经存在，那么立刻终止并新建一个
                  if (xCustomView != null) {
                        callback.onCustomViewHidden();
                        return;
                  }
                  flVideo.addView(view);
                  xCustomView = view;
                  xCustomViewCallback = callback;
                  flVideo.setVisibility(View.VISIBLE);
                  setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }

            @Override
            //视频播放退出全屏会被调用的
            public void onHideCustomView() {
                  if (xCustomView == null)//不是全屏播放状态
                        return;
                  // Hide the custom view.

                  xCustomView.setVisibility(View.GONE);
                  // Remove the custom view from its container.
                  flVideo.removeView(xCustomView);
                  xCustomView = null;
                  flVideo.setVisibility(View.GONE);
                  xCustomViewCallback.onCustomViewHidden();
                  videoweb.setVisibility(View.VISIBLE);
                  setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                  super.onHideCustomView();
                  //Log.i(LOGTAG, "set it to webVew");
            }

            //视频加载添加默认图标
            @Override
            public Bitmap getDefaultVideoPoster() {
                  //Log.i(LOGTAG, "here in on getDefaultVideoPoster");
                  xdefaltvideo = BitmapFactory.decodeResource(
                            getResources(), R.drawable.home_banner);

                  return xdefaltvideo;
            }

            //网页标题
            @Override
            public void onReceivedTitle(WebView view, String title) {
                  //  (VideoPlayActivity.this).setTitle(vide.getTitle());
            }
//   @Override
//  //当WebView进度改变时更新窗口进度
//   public void onProgressChanged(WebView view, int newProgress) {
//    (MainActivity.this).getWindow().setFeatureInt(Window.FEATURE_PROGRESS, newProgress*100);
//   }
      }

      /**
       * 处理各种通知、请求等事件
       *
       * @author
       */
      public class xWebViewClientent extends WebViewClient {
            @Override
            public void onReceivedSslError(WebView view,
                                           final SslErrorHandler handler, SslError error) {

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                  RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) videoweb.getLayoutParams();
                  height1 = params.height / 3;
                  width1 = params.width;
                  view.setLayoutParams(params);
                  //  view.loadUrl(BrowserJsInject.fullScreenByJs(url));

            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
//                  if (vide.getAddress().startsWith("http:") || vide.getAddress().startsWith("https:")) {
//                        return true;
//                  }
//                  try {
//                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(vide.getAddress()));
//                        startActivity(intent);
//                  } catch (Exception e) {
//                  }
                  return true;
            }

      }

//      public void kaiping() {
//            BaseUrlUtil baseUrlUtil = new BaseUrlUtil();
//            baseUrlUtil.getInstance().getDetail(id).observeOn(
//                      AndroidSchedulers.mainThread()
//            ).subscribeOn(Schedulers.io())
//                      .subscribe(new Observer<HoleResponse<KnowleModle>>() {
//                            @Override
//                            public void onCompleted() {
//
//                            }
//
//                            @Override
//                            public void onError(Throwable e) {
//
//                            }
//
//                            @Override
//                            public void onNext(HoleResponse<KnowleModle> code) {
//                                  if (code != null) {
//                                        if (code.getData() != null) {
//
//                                              videoIntruction.setText(code.getData().getFtitle());
//                                              videoweb.loadUrl("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4");
//
//                                        }
//                                  }
//                            }
//                      });
//      }

      public class JsObject {
            Context mContext;

            JsObject(Context c) {
                  mContext = c;
            }

            @JavascriptInterface
            public void log() {
                  System.out.println("返回结果");
                  setFullScreen();
            }
      }

      /**
       * 设置全屏
       */
      private void setFullScreen() {
            //  Log.i("视频全屏-->", "竖屏切换到横屏");
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
// 设置全屏的相关属性，获取当前的屏幕状态，然后设置全屏
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                      WindowManager.LayoutParams.FLAG_FULLSCREEN);

// 全屏下的状态码：1098974464
// 窗口下的状态吗：1098973440
      }


}