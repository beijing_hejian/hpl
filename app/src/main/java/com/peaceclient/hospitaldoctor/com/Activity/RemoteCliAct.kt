package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.Button
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.Hy.Constant
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.GhModle
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.ui.ChatActivity
import com.vondear.rxtools.view.RxToast
import com.vondear.rxtools.view.dialog.RxDialog
import com.vondear.rxtools.view.dialog.RxDialogSureCancel
import kotlinx.android.synthetic.main.remocli_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor222
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/10/12 16:55
 * @change
 * @chang time
 * @class describe 远程接诊列表
 */

class RemoteCliAct : HoleBaseActivity() {
    var adapter: PeopleAdapter? = null
    var list: ArrayList<GhModle> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.remocli_act)
        arrow_back.setOnClickListener {
            finish()
        }
        var kong = View.inflate(this@RemoteCliAct, R.layout.empty_view, null)
        var manager: LinearLayoutManager = LinearLayoutManager(this)
        recycle.layoutManager = manager
        adapter = PeopleAdapter(R.layout.remote_item, list)
        adapter?.bindToRecyclerView(recycle)
        adapter?.setEmptyView(kong)
        recycle.adapter = adapter
        remo_smart.setOnRefreshListener {
            list.clear()
            getRemoteList()
        }
        adapter?.setOnItemChildClickListener { adapter, view, position ->
            when (view) {
                adapter.getViewByPosition(recycle, position, R.id.tingzhen) -> {
                    dialogShow(list.get(position).appointid.toString())
                }
                adapter.getViewByPosition(recycle, position, R.id.stop) -> {
                    dialogShow(list.get(position).appointid.toString())
                }
                adapter.getViewByPosition(recycle, position, R.id.enter) -> {
                    var intent: Intent = Intent(this@RemoteCliAct, ChatActivity::class.java)
                    intent.putExtra(Constant.EXTRA_USER_ID, list.get(position).imdaccount)
                    intent.putExtra(Constant.EXTRA_CHAT_TYPE, Constant.CHATTYPE_SINGLE)
                    intent.putExtra("userId", list.get(position).imraccount)
                    println(list.get(position).imraccount+"{{{{{{{{{{{{{{{{{")
                    startActivity(intent)
                }

                adapter.getViewByPosition(recycle, position, R.id.kaifang) -> {
                    if (list.get(position).status.equals("8")) {
                        // 表示已经开方
                        var intent: Intent = Intent(this@RemoteCliAct, DiagonsisDetialAct::class.java)
                        intent.putExtra("appointid", list.get(position).appointid)
                        intent.putExtra("id", list.get(position).billid)
                        startActivity(intent)
                    } else  //待开方状态
                        if (list.get(position).status.equals("7")) {
                            var intent: Intent = Intent(this@RemoteCliAct, DiagnosisAct::class.java)
                            intent.putExtra("appointid", list.get(position).appointid)
                            intent.putExtra("id", list.get(position).billid)
                            startActivity(intent)
                        }


                }
                adapter.getViewByPosition(recycle, position, R.id.jiezhen) -> {
                    if (list.get(position).status.equals("3")) {
                        dialogShows(list.get(position).appointid.toString(), position)
                    } else if (list.get(position).status.equals("5")) {
                        var intent: Intent = Intent(this@RemoteCliAct, ChatActivity::class.java)
                        intent.putExtra(Constant.EXTRA_USER_ID, list.get(position).imdaccount)
                        intent.putExtra(Constant.EXTRA_CHAT_TYPE, Constant.CHATTYPE_SINGLE)
                        intent.putExtra("userId", list.get(position).imraccount)
                        startActivity(intent)
                    }
                }

                adapter.getViewByPosition(recycle, position, R.id.end) -> {
                    dialogShowss(list.get(position).appointid.toString())
                }
                adapter.getViewByPosition(recycle, position, R.id.chakan) -> {
                    var intent: Intent = Intent(this@RemoteCliAct, ChatActivity::class.java)
                    intent.putExtra(Constant.EXTRA_USER_ID, list.get(position).imdaccount)
                    intent.putExtra(Constant.EXTRA_CHAT_TYPE, Constant.CHATTYPE_SINGLE)
                    intent.putExtra("userId", list.get(position).imraccount)
                    startActivity(intent)
                }
            }
        }
    }

    private fun dialogShow(ghid: String
    ) {
        var dialog: RxDialogSureCancel = RxDialogSureCancel(this@RemoteCliAct)
        dialog.setTitle("提示")
        dialog.setContent("您是否确定停诊，停诊后会自动帮患者退款退号？")
        dialog.contentView.gravity = Gravity.LEFT
        dialog.contentView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0f)
        dialog.contentView.setTextColor(Color.parseColor("#050505"))
        dialog.sureView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.sureView.setTextColor(Color.parseColor("#376FE9"))
        dialog.cancelView.setTextColor(Color.parseColor("#376FE9"))
        dialog.titleView.setTextColor(Color.parseColor("#050505"))
        dialog.titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.setSure("否")
        dialog.setCancel("是")
        dialog.show()
        dialog.setSureListener({

            dialog.dismiss()
        })
        dialog.setCancelListener {
            Tingzhen(ghid)
            dialog.dismiss()
        }
    }

    private fun dialogShowss(ghid: String
    ) {
        var dialog: RxDialogSureCancel = RxDialogSureCancel(this@RemoteCliAct)
        dialog.setTitle("提示")
        dialog.setContent("您是否要结束接诊？")
        dialog.contentView.gravity = Gravity.CENTER
        dialog.contentView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0f)
        dialog.contentView.setTextColor(Color.parseColor("#050505"))
        dialog.sureView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.sureView.setTextColor(Color.parseColor("#376FE9"))
        dialog.cancelView.setTextColor(Color.parseColor("#376FE9"))
        dialog.titleView.setTextColor(Color.parseColor("#050505"))
        dialog.titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.setSure("否")
        dialog.setCancel("是")
        dialog.show()
        dialog.setSureListener({
            dialog.dismiss()
        })
        dialog.setCancelListener {
            jieshu(ghid)
            dialog.dismiss()
        }
    }

    private fun dialogShows(id: String, pos: Int) {
        var dialog: RxDialogSureCancel = RxDialogSureCancel(this@RemoteCliAct)
        dialog.setTitle("提示")
        dialog.setContent("您是否要开始接诊该患者？")
        dialog.contentView.gravity = Gravity.LEFT
        dialog.contentView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.contentView.setTextColor(Color.parseColor("#050505"))
        dialog.sureView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0f)
        dialog.cancelView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0f)
        dialog.contentView.gravity = Gravity.CENTER
        dialog.sureView.setTextColor(Color.parseColor("#376FE9"))
        dialog.cancelView.setTextColor(Color.parseColor("#376FE9"))
        dialog.titleView.setTextColor(Color.parseColor("#050505"))
        dialog.titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.setSure("否")
        dialog.setCancel("是")
        dialog.show()
        dialog.setSureListener({
            dialog.dismiss()
        })
        dialog.setCancelListener {
            dialog.dismiss()
            jiezhen(id, pos)
        }
    }


    inner class PeopleAdapter(layoutResId: Int, data: ArrayList<GhModle>?) : com.chad.library.adapter.base.BaseQuickAdapter<GhModle, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: GhModle) {
            helper.setText(R.id.name, item?.uname ?: "")
                    .setText(R.id.time_duan, item.jzsj ?: "")
                    .setText(R.id.times, item?.createtime ?: "")
            when (item.type) {
                "1" -> {
                    helper.setText(R.id.time_duan, "上午")
                }
                "2" -> {
                    helper.setText(R.id.time_duan, "下午")
                }
            }
            var tz = helper.getView<Button>(R.id.tingzhen) // 停诊
            var stop = helper.getView<Button>(R.id.stop) // 停诊
            var enter = helper.getView<Button>(R.id.enter) // 进入
            var jz = helper.getView<Button>(R.id.jiezhen) // 接诊
            var js = helper.getView<Button>(R.id.end) // 结束
            var ck = helper.getView<Button>(R.id.chakan) // 查看
            var kf = helper.getView<Button>(R.id.kaifang) // 开方
            when (item.status) {
//                3 接诊 停诊
//                    5 结束 停诊 进入
//                        7 开方 查看
//                        8 开方 查看

                //待接诊状态
                "3" -> {
                    jz.visibility = View.VISIBLE
                    tz.visibility = View.VISIBLE
                    stop.visibility = View.GONE
                    enter.visibility = View.GONE
                    js.visibility = View.GONE
                    ck.visibility = View.GONE

                    
                    kf.visibility = View.GONE
                }
                "5" -> {
                    //接诊中状态
                    tz.visibility = View.GONE
                    jz.visibility = View.GONE
                    js.visibility = View.VISIBLE
                    stop.visibility = View.VISIBLE
                    enter.visibility = View.VISIBLE
                    ck.visibility = View.GONE
                    kf.visibility = View.GONE
                }
                "7" -> {
                    //等待开方状态
                    tz.visibility = View.GONE
                    stop.visibility = View.GONE
                    enter.visibility = View.GONE
                    jz.visibility = View.GONE
                    js.visibility = View.GONE
                    ck.visibility = View.VISIBLE
                    kf.visibility = View.VISIBLE
                }
                "8" -> {
                    // 已开方状态
                    tz.visibility = View.GONE
                    jz.visibility = View.GONE
                    stop.visibility = View.GONE
                    enter.visibility = View.GONE
                    js.visibility = View.GONE
                    ck.visibility = View.VISIBLE
                    kf.visibility = View.VISIBLE
                }
            }


            helper.addOnClickListener(R.id.kaifang)
                    .addOnClickListener(R.id.chakan)
                    .addOnClickListener(R.id.end)
                    .addOnClickListener(R.id.stop)
                    .addOnClickListener(R.id.enter)
                    .addOnClickListener(R.id.tingzhen)
                    .addOnClickListener(R.id.jiezhen)

        }

    }

    /***
     * 远程接诊首页列表
     */
    fun getRemoteList() {
        IpUrl.getInstance()!!.getRemoteList(ConstantViewMolde?.getToken()
        )
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<java.util.ArrayList<GhModle>>> {
                    override fun onError(e: Throwable?) {
                        //dialog?.dismiss()
                        remo_smart?.finishRefresh()
                    }

                    override fun onNext(t: HoleResponse<java.util.ArrayList<GhModle>>?) {
                        // dialog?.dismiss()
                        if (t != null) {
                            list?.addAll(t?.data!!)
                            adapter?.notifyDataSetChanged()
                        }
                    }

                    override fun onCompleted() {
                        remo_smart?.finishRefresh()
                    }
                })
    }

    /***
     * 停诊 根据ghid 停诊
     * @param ghid String
     */

    fun Tingzhen(ghid: String) {
        IpUrl.getInstance()!!.tingzhen(ConstantViewMolde?.getToken(), ghid)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<GhModle>> {
                    override fun onError(e: Throwable?) {
                        //dialog?.dismiss()
                        remo_smart?.finishRefresh()
                    }

                    override fun onNext(t: HoleResponse<GhModle>?) {
                        // dialog?.dismiss()
                        if (t?.code == 0) {
                            RxToast.normal(t.msg ?: "")
                            remo_smart?.autoRefresh()
                        }
                    }

                    override fun onCompleted() {
                        remo_smart?.finishRefresh()
                    }
                })
    }

    /***
     * 结束接诊
     * @param ghid String
     */
    fun jieshu(ghid: String) {
        IpUrl.getInstance()!!.endWZ(ConstantViewMolde?.getToken(), ghid)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<Any>> {
                    override fun onError(e: Throwable?) {
                        //dialog?.dismiss()
                        remo_smart?.finishRefresh()
                    }

                    override fun onNext(t: HoleResponse<Any>?) {
                        // dialog?.dismiss()
                        if (t?.code == 0) {
                            RxToast.normal(t.msg ?: "")
                            remo_smart?.autoRefresh()
                        }
                    }

                    override fun onCompleted() {
                        remo_smart?.finishRefresh()
                    }
                })
    }

    /***
     *  接诊操作
     * @param id String
     * @param pos Int
     */
    fun jiezhen(id: String, pos: Int) {
        var rxdia: RxDialog = RxDialog(this@RemoteCliAct)
        rxdia.setContentView(R.layout.dialog_loading)
        rxdia.show()
        IpUrl.getInstance()!!.getRemote(ConstantViewMolde?.getToken(), id)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<java.util.ArrayList<GhModle>>> {
                    override fun onError(e: Throwable?) {
                        rxdia?.dismiss()
                    }

                    override fun onNext(t: HoleResponse<java.util.ArrayList<GhModle>>?) {
                        if (t?.code == 0) {
                            var intent: Intent = Intent(this@RemoteCliAct, ChatActivity::class.java)
                            intent.putExtra(Constant.EXTRA_USER_ID, list.get(pos).imdaccount)
                            intent.putExtra(Constant.EXTRA_CHAT_TYPE, Constant.CHATTYPE_SINGLE)
                            intent.putExtra("userId", list.get(pos).imraccount)
                            startActivity(intent)
                        }
                    }

                    override fun onCompleted() {
                        rxdia?.dismiss()
                    }
                })
    }

    override fun onResume() {
        super.onResume()
        remo_smart?.autoRefresh()
    }
}