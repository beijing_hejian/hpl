package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Vibrator
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import cn.jpush.android.api.JPushInterface
import com.hyphenate.EMCallBack
import com.hyphenate.chat.EMClient
import com.peaceclient.hospitaldoctor.com.Base.BaseUrlUtil
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.Hy.DemoHelper
import com.peaceclient.hospitaldoctor.com.MainActivity
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.FileUtils
import com.peaceclient.hospitaldoctor.com.Utils.Utils
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.TokenBean
import com.peaceclient.hospitaldoctor.com.modle.UserModle
import com.vondear.rxtools.view.RxToast
import com.vondear.rxtools.view.dialog.RxDialog
import kotlinx.android.synthetic.main.login_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/9/16 15:10
 * @change
 * @chang time
 * @class describe 新登录页面
 */

class LoginDact : HoleBaseActivity() {

    var eyestatus: Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_act)
        val spannable = SpannableStringBuilder("同意和平里医院《用户协议》和《隐私政策》")
        //设置文字的前景色，2、4分别表示可以点击文字的起始和结束位置。
        spannable.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.home_green)),
            7,
            13,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        spannable.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.home_green)),
            14,
            spannable.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        //这个一定要记得设置，不然点击不生效
        agreement.setMovementMethod(LinkMovementMethod.getInstance())
        spannable.setSpan(TextClick(), 7, 13, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannable.setSpan(TextClickS(), 14, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        agreement.setText(spannable)

        password_eye.setOnClickListener {

            // 不可见密码
            if (!eyestatus) {
                password_eye.setImageResource(R.drawable.passord_open)
                passwords.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                passwords.setSelection(passwords.text.length)
            } else
            // 可见的密码
            {
                password_eye.setImageResource(R.drawable.password_close)
                passwords.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD or InputType.TYPE_CLASS_TEXT)
                passwords.setSelection(passwords.text.length)
            }
            eyestatus = !eyestatus
        }
        forget_.setOnClickListener {
            startActivity(Intent(this, ForgetPassAct::class.java))
        }
        login.setOnClickListener {
            if (postAction()) {
                GetToken(account.text.toString(), passwords.text.toString())
            }
        }
        regist.setOnClickListener {
            startActivity(Intent(this, RegisDact::class.java))
        }
    }

    private inner class TextClick() : ClickableSpan() {
        override fun onClick(widget: View) {
            //在此处理点击事件
            startActivity(Intent(this@LoginDact, AgreeMentAct::class.java))
        }

        override fun updateDrawState(ds: TextPaint) {
            ds.isUnderlineText = false
            //文字的颜色 ds.setUnderlineText(true); //是否设置下划线，true表示设置。
        }
    }

    private inner class TextClickS() : ClickableSpan() {
        override fun onClick(widget: View) {
            //在此处理点击事件
            startActivity(Intent(this@LoginDact, PrivacyAct::class.java))
        }

        override fun updateDrawState(ds: TextPaint) {
            ds.isUnderlineText = false
            //文字的颜色 ds.setUnderlineText(true); //是否设置下划线，true表示设置。
        }
    }

    private fun postAction(): Boolean {
        if (TextUtils.isEmpty(passwords.text)) {
            RxToast.normal("请输入密码")
            return false;
        } else {
            if (!Utils.isPasswordss(passwords.text!!.toString())) {
                RxToast.normal("密码格式不正确")
                return false
            }
        }
        if (checkbox.isChecked) {
            return true
        } else {
            var ani = AnimationUtils.loadAnimation(this, R.anim.shake_anim)
            bottome_layout.startAnimation(ani)
            val systemService: Vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            systemService.vibrate(300)
            RxToast.normal("请同意协议和隐私政策")

            return false
        }
        return true;
    }

    fun GetToken(account: String, password: String) {
        var loading: RxDialog = RxDialog(this)
        loading.setContentView(R.layout.dialog_loading)
        loading.show()
        var baseUrlUtil: BaseUrlUtil = BaseUrlUtil()
        baseUrlUtil.getInstance().Token("doctorApp", "496F5C0C-DFCD-4527-9", account, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<HoleResponse<TokenBean>> {
                override fun onError(e: Throwable?) {
                    loading?.dismiss()
                    RxToast.normal("账号或密码错误")
                }

                override fun onNext(t: HoleResponse<TokenBean>?) {
                    when (t?.code) {
                        0 -> {
                            ConstantViewMolde.setToken(t?.data?.access_token ?: "")
                            // 获取用户信息
                            baseUrlUtil.getInstance().getUser(
                                t?.data?.access_token
                                    ?: ""
                            ).subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(object : Observer<HoleResponse<UserModle>> {
                                    override fun onError(e: Throwable?) {
                                        loading?.dismiss()
                                        // RxToast.normal("账号或密码错误")
                                    }

                                    override fun onNext(t1: HoleResponse<UserModle>?) {
                                        if (t1?.data != null) {
                                            //  getList(t1?.data!!,t?.data?.access_token?:"")
                                            if (TextUtils.isEmpty(t1?.data?.imAccount)) {
                                                Myapplication.editor.putBoolean("isLogin", true)
                                                    .commit()
                                                FileUtils.saveObject(
                                                    this@LoginDact,
                                                    "LoginUser",
                                                    t1?.data
                                                );
                                                ConstantViewMolde.SetUser(t1?.data)
                                                handleLoginSuccess3()
                                                loading?.dismiss()
                                            } else {
                                                // 登录环信
                                                login(t1.data!!, loading)

                                            }
                                        } else {
                                            loading?.dismiss()
                                            RxToast.normal("账号或密码错误")
                                        }
                                    }

                                    override fun onCompleted() {

                                    }
                                })


                        }

                        1 -> {
                            loading?.dismiss();RxToast.normal(t?.msg!!)
                        }

                        else -> RxToast.normal(t?.msg!!)
                    }
                }

                override fun onCompleted() {
                    loading?.dismiss()
                }
            })
    }

    private fun login(user: UserModle, dia: RxDialog) {
        try {
            if (!TextUtils.isEmpty(user?.imAccount)) {
                EMClient.getInstance().login(user.imAccount, user.imSecret, object : EMCallBack {
                    override fun onSuccess() {
                        DemoHelper.getInstance().setCurrentUserName(user.account as String)
                        EMClient.getInstance().groupManager().loadAllGroups()
                        EMClient.getInstance().chatManager().loadAllConversations()
                        ConstantViewMolde.SetUser(user)
                        Myapplication.editor.putBoolean("isLogin", true).commit()
                        FileUtils.saveObject(this@LoginDact, "LoginUser", user)
                        // 登录成功后，如果后端云没有缓存用户信息，则新增一个用户
                        // UserWebManager.createUser(userId, nickName, avatarUrl);
                        // 自动加martin为好友，并自动发送一条消       息
                        // ** manually load all local groups and conversation
                        //推送的昵称
                        if (!TextUtils.isEmpty(user?.user?.docName)) {
                            val nick = user.user?.docName as String
                        }

                        val updatenick = EMClient.getInstance().pushManager()
                            .updatePushNickname(if (user.user?.docName === "") "" else user.user?.depName)
                        if (!updatenick) {
                            Log.e("LoginActivity", "update current user nick fail")
                        }
                        DemoHelper.getInstance().getUserProfileManager().asyncGetCurrentUserInfo()
                        JPushInterface.setAlias(this@LoginDact, 0, user?.account ?: "")
                        startActivity(Intent(this@LoginDact, MainActivity::class.java))
                        finish()
                        dia?.dismiss()
                    }

                    override fun onError(i: Int, s: String) {
                        runOnUiThread {
                            println(i.toString() + "错误码" + s)
                            RxToast.normal("登录失败,请稍后重试")
                        }
                        dia?.dismiss()
                    }

                    override fun onProgress(i: Int, s: String) {

                    }

                })
            } else {

            }
        } catch (e: Exception) {
            println(e.toString())
        }

    }

    fun handleLoginSuccess3() {
        startActivity(Intent(this@LoginDact, MainActivity::class.java))
//        val bundle = intent.getBundleExtra(Myapplication.SHARED_NAME)
//        val className = bundle.getString(Myapplication.CLASSNAME)
//        if (className.equals("com.peaceclient.com.Activity.LoginAct")) {
//            CallBackUtils.LoginSuccess(0)
//            CallBackUtils.ChangeState(0)
//        } else {
//            intent.component = ComponentName(this@LoginDact, className)
//            bundle.remove(Myapplication.CLASSNAME)
//            intent.putExtra(Myapplication.SHARED_NAME, bundle)
//            CallBackUtils.LoginSuccess(intent)
//        }
        finish()
    }


//    fun getList(user: UserModle,token: String) {
//        IpUrl.getInstance()!!.getModle(token,"")
//                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<UserModle>> {
//                    override fun onError(e: Throwable?) {
//                        prosmarts?.finishRefresh()
//                    }
//
//                    override fun onNext(t: HoleResponse<UserModle>?) {
//                        if (t?.code == 0) {
//                            if (t?.data != null) {
//                                user.depCode = t?.data?.depCode?:""
//                                user.depId = t?.data?.depId?:""
//                                user.depName = t?.data?.depName?:""
//                                user.goodAt = t?.data?.goodAt?:""
//                                user.introduction = t?.data?.introduction?:""
//                                user.openGraphic = t?.data?.openGraphic !!
//                                user.visitorsGraphic = t?.data?.visitorsGraphic?:""
//                                user.costGraphic = t?.data?.costGraphic ?:""
//                                user.costRemote = t?.data?.costRemote ?:""
//
//                            }
//                        }
//                    }
//
//                    override fun onCompleted() {
//                        prosmarts?.finishRefresh()
//                    }
//                })
//    }


}