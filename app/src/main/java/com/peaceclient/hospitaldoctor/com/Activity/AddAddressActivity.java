package com.peaceclient.hospitaldoctor.com.Activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.google.gson.Gson;
import com.jakewharton.rxbinding4.view.RxView;
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity;
import com.peaceclient.hospitaldoctor.com.R;
import com.peaceclient.hospitaldoctor.com.Utils.FileUtils;
import com.peaceclient.hospitaldoctor.com.Utils.KeyboardUtils;
import com.peaceclient.hospitaldoctor.com.Utils.Utils;
import com.peaceclient.hospitaldoctor.com.View.StatusBarHeightView;
import com.peaceclient.hospitaldoctor.com.View.SwitchButton;
import com.peaceclient.hospitaldoctor.com.modle.ProvinceBean;
import com.vondear.rxtools.view.RxToast;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.rxjava3.functions.Consumer;
import kotlin.Unit;

/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com
 * @class describe
 * @anthor admin
 * @time 2021/9/4 14:00
 * @change
 * @chang time
 * @class describe 添加地址页面
 */

public class AddAddressActivity extends HoleBaseActivity {

    @BindView(R.id.arrow_back)
    ImageView arrowBack;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.add_person)
    TextView addPerson;
    @BindView(R.id.rel)
    RelativeLayout rel;
    @BindView(R.id.top)
    StatusBarHeightView top;
    @BindView(R.id.recent)
    View recent;
    @BindView(R.id.shengshi)
    TextView shengshi;
    @BindView(R.id.add)
    Button add;
    @BindView(R.id.swic)
    SwitchButton swic;
    @BindView(R.id.shoujianren)
    EditText shoujianren;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.adderss_detial)
    EditText adderssDetial;
    private List<ProvinceBean> options1Items = new ArrayList<>();
    private ArrayList<ArrayList<String>> options2Items = new ArrayList<>();
    private ArrayList<ArrayList<ArrayList<String>>> options3Items = new ArrayList<>();
    private Thread thread;
    private static final int MSG_LOAD_DATA = 0x0001;
    private static final int MSG_LOAD_SUCCESS = 0x0002;
    private static final int MSG_LOAD_FAILED = 0x0003;

    private static boolean isLoaded = false;
    private OptionsPickerView pvOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addaddress_act);
        ButterKnife.bind(this);
        mHandler.sendEmptyMessage(MSG_LOAD_DATA);
        shengshi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLoaded) {
                    KeyboardUtils.hideKeyboard(shengshi);
                    showPickerView();
                } else {
                    //Toast.makeText(JsonDataActivity.this, "Please waiting until the data is parsed", Toast.LENGTH_SHORT).show();
                }
            }

        });
        swic.setmOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void OnCheckedChanged(boolean isChecked) {

            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Vertify()) {
                    //swic.isChecked();
                }
            }
        });
        RxView.clicks(arrowBack).subscribe(new Consumer<Unit>() {
            @Override
            public void accept(Unit unit) throws Throwable {
                finish();
            }
        });
    }

    private boolean Vertify() {
        if (TextUtils.isEmpty(shoujianren.getText())) {
            RxToast.normal("收件人不能为空");
            return false;
        }
        if (TextUtils.isEmpty(phone.getText())) {
            RxToast.normal("手机号不能为空");
            return false;
        } else if (Utils.isPhone(phone.getText().toString())) {
            RxToast.normal("请输入正确的手机号");
            return false;
        }
        if (TextUtils.isEmpty(adderssDetial.getText())) {
            RxToast.normal("");
            return false;
        }
        return true;

    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_LOAD_DATA:
                    if (thread == null) {//如果已创建就不再重新创建子线程了
                        Toast.makeText(AddAddressActivity.this, "Begin Parse Data", Toast.LENGTH_SHORT).show();
                        thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                // 子线程中解析省市区数据
                                initJsonData();
                            }
                        });
                        thread.start();
                    }
                    break;
                case MSG_LOAD_SUCCESS:
                    isLoaded = true;
                    break;
                case MSG_LOAD_FAILED:
                    break;
            }
        }
    };


    private void showPickerView() {// 弹出选择器
        if (pvOptions != null) {
            pvOptions.show();
        } else {


            //返回的分别是三个级别的选中位置
// Toast.makeText(AddAddressActivity.this, tx, Toast.LENGTH_SHORT).show();
//设置选中项文字颜色
            pvOptions = new OptionsPickerBuilder(this, new OnOptionsSelectListener() {
                @Override
                public void onOptionsSelect(int options1, int options2, int options3, View v) {
                    //返回的分别是三个级别的选中位置
                    String opt1tx = options1Items.size() > 0 ?
                            options1Items.get(options1).getPickerViewText() : "";
                    String opt2tx = options2Items.size() > 0
                            && options2Items.get(options1).size() > 0 ?
                            options2Items.get(options1).get(options2) : "";
                    String opt3tx = options2Items.size() > 0
                            && options3Items.get(options1).size() > 0
                            && options3Items.get(options1).get(options2).size() > 0 ?
                            options3Items.get(options1).get(options2).get(options3) : "";
                    String tx = opt1tx + opt2tx + opt3tx;
                    shengshi.setText(tx);
                    // Toast.makeText(AddAddressActivity.this, tx, Toast.LENGTH_SHORT).show();
                }
            })
                    .setTitleText("请选择城市")
                    .setTitleBgColor(getResources().getColor(R.color.eee))
                    .setBgColor(getResources().getColor(R.color.hole_background_gree))
                    .setCancelColor(getResources().getColor(R.color.c21))
                    .setTitleColor(getResources().getColor(R.color.c21))
                    .setSubmitColor(getResources().getColor(R.color.home_green))
                    .setDividerColor(getResources().getColor(R.color.eee))
                    .setTextColorCenter(getResources().getColor(R.color.c21)) //设置选中项文字颜色
                    .setContentTextSize(18)

                    .setDecorView((ViewGroup) getWindow().getDecorView().findViewById(android.R.id.content))
                    .build();
        /*pvOptions.setPicker(options1Items);//一级选择器
        pvOptions.setPicker(options1Items, options2Items);//二级选择器*/
            pvOptions.setPicker(options1Items, options2Items, options3Items);//三级选择器

            pvOptions.show();
        }
    }

    private void initJsonData() {//解析数据

        /**
         * 注意：assets 目录下的Json文件仅供参考，实际使用可自行替换文件
         * 关键逻辑在于循环体
         *
         * */
        String JsonData = FileUtils.getJson(this, "province.json");//获取assets目录下的json文件数据

        ArrayList<ProvinceBean> jsonBean = parseData(JsonData);//用Gson 转成实体

        /**
         * 添加省份数据
         *
         * 注意：如果是添加的JavaBean实体，则实体类需要实现 IPickerViewData 接口，
         * PickerView会通过getPickerViewText方法获取字符串显示出来。
         */
        options1Items = jsonBean;

        for (int i = 0; i < jsonBean.size(); i++) {//遍历省份
            ArrayList<String> cityList = new ArrayList<>();//该省的城市列表（第二级）
            ArrayList<ArrayList<String>> province_AreaList = new ArrayList<>();//该省的所有地区列表（第三极）

            for (int c = 0; c < jsonBean.get(i).getCity().size(); c++) {//遍历该省份的所有城市
                String cityName = jsonBean.get(i).getCity().get(c).getName();
                cityList.add(cityName);//添加城市
                ArrayList<String> city_AreaList = new ArrayList<>();//该城市的所有地区列表

                //如果无地区数据，建议添加空字符串，防止数据为null 导致三个选项长度不匹配造成崩溃
                /*if (jsonBean.get(i).getCityList().get(c).getArea() == null
                        || jsonBean.get(i).getCityList().get(c).getArea().size() == 0) {
                    city_AreaList.add("");
                } else {
                    city_AreaList.addAll(jsonBean.get(i).getCityList().get(c).getArea());
                }*/
                city_AreaList.addAll(jsonBean.get(i).getCity().get(c).getArea());
                province_AreaList.add(city_AreaList);//添加该省所有地区数据
            }

            /**
             * 添加城市数据
             */
            options2Items.add(cityList);

            /**
             * 添加地区数据
             */
            options3Items.add(province_AreaList);
        }

        mHandler.sendEmptyMessage(MSG_LOAD_SUCCESS);

    }


    public ArrayList<ProvinceBean> parseData(String result) {//Gson 解析
        ArrayList<ProvinceBean> detail = new ArrayList<>();
        try {
            JSONArray data = new JSONArray(result);
            Gson gson = new Gson();
            for (int i = 0; i < data.length(); i++) {
                ProvinceBean entity = gson.fromJson(data.optJSONObject(i).toString(), ProvinceBean.class);
                detail.add(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
            mHandler.sendEmptyMessage(MSG_LOAD_FAILED);
        }
        return detail;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }
    }
}
