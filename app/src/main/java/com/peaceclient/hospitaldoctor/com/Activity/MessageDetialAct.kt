package com.peaceclient.hospitaldoctor.com.Activity

import android.os.Bundle
import com.jakewharton.rxbinding4.view.clicks
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.MessageModle
import kotlinx.android.synthetic.main.message_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor222
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2022/9/6 17:37
 * @change
 * @chang time
 * @class describe 消息详情页面
 */

class MessageDetialAct : HoleBaseActivity(){
    var id :String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.message_act)
        id = intent.getStringExtra("id")
        GetMessdetial(id);
        arrow_back.clicks().throttleFirst(2000, TimeUnit.MICROSECONDS).subscribeOn(io.reactivex.rxjava3.android.schedulers.AndroidSchedulers.mainThread())
                .subscribe {
                    finish()
                }

    }

    fun GetMessdetial(string: String ) {
        IpUrl.getInstance()!!.GetMess(id)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<MessageModle.MessageDetail>> {
                    override fun onError(e: Throwable?) {

                    }
                    override fun onNext(t: HoleResponse<MessageModle.MessageDetail>?) {
                        if (t?.data != null){
                            type.setText( t.data?.title ?: "");
                                   time .setText( t.data?.createTime ?: "")
                                 introduction   .setText( t.data?.noticeContent ?: "")

                        }

                    }
                    override fun onCompleted() {
                    }
                })
    }

}