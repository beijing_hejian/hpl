/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Utils;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

import java.io.File;
import java.io.IOException;

/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name project
 * @class name：com.homeclientz.com.Utils
 * @class describe
 * @anthor admin
 * @time 2019/10/15 15:38
 * @change
 * @chang time
 * @class describe
 */

public class ImageFileUtils {

      public static String getDiskCacheDir(Context context) {
            String cachePath = null;
            if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                      || !Environment.isExternalStorageRemovable()) {
                  cachePath = context.getExternalCacheDir().getPath();
            } else {
                  cachePath = context.getCacheDir().getPath();
            }
            return cachePath;
      }
      public static File getCacheFile(File parent, String child) {
            // 创建File对象，用于存储拍照后的图片
            File file = new File(parent, child);
            if (file.exists()) {
                  file.delete();
            }
            try {
                  file.createNewFile();
            } catch (IOException e) {
                  e.printStackTrace();
            }
            return file;
      }

      public static String uriToPath(Uri uri,Context context) {
            String path = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                  if (DocumentsContract.isDocumentUri(context, uri)) {
                        // 如果是document类型的Uri，则通过document id处理
                        String docId = DocumentsContract.getDocumentId(uri);
                        if ("com.android.providers.media.documents".equals(uri.getAuthority())) {
                              String id = docId.split(":")[1]; // 解析出数字格式的id
                              String selection = MediaStore.Images.Media._ID + "=" + id;
                              path = getImagePath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, selection,context);
                        } else if ("com.android.providers.downloads.documents".equals(uri.getAuthority())) {
                              Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(docId));
                              path = getImagePath(contentUri, null,context);
                        }
                  } else if ("content".equalsIgnoreCase(uri.getScheme())) {
                        // 如果是content类型的Uri，则使用普通方式处理
                        path = getImagePath(uri, null,context);
                  } else if ("file".equalsIgnoreCase(uri.getScheme())) {
                        // 如果是file类型的Uri，直接获取图片路径即可
                        path = uri.getPath();
                  }
            }
            return path;
      }
      public static String getImagePath(Uri uri, String selection,Context context) {
            String path = null;
            // 通过Uri和selection来获取真实的图片路径
            Cursor cursor = context.getContentResolver().query(uri, null, selection, null, null);
            if (cursor != null) {
                  if (cursor.moveToFirst()) {
                        path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                  }
                  cursor.close();
            }
            return path;
      }
//      /**
//       * 剪裁图片
//       */
//      private void startPhotoZoom(File file, int size) {
////        Log.i("TAG", getImageContentUri(getActivity(), file) + "裁剪照片的真实地址");
//            try {
//                  Intent intent = new Intent("com.android.camera.action.CROP");
//                  intent.setDataAndType(getImageContentUri(getActivity(), file), "image/*");//自己使用Content Uri替换File Uri
//                  intent.putExtra("crop", "true");
//                  intent.putExtra("aspectX", 1);
//                  intent.putExtra("aspectY", 1);
//                  intent.putExtra("outputX", 400);
//                  intent.putExtra("outputY", 400);
//                  intent.putExtra("scale", true);
//                  intent.putExtra("return-data", false);
//                  intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cacheFile));//定义输出的File Uri
//                  intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
//                  intent.putExtra("noFaceDetection", true);
//                  startActivityForResult(intent, REQUESTCODE_CUTTING);
//            } catch (ActivityNotFoundException e) {
//                  String errorMessage = "Your device doesn't support the crop action!";
//                  Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
//            } catch (Exception e) {
//                  e.printStackTrace();
//            }
//      }

      /**
       * 转化地址为content开头
       *
       * @param context
       * @param imageFile
       * @return
       */
      public static Uri getImageContentUri(Context context, File imageFile) {
            String filePath = imageFile.getAbsolutePath();
            Cursor cursor = context.getContentResolver().query(
                      MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                      new String[]{MediaStore.Images.Media._ID},
                      MediaStore.Images.Media.DATA + "=? ",
                      new String[]{filePath}, null);

            if (cursor != null && cursor.moveToFirst()) {
                  int id = cursor.getInt(cursor
                            .getColumnIndex(MediaStore.MediaColumns._ID));
                  Uri baseUri = Uri.parse("content://media/external/images/media");
                  return Uri.withAppendedPath(baseUri, "" + id);
            } else {
                  if (imageFile.exists()) {
                        ContentValues values = new ContentValues();
                        values.put(MediaStore.Images.Media.DATA, filePath);
                        return context.getContentResolver().insert(
                                  MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                  } else {
                        return null;
                  }
            }
      }

}
