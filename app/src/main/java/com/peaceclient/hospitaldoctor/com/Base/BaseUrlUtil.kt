/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Base


import com.peaceclient.hospitaldoctor.com.InterFace.RetrofitUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Base
 * @class describe
 * @anthor admin
 * @time 2021/7/28 15:35
 * @change
 * @chang time
 * @class describe retrofit基础工具
 */

class BaseUrlUtil {
    private var retrofitService: RetrofitUrl? = null
   private val TestUrl = "https://api.app.hplyy.com/"
    private val TestUrls = "http://192.168.1.31:9200/"
//    private val TestUrl = "https://v29047i063.oicp.vip/"
    var Urlhead = "http://www.j1dc.com/sss-web"
    var newlogin = "/user/userLogin"
    var picture = "/download?filePath="
    private var retrofitServices: RetrofitUrl? = null
    private var retrofitServicess: RetrofitUrl? = null
    fun getInstance(): RetrofitUrl {
        if (retrofitService == null) {
            synchronized(RetrofitUrl::class.java) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val client = OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .addInterceptor(interceptor)
                        .retryOnConnectionFailure(true)
                        .build()
                val retrofit = Retrofit.Builder() //设置数据解析器
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .client(client) //设置网络请求的Url地址
                        .baseUrl(getBase())
                        .build() // 创建网络请求接口的实例
                retrofitService = retrofit.create(RetrofitUrl::class.java)
            }
        }
        return retrofitService!!
    }
    fun getInstanceWX(): RetrofitUrl {
        if (retrofitServices == null) {
            synchronized(BaseUrlUtil::class.java) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                //RetrofitLogInterceptor interceptor1 = new RetrofitLogInterceptor();
                val client = OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .addInterceptor(interceptor).retryOnConnectionFailure(true)
                        .build()
                val retrofit = Retrofit.Builder()//设置数据解析器
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .client(client)
                        //设置网络请求的Url地址
                        .baseUrl("https://api.weixin.qq.com/sns/oauth2/")
                        .build()// 创建网络请求接口的实例
                retrofitServices = retrofit.create(RetrofitUrl::class.java)
            }
        }
        return retrofitServices!!
    };
    fun getFileInstance(): RetrofitUrl {
        if (retrofitServicess == null) {
            synchronized(RetrofitUrl::class.java) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val client = OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .addInterceptor(interceptor)
                        .retryOnConnectionFailure(true)
                        .build()
                val retrofit = Retrofit.Builder()//设置数据解析器
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .client(client)//设置网络请求的Url地址
                        .baseUrl("http://192.168.0.105:8084")
                        .build()// 创建网络请求接口的实例
                retrofitServicess = retrofit.create(RetrofitUrl::class.java)
            }
        }
        return retrofitServicess!!
    }
    fun getBase(): String {
        return TestUrls

    }
}