/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Receiver;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.gson.Gson;
import com.peaceclient.hospitaldoctor.com.MainActivity;
import com.peaceclient.hospitaldoctor.com.R;

import androidx.annotation.RequiresApi;
import cn.jpush.android.api.CmdMessage;
import cn.jpush.android.api.CustomMessage;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.JPushMessage;
import cn.jpush.android.api.NotificationMessage;
import cn.jpush.android.service.JPushMessageReceiver;

/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name tags
 * @class name：com.denet.nei.com.Receiver
 * @class describe
 * @anthor admin
 * @time 2021/3/12 10:12
 * @change
 * @chang time
 * @class describe
 */

public class PushMessageReceiver extends JPushMessageReceiver {
      private static final String TAG = "PushMessageReceiver";

      @RequiresApi(api = Build.VERSION_CODES.M)
      @Override
      public void onMessage(Context context, CustomMessage customMessage) {
            Log.e(TAG, "[onMessage] " + customMessage);
            processCustomMessage(context, customMessage.extra);
      }

      @Override
      public void onNotifyMessageOpened(Context context, NotificationMessage message) {
            Log.e(TAG, "[onNotifyMessageOpened] " + message);
            Intent intent = null;
            try {
                  Gson gson = new Gson();
                  //打开自定义的Activity
//                  MessageNoti noBean = JsonUtils.json2bean(message.notificationExtras, messageNoBean.class);
//                  MessageNoti noBean = gson.fromJson(message.notificationExtras, MessageNoti.class);
//                  Map<String, String> map = new HashMap<>();
//                  map.put("1", "com.denet.nei.com.Activity.MessageNotiActivity");
//                  map.put("2", "com.denet.nei.com.Activity.SchduleNotiActivity");
//                  map.put("3", "com.denet.nei.com.Activity.StageNotiActivity");
//                  map.put("4", "com.denet.nei.com.Activity.MileNotiActivity");
//                  map.put("5", "com.denet.nei.com.Activity.OtherHolActivity");
//                  map.put("6", "com.denet.nei.com.Activity.DetaiPartDetailActivity");
//
//                  if (!TextUtils.isEmpty(noBean.getType())) {
//
//                        switch (noBean.getType()) {
//                              //消息详情
//
//                              case "1":
//                                    Class<?> forName = Class.forName(map.get("1"));
//                                    intent = new Intent(Myapplication.mcontext, forName);
//                                    intent.putExtra("id", noBean.getId());
//                                    break;
//                              //日程详情
//                              case "2":
//                                    intent = new Intent(Myapplication.mcontext, Class.forName(map.get("2")));
//                                    intent.putExtra("id", noBean.getId());
//                                    break;
//                              //项目记录详情
//                              case "3":
//                                    intent = new Intent(Myapplication.mcontext, Class.forName(map.get("3")));
//                                    intent.putExtra("id", Integer.parseInt(noBean.getId()));
//                                    break;
//                              //项目里程碑
//                              case "4":
//                                    intent = new Intent(Myapplication.mcontext, Class.forName(map.get("4")));
//                                    intent.putExtra("id", noBean.getId());
//                                    break;
//                              // 审批详情
//                              case "5":
//                                    intent = new Intent(Myapplication.mcontext, Class.forName(map.get("5")));
//                                    intent.putExtra("id", Integer.parseInt(noBean.getId()));
//                                    break;
//                                    case "6":
//                                    intent = new Intent(Myapplication.mcontext, Class.forName(map.get("6")));
//                                    intent.putExtra("id", Integer.parseInt(noBean.getId()));
//                                    break;
//                        }
//                  }
//                  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                  context.startActivity(intent);

            } catch (Throwable throwable) {
                  System.out.println(throwable.toString());
            }
      }

      @Override
      public void onMultiActionClicked(Context context, Intent intent) {
            Log.e(TAG, "[onMultiActionClicked] 用户点击了通知栏按钮");
            String nActionExtra = intent.getExtras().getString(JPushInterface.EXTRA_NOTIFICATION_ACTION_EXTRA);
            //开发者根据不同 Action 携带的 extra 字段来分配不同的动作。
            if (nActionExtra == null) {
                  Log.d(TAG, "ACTION_NOTIFICATION_CLICK_ACTION nActionExtra is null");
                  return;
            }
            if (nActionExtra.equals("my_extra1")) {
                  // Log.e(TAG, "[onMultiActionClicked] 用户点击通知栏按钮一");
            } else if (nActionExtra.equals("my_extra2")) {
                  //Log.e(TAG, "[onMultiActionClicked] 用户点击通知栏按钮二");
            } else if (nActionExtra.equals("my_extra3")) {
                  //Log.e(TAG, "[onMultiActionClicked] 用户点击通知栏按钮三");
            } else {
                  //Log.e(TAG, "[onMultiActionClicked] 用户点击通知栏按钮未定义");
            }
      }

      @RequiresApi(api = Build.VERSION_CODES.M)
      @Override
      public void onNotifyMessageArrived(Context context, NotificationMessage message) {
            Log.e(TAG, "[onNotifyMessageArrived] " + message);
            processCustomMessage(context, message.notificationExtras);
      }

      @Override
      public void onNotifyMessageDismiss(Context context, NotificationMessage message) {
            Log.e(TAG, "[onNotifyMessageDismiss] " + message);
      }

      @Override
      public void onRegister(Context context, String registrationId) {
            Log.e(TAG, "[onRegister] " + registrationId);
      }

      @Override
      public void onConnected(Context context, boolean isConnected) {
            Log.e(TAG, "[onConnected] " + isConnected);
      }

      @Override
      public void onCommandResult(Context context, CmdMessage cmdMessage) {
            Log.e(TAG, "[onCommandResult] " + cmdMessage);
      }

      @Override
      public void onTagOperatorResult(Context context, JPushMessage jPushMessage) {
            // TagAliasOperatorHelper.getInstance().onTagOperatorResult(context,jPushMessage);
            super.onTagOperatorResult(context, jPushMessage);
      }

      @Override
      public void onCheckTagOperatorResult(Context context, JPushMessage jPushMessage) {
            //  TagAliasOperatorHelper.getInstance().onCheckTagOperatorResult(context,jPushMessage);
            super.onCheckTagOperatorResult(context, jPushMessage);
      }

      @Override
      public void onAliasOperatorResult(Context context, JPushMessage jPushMessage) {
            // TagAliasOperatorHelper.getInstance().onAliasOperatorResult(context,jPushMessage);
            super.onAliasOperatorResult(context, jPushMessage);
      }

      @Override
      public void onMobileNumberOperatorResult(Context context, JPushMessage jPushMessage) {
            //  TagAliasOperatorHelper.getInstance().onMobileNumberOperatorResult(context,jPushMessage);
            super.onMobileNumberOperatorResult(context, jPushMessage);
      }

      //send msg to MainActivity
      private void processCustomMessage(Context context, CustomMessage message) {
            Log.e(TAG, "[onNotifyMessageOpened] " + message);
            Intent intent = null;
            intent.setClass(context, MainActivity.class);
            context.startActivity(intent);
      }


      @Override
      public void onNotificationSettingsCheck(Context context, boolean isOn, int source) {
            super.onNotificationSettingsCheck(context, isOn, source);
            Log.e(TAG, "[onNotificationSettingsCheck] isOn:" + isOn + ",source:" + source);
      }


      /**
       * 自定义推送的声音
       *
       * @param context
       * @param
       */
      @RequiresApi(api = Build.VERSION_CODES.M)
      private void processCustomMessage(Context context, String string) {
            NotificationCompat.Builder notification = new NotificationCompat.Builder(context);
            //这一步必须要有而且setSmallIcon也必须要，没有就会设置自定义声音不成功
            notification.setAutoCancel(true).setSmallIcon(R.mipmap.ic_launcher);
            notification.setVibrate(new long[]{10, 5});
            notification.setLights(1, 0, 1);
            String alert = string;
            if (alert != null && !alert.equals("")) {
                  notification.setSound(
                            Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.smile));
            }
            //最后刷新notification是必须的
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(1, notification.build());

      }


}
