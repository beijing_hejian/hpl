/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Activity

import android.graphics.Color
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.TypedValue
import android.view.Gravity
import android.widget.CheckBox
import android.widget.TextView
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.JzCardModle
import com.vondear.rxtools.view.dialog.RxDialog
import com.vondear.rxtools.view.dialog.RxDialogSureCancel
import kotlinx.android.synthetic.main.people_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/8/31 16:34
 * @change
 * @chang time
 * @class describe 选择就诊人页面
 */

class PeopleAct : HoleBaseActivity() {
    private var list: ArrayList<JzCardModle> = arrayListOf()
    private var adapter: PeopleAdapter? = null
    private var PeopleTem: JzCardModle = JzCardModle()
    private var  dialog: RxDialog? = null
    private var con: String = "1、保证患者就医安全，按国家网络安全要求，所有患者必须进行实名认证。\n" +
            "2、未实名认证的患者不能使用本院线上医疗服务。\n" +
            "3、实名认证: 身份证建卡绑卡:上传身份证照片，实名认证通过后，可在线建卡或绑定已有就诊卡。\n" +
            "4、限于北京市医保政策要求，暂不支持在线医保身份认证。\n" +
            "5、为保护患者权益，严禁未经允许绑定他人就诊卡，否则医院有权禁用您的账户，产生的一切不良后果由用户本人承担。"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.people_act)
        dialog = RxDialog(this@PeopleAct,R.layout.dialog_loading)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setCanceledOnTouchOutside(false)
        var manager: LinearLayoutManager = LinearLayoutManager(this)
        people_recycle.layoutManager = manager
        adapter = PeopleAdapter(R.layout.people_item, list)
        people_recycle.adapter = adapter
        adapter?.setOnItemClickListener { adapter, view, position ->
            dialog?.show()
            IpUrl.getInstance()!!.SetDefultJz(ConstantViewMolde?.getToken(),list.get(position).id.toString())
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<String>> {
                        override fun onError(e: Throwable?) {
                            dialog?.dismiss()
                        }
                        override fun onNext(t: HoleResponse<String>?) {
                            dialog?.dismiss()
                            if (t?.code == 0){
                                PeopleTem.defaultCard =false
                                adapter.notifyDataSetChanged()
                                list.get(position).defaultCard = true
                                PeopleTem = list.get(position)
                                adapter.notifyDataSetChanged()
                            }
                        }
                        override fun onCompleted() {
                        }
                    })
        }
        getZIXUN()
        add_people.setOnClickListener {
           dialogShow()
        }
    }
    inner class PeopleAdapter(layoutResId: Int, data: ArrayList<JzCardModle>?) : com.chad.library.adapter.base.BaseQuickAdapter<JzCardModle, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: JzCardModle) {
            var checkBox: CheckBox = helper.getView<CheckBox>(R.id.icon)
            helper.getView<TextView>(R.id.idcard).setText(com.peaceclient.hospitaldoctor.com.Utils.Utils.MakeCardID(item?.idCard ?: ""))
            helper.getView<TextView>(R.id.phone).setText(item?.phone ?: "")
            helper.getView<TextView>(R.id.name).setText(item?.fullName ?: "")
            if (item?.defaultCard!!) {
                checkBox.isChecked = true
            } else {
                checkBox.isChecked = false
            }
        }
    }
    private fun dialogShow() {
        var dialog: RxDialogSureCancel = RxDialogSureCancel(this@PeopleAct)
        dialog.setTitle("绑卡须知")
        dialog.setContent(con)
        dialog.contentView.gravity = Gravity.LEFT
        dialog.contentView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12.0f)
        dialog.contentView.setTextColor(Color.parseColor("#050505"))
        dialog.sureView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.cancelView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.sureView.setTextColor(Color.parseColor("#376FE9"))
        dialog.cancelView.setTextColor(Color.parseColor("#376FE9"))
        dialog.titleView.setTextColor(Color.parseColor("#050505"))
        dialog.titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.setCancel("同意")
        dialog.setSure("取消")
        dialog.show()
        dialog.setCancelListener({
            dialog.dismiss()
           // startActivity(Intent(this@PeopleAct, CreatCardAct::class.java))
        })
        dialog.setSureListener({
            dialog.dismiss()

        })
    }
    fun getZIXUN() {
        dialog?.show()
        IpUrl.getInstance()!!.SearchJZ(ConstantViewMolde?.getToken())
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<java.util.ArrayList<JzCardModle>>> {
                    override fun onError(e: Throwable?) {
                        dialog?.dismiss()
                    }
                    override fun onNext(t: HoleResponse<java.util.ArrayList<JzCardModle>>?) {
                        dialog?.dismiss()
                        if (t != null) {
                            list?.addAll(t?.data!!)
                            for (i in 0 until list?.size - 1) {
                                if (list.get(i).defaultCard == true) {
                                    PeopleTem = list.get(i)
                                }
                            }
                            adapter?.notifyDataSetChanged()
                        }
                    }

                    override fun onCompleted() {
                    }
                })
    }
}