package com.peaceclient.hospitaldoctor.com.Utils;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.core.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Transformation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.peaceclient.hospitaldoctor.com.R;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**

 */
public class Utils {
    private static long lastClickTime =0;
    private static Transformation transformation;
    static double x_pi = 3.14159265358979324 * 3000.0 / 180.0;
    // π
    static double pi = 3.1415926535897932384626;
    // 长半轴
    static double a = 6378245.0;
    // 扁率
    static double ee = 0.00669342162296594323;
    /**
     * @return 防止按钮重复点击
     */
    public static boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if (0 < timeD && timeD < 500) {
            return true;
        }
        lastClickTime = time;
        return false;
    }


    /**
     * @param
     */
    public static void disabledView(final View v) {
        v.setClickable(false);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                v.setClickable(true);
            }
        }, 1500);
    }
    public static void saveBitmapFile(Bitmap bitmap, String s) {
        File file = new File(s);//将要保存图片的路径
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
            bos.flush();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap compressImage(Bitmap image, long maxSize) {
        int byteCount = image.getByteCount();
        // Log.i("yc压缩图片", "压缩前大小" + byteCount);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        // 把ByteArrayInputStream数据生成图片
        Bitmap bitmap = null;
        // 质量压缩方法，options的值是0-100，这里100表示原来图片的质量，不压缩，把压缩后的数据存放到baos中
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);
        int options = 90;
        // 循环判断如果压缩后图片是否大于maxSize,大于继续压缩
        while (baos.toByteArray().length > maxSize) {
            // 重置baos即清空baos
            baos.reset();
            // 这里压缩options%，把压缩后的数据存放到baos中
            image.compress(Bitmap.CompressFormat.PNG, options, baos);
            // 每次都减少10，当为1的时候停止，options<10的时候，递减1
            if (options == 1) {
                break;
            } else if (options <= 10) {
                options -= 1;
            } else {
                options -= 10;
            }
        }
        byte[] bytes = baos.toByteArray();
        if (bytes.length != 0) {
            // 把压缩后的数据baos存放到bytes中
            bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            int byteCount1 = bitmap.getByteCount();
            // Log.i("yc压缩图片", "压缩后大小" + byteCount1);
        }
        return bitmap;
    }

    /*设置按钮什么时候可以点击*/
    public static void disabledView(final View v, long delayMillis) {
        v.setEnabled(false);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                v.setEnabled(true);
            }
        }, delayMillis);
    }

       public static boolean isAccount(String telphone) {
        try {
            // 用户名
            Pattern p = Pattern.compile("^[a-z,A-Z][a-zA-Z0-9_]{3,11}$"); //
            // 验证用户名
            Matcher m = p.matcher(telphone);
            return m.matches();
        } catch (Exception e) {
            return false;
        }
    }
    public static boolean isPasswordss(String password) {
        try {

            Pattern p = Pattern.compile("^(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[._\\-])[a-zA-Z0-9_\\-.]{8,20}$"); //
            // 验证用户名
            Matcher m = p.matcher(password);
            return m.matches();
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isElectronicMail(String telphone) {
        try {
            // 电子邮箱
            // Pattern p = Pattern
            // .compile("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$");
            // //
            Pattern p = Pattern
                    .compile("^[A-Za-z0-9][\\w\\._]*[a-zA-Z0-9]+@[A-Za-z0-9-_]+\\.([A-Za-z]{2,4})");
            // 验证电子邮箱
            Matcher m = p.matcher(telphone);
            return m.matches();
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isLicenseNumber(String licenseNumber) {
        try {
            // 车牌号
            Pattern p = Pattern
                    .compile("^[\u4e00-\u9fa5][A-Z][-.]?[A-Z0-9]{5}"); //
            Matcher m = p.matcher(licenseNumber);
            return m.matches();
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isChineseCharacters(String str) {
        try {
            Pattern p = Pattern.compile("^[\u4e00-\u9fa5]"); //
            Matcher m = p.matcher(str);
            return m.matches();
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isSize(String str) {
        try {
            String pattern = "^([1-9][0-9]?|0)(\\.([0-9]$))?$";
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(str);
            return m.matches();
        } catch (Exception e) {
            return false;
        }
    }

    /*对成功后的身份证号后八位密文处理*/
    public static String MakeCardID(String s) {
        if (TextUtils.isEmpty(s)){
            return "";
        }else{
            if (s.length()<7){
                return "";
            } else {
                String s1 = s.substring(0, 6);
                String s2 = s.substring(s.length() - 4, s.length());
                return s1 + "********"+s2;
            }
        }


    } /*对成功后的身份证号后八位密文处理*/
    public static String makephone(String s) {

        String s3 = s.substring(4, s.length() - 4);
        String s1 = s.substring(0, 3);
        String s2 = s.substring(s.length()-4,s.length());
        return s1 + "****"+s2;
    }

    public static boolean isName(String name) {
        try {
            Pattern p = Pattern.compile("/[\\u4E00-\\u9FA5]{2,5}(?:·[\\u4E00-\\u9FA5]{2,5})*/"); //
            Matcher m = p.matcher(name);
            return m.matches();
        } catch (Exception e) {
            return false;
        }
    }


    public static boolean isFixationNumber(String fixation) {
        try {
            // 固定电话
            Pattern p = Pattern.compile("^(^0\\d{2}-?\\d{8}$)|(^0\\d{3}-?\\d{7}$)|(^\\(0\\d{2}\\)-?\\d{8}$)|(^\\(0\\d{3}\\)-?\\d{7}$)$"); // 验证固定电话
            Matcher m = p.matcher(fixation);
            return m.matches();
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isPhone(String telphone) {
        try {
            // 手机号码
      Pattern p = Pattern.compile("^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(16([6]))|(17[0|1|3|6|7|8])|(18[0-9])|(19([8-9])))\\d{8}$"); //  验证手机号 首位为1 第二位为3,4,5,7,8，为9位数字
            //Pattern p = Pattern.compile("^[1][0-9]{10}$"); // 验证手机号
            Matcher m = p.matcher(telphone);
            return m.matches();
        } catch (Exception e) {
            return false;
        }

    }

    public static boolean isPassword(String password) {
        try {
            // 密码由字母、数字、下划线组成
            if (password != null) {
                Pattern regexpwd = Pattern
                        .compile("^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{5,18}$");
                Matcher m = regexpwd.matcher(password);
                return m.matches();
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
    public static boolean isPasswords(String password) {
        try {
            // 密码由字母、数字、下划线组成
            if (password != null) {
                Pattern regexpwd = Pattern
                          .compile("^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{5,18}$");
                Matcher m = regexpwd.matcher(password);
                return m.matches();
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    //科学计数法
    public static String TransformData(Double price) {
        String xprice = String.valueOf(price);
        BigDecimal db = new BigDecimal(xprice);
        String ii = db.toPlainString();
        return ii;
    }

    public static String TransformData(String price) {
        BigDecimal db = new BigDecimal(price);
        return db.toPlainString();
    }

    /**
     * 对数据进行".0"判断处理
     *
     * @param
     * @return
     */
    public static String DisposeData(Object o) {
        String str = String.valueOf(o);
        if (str.trim().endsWith(".0")) {
            return str.split("\\.")[0];
        } else {
            return str;
        }
    }

    /**
     * 对数据进行科学计数法和".00"判断处理
     *
     * @param
     * @return
     */
    public static String DisposeDataTwo(Object o) {
        String str = String.valueOf(new DecimalFormat("0.00").format(o));
        if (str.trim().endsWith(".00")) {
            return str.split("\\.")[0];
        } else {
            return str;
        }
    }

    // 判断运输报价是否符合规范0.1-99999999.9
    public static boolean isShippingCharge(String str) {
        try {
            if ("0.0".equals(str)) {
                return false;
            }
            String pattern = "^(?!0$)([1-9][0-9]{0,7}|0)(\\.[0-9]{0,1})?$";
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(str);
            return m.matches();
        } catch (Exception e) {
            return false;
        }
    }

    // 拨打电话
    public static void makeCall(Context context, String number) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        context.startActivity(intent);
    }

    public static String deletehaha(String str) {
        if (str.indexOf("+86") != -1) {
            return str.replace("+86", "");
        } else if (str.indexOf("-") != -1) {
            return str.replace("-", "");
        } else if (str.indexOf(" ") != -1) {
            return str.replace(" ", "");
        }

        return str;

    }

    /**
     * @return 验证有没有输入内容
     */
    public static boolean verifyCorrectPrint(Context mContext, EditText et, int rsd) {
        if (et != null) {
            if (et.getText().toString().isEmpty()) {
                Toast.makeText(mContext,"请输入内容",Toast.LENGTH_SHORT).show();
               // ToastUtils.shorttoast(mContext, "请输入内容");
                return false;
            }
        }
        return true;
    }

    //判断是否是身份证号
    public static boolean isCardID(String str) {
        try {
            if (str.length() == 18) {
                Pattern p = Pattern.compile("^[0-9]{17}[0-9|xX]{1}$");
                Matcher m = p.matcher(str);
                return m.matches();
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }


    /*动态添加内容*/
    public static void setTextView(Context context, int content, LinearLayout layout) {
        TextView textView = new TextView(context);
        textView.setText(content);
        textView.setTextColor(context.getResources().getColor(R.color.white));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        layout.addView(textView);
    }

    /*动态添加图片*/
    public static void setImgView(Context context, int imageRes, LinearLayout layout) {
        ImageView img = new ImageView(context);
        img.setImageResource(imageRes);
        layout.addView(img);
    }


//设置圆角矩形图片暂时用不着.
   /* public static Transformation getTransformation() {
        if (transformation == null) {
            transformation = new RoundedTransformationBuilder()
                    .borderColor(R.color.border_color)
                    .borderWidthDp(0.3f)
                    .cornerRadiusDp(5)
                    .oval(false)
                    .build();
        }
        return transformation;
    }*/

    /**
     * @param //activity    当前的activity实例
     * @param //currentTime 初始时间
     * @param //oldTime     获取的当前时间
     * @return 拼接的时间字符串
     */
//    public static StringBuilder getCurrent(Context activity, String currentTime, String oldTime) {
//        StringBuilder timerBuilder = MyAdwlFactory.getStringBuilder();
//        try {
//            if (null != currentTime && !"".equals(currentTime)) {
//                Long currenttime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(currentTime).getTime();
//                Long publishtime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(oldTime).getTime();
//                Long time = (currenttime - publishtime) / 1000;
//                if (time >= 0 && time <= 60 * 30) {
//                    if (time <= 60) {
//                        timerBuilder.append(activity.getResources().getString(R.string.text_just));
//                    } else {
//                        timerBuilder.append(time / 60).append(activity.getResources().getString(R.string.text_minute));
//                    }
//                } else {
//                    String currentString = new SimpleDateFormat("yyyy-MM-dd").format(new Date(currenttime));
//                    String publishString = new SimpleDateFormat("yyyy-MM-dd").format(new Date(publishtime));
//                    int currentDay = Integer.valueOf(currentString.substring(8, 10));
//                    int publishDay = Integer.valueOf(publishString.substring(8, 10));
//
//                    if (currentString.equals(publishString)) {
//                        if (Integer.valueOf(oldTime.substring(11, 13)) < 12) {
//                            timerBuilder.append("上午").append(oldTime.substring(11, 16));
//                        } else {
//                            timerBuilder.append("下午").append(oldTime.substring(11, 16));
//                        }
//                    } else if (currentDay == publishDay + 1) {
//                        //昨天发布
//                        timerBuilder.append("昨天").append(oldTime.substring(11, 16));
//                    } else if (currentDay == publishDay + 2) {
//                        //2天前天发布
//                        timerBuilder.append("前天").append(oldTime.substring(11, 16));
//                    } else if (currentDay == publishDay + 3) {
//                        // 3天前发布
//                        timerBuilder.append(oldTime.substring(5, 16));
//                    } else {
//                        if (currentString.charAt(3) != publishString.charAt(3)) {
//                            timerBuilder.append(oldTime.substring(0, 16));
//                        } else {
//                            timerBuilder.append(oldTime.substring(5, 16));
//                        }
//                    }
//                }
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return timerBuilder;
//    }
    public static DisplayMetrics getScreenWH(Context context) {
        DisplayMetrics dMetrics = new DisplayMetrics();
        dMetrics = context.getResources().getDisplayMetrics();
        return dMetrics;
    }

    /**
     * 计算焦点及测光区域
     *
     * @param focusWidth
     * @param focusHeight
     * @param areaMultiple
     * @param x
     * @param y
     * @param previewleft
     * @param previewRight
     * @param previewTop
     * @param previewBottom
     * @return Rect(left, top, right, bottom) : left、top、right、bottom是以显示区域中心为原点的坐标
     */
    public static Rect calculateTapArea(int focusWidth, int focusHeight,
                                        float areaMultiple, float x, float y, int previewleft,
                                        int previewRight, int previewTop, int previewBottom) {
        int areaWidth = (int) (focusWidth * areaMultiple);
        int areaHeight = (int) (focusHeight * areaMultiple);
        int centerX = (previewleft + previewRight) / 2;
        int centerY = (previewTop + previewBottom) / 2;
        double unitx = ((double) previewRight - (double) previewleft) / 2000;
        double unity = ((double) previewBottom - (double) previewTop) / 2000;
        int left = clamp((int) (((x - areaWidth / 2) - centerX) / unitx),
                -1000, 1000);
        int top = clamp((int) (((y - areaHeight / 2) - centerY) / unity),
                -1000, 1000);
        int right = clamp((int) (left + areaWidth / unitx), -1000, 1000);
        int bottom = clamp((int) (top + areaHeight / unity), -1000, 1000);

        return new Rect(left, top, right, bottom);
    }

    public static int clamp(int x, int min, int max) {
        if (x > max)
            return max;
        if (x < min)
            return min;
        return x;
    }

    /**
     * 检测摄像头设备是否可用
     * Check if this device has a camera
     *
     * @param context
     * @return
     */
    public static boolean checkCameraHardware(Context context) {
        if (context != null && context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    /**
     * bitmap旋转
     *
     * @param b
     * @param degrees
     * @return
     */
    public static Bitmap rotate(Bitmap b, int degrees) {
        if (degrees != 0 && b != null) {
            Matrix m = new Matrix();
            m.setRotate(degrees, (float) b.getWidth() / 2, (float) b.getHeight() / 2);
            try {
                Bitmap b2 = Bitmap.createBitmap(
                        b, 0, 0, b.getWidth(), b.getHeight(), m, true);
                if (b != b2) {
                    b.recycle();  //Android开发网再次提示Bitmap操作完应该显示的释放
                    b = b2;
                }
            } catch (OutOfMemoryError ex) {
                // Android123建议大家如何出现了内存不足异常，最好return 原始的bitmap对象。.
                return b;
            }
        }
        return b;
    }

    public static final int getHeightInPx(Context context) {
        final int height = context.getResources().getDisplayMetrics().heightPixels;
        return height;
    }

    public static final int getWidthInPx(Context context) {
        final int width = context.getResources().getDisplayMetrics().widthPixels;
        return width;
    }

    public static Bitmap rotaingImageView(int angle, Bitmap bitmap) {
        //旋转图片 动作
        try {
            Matrix matrix = new Matrix();

            matrix.postRotate(angle);
            System.out.println("angle2=" + angle);
            // 创建新的图片
            Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                    bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            return resizedBitmap;
        } catch (OutOfMemoryError error) {
            error.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * 读取图片属性：旋转的角度
     *
     * @param path 图片绝对路径
     * @return degree旋转的角度
     */
    public static int readPictureDegree(String path) {
        int degree = 0;
        try {
            ExifInterface exifInterface = new ExifInterface(path);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return degree;
    }

    /**
     * 存储图像并将信息添加入媒体数据库
     */
    public static Uri insertImage(ContentResolver cr, String name, long dateTaken,
                                  String directory, String filename, Bitmap source, byte[] jpegData) {
        OutputStream outputStream = null;
        String filePath = directory + filename;
        try {
            File dir = new File(directory);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            File file = new File(directory, filename);
            if (file.createNewFile()) {
                outputStream = new FileOutputStream(file);
                if (source != null) {
                    source.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                } else {
                    outputStream.write(jpegData);
                }
            }
        } catch (FileNotFoundException e) {
          //  LogUtil.e("", e.getMessage());
            return null;
        } catch (IOException e) {
           // LogUtil.e("", e.getMessage());
            return null;
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Throwable t) {
                }
            }
        }
        ContentValues values = new ContentValues(7);
        values.put(MediaStore.Images.Media.TITLE, name);
        values.put(MediaStore.Images.Media.DISPLAY_NAME, filename);
        values.put(MediaStore.Images.Media.DATE_TAKEN, dateTaken);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.Images.Media.DATA, filePath);
        return cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }

    /*
     * 货源；列表显示姓名或公司名称处理
     */
    public static String DealString(String str) {
        if (str.length() > 4) {
            return str.substring(0, 4) + "...";
        }
        return str;
    }
    public static String getDateTimeFromMillisecond(Long millisecond){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(millisecond);
        String dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

        private final static int SPACE_TIME = 500;//2次点击的间隔时间，单位ms
        private static long lastClickTimes;

        public synchronized static boolean isDoubleClick() {
            long currentTime = System.currentTimeMillis();
            boolean isClick;
            if (currentTime - lastClickTime > SPACE_TIME) {
                isClick = false;
            } else {
                isClick = true;
            }
            lastClickTime = currentTime;
            return isClick;
        }
    /**
     * @return 防止按钮重复点击
     */
    public static boolean isFastDoubleClicks() {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if (0 < timeD && timeD < 500) {
            return true;
        }
        lastClickTime = time;
        return false;
    }
    /**
     * 通过对比得到与宽高比最接近的预览尺寸（如果有相同尺寸，优先选择）
     *
     * @param isPortrait    是否竖屏
     * @param surfaceWidth  需要被进行对比的原宽
     * @param surfaceHeight 需要被进行对比的原高
     * @param preSizeList   需要对比的预览尺寸列表
     * @return 得到与原宽高比例最接近的尺寸
     */
    public static Camera.Size getCloselyPreSize(boolean isPortrait, int surfaceWidth, int surfaceHeight, List<Camera.Size> preSizeList) {
        int reqTmpWidth;
        int reqTmpHeight;
        // 当屏幕为垂直的时候需要把宽高值进行调换，保证宽大于高
        if (isPortrait) {
            reqTmpWidth = surfaceHeight;
            reqTmpHeight = surfaceWidth;
        } else {
            reqTmpWidth = surfaceWidth;
            reqTmpHeight = surfaceHeight;
        }
        //先查找preview中是否存在与surfaceview相同宽高的尺寸
        for (Camera.Size size : preSizeList) {
            if ((size.width == reqTmpWidth) && (size.height == reqTmpHeight)) {
                return size;
            }
        }
        // 得到与传入的宽高比最接近的size
        float reqRatio = ((float) reqTmpWidth) / reqTmpHeight;
        float curRatio, deltaRatio;
        float deltaRatioMin = Float.MAX_VALUE;
        Camera.Size retSize = null;
        for (Camera.Size size : preSizeList) {
            curRatio = ((float) size.width) / size.height;
            deltaRatio = Math.abs(reqRatio - curRatio);
            if (deltaRatio < deltaRatioMin) {
                deltaRatioMin = deltaRatio;
                retSize = size;
            }
        }
        return retSize;
    }


    /**
     * 检查是否拥有指定的所有权限
     */
    public static boolean checkPermissionAllGranted(Context context, String[] permissions) {
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                // 只要有一个权限没有被授予, 则直接返回 false
                return false;
            }
        }
        return true;
    }
    /**
     * 不同文字数目2端对齐工具类 (支持2-6个数字)
     *
     * @author yuhao
     * @time 2016年6月28日 */


        private static int n = 0;// 原Str拥有的字符个数
        private static SpannableString spannableString;
        private static double multiple = 0;// 放大倍数

        /**
         * 对显示的字符串进行格式化 比如输入：出生年月 输出结果：出正生正年正月
         */
        public static String formatStr(String str) {
            if (TextUtils.isEmpty(str)) {
                return "";
            }
            n = str.length();
            if (n >= 6) {
                return str;
            }
            StringBuilder sb = new StringBuilder(str);
            for (int i = n - 1; i > 0; i--) {
                sb.insert(i, "正");
            }
            return sb.toString();
        }

        /**
         * 对显示字符串进行格式化 比如输入：安正卓正机正器正人 输出结果：安 卓 机 器 人
         *
         * @param str
         * @return     */
        public static SpannableString formatText(String str){
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            str = formatStr(str);
            if (str.length()<=6){
                return null;
            }
            spannableString = new SpannableString(str);
            switch (n) {
                case 2:
                    multiple = 4;
                    break;
                case 3:
                    multiple = 1.5;
                    break;
                case 4:
                    multiple =   0.66666666666666666666666666666666667;
                    break;
                case 5:
                    multiple = 0.25;
                    break;
                default:
                    break;
            }
            for (int i = 1; i < str.length(); i = i + 2)
            {
                spannableString.setSpan(new RelativeSizeSpan((float) multiple), i, i + 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                spannableString.setSpan(new ForegroundColorSpan(Color.TRANSPARENT), i, i + 1,  Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            }
            return spannableString;
        }
    /**

     * WGS84转GCJ02(火星坐标系)

     *

     * @param lng WGS84坐标系的经度

     * @param lat WGS84坐标系的纬度

     * @return 火星坐标数组

     */

    public static double[] wgs84togcj02(double lng, double lat) {

        if (out_of_china(lng, lat)) {
            return new double[] { lng, lat };
        }
        double dlat = transformlat(lng - 105.0, lat - 35.0);
        double dlng = transformlng(lng - 105.0, lat - 35.0);
        double radlat = lat / 180.0 * pi;
        double magic = Math.sin(radlat);
        magic = 1 - ee * magic * magic;
        double sqrtmagic = Math.sqrt(magic);
        dlat = (dlat * 180.0) / ((a * (1 - ee)) / (magic * sqrtmagic) * pi);
        dlng = (dlng * 180.0) / (a / sqrtmagic * Math.cos(radlat) * pi);
        double mglat = lat + dlat;
        double mglng = lng + dlng;
        return new double[] { mglng, mglat };
    }


    public static boolean out_of_china(double lng, double lat) {

        if (lng < 72.004 || lng > 137.8347) {

            return true;

        } else if (lat < 0.8293 || lat > 55.8271) {

            return true;

        }

        return false;

    }


    /**

     * 纬度转换

     *

     * @param lng

     * @param lat

     * @return

     */

    public static double transformlat(double lng, double lat) {

        double ret = -100.0 + 2.0 * lng + 3.0 * lat + 0.2 * lat * lat + 0.1 * lng * lat + 0.2 * Math.sqrt(Math.abs(lng));

        ret += (20.0 * Math.sin(6.0 * lng * pi) + 20.0 * Math.sin(2.0 * lng * pi)) * 2.0 / 3.0;

        ret += (20.0 * Math.sin(lat * pi) + 40.0 * Math.sin(lat / 3.0 * pi)) * 2.0 / 3.0;

        ret += (160.0 * Math.sin(lat / 12.0 * pi) + 320 * Math.sin(lat * pi / 30.0)) * 2.0 / 3.0;

        return ret;

    }



    /**

     * 经度转换

     *

     * @param lng

     * @param lat

     * @return

     */

    public static double transformlng(double lng, double lat) {

        double ret = 300.0 + lng + 2.0 * lat + 0.1 * lng * lng + 0.1 * lng * lat + 0.1 * Math.sqrt(Math.abs(lng));

        ret += (20.0 * Math.sin(6.0 * lng * pi) + 20.0 * Math.sin(2.0 * lng * pi)) * 2.0 / 3.0;

        ret += (20.0 * Math.sin(lng * pi) + 40.0 * Math.sin(lng / 3.0 * pi)) * 2.0 / 3.0;

        ret += (150.0 * Math.sin(lng / 12.0 * pi) + 300.0 * Math.sin(lng / 30.0 * pi)) * 2.0 / 3.0;

        return ret;

    }





}