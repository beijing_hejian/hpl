/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Activity


import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.widget.*
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.PeopleModle
import com.peaceclient.hospitaldoctor.com.modle.TwModle
import kotlinx.android.synthetic.main.sphis_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers


/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/9/1 11:22
 * @change
 * @chang time
 * @class describe 视频接诊记录
 */

class SpHisAct : HoleBaseActivity() {
    private var rootView: View? = null
    private var kong: View? = null
    private var adapter: CheckHisAdapter? = null
    private var messageList: ArrayList<TwModle> = arrayListOf()
    private var weeks: String = "1"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sphis_act)
        kong = View.inflate(Myapplication.mcontext, R.layout.empty_view, null)
        var manager: LinearLayoutManager = LinearLayoutManager(this)
        tw_recycle.layoutManager = manager
        adapter = CheckHisAdapter(R.layout.sphis_his_item, messageList)
        adapter?.bindToRecyclerView(tw_recycle)
        adapter?.setEmptyView(kong)
        adapter?.isUseEmpty(true)
        tw_recycle.adapter = adapter
        adapter?.notifyDataSetChanged()
        prosmarts?.autoRefresh()
        prosmarts?.isEnableLoadmore = false
        prosmarts?.setOnRefreshListener {
            messageList?.clear()
            getList()
        }
        search_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (TextUtils.isEmpty(s)) {
                    messageList?.clear()
                    getList()
                    //GetCheckHis(prosmarts)
                } else {
                    messageList?.clear()
                    getList()
                    // GetCheckHis(prosmarts)
                }
            }
        })

        adapter?.setOnItemClickListener { adapter, view, position -> }
        adapter?.setOnItemChildClickListener { adapter, view, position ->
            if (view == adapter.getViewByPosition(tw_recycle, position, R.id.chakan)) {

                //代表已结束的查看
                if (messageList.get(position).orderStatus == "5") {
                    var inte: Intent = Intent(this@SpHisAct, VideoReceiDoneAct::class.java)
                    inte.putExtra("id", messageList.get(position).id)
                    startActivity(inte)
                } else {
                    // 代表未响应的查看
                    var inte: Intent = Intent(this@SpHisAct, VideoReceiAct::class.java)
                    inte.putExtra("id", messageList.get(position).id)
                    startActivity(inte)
                }

            }


        }
        spinner.prompt = "请选择时间"
        var spinnerAdapter: ArrayAdapter<CharSequence> = ArrayAdapter.createFromResource(this@SpHisAct, R.array.week, R.layout.simple_spinner_item)
        spinnerAdapter.setDropDownViewResource(R.layout.drop_item_layout)
        spinner.adapter = spinnerAdapter
        spinnerAdapter.notifyDataSetChanged()
        var selec: MyItemSelect = MyItemSelect()
        spinner.onItemSelectedListener = selec
    }

    internal inner class MyItemSelect : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            messageList.clear()
            when (position) {
                0 -> {
                    weeks = "1"
                    getList()
                }
                1 -> {
                    weeks = "2"
                    getList()
                }
                2 -> {
                    weeks = "3"
                    getList()
                }
                3 -> {
                    weeks = "4"
                    getList()
                }
                4 -> {
                    weeks = "12"
                    getList()
                }
                5 -> {
                    weeks = "24"
                    getList()
                }

            }
        }
    }

//    private fun initDate() {
//        for (z in 0..7) {
//            var visitModle: VisitModle.DataBean = VisitModle.DataBean()
//            visitModle.attendanceName = "z" + z
//            if (z % 2 == 0) {
//                visitModle.timeZ = "下午"
//                visitModle.isDone = false
//            } else {
//                visitModle.timeZ = "上午"
//                visitModle.isDone = true
//            }
//
//            visitModle.time = TimeFormatUtils.ms2DateOnlyDay(System.currentTimeMillis())
//            messageList.add(visitModle)
//            adapter?.notifyDataSetChanged()
//            prosmarts?.finishRefresh()
//            prosmarts?.finishLoadmore()
//        }
//    }

    internal inner class CheckHisAdapter(layoutResId: Int, data: List<TwModle>?) : BaseQuickAdapter<TwModle, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: TwModle) {
            helper.setText(R.id.name, item?.resName ?: "")
                    .setText(R.id.visit_time, item?.createTime ?: "")
                    .addOnClickListener(R.id.chakan)
            var box: ImageView = helper.getView<ImageView>(R.id.pm_am)
            var paidui: TextView = helper.getView<TextView>(R.id.number)
            var status: TextView = helper.getView<TextView>(R.id.status)
            var layoutdone: LinearLayout = helper.getView(R.id.lay2)
            if (item?.makeAppointmentPm == 0) {
                if (item?.orderStatus == "5") {
                    box.setImageResource(R.drawable.morninggray)
                    paidui.isSelected = false
                    status.isSelected = false
                    status.setText("已结束")
                } else {
                    box.setImageResource(R.drawable.morninggray)
                    paidui.isSelected = false
                    status.isSelected = false
                    status.setText("未响应")
                }
            } else {
                if (item?.orderStatus == "5") {
                    box.setImageResource(R.drawable.afternoongray)
                    paidui.isSelected = false
                    status.isSelected = false
                    status.setText("已结束")
                } else {
                    box.setImageResource(R.drawable.afternoongray)
                    paidui.isSelected = false
                    status.isSelected = false
                    status.setText("未响应")
                }
            }

        }


    }

    inner class PeopleAdapter(layoutResId: Int, data: ArrayList<PeopleModle>?) : com.chad.library.adapter.base.BaseQuickAdapter<PeopleModle, BaseViewHolder>(layoutResId, data) {

        override fun convert(helper: BaseViewHolder, item: PeopleModle) {
            var button: Button = helper.getView<Button>(R.id.judge)


        }
    }


    fun getList() {
        IpUrl.getInstance()!!.getSpHis(ConstantViewMolde.getToken(), search_name.text.toString(), weeks)
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<TwModle>>> {
                    override fun onError(e: Throwable?) {
                        prosmarts?.finishRefresh()
                    }

                    override fun onNext(t: HoleResponse<ArrayList<TwModle>>?) {
                        if (t?.code == 0) {
                            if (t?.data != null) {
                                messageList.addAll(t?.data!!)
                                adapter?.notifyDataSetChanged()
                            }
                        }
                    }

                    override fun onCompleted() {
                        prosmarts?.finishRefresh()
                    }
                })
    }
}