package com.peaceclient.hospitaldoctor.com.Activity;

import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.bigkoo.pickerview.view.TimePickerView;
import com.jakewharton.rxbinding4.view.RxView;
import com.peaceclient.hospitaldoctor.com.Base.BaseUrlUtil;
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity;
import com.peaceclient.hospitaldoctor.com.R;
import com.peaceclient.hospitaldoctor.com.Utils.TimeFormatUtils;
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde;
import com.peaceclient.hospitaldoctor.com.modle.DangAnModle;
import com.peaceclient.hospitaldoctor.com.modle.GhModle;
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse;
import com.vondear.rxtools.view.RxToast;
import com.vondear.rxtools.view.dialog.RxDialog;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name HospitalDoctor222
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/10/13 17:36
 * @change
 * @chang time
 * @class describe 添加随访页面
 */

/***
 *
 */
public class AddSuiActivity extends HoleBaseActivity {
      @BindView(R.id.arrow_back)
      ImageView arrowBack;
      @BindView(R.id.title)
      TextView title;
      @BindView(R.id.pro_dengji)
      Button proDengji;
      @BindView(R.id.rel)
      RelativeLayout rel;
      @BindView(R.id.view)
      View view;
      @BindView(R.id.name)
      TextView name;


      @BindView(R.id.view2)
      View view2;
      @BindView(R.id.name2)
      TextView name2;
      @BindView(R.id.view3)
      View view3;
      @BindView(R.id.name3)
      TextView name3;
      @BindView(R.id.view4)
      View view4;
      @BindView(R.id.name4)
      ImageView name4;
      @BindView(R.id.sn)
      TextView sn;
      @BindView(R.id.time)
      TextView time;
      @BindView(R.id.timeBu)
      Button timeBu;
      @BindView(R.id.text1)
      TextView text1;
      @BindView(R.id.view5)
      View view5;
      @BindView(R.id.text2)
      TextView text2;
      @BindView(R.id.name5)
      TextView name5;
      @BindView(R.id.text3)
      TextView text3;
      @BindView(R.id.text4)
      TextView text4;
      private GhModle modle;

      @Override
      protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.add_sui);
            ButterKnife.bind(this);
            modle = (GhModle) getIntent().getSerializableExtra("modle");
            cxdn();
            arrowBack.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        finish();
                  }
            });
            time.setText(TimeFormatUtils.ms2DateOnlyDay(System.currentTimeMillis()));
            time.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        showDateDialog(time);
                  }
            });
            RxView.clicks(timeBu).throttleFirst(1500, TimeUnit.MICROSECONDS)
                      .subscribe(unit -> {
                            if (TextUtils.isEmpty(name5.getText())) {
                                  RxToast.normal("请选择随访日期");
                            } else {
                                  addsui();
                            }
                      });


      }

      private void showDateDialog(final TextView time) {
            Calendar startDate = Calendar.getInstance();
            final Calendar endDate = Calendar.getInstance();
            //正确设置方式 原因：注意事项有说明
            Calendar end = Calendar.getInstance();
            end.set(2099, 11, 31);
            TimePickerView timePickerView = new TimePickerBuilder(
                      AddSuiActivity.this, new OnTimeSelectListener() {
                  @Override
                  public void onTimeSelect(Date date, View v) {
                        long time1 = date.getTime();
                        time.setText(TimeFormatUtils.ms2DateOnlyDay(time1));
//                       long nowsecend = System.currentTimeMillis();

                  }
            }).setSubmitText("确定").setCancelText("取消").setDividerColor(Color.GRAY).isCyclic(false).setOutSideCancelable(true)
                      .isCenterLabel(true)
                      .setType(new boolean[]{true, true, true, false, false, false})// 默认全部显示
                      .setRangDate(startDate, end)
                      .isCenterLabel(true)
                      .setTitleText("请选择日期")
                      .setTitleColor(getResources().getColor(R.color.txtcode_color))
                      .setDecorView((ViewGroup) getWindow().getDecorView().findViewById(android.R.id.content))
                      // .setRange(Integer.parseInt(split[0]), startDate.get(Calendar.YEAR) + 1)
                      .build();

            timePickerView.setDialogOutSideCancelable();
            timePickerView.show();
      }

      /***
       * 添加随访请求
       */
      public void addsui() {
            RxDialog dia = new RxDialog(this);
            dia.setContentView(R.layout.dialog_loading);
            dia.show();
            BaseUrlUtil baseUrlUtil = new BaseUrlUtil();
            baseUrlUtil.getInstance().addSui(ConstantViewMolde.Companion.getToken(), modle.getGhid(), time.getText().toString(), ConstantViewMolde.Companion.GetUser().getUser().getId(), modle.getZhid()).observeOn(
                      AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                      .subscribe(new Observer<HoleResponse<DangAnModle>>() {
                            @Override
                            public void onCompleted() {
                                  dia.dismiss();
                            }

                            @Override
                            public void onError(Throwable e) {
                                  dia.dismiss();
                                  RxToast.normal("添加失败");
                            }

                            @Override
                            public void onNext(HoleResponse<DangAnModle> code) {
                                  if (code.getCode() == 0) {

                                        RxToast.normal(code.getMsg());
                                        finish();
                                  }

                            }
                      });
      }

      /***
       * 查询档案
       */
      public void cxdn() {
            BaseUrlUtil baseUrlUtil = new BaseUrlUtil();
            baseUrlUtil.getInstance().getDanan(ConstantViewMolde.Companion.getToken(), modle.getUid()).observeOn(
                      AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                      .subscribe(new Observer<HoleResponse<DangAnModle>>() {
                            @Override
                            public void onCompleted() {
                            }

                            @Override
                            public void onError(Throwable e) {
                            }

                            @Override
                            public void onNext(HoleResponse<DangAnModle> code) {
                                  if (code.getCode() == 0) {
                                        name.setText(code.getData().getHzxm() == null ? "" : code.getData().getHzxm());
// 出生日期
                                        name2.setText(code.getData().getCsrq() == null ? "" : code.getData().getCsrq());
//联系电话
                                        name3.setText(code.getData().getLxdh() == null ? "" : code.getData().getLxdh());
                                        //shijian
                                        name5.setText(code.getData().getHzxb() == null ? "" : code.getData().getHzxb());
                                  }

                            }
                      });
      }
}
