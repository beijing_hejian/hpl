/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Fragment

import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.google.android.material.tabs.TabLayout

import com.peaceclient.hospitaldoctor.com.R


/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Fragment
 * @class describe
 * @anthor admin
 * @time 2021/7/13 17:45
 * @change
 * @chang time
 * @class describe
 */

class InteFragment : BaseLazyFragment() {
    private var rootview: View? = null
    override fun lazyLoad() {
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootview = inflater.inflate(R.layout.inter_fragment, null)
        var viewpager = rootview!!.findViewById(R.id.viewpagers) as ViewPager
        var tablayout = rootview!!.findViewById(R.id.tab) as TabLayout
        var onlineFragment: OnlineFragment = OnlineFragment()
        var consultationFragment: ConsultationFragment = ConsultationFragment();
        var list = mutableListOf<Fragment>( consultationFragment,onlineFragment)
        var listTitle = listOf<String>("图文问诊","视频问诊")
        var interAdapter = InterAdapter(childFragmentManager)
        interAdapter.setTitle(listTitle)
        viewpager.adapter = interAdapter
        interAdapter.notifyDataSetChanged()
        tablayout.setupWithViewPager(viewpager)
        for (int in 0..1) {
            var tabAt = tablayout.getTabAt(int)
            when (int) {
                0 -> {
                    tabAt?.setCustomView(R.layout.consulta_item)
                }
                1 -> {
                     tabAt?.setCustomView(R.layout.online_item)
                }
            }
    }
        tablayout.addOnTabSelectedListener(object :TabLayout.OnTabSelectedListener{
            override fun onTabReselected(p0: TabLayout.Tab?) {
            }
            override fun onTabUnselected(p0: TabLayout.Tab?) {
                // 只能取到是哪一个tab ，无法区分是哪一个
               if ((p0?.customView?.findViewById(R.id.name) as TextView).text .equals("视频问诊")){
                   p0?.customView?.setBackgroundResource(R.color.color_transparent)
                   (p0?.customView?.findViewById(R.id.name) as TextView).setTextColor(resources.getColor(R.color.sss))
                   (p0?.customView?.findViewById(R.id.image) as ImageView).setImageResource(R.drawable.uninter_consalt)


               }else{
                   p0?.customView?.setBackgroundResource(R.color.color_transparent)
                   (p0?.customView?.findViewById(R.id.name) as TextView).setTextColor(resources.getColor(R.color.sss))
                   (p0?.customView?.findViewById(R.id.image) as ImageView).setImageResource(R.drawable.uninter_zixun)

               }
            }

            @RequiresApi(Build.VERSION_CODES.P)
            override fun onTabSelected(p0: TabLayout.Tab?) {
                if ((p0?.customView?.findViewById(R.id.name) as TextView).text .equals("视频问诊")){
                    p0?.customView?.setBackgroundResource(R.color.white)
                    (p0?.customView?.findViewById(R.id.name) as TextView).setTextColor(resources.getColor(R.color.home_green))
                    (p0?.customView?.findViewById(R.id.image) as ImageView).setImageResource(R.drawable.inter_consalt)
                   // (p0?.customView?.findViewById(R.id.layout) as RelativeLayout).outlineSpotShadowColor  = resources.getColor(R.color.home_green)
                }else{
                    p0?.customView?.setBackgroundResource(R.color.white)
                    (p0?.customView?.findViewById(R.id.name) as TextView).setTextColor(resources.getColor(R.color.home_green))
                    (p0?.customView?.findViewById(R.id.image) as ImageView).setImageResource(R.drawable.inter_zixun)
                   // (p0?.customView?.findViewById(R.id.layout) as RelativeLayout).outlineSpotShadowColor  = resources.getColor(R.color.home_green)

                }
            }

        } )
    return rootview
}

//    fun BindTab() {
//        for (int in 0..listi)
//    }


class InterAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    private val myFragment1: OnlineFragment
    private val myFragment2: ConsultationFragment

    init {
        myFragment1 = OnlineFragment()
        myFragment2 = ConsultationFragment()
    }

    var listitle: List<String>? = null
    override fun getItem(p0: Int): Fragment? {
        var fragment: Fragment? = null
        when (p0) {
            0 -> fragment = myFragment2

            1 -> fragment =    myFragment1
        }
        return fragment
    }

    override fun instantiateItem(container: View, position: Int): Any {
        return super.instantiateItem(container, position)
    }

    override fun getCount(): Int {
        return 2
    }

    fun setTitle(list: List<String>) {
        this.listitle = list
    }

    override fun getItemPosition(`object`: Any): Int {
        return `object` as Int
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return listitle!!.get(position)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        super.destroyItem(container, position, `object`)
        println("position Destory$position")
    }
}
}