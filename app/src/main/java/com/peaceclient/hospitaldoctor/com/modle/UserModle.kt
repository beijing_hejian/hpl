/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.modle

import java.io.Serializable

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.modle
 * @class describe
 * @anthor admin
 * @time 2021/8/21 15:11
 * @change
 * @chang time
 * @class describe
 */

class UserModle : Serializable {
    companion object {
        private const val serialVersionUID = 20180617104400L
    }

    var id: String? = null
    var headImgUrl: String? = null
    var account: String? = null
    var identity: String? = null
    var imSecret: String? = null
    var isBindWechat: Boolean = false
    var isBindAlipay: Boolean = false
    var imAccount: String? = null
    var user: User? = null

    class User : Serializable {
        companion object {
            private const val serialVersionUID = 20180617104402L
        }

        var personType: Int = 1
        var goodAt: String = ""
        var openGraphic: Boolean = false
        var costGraphic: String = ""
        var title: String = ""
        var depName: String = ""
        var depCode: String = ""
        var costRemote: String = ""
        var docName: String = ""
        var headImgUrl: String = ""
        var visitorsGraphic: String = ""
        var alias: String = ""
        var depId: String = ""
        var id: String = ""
        var introduction: String = ""
        override fun toString(): String {
            return "User(goodAct='$goodAt', openGraphic=$openGraphic, costGraphic='$costGraphic', title='$title', depName='$depName', depCode='$depCode', costRemote='$costRemote', docName='$docName', headImgUrl='$headImgUrl', visitorsGraphic='$visitorsGraphic', alias='$alias', depId='$depId', id='$id', introduction='$introduction')"
        }

    }

    override fun toString(): String {
        return "UserModle(id=$id, headImgUrl=$headImgUrl, account=$account, identity=$identity, imSecret=$imSecret, isBindWechat=$isBindWechat, isBindAlipay=$isBindAlipay, imAccount=$imAccount, user=$user)"
    }


}