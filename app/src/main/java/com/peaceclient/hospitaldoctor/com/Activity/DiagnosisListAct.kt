package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.widget.TextView
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.PeisongBean
import kotlinx.android.synthetic.main.medicinelist_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor222
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/11/6 15:24
 * @change
 * @chang time
 * @class describe 处方审核列表页面
 */

class DiagnosisListAct : HoleBaseActivity() {
    private var kong: View? = null
    private var adapter: PeopleAdapter? = null
    private var list: ArrayList<PeisongBean> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.diagn_list_act)
        kong = View.inflate(this@DiagnosisListAct, R.layout.empty_view, null)
        var manager: LinearLayoutManager = LinearLayoutManager(this)
        medic_recycle.layoutManager = manager
        adapter = PeopleAdapter(R.layout.diagnosis_item, list)
        medic_recycle.adapter = adapter
        adapter?.bindToRecyclerView(medic_recycle)
        adapter?.setEmptyView(kong)
        adapter?.isUseEmpty(true)
        medismart.setOnRefreshListener {
            list?.clear()
            adapter?.notifyDataSetChanged()
            getRecipList()
        }
        arrow_back.setOnClickListener {
            finish()
        }
        adapter?.setOnItemChildClickListener { adapter, view, position ->
            when (view) {
                adapter.getViewByPosition(medic_recycle, position, R.id.transport) -> {
                    var intent :Intent = Intent(this@DiagnosisListAct, DiagonsisShenAct::class.java)
                    intent.putExtra("id",list.get(position).id)
                    startActivity(intent)
                }
            }
        }
    }



    inner class PeopleAdapter(layoutResId: Int, data: ArrayList<PeisongBean>?) : com.chad.library.adapter.base.BaseQuickAdapter<PeisongBean, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: PeisongBean) {
            helper.getView<TextView>(R.id.name).setText(item.uname?: "")
            helper.getView<TextView>(R.id.time).setText(item.billtime ?: "")
            helper.addOnClickListener(R.id.transport).setText(R.id.keshi, item.regname ?: "")
        }


    }

    /**
     * 处方详情列表
     */
    fun getRecipList() {
        IpUrl.getInstance()!!.Repclist(ConstantViewMolde.getToken())
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<java.util.ArrayList<PeisongBean>>> {
                    override fun onError(e: Throwable?) {
                        medismart.finishRefresh()
                    }

                    override fun onNext(t: HoleResponse<java.util.ArrayList<PeisongBean>>?) {
                        if (t != null) {
                            list?.addAll(t?.data!!)
                            adapter?.notifyDataSetChanged()
                        }
                    }

                    override fun onCompleted() {
                        medismart.finishRefresh()
                    }
                })
    }

    override fun onResume() {
        super.onResume()

        medismart?.autoRefresh()
    }
}