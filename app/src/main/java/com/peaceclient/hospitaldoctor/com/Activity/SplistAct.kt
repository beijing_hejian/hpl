package com.peaceclient.hospitaldoctor.com.Activity

import android.os.Bundle

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Fragment.OtherDayFragment
import com.peaceclient.hospitaldoctor.com.Fragment.TodayFragment
import com.peaceclient.hospitaldoctor.com.R
import kotlinx.android.synthetic.main.twlist_act.*
import java.util.*

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/9/15 16:03
 * @change
 * @chang time
 * @class describe 视频接诊列表（废弃）
 */

class SplistAct  :HoleBaseActivity(){

    private var listTitles: ArrayList<String> = arrayListOf("今日接诊","预约列表")
    private var fragmentList: ArrayList<Fragment>? = arrayListOf()
    private var adapter:MyPagerAdapter?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splist_act)
        initda()
        arrow_back.setOnClickListener {
            finish()
        }
    }
    private fun initda() {
        for (i in 0..listTitles.size) {
            when (i) {
                0 -> fragmentList?.add(TodayFragment())
                1 -> fragmentList?.add(OtherDayFragment())
            }
        }
        adapter = MyPagerAdapter(getSupportFragmentManager())
        adapter?.setFragments(fragmentList!!)
        health_viewpager.setAdapter(adapter)
        health_tab.setupWithViewPager(health_viewpager)
        adapter?.notifyDataSetChanged()
    }

    inner class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        private var mFragmentList: ArrayList<Fragment>? = null

        fun setFragments(fragments: ArrayList<Fragment>) {
            mFragmentList = fragments
        }

        override fun getItem(position: Int): Fragment {
            return mFragmentList!![position]
        }

        override fun getCount(): Int {
            return mFragmentList?.size ?:0
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return listTitles.get(position).toString()
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {}


    }

}