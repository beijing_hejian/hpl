package com.peaceclient.hospitaldoctor.com.Activity

import android.content.Intent
import android.os.Bundle

import androidx.recyclerview.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.InterFace.OnFinishListener
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.TimeFormatUtils
import com.peaceclient.hospitaldoctor.com.View.RushTime
import com.peaceclient.hospitaldoctor.com.modle.ClientModle
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.ui.VideoCallActivity
import com.vondear.rxtools.view.RxToast
import com.vondear.rxtools.view.dialog.RxDialog
import kotlinx.android.synthetic.main.videodone_act.status
import kotlinx.android.synthetic.main.videorece_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import java.text.SimpleDateFormat

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/9/18 10:31
 * @change
 * @chang time
 * @class describe 远程接诊页面（废弃）
 */
class VideoReceiAct : HoleBaseActivity(), OnFinishListener {
    private var toChatUsername: String? = null
    private var startTime: String? = null
    private var endTime: String? = null
    private var adapters: DoctorAdapters? = null
    private var imagelist: ArrayList<ClientModle.urlMo> = arrayListOf()
    private var id: String? = ""
    override fun onFinish(pos: Int) {
        status.setText("未响应")
        notify.visibility = View.GONE
        waitlayout.visibility = View.GONE
        kaishi_layout.visibility = View.GONE
    }
    private var statu: String? = "0"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.videorece_act)
        id = intent.getStringExtra("id");
        var linerlay: GridLayoutManager = GridLayoutManager(this@VideoReceiAct, 3, LinearLayoutManager.VERTICAL, true)
        recycle.layoutManager = linerlay
        adapters = DoctorAdapters(R.layout.images_item, imagelist)
        recycle.adapter = adapters
        adapters?.notifyDataSetChanged()
        getList()
        startTime = TimeFormatUtils.ms2Date(System.currentTimeMillis())
        notify.setOnClickListener {
            notify.visibility = View.GONE
            change(id!!)
            if (!TextUtils.isEmpty(startTime) && !TextUtils.isEmpty(endTime)) {
                dillTime(startTime!!, endTime!!, shengyu_time, 0)
            }
        }
        if (shengyu_time.isStop) {
            status.setText("未响应")
            notify.visibility = View.GONE
            waitlayout.visibility = View.GONE
            kaishi_layout.visibility = View.GONE
        } else {
            shengyu_time.setListener(this)
        }
        start_video.setOnClickListener {
            if (!TextUtils.isEmpty(toChatUsername)) {
                startActivity(Intent(this@VideoReceiAct, VideoCallActivity::class.java).putExtra("username",toChatUsername?:"" )
                        .putExtra("isComingCall", false))
            }
        }
        end_video.setOnClickListener {
            getAsk(id!!)
        }
        adapters?.setOnItemClickListener { adapter, view, position ->
            var inte: Intent = Intent(this@VideoReceiAct, MyImageActivity::class.java)
            inte.putExtra("url", imagelist.get(position).url)
            startActivity(inte)
        }
    }
    fun dillTime(startTime: String, endTime: String, timerView: RushTime, int: Int) {
        var days: Int = 0
        val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val d1 = df.parse(startTime)
        val d2 = df.parse(endTime)
        val diff = d2.getTime() - d1.getTime() // mslong days = diff / (1000 * 60 * 60 * 24);
        var hours = (diff - days * (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
        val minutes = (diff - days * (1000 * 60 * 60 * 24) - hours * (1000 * 60 * 60)) / (1000 * 60)
        val second = diff / 1000 - days * 24 * 60 * 60 - hours * 60 * 60 - minutes * 60
        hours += days * 24
        timerView.setTime(hours.toString() + "", minutes.toString() + "", second.toString() + "")
        timerView.start(int)
    }
    internal inner class DoctorAdapters(layoutResId: Int, data: ArrayList<ClientModle.urlMo>) : com.chad.library.adapter.base.BaseQuickAdapter<ClientModle.urlMo, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder?, item: ClientModle.urlMo?) {
            var ima: ImageView = helper?.getView(R.id.tupian) as ImageView
            Glide.with(this@VideoReceiAct).load(item?.url
                    ?: "").placeholder(R.drawable.tblr_redius_homegreen).into(ima)
        }
    }
    fun getList() {
        IpUrl.getInstance()!!.spDetial(ConstantViewMolde.getToken(), id ?: "")
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ClientModle>> {
                    override fun onError(e: Throwable?) {}
                    override fun onNext(t: HoleResponse<ClientModle>?) {
                        if (t?.code == 0) {
                            if (t?.data != null) {
                                var pm: String = ""
                                if (t?.data?.makeAppointmentPm == "0") {
                                    pm = "上午"
                                } else {
                                    pm = "下午"
                                }
                                textname.text = (t?.data?.makeAppointmentDate ?: "") + "\r\r" + pm
                                name.text = t?.data?.resName ?: ""
                                paihao.setText(t?.data?.makeAppointmentNo ?: "")
                                toChatUsername = t?.data?.resImAccount ?: ""
                                endTime = t?.data?.endTime
                                when (t?.data?.orderStatus) {
                                    "1" -> {
                                        status.setText("待接诊")
                                        notify.visibility = View.VISIBLE
                                        waitlayout.visibility = View.GONE
                                        kaishi_layout.visibility = View.GONE
                                    }
                                    "2" -> {
                                        status.setText("接诊中")
                                        notify.visibility = View.VISIBLE
                                        waitlayout.visibility = View.GONE
                                        kaishi_layout.visibility = View.GONE
                                    }
                                    "3" -> {
                                        status.setText("接诊待响应")
                                        notify.visibility = View.GONE
                                        waitlayout.visibility = View.VISIBLE
                                        kaishi_layout.visibility = View.GONE
                                        if (!TextUtils.isEmpty(startTime) && !TextUtils.isEmpty(endTime)) {
                                            dillTime(startTime!!, endTime!!, shengyu_time, 0)
                                        }
                                        else {
                                        }
                                    }
                                    "4" -> {
                                        status.setText("已响应")
                                        notify.visibility = View.GONE
                                        waitlayout.visibility = View.GONE
                                        kaishi_layout.visibility = View.VISIBLE
                                    }
                                    "5" -> {
                                        status.setText("已结束")
                                    }
                                    "8" -> {
                                        status.setText("已停诊")
                                        notify.visibility = View.GONE
                                        waitlayout.visibility = View.GONE
                                        kaishi_layout.visibility = View.GONE
                                    }
                                    "7" -> {
                                        status.setText("未响应")
                                        notify.visibility = View.GONE
                                        waitlayout.visibility = View.GONE
                                        kaishi_layout.visibility = View.GONE
                                    }
                                }
                                if (t?.data?.urls != null) {
                                    imagelist.addAll(t?.data?.urls!!)
                                    adapters?.notifyDataSetChanged()
                                }
                            }
                        }
                    }
                    override fun onCompleted() {}
                })
    }
    fun change(id: String) {
        var rxd: RxDialog = RxDialog(this@VideoReceiAct)
        rxd.setContentView(R.layout.dialog_loading)
        rxd.show()
        IpUrl.getInstance()!!.changeStatus(ConstantViewMolde.getToken(), "3", id)
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ClientModle>> {
                    override fun onError(e: Throwable?) {
                        rxd?.dismiss()
                    }
                    override fun onNext(t: HoleResponse<ClientModle>?) {
                        if (t?.code == 0) {
                            if (t?.data != null) {
                                waitlayout.visibility = View.VISIBLE
                                startTime = t?.data?.createTime
                                endTime = t?.data?.endTime
                                if (!TextUtils.isEmpty(startTime) && !TextUtils.isEmpty(endTime)) {
                                    dillTime(startTime!!, endTime!!, shengyu_time, 0)
                                } else {
                                }
                            } else {
                                RxToast.normal(t?.msg ?: "")
                            }
                        } else {
                            RxToast.normal(t?.msg ?: "")
                        }
                    }
                    override fun onCompleted() {
                        rxd?.dismiss()
                    }
                })
    }
    fun getAsk(id: String) {
        var rxdia: RxDialog = RxDialog(this@VideoReceiAct)
        rxdia.setContentView(R.layout.dialog_loading)
        rxdia.show()
        IpUrl.getInstance()!!.Spfinish(ConstantViewMolde.getToken(), id)
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ClientModle>> {
                    override fun onError(e: Throwable?) {
                        rxdia?.dismiss()
                    }
                    override fun onNext(t: HoleResponse<ClientModle>?) {
                        if (t?.code == 0) {
                            rxdia?.dismiss()
                            finish()
                        }
                    }
                    override fun onCompleted() {
                        rxdia?.dismiss()
                    }
                })
    }
}