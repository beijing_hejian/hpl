package com.peaceclient.hospitaldoctor.com.View;

import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class MyViewPager extends ViewPager {
      //是否能滑动,默认不能滑动
      private boolean isCanScroll = false;

      private float startX;
      private float moveX;

      public MyViewPager(Context context) {
            super(context);
      }

      public MyViewPager(Context context, AttributeSet attrs) {
            super(context, attrs);
      }

      /**
       * 事件的分发
       */
      @Override
      public boolean dispatchTouchEvent(MotionEvent ev) {
            if(getCurrentItem() == 1){
                  isCanScroll = false;
                  //告诉ViewPager不要拦截子View中的各种事件
                  this.requestDisallowInterceptTouchEvent(true);
            }
            return super.dispatchTouchEvent(ev);
      }

      /**
       * 事件是否拦截
       */
      @Override
      public boolean onInterceptTouchEvent(MotionEvent event) {
            boolean intercepted = false;
            int x = (int) event.getX();
            int y = (int) event.getY();
            switch (event.getAction()) {
                  case MotionEvent.ACTION_DOWN: {
                        intercepted = false;
                        break;
                  }
                  case MotionEvent.ACTION_MOVE: {
                        intercepted=false;
                        break;
                  }
                  case MotionEvent.ACTION_UP: {
                        intercepted = false;
                        break;
                  }
                  default:
                        break;
            }

            return intercepted;
      }
      /**
       * 事件的处理
       */
      @Override
      public boolean onTouchEvent(MotionEvent ev) {
            if (isCanScroll) {    //如果能滑动
                  //return super.onTouchEvent(ev);//如果只是让其能滑动,则直接返回super就行
                  //模拟一个场景,只能向左滑动
                  switch (ev.getAction()) {
                        case MotionEvent.ACTION_DOWN:   //按下
                              startX = ev.getX();
                              break;
                        case MotionEvent.ACTION_MOVE:   //移动
                              moveX = ev.getX() - startX; //得出移动的距离
                              startX=ev.getX();   //手指移动时，再把当前的坐标作为下一次的‘上次坐标’，解决上述问
                              if (moveX>0) {  //小于0说明向左滑,大于0说明向右滑
                                    return true;   //返回false,说明当向右滑动时,啥都不做,如果是向左滑,交给下面的super处理
                              }
                  }
                  return super.onTouchEvent(ev);
            } else { //如果设置不能滑动
                  //则直接返回flase或true,即啥都不做
                  return true;
            }

      }
      /**
       * 对外提供一个方法,来决定是否能滑动
       */
      public void setIsCanScroll(boolean isCanScroll) {
            this.isCanScroll = isCanScroll;
      }

      @Override
      public void setCurrentItem(int item) {
            super.setCurrentItem(item,false);
      }
}
