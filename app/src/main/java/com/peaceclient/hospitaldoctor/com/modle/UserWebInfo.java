package com.peaceclient.hospitaldoctor.com.modle;

import com.google.gson.Gson;

/**
 * Created by 20906 on 2017/6/27.
 */
public class UserWebInfo {
    /**
     * openID : 0
     * nickName : string
     * avatarUrl : string
     */

    public String openID;
    public String nickName;
    public String avatarUrl;
    public String remarkName;

    public String getRemarkName() {
        return remarkName;
    }

    public void setRemarkName(String remarkName) {
        this.remarkName = remarkName;
    }

    public String getOpenID() {
        return openID;
    }

    public void setOpenID(String openID) {
        this.openID = openID;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }



    public static UserWebInfo objectFromData(String str) {

        return new Gson().fromJson(str, UserWebInfo.class);
    }

    @Override
    public String toString() {
        return "UserWebInfo{" +
                  "openID='" + openID + '\'' +
                  ", nickName='" + nickName + '\'' +
                  ", avatarUrl='" + avatarUrl + '\'' +
                  ", remarkName='" + remarkName + '\'' +
                  '}';
    }
}
