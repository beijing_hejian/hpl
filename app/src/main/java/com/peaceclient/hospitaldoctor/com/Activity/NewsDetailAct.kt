/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Activity

import android.os.Bundle
import com.jakewharton.rxbinding4.view.clicks


import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.KnowleModle
import kotlinx.android.synthetic.main.news_detail_act.*
import org.jsoup.Jsoup
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/7/20 15:26
 * @change
 * @chang time
 * @class describe 健康资讯、医院介绍等详情页面
 */

class NewsDetailAct : HoleBaseActivity() {
    private var id: String? = null
    private var p :String?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.news_detail_act)
        id = intent.getStringExtra("id")
        getDetail();
        arrow_back.clicks().throttleFirst(2000, TimeUnit.MICROSECONDS).subscribeOn(io.reactivex.rxjava3.android.schedulers.AndroidSchedulers.mainThread())
                .subscribe {
                    finish()
                }
    }
    private fun getNewContent(htmltext: String): String {
        val doc = Jsoup.parse(htmltext)
        val elements = doc.getElementsByTag("img")
        for (element in elements) {
            element.attr("width", "100%").attr("height", "auto")
        }
        return doc.toString()
    }

    /***
     * 根据id 获取详情信息
     */
    fun getDetail() {
        IpUrl.ins.getInstance()?.getDetail(id!!)?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe(object : Observer<HoleResponse<KnowleModle>> {
            override fun onError(e: Throwable?) {
            }
            override fun onNext(t: HoleResponse<KnowleModle>?) {
                if (t?.data != null) {
                    detail_title.setText(t?.data?.title ?: "")
                    detail_time.setText(t?.data?.publishTime ?: "")
                    web.loadDataWithBaseURL(null, getNewContent(t?.data?.content!!), "text/html", "utf-8", null)
                    when (t?.data?.category) {
                        1->{
                            titles.setText("健康资讯")
                        }
                        3 -> {
                            titles.setText("健康资讯")
                        }
                        2->{
                            titles.setText("健康资讯")
                        }
                        4->{
                            titles.setText("名医风采")
                        }
                        6->{
                            titles.setText("就诊指南")
                        }
                        7->{
                            titles.setText("科室详情")
                        }
                    }
                }
            }
            override fun onCompleted() {
            }
        })
    }

}