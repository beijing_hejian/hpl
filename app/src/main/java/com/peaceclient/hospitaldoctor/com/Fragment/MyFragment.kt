/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Fragment


import android.Manifest
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import androidx.core.content.FileProvider
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import cn.jpush.android.api.JPushInterface
import com.bumptech.glide.Glide
import com.jakewharton.rxbinding4.view.clicks
import com.peaceclient.hospitaldoctor.com.Activity.*
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.BuildConfig
import com.peaceclient.hospitaldoctor.com.Hy.DemoHelper
import com.peaceclient.hospitaldoctor.com.InterFace.ILaunchManagerService
import com.peaceclient.hospitaldoctor.com.InterFace.LaunchInvocationHandler
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.Uri2PathUtil
import com.peaceclient.hospitaldoctor.com.View.AutoImageView
import com.peaceclient.hospitaldoctor.com.modle.*
import com.permissionx.guolindev.PermissionX
import com.permissionx.guolindev.callback.ExplainReasonCallback
import com.permissionx.guolindev.callback.ForwardToSettingsCallback
import com.permissionx.guolindev.callback.RequestCallback
import com.permissionx.guolindev.request.ExplainScope
import com.permissionx.guolindev.request.ForwardScope
import com.vondear.rxtools.RxPhotoTool
import com.vondear.rxtools.view.RxToast
import com.vondear.rxtools.view.dialog.RxDialog
import com.vondear.rxtools.view.dialog.RxDialogChooseImage
import com.vondear.rxtools.view.dialog.RxDialogLoading
import io.reactivex.rxjava3.functions.Consumer
import kotlinx.android.synthetic.main.my_fragment.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.io.File
import java.lang.reflect.Proxy

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Fragment
 * @class describe
 * @anthor admin
 * @time 2021/7/13 17:45
 * @change
 * @chang time
 * @class describe
 */

class MyFragment : BaseLazyFragment(), ILaunchManagerService {
    var arraye = arrayOf(R.drawable.home_video, R.drawable.home_medicine, R.drawable.home_zixun)
    var listOne: MutableList<LevelBean>? = mutableListOf()
    // var arrays = arrayOf("智能导诊", "就诊指南", "医院介绍", "科室介绍", "电子就诊卡", "候诊叫号", "报告查询", "费用查询")
    var arrayet = arrayOf("远程接诊记录", "药品配送记录", "预约随访记录")
    private val CROP_SMALL_PICTURE = 4400
    private var tempFile: File? = null
    private val CAMERA_REQUEST_CODE = 4200
    private var mFile: String? = null
    private var rootview: View? = null
    private var header: ImageView? = null
    private var keshi: TextView? = null
    private var dialogChooseImage: RxDialogChooseImage? = null
    private var launchManagerService: ILaunchManagerService? = null
    internal var permissions = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
    override fun lazyLoad() {}
    var adapter: HomeFragemnts.GideAdapter? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootview = inflater.inflate(R.layout.my_fragment, null)
        var layouts: LinearLayout = rootview?.findViewById(R.id.tw_layout) as LinearLayout
        var mzLayout: LinearLayout = rootview?.findViewById(R.id.mz_layout) as LinearLayout
        var pwlayout: LinearLayout = rootview?.findViewById(R.id.pw_layout) as LinearLayout
        var chageLayout: LinearLayout = rootview?.findViewById(R.id.change_layout) as LinearLayout
        var agreelayout: LinearLayout = rootview?.findViewById(R.id.agree_layout) as LinearLayout
        var privacyLayout: LinearLayout = rootview?.findViewById(R.id.privacy_layout) as LinearLayout
        var medicLayout: LinearLayout = rootview?.findViewById(R.id.medic_layout) as LinearLayout
        var twhis: LinearLayout = rootview?.findViewById(R.id.health_layout) as LinearLayout
        var videohis: LinearLayout = rootview?.findViewById(R.id.video_layout) as LinearLayout
        var help: LinearLayout = rootview?.findViewById(R.id.help_layout) as LinearLayout
        var jzlayout: LinearLayout = rootview?.findViewById(R.id.jz_layout) as LinearLayout
        var account: TextView = rootview?.findViewById(R.id.account) as TextView
        header = rootview?.findViewById(R.id.my_header) as ImageView
        keshi = rootview?.findViewById(R.id.keshi) as TextView
        launchManagerService = Proxy.newProxyInstance(javaClass.classLoader, arrayOf<Class<*>>(ILaunchManagerService::class.java), LaunchInvocationHandler(this, Myapplication.mcontext)) as ILaunchManagerService
        help.setOnClickListener {
            startActivity(Intent(Myapplication.mcontext, AboutUsAct::class.java))
        }
        videohis.setOnClickListener {
            startActivity(Intent(Myapplication.mcontext, SpHisAct::class.java))
        }
        twhis.setOnClickListener {
            startActivity(Intent(Myapplication.mcontext, TwHisAct::class.java))
        }
        medicLayout.setOnClickListener {
            startActivity(Intent(Myapplication.mcontext, MedicineHisAct::class.java))
        }
        mzLayout.setOnClickListener {
            /*startActivity(Intent(Myapplication.mcontext, IntroAct::class.java))*/
        }


        jzlayout.setOnClickListener {
            startActivity(Intent(Myapplication.mcontext, PersonGoodAct::class.java))
//            var inten: Intent = Intent(Myapplication.mcontext, PeopleCardAct::class.java)
//            var bundle: Bundle = Bundle();
//            inten.putExtra(Myapplication.SHARED_NAME, bundle)
//            launchManagerService?.startActivity(inten)
        }

        layouts.setOnClickListener(View.OnClickListener {
          //   startActivity(Intent(Myapplication.mcontext, TwSettingAct::class.java))
//            var inten: Intent = Intent(Myapplication.mcontext, TwHisAct::class.java)
//            var bundle: Bundle = Bundle();
//            inten.putExtra(Myapplication.SHARED_NAME, bundle)
//            launchManagerService?.startActivity(inten)
        })
        pwlayout.setOnClickListener {
            //            var inten: Intent = Intent(Myapplication.mcontext, ChangePassAct::class.java)
//            var bundle: Bundle = Bundle();
//            inten.putExtra(Myapplication.SHARED_NAME, bundle)
//            launchManagerService?.startActivity(inten)
            startActivity(Intent(Myapplication.mcontext, ChangePassAct::class.java))
        }
        chageLayout.setOnClickListener {
            if (ConstantViewMolde.IsLogin()) {
                showDialog()
            } else {
//                var inten: Intent = Intent(Myapplication.mcontext, LoginAct::class.java)
//                var bundle: Bundle = Bundle();
//                inten.putExtra(Myapplication.SHARED_NAME, bundle)
//                launchManagerService?.startActivity(inten)
            }

        }
        agreelayout.setOnClickListener {
            startActivity(Intent(Myapplication.mcontext, AgreeMentAct::class.java))
        }
        privacyLayout.setOnClickListener {
            startActivity(Intent(Myapplication.mcontext, PrivacyAct::class.java))
        }
        val grid = rootview?.findViewById(R.id.my_grid) as GridView
        grid.setSelector(R.color.color_transparent)
        adapter = HomeFragemnts.GideAdapter(listOne!!)
        grid.adapter = adapter
        adapter?.notifyDataSetChanged()
        if (ConstantViewMolde.IsLogin()) {
            var modle: UserModle? = ConstantViewMolde.GetUser()
            account.setText(modle?.user?.docName ?: "")
            modle?.headImgUrl ?: ""
            keshi?.setText(modle?.user?.depName)
            Glide.with(Myapplication.mcontext).load(modle?.headImgUrl
                    ?: "").placeholder(R.drawable.user_header).into(header)
        } else {
            account.setText("医生姓名")
            keshi?.setText("科室")
            Glide.with(Myapplication.mcontext).load(R.drawable.guide3)
        }
        header?.setOnClickListener {
            RequestMission();
        }
        grid.setOnItemClickListener { parent, view, position, id ->

            when (position) {
                0 -> {
                    //remote

                    if (ConstantViewMolde.GetUser()!!.user?.personType == 2) {
                        RxToast.normal("您不是医生无法查看远程接诊记录")
                    } else {
                        startActivity(Intent(activity, RemoteHisAct::class.java))
                    }
                }
                1 -> {
                    startActivity(Intent(activity, MedicineHisAct::class.java))
                }
                2 -> {
                    startActivity(Intent(activity, SuifangHisAct::class.java))
                }
            }

        }
        initdate()
        return rootview
    }

    fun initdate() {
        listOne?.clear()
        for (intz in 0..1) {
            var bean: LevelBean = LevelBean()
            bean.iconid = arraye[intz]
            bean.desc = arrayet[intz]
            listOne?.add(bean)
        }
        adapter?.notifyDataSetChanged()
    }

    private fun showDialog() {
        var rxDialog: RxDialog = RxDialog(activity)
        println(activity)
        rxDialog.setContentView(R.layout.exit_layout)
        rxDialog.show()
        rxDialog.findViewById<TextView>(R.id.exit).setOnClickListener {
            ConstantViewMolde.clearUser();
            rxDialog.dismiss()
            DemoHelper.getInstance().logout(false, null)
            account?.setText("")
            JPushInterface.deleteAlias(Myapplication.mcontext, 0)
            Glide.with(Myapplication.mcontext).load(R.drawable.user_header).placeholder(R.drawable.user_header).into(header)
            var intent: Intent = Intent(Myapplication.mcontext, LoginDact::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
        rxDialog.findViewById<TextView>(R.id.noexit).setOnClickListener {
            rxDialog.dismiss()
        }
    }

    /***
     *  运行时请求权限
     */
    fun RequestMission() {
        PermissionX.init(this).permissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .onExplainRequestReason(
                        object : ExplainReasonCallback {
                            override fun onExplainReason(scope: ExplainScope, deniedList: List<String>) {
                                scope.showRequestReasonDialog(deniedList, "即将申请的权限是程序必须依赖的权限", "我已明白")
                            }
                        }
                ).onForwardToSettings(
                        object : ForwardToSettingsCallback {
                            override fun onForwardToSettings(scope: ForwardScope, deniedList: List<String>) {}
                        }).request(
                        object : RequestCallback {
                            override fun onResult(allGranted: Boolean, grantedList: List<String>, deniedList: List<String>) {
                                if (allGranted) {
                                    if (ConstantViewMolde.IsLogin()) {
                                        dialogChooseImage = RxDialogChooseImage(this@MyFragment, RxDialogChooseImage.LayoutType.NO_TITLE)
                                        dialogChooseImage!!.setContentView(R.layout.select_camer_layout)
                                        dialogChooseImage!!.show()
                                    } else {
                                        var inten: Intent = Intent(Myapplication.mcontext, LoginAct::class.java)
                                        var bundle: Bundle = Bundle();
                                        inten.putExtra(Myapplication.SHARED_NAME, bundle)
                                        launchManagerService?.startActivity(inten)
                                    }
                                } else {
                                    RxToast.normal("您拒绝了如下权限：$deniedList")
                                }
                            }
                        })
        dialogChooseImage?.findViewById<View>(R.id.tv_file)?.clicks()?.subscribe(Consumer<Unit> {
            RxPhotoTool.openLocalImage(this@MyFragment)
            dialogChooseImage?.dismiss()
        })
        dialogChooseImage?.findViewById<View>(R.id.tv_camera)?.clicks()?.subscribe(Consumer<Unit> {
            getPicFromCamera()
            dialogChooseImage?.dismiss()
        })
        dialogChooseImage?.findViewById<View>(R.id.tv_cancel)?.clicks()?.subscribe(Consumer<Unit> { dialogChooseImage?.dismiss() })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            5002 -> {
                /*选择文件回调*/
                val uri: Uri?
                if (data != null) {
                    uri = data.data
                    if (Uri2PathUtil.getRealPathFromUri(Myapplication.mcontext, uri) != null) {
                        //从uri得到绝对路径，并获取到file文件
                        val file = File(Uri2PathUtil.getRealPathFromUri(Myapplication.mcontext, uri))
                        var fileResopnse = File(RxPhotoTool.getImageAbsolutePath(Myapplication.mcontext, uri))

                        Glide.with(this@MyFragment.mActivity).load(file).into(header)
                        Upload(file)


                    } else {


                    }
                } else {
                    uri = null
                }

            }
            4400 ->
                /*剪裁完成返回*/
                if (data != null) {
                    // 让刚才选择裁剪得到的图片显示在界面上
                    val photos = BitmapFactory.decodeFile(mFile)
                    if (Uri2PathUtil.getRealPathFromUri(Myapplication.mcontext, data.data) != null) {
                        //从uri得到绝对路径，并获取到file文件
                        val file = File(Uri2PathUtil.getRealPathFromUri(Myapplication.mcontext, data.data))
                        Log.e("myace", "chengong")
                        Upload(file)

                        //uploadFileViewModel.uploadHead(file)
                    } else {
                        //LogUtils.e("获取文件失败")
                        Log.e("myace", "shibai")
                    }

                } else {
                    Log.e("data", "data为空")
                }
            4200 ->
                /*用相机返回的照片去调用剪裁也需要对Uri进行处理*/
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    val contentUri = FileProvider.getUriForFile(Myapplication.mcontext, BuildConfig.APPLICATION_ID + ".fileprovider", tempFile!!)
                    startPhotoZoom(contentUri)//开始对图片进行裁剪处理
                } else {
                    startPhotoZoom(Uri.fromFile(tempFile))//开始对图片进行裁剪处理
                }
        }

    }

    /**
     * 裁剪图片方法实现
     *
     * @param uri
     */
    protected fun startPhotoZoom(uri: Uri?) {
        if (uri == null) {
            Log.i("tag", "The uri is not exist.")
        }
        val intent = Intent("com.android.camera.action.CROP")
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        intent.setDataAndType(uri, "image/*")
        intent.putExtra("crop", "true")
        intent.putExtra("aspectX", 1)
        intent.putExtra("aspectY", 1)
        intent.putExtra("outputX", 1000)
        intent.putExtra("outputY", 1000)
        intent.putExtra("return-data", false)
        val out = File(getPath())
        if (!out.parentFile.exists()) {
            out.parentFile.mkdirs()
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(out))
        intent.putExtra("file", out)
        startActivityForResult(intent, CROP_SMALL_PICTURE)
        Upload(out)
    }

    /*裁剪后的地址*/
    fun getPath(): String {

        mFile = Environment.getExternalStorageDirectory().toString() + "/" + System.currentTimeMillis() + "/" + "outtemp.png"

        return mFile!!
    }

    /**
     * 从相机获取图片
     */
    private fun getPicFromCamera() {
        //用于保存调用相机拍照后所生成的文件
        tempFile = File(Environment.getExternalStorageDirectory().path, System.currentTimeMillis().toString() + ".png")
        //跳转到调用系统相机
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        //判断版本
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {   //如果在Android7.0以上,使用FileProvider获取Uri
            intent.flags = Intent.FLAG_GRANT_WRITE_URI_PERMISSION
            val contentUri = FileProvider.getUriForFile(Myapplication.mcontext, BuildConfig.APPLICATION_ID + ".fileprovider", tempFile!!)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, contentUri)
            Log.e("getPicFromCamera", contentUri.toString())
        } else {    //否则使用Uri.fromFile(file)方法获取Uri
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile))
        }
        startActivityForResult(intent, CAMERA_REQUEST_CODE)
    }

    private fun Upload(file: File) {
        if (file.totalSpace > 9.5 * 1024) {
        } else {
        }
        var loading: RxDialogLoading = RxDialogLoading(activity, R.layout.dialog_loading)
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val body = MultipartBody.Part.createFormData("file", file.name, requestFile)
        IpUrl.getInstance()!!.Upload(ConstantViewMolde.getToken(), body!!)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<FileResopnse>> {
                    override fun onError(e: Throwable?) {
                        loading!!.dismiss()
                    }

                    override fun onNext(t: HoleResponse<FileResopnse>?) {
                        postImage(t?.data?.url ?: "", loading)
                    }

                    override fun onCompleted() {
                    }
                })
    }

    private fun postImage(url: String, dia: RxDialogLoading) {
        IpUrl.getInstance()!!.PostImage(ConstantViewMolde.getToken(), url)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<String>> {
                    override fun onError(e: Throwable?) {
                        dia?.dismiss()
                    }

                    override fun onNext(t: HoleResponse<String>?) {
                        Glide.with(this@MyFragment.mActivity).load(url)
                        //  修改头像
                        val getUser = ConstantViewMolde.GetUser();
                        getUser?.headImgUrl = url
                        ConstantViewMolde.SetUser(getUser)
                        Glide.with(this@MyFragment.mActivity).load(url).into(header)
                    }

                    override fun onCompleted() {
                        dia?.dismiss()
                    }
                })
    }

    class GideAdapter : BaseAdapter {
        var list: MutableList<LevelBean>

        //构造
        constructor(arr: MutableList<LevelBean>) {
            this.list = arr
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var view = View.inflate(Myapplication.mcontext, R.layout.gides_item, null)
            var imageview: AutoImageView = view.findViewById(R.id.header)
            var textview: TextView = view.findViewById(R.id.text)
            imageview.setImageResource(list.get(position).iconid)
            textview.setText(list.get(position).desc)
            return view!!
        }

        override fun getItem(position: Int): Any {
            return list!!.get(position)
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return list!!.size
        }
    }


}