/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Fragment


import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Activity.MessageDetialAct
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.MessageModle
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Fragment
 * @class describe
 * @anthor admin
 * @time 2021/7/13 17:45
 * @change
 * @chang time
 * @class describe 消息页面
 */

class MessageFragment : BaseLazyFragment() {
    private var rootview: View? = null
    private var messmart: SmartRefreshLayout? = null
    var kong: View? = null
    private var lists: ArrayList<MessageModle.MessageDetail> = arrayListOf()
    private var messRecycle: RecyclerView? = null
    private var doctoradapter: MessAdapter? = null
    override fun lazyLoad() {

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootview = inflater.inflate(R.layout.message_fragment, null)
        messRecycle = rootview?.findViewById(R.id.MessRecycle)
        messmart = rootview?.findViewById(R.id.mess_smart)
        kong = View.inflate(Myapplication.mcontext, R.layout.empty_view, null)
        var linerlay: LinearLayoutManager = LinearLayoutManager(Myapplication.mcontext)
        messRecycle?.layoutManager = linerlay
        doctoradapter = MessAdapter(R.layout.hospital_item, lists)
        doctoradapter?.bindToRecyclerView(messRecycle)
        doctoradapter?.setEmptyView(kong)
        doctoradapter?.isUseEmpty(true)
        messRecycle?.adapter = doctoradapter
        doctoradapter?.notifyDataSetChanged()
        messmart!!.setOnRefreshListener {
            lists.clear()
            doctoradapter?.notifyDataSetChanged()
            getZIXUN()
        }
        messmart!!.autoRefresh()
        doctoradapter?.setOnItemClickListener { adapter, view, position ->
            if (lists.get(position).messageType.equals("2")) {
                //RxToast.normal("点击处方消息")
                var intent = Intent(Myapplication.mcontext, MessageDetialAct::class.java)
                intent.putExtra("id", lists.get(position).id)
                startActivity(intent)
            } else {
                //RxToast.normal("点击通知消息")
                var intent = Intent(Myapplication.mcontext, MessageDetialAct::class.java)
                intent.putExtra("id", lists.get(position).id)
                startActivity(intent)
            }

        }


//        layout.setOnClickListener(View.OnClickListener {
//            var intent = Intent(Myapplication.mcontext, MessageDetialAct::class.java)
//            intent.putExtra("id",lists.get())
//            startActivity(Intent()
//        })

        return rootview
    }

    fun getZIXUN() {

        IpUrl.getInstance()!!.getMessage("2", "3", ConstantViewMolde.GetUser()?.id ?: "", "")
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<MessageModle>> {
                    override fun onError(e: Throwable?) {
                        messmart?.finishRefresh()
                    }

                    override fun onNext(t: HoleResponse<MessageModle>?) {
                        if (t != null) {
                            if (t.data != null) {
                                lists.addAll(t?.data?.content!!)
                                doctoradapter?.notifyDataSetChanged()
                            }

                        }
                    }

                    override fun onCompleted() {
                        messmart?.finishRefresh()
                    }
                })
    }

    internal inner class MessAdapter(layoutResId: Int, data: ArrayList<MessageModle.MessageDetail>?) : com.chad.library.adapter.base.BaseQuickAdapter<MessageModle.MessageDetail, BaseViewHolder>(layoutResId, data) {

        override fun convert(helper: BaseViewHolder, item: MessageModle.MessageDetail) {
            helper.setText(R.id.type, item?.title ?: "")
                    .setText(R.id.time, item?.createTime ?: "")
                    .setText(R.id.introduction, item?.noticeContent ?: "")
            var textView = helper.getView<LinearLayout>(R.id.detail_lay)

            if (item.messageType.equals("2")) {
                textView.visibility = View.GONE
            } else {
                textView.visibility = View.VISIBLE
            }


        }
    }

/*
    internal inner class NewsAdapter(data: ArrayList<KnowleModle>?) : com.chad.library.adapter.base.BaseQuickAdapter<KnowleModle, BaseViewHolder>(data) {
        init {
            multiTypeDelegate = object : MultiTypeDelegate<KnowleModle>() {
                override fun getItemType(javaBean: KnowleModle): Int {
                    return javaBean.category!!
                }
            }
            // 3 健康资讯 4 ，名义风采， 6，就诊指南，5 医院介绍
            multiTypeDelegate.registerItemType(3, R.layout.newslist_item)
                    .registerItemType(4, R.layout.newslist_item)
                    .registerItemType(6, R.layout.item_jiuzhen_layout)
                    .registerItemType(5, R.layout.newslist_item)


        }

        override fun convert(helper: BaseViewHolder, item: KnowleModle) {
            when (helper.itemViewType) {
                6 -> {
                    helper.setText(R.id.text1, item?.title)
                    var image = helper.getView(R.id.message_chufang) as AutoImageView
                    Glide.with(this@MessageFragment).load(item?.imgUrl).into(image)
                    //  image.setImageDrawable(resources.getDrawable(item.url))
                }
                4 -> {
                    helper.setText(R.id.title, item?.title)
                            .setText(R.id.news_time, item?.publishTime)
                    var image = helper.getView(R.id.news_header) as TypeImageView
                    image.type = TypeImageView.TYPE_ROUND
                    image.roundRadius = 10
                    Glide.with(this@MessageFragment).load(item?.imgUrl).into(image)
                }
                5 -> {
                    helper.setText(R.id.title, item?.title)
                            .setText(R.id.news_time, item?.publishTime)
                    var image = helper.getView(R.id.news_header) as TypeImageView
                    image.type = TypeImageView.TYPE_ROUND
                    image.roundRadius = 10
                    Glide.with(this@MessageFragment).load(item?.imgUrl).into(image)
                }
                3 -> {
                    helper.setText(R.id.title, item?.title)
                            .setText(R.id.news_time, item?.publishTime)
                    var image = helper.getView(R.id.news_header) as TypeImageView
                    image.type = TypeImageView.TYPE_ROUND
                    image.roundRadius = 10
                    Glide.with(this@MessageFragment).load(item?.imgUrl).into(image)
                }
            }
//            helper.setText(R.id.title, item.title)
//                    .setText(R.id.news_time, item!!.time)
//            var image = helper.getView(R.id.news_header) as TypeImageView
//            image.type = TypeImageView.TYPE_ROUND
//            image.roundRadius = 10
//            image.setImageDrawable(resources.getDrawable(item.url))


            // .setText(R.id.introduction,item!!.introduction)
        }


    }
*/

}