package com.peaceclient.hospitaldoctor.com.Activity

import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.Gravity
import android.view.View
import android.widget.*
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jakewharton.rxbinding4.view.clicks
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.InterFace.MyGridDecorations
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.Utils.KeyboardUtils
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.RecipeModlex
import com.peaceclient.hospitaldoctor.com.modle.SymptomModle
import com.vondear.rxtools.view.RxToast
import com.vondear.rxtools.view.dialog.RxDialog
import com.vondear.rxtools.view.dialog.RxDialogSure
import io.reactivex.rxjava3.functions.Consumer
import kotlinx.android.synthetic.main.diagnosis_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.math.BigDecimal
import java.text.DecimalFormat
import java.util.concurrent.TimeUnit


/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/9/16 20:17
 * @change
 * @chang time
 * @class describe 开方页面
 */

class DiagnosisAct : HoleBaseActivity() {
    val SELECT_CODE = 1001 // 药品信息状态码
    val SELECT_ZZ = 1002 // 症状得状态码
    val SELECT_OTHER = 1003 // 其他症状得状态码
    var id: String? = null
    var list: ArrayList<RecipeModlex.BillDetail> = arrayListOf() // 药品信息
    var billDiagsList: ArrayList<RecipeModlex.Others> = arrayListOf() // 其他症状列表
    var yypcNamelist: ArrayList<String> = arrayListOf()
    var yypclist: ArrayList<RecipeModlex.Others> = arrayListOf()
    var ypyfNamelist: ArrayList<String> = arrayListOf()
    var ypyflist: ArrayList<RecipeModlex.Others> = arrayListOf()
    var adapter: DoctorAdapter? = null
    var Diaadapter: BillDiagsAdapter? = null
    var type: String = "1"
    var code: String = ""
    var name: String = ""
    val max: Int = 9
    var isRemote = true
    var appoinid: String = ""
    var jlspinner: Spinner? = null
    var otherList: ArrayList<SymptomModle> = arrayListOf();
    var recipeModlex: RecipeModlex = RecipeModlex()
    var dia: RecipeModlex.Others = RecipeModlex.Others();
    var others: ArrayList<RecipeModlex.Others> = arrayListOf();
    var bills: RecipeModlex.Bills = RecipeModlex.Bills();
    var billDetails: ArrayList<RecipeModlex.BillDetail> = arrayListOf();
    // 暂定为中医诊断和西医诊断
    var types: ArrayList<String> = arrayListOf("中医诊断", "西医诊断")
    var format = DecimalFormat("#0.00")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.diagnosis_act)
        var linerma: LinearLayoutManager = LinearLayoutManager(this)
        // 药品列表
        remote.isSelected  = isRemote
        recycle.layoutManager = linerma
        adapter = DoctorAdapter(R.layout.diagnosis_delete_item, list)
        recycle.adapter = adapter
        id = intent.getStringExtra("id")
        appoinid = intent.getStringExtra("appointid")
        adapter?.notifyDataSetChanged()
        Diaadapter = BillDiagsAdapter(R.layout.dia_item, billDiagsList)
        var item: MyGridDecorations = MyGridDecorations()
        // 其他症状列表
        recy.adapter = Diaadapter
        var linermas = GridLayoutManager(this@DiagnosisAct, 3)
        linermas.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(p0: Int): Int {
                return setSpanSize(p0, billDiagsList)
            }
        }

        recy.layoutManager = linermas
        Diaadapter?.notifyDataSetChanged()
        /***
         *  实现获取用药方法和用量单位
         */
        synchronized(this@DiagnosisAct) {
            getYPYF()
            getYYPC()
            //getRecipList()
        }
        hole2.setOnClickListener {
            remote.isSelected = !isRemote
            isRemote = !isRemote
        }

        Diaadapter?.setOnItemChildClickListener { adapter, view, position ->
            when (view) {
                adapter.getViewByPosition(recy, position, R.id.delete) -> {
                    billDiagsList.removeAt(position)
                    adapter.notifyItemRemoved(position)
                }
            }
        }


//        save.clicks().subscribe {
//            postKaiFang()
//        }
        save.clicks().throttleFirst(1500, TimeUnit.MICROSECONDS).subscribe(Consumer {
            KeyboardUtils.hideKeyboard(content)
            var dialog: RxDialogSure = RxDialogSure(this@DiagnosisAct)
            dialog.show()
            dialog.setTitle("提示")
            dialog.contentView.textAlignment = TextView.TEXT_ALIGNMENT_TEXT_START
            dialog.contentView.setText("\t1.不能开含有'麻醉、精神类药品和儿童处方药'等药品"+"\n\t2.按照互联网诊疗监管规定，处方尽量不要超过5个，否则会扣分。")
            dialog.setSureListener {
                dialog.dismiss()
                if (list.size == 0) {
                    RxToast.normal("请选择药品")
                } else {
                    if (vertiresult())
                        postKaiFang();
                }
            }
            dialog.setCanceledOnTouchOutside(true)


        })


        //将spinnertext添加到OnTouchListener对内容选项触屏事件处理
        adapter?.setOnItemChildClickListener { adapter, view, position ->
            when (view) {
                adapter.getViewByPosition(recycle, position, R.id.delete) -> {
                    list.removeAt(position)
                    adapter.notifyItemRemoved(position)
                }
                adapter.getViewByPosition(recycle, position, R.id.jl) -> {
                    initJlSpinner(view as Spinner, position)
                }
                adapter.getViewByPosition(recycle, position, R.id.yf) -> {
                    initJlSpinnery(view as Spinner, position)
                }
            }

        }
        layout.clicks().throttleFirst(1500, TimeUnit.MICROSECONDS).subscribe(Consumer {
            KeyboardUtils.hideKeyboard(content)
            var intent = Intent(this@DiagnosisAct, SelectSympAct::class.java)
            intent.putExtra("type", type)
            startActivityForResult(intent, SELECT_ZZ)
        })
        qita.clicks().throttleFirst(1500, TimeUnit.MICROSECONDS).subscribe(Consumer {
            KeyboardUtils.hideKeyboard(content)

            var intent = Intent(this@DiagnosisAct, SelectSympAct::class.java)
            intent.putExtra("type", type)
            startActivityForResult(intent, SELECT_OTHER)

        })
        add_diag.setOnClickListener {

            startActivityForResult(Intent(this@DiagnosisAct, SelectMedicineAct::class.java), SELECT_CODE)
        }
        arrow_back.setOnClickListener {
            finish()
        }
        add_diag.clicks().throttleFirst(1500, TimeUnit.MICROSECONDS)
                .subscribe {
                    var intent = Intent(this@DiagnosisAct, SelectMedicineAct::class.java)
                    intent.putExtra("type", type)
                    startActivityForResult(intent, SELECT_CODE)
                }

        contents.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                zishus.setText((s?.length.toString() ?: 0.toString()) + "/30")

            }
        })
// 首先类型显示，药品数量显示
        spinner.prompt = "请选择诊断类型"
        spinner.dropDownVerticalOffset = resources.getDimensionPixelOffset(R.dimen.dp_27)
        spinner.dropDownHorizontalOffset = resources.getDimensionPixelOffset(R.dimen.dp_15)
        var spinnerAdapter: ArrayAdapter<String> = ArrayAdapter(this@DiagnosisAct, R.layout.spinner_select, types)
        spinnerAdapter.setDropDownViewResource(R.layout.item_drop_layout)
        spinner.adapter = spinnerAdapter
        spinner.setSelection(1)
        spinnerAdapter.notifyDataSetChanged()
        var selec: MyItemSelect = MyItemSelect()
        spinner.onItemSelectedListener = selec

    }

    /***
     *验证提交结果，根据提交结果中的药品数量和药品名称以及药品用量
     * @return Boolean
     */
    private fun vertiresult(): Boolean {

        //todo
        for (z in 0..list.size - 1) {
            if (list.get(z).unitcount.equals("")) {
                if (list.size > 1) {
                    RxToast.normal("请填写" + list[z].name + "的数量")
                    return false
                } else {
                    RxToast.normal("请填写药品的数量")
                    return false
                }
                return false
            } else if (list[z].percount.equals("")) {
                if (list.size > 1) {
                    RxToast.normal("请填写" + list[z].name + "的用量")
                    return false
                } else {
                    RxToast.normal("请填写药品的用量")
                    return false
                }

                return false
            }

        }
        return true;
    }

    private fun setSpanSize(position: Int, list: ArrayList<RecipeModlex.Others>): Int {
        var maxWidth = windowManager.defaultDisplay.width
        println(maxWidth.toString() + "宽度")
        var textView = View.inflate(this@DiagnosisAct, R.layout.dia_item, null).findViewById<TextView>(R.id.text)
        var itemWidth = textView.paint.measureText(billDiagsList.get(position).name)
        //int itemWidth = (int) (textView.getPaint().measureText(favoriteList.get(position).getName()));

        var count = 3
        if ((maxWidth / 2) / 3 > itemWidth) {
            count = 1;
        } else if ((maxWidth / 2) / 3 < itemWidth && maxWidth / 3 > itemWidth) {
            count = 2;
        } else if (maxWidth / 2 > itemWidth && maxWidth / 3 < itemWidth) {
            count = 3;
        }

        return count
    }

    /***
     * 采用spinner 模式展现下拉框 使用 ArrayAdapter
     * @param view Spinner
     * @param pos Int
     */
    private fun initJlSpinner(view: Spinner, pos: Int) {
        view!!.prompt = "请选择剂量"
        var JlAdapter: ArrayAdapter<String> = ArrayAdapter(this@DiagnosisAct, R.layout.spinner_select, yypcNamelist)
        JlAdapter.setDropDownViewResource(R.layout.item_drop_layout)
        view.dropDownHorizontalOffset = resources.getDimensionPixelOffset(R.dimen.dp_44)
        view!!.adapter = JlAdapter
        JlAdapter.notifyDataSetChanged()
        var selecS: MyItemSelects = MyItemSelects(view, pos)
        view?.onItemSelectedListener = selecS
    }

    private fun initJlSpinnery(view: Spinner, position: Int) {
        view!!.prompt = "请选择用法"
        var JlAdapter: ArrayAdapter<String> = ArrayAdapter(this@DiagnosisAct, R.layout.spinner_select, ypyfNamelist)
        JlAdapter.setDropDownViewResource(R.layout.item_drop_layout)
        view.dropDownHorizontalOffset = resources.getDimensionPixelOffset(R.dimen.dp_44)
        view!!.adapter = JlAdapter
        JlAdapter.notifyDataSetChanged()
        var selecS: MyItemSelectY = MyItemSelectY(view, position)
        view.onItemSelectedListener = selecS;
    }

    internal inner class MyItemSelect : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            when (position) {
                0 -> {
                    type = "1"
                    // 中医诊断
                }
                1 -> {
                    type = "2"
                    //西医诊断
                }
            }
        }
    }

    internal inner class MyItemSelectY(var spinner: Spinner, var pos: Int) : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            // messageList.clear()
            spinner.setSelection(position)
            //   println(yypclist[position].toString() )
            name = ypyflist.get(position).xmmc
            code = ypyflist.get(position).ypyf
            list.get(pos).gytj = code
            list.get(pos).gytjname = name


        }
    }

    internal inner class MyItemSelects(var spinner: Spinner, var positions: Int) : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            // messageList.clear()
            spinner.setSelection(position)
            name = yypclist.get(position).name
            code = yypclist.get(position).code
            list.get(positions).howtouse = code
            list.get(positions).freqname = name


        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == AppCompatActivity.RESULT_OK) {
            if (requestCode == SELECT_CODE) {
                list.add(data?.getSerializableExtra("date") as RecipeModlex.BillDetail)
                adapter?.notifyDataSetChanged()
            } else if (requestCode == SELECT_ZZ) {
                var sym = data?.getSerializableExtra("zz") as RecipeModlex.Others
                if (TextUtils.isEmpty(sym?.name)) {
                    layout.isSelected = false
                } else {
                    layout.setText(sym?.name + " >")
                    layout.isSelected = true
                }
                dia.name = sym.name
                dia.prdiagtype = type
                dia.code = sym.code
                dia.id = sym.id


            } else if (requestCode == SELECT_OTHER) {
                var sym = data?.getSerializableExtra("zz") as RecipeModlex.Others
                billDiagsList.add(sym)
                Diaadapter?.notifyDataSetChanged()
            }
        }
    }

    /***
     *  药品用法请求
     */
    fun getYPYF() {
        var rxdia: RxDialog = RxDialog(this@DiagnosisAct)
        rxdia.setContentView(R.layout.dialog_loading)
        rxdia.show()
        IpUrl.getInstance()!!.ypyf(ConstantViewMolde.getToken())
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<RecipeModlex.Others>>> {
                    override fun onError(e: Throwable?) {
                        rxdia?.dismiss()
                    }

                    override fun onNext(t: HoleResponse<ArrayList<RecipeModlex.Others>>?) {
                        if (t?.code == 0) {
                            rxdia?.dismiss()
                            ypyflist.addAll(t?.data!!)
                            println(ypyflist.toString())
                            for (o in 0..ypyflist.size) {
                                ypyfNamelist.add(ypyflist.get(o).xmmc)

                            }
                            adapter?.notifyDataSetChanged()
                        } else {
                            RxToast.normal(t?.msg ?: "")

                        }

                    }

                    override fun onCompleted() {
                        rxdia?.dismiss()
                    }
                })
    }

    /***
     * 药品频次请求
     */
    fun getYYPC() {
        var rxdia: RxDialog = RxDialog(this@DiagnosisAct)
        rxdia.setContentView(R.layout.dialog_loading)
        rxdia.show()
        IpUrl.getInstance()!!.yypc(ConstantViewMolde.getToken())
                .subscribeOn(rx.schedulers.Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<ArrayList<RecipeModlex.Others>>> {
                    override fun onError(e: Throwable?) {
                        rxdia?.dismiss()
                    }

                    override fun onNext(t: HoleResponse<ArrayList<RecipeModlex.Others>>?) {
                        if (t?.code == 0) {
                            rxdia?.dismiss()
                            yypclist.addAll(t?.data!!)
                            for (o in 0..yypclist.size) {
                                yypcNamelist.add(yypclist.get(o).name)
                            }
                            adapter?.notifyDataSetChanged()
                        } else {
                            RxToast.normal(t?.msg ?: "")
                        }
                    }

                    override fun onCompleted() {
                        rxdia?.dismiss()
                    }
                })
    }

    /***
     *
     */
    fun getRecipList() {
        var rx: RxDialog = RxDialog(this@DiagnosisAct)
        rx.setContentView(R.layout.dialog_loading)
        rx.show()
        IpUrl.getInstance()!!.recipDetial(ConstantViewMolde.getToken(), id!!)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<HoleResponse<RecipeModlex>> {
                    override fun onError(e: Throwable?) {
                        rx.dismiss()
                    }

                    override fun onNext(t: HoleResponse<RecipeModlex>?) {
                        if (t?.code == 0) {
                            list.addAll(t?.data?.bills?.billDetails!!)
                            adapter?.notifyDataSetChanged()
                            if (t.data!!.diags == null) {
                            } else {
                                layout.setText(t.data!!.diags?.name ?: "")
                                layout.isSelected = false
                                when (t.data!!.diags?.prdiagtype) {
//                           1、西医诊断；2、中医病症；3、中医证候
                                    "1" -> {
                                        spinner.setSelection(0)
                                    }
                                    "2" -> {
                                        spinner.setSelection(1)
                                    }
                                }
                                var str: String = t.data!!.diags!!.name ?: ""
                                if (t.data?.others == null || t.data?.others?.size == 0) {
                                } else {
                                    for (i in 0..t?.data?.others?.size!! - 1) {
                                        billDiagsList.add(t?.data?.others!![i])
                                        Diaadapter?.notifyDataSetChanged()
                                    }
                                }
//
                            }
                        }
                    }

                    override fun onCompleted() {
                        rx.dismiss()
                    }
                })
    }

    inner class DoctorAdapter(layoutResId: Int, data: ArrayList<RecipeModlex.BillDetail>?) : com.chad.library.adapter.base.BaseQuickAdapter<RecipeModlex.BillDetail, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: RecipeModlex.BillDetail) {
            helper.setText(R.id.result, item.name + item.spec + item.unit)
            var editText = helper.getView<EditText>(R.id.content)

            var zishu = helper.getView<TextView>(R.id.zishu)
            var yl = helper.getView<TextView>(R.id.yl)
            zishu.setText(item.unitcount ?: "")
            yl.setText(item.percount ?: "")
            zishu.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    item.unitcount = s.toString();
                }
            })
            yl.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    item.percount = s.toString();
                }
            })
            var alyout = helper.getView<LinearLayout>(R.id.layout)
            helper.addOnClickListener(R.id.delete)
            var spinner = helper.getView<Spinner>(R.id.jl)
            var spinners = helper.getView<Spinner>(R.id.yf)
            spinner.dropDownVerticalOffset = resources.getDimensionPixelOffset(R.dimen.dp_32)
            var parm: LinearLayout.LayoutParams = alyout.layoutParams as LinearLayout.LayoutParams
            spinner!!.dropDownHorizontalOffset = ((parm.weight - spinner.layoutParams.width) / 2).toInt()
            spinner.gravity = Gravity.CENTER
            initJlSpinner(spinner, helper.adapterPosition);
            initJlSpinnery(spinners, helper.adapterPosition);

        }
    }

    inner class BillDiagsAdapter(layoutResId: Int, data: ArrayList<RecipeModlex.Others>?) : BaseQuickAdapter<RecipeModlex.Others, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder?, item: RecipeModlex.Others?) {
            helper?.setText(R.id.text, item?.name)
            helper?.addOnClickListener(R.id.delete)
        }
    }

    /***
     * 提交开放请求
     */
    fun postKaiFang() {
        var rx: RxDialog = RxDialog(this@DiagnosisAct)
        rx.setContentView(R.layout.dialog_loading)
        rx.show()
        if (isRemote){
            recipeModlex.isCommon = 1
        }else{
            recipeModlex.isCommon = 0
        }
        recipeModlex.appointid = appoinid ?: ""
        bills.appointid = appoinid ?: ""
        bills.billtype = "1"
        bills.billDetails = list
        for (i in 0..billDiagsList.size - 1) {
            billDiagsList[i].zxlb = type
        }
        recipeModlex.others = billDiagsList
        recipeModlex.diags = dia
        var fee: BigDecimal = BigDecimal(0)
        for (i in 0..list.size - 1) {
            fee = fee.plus(((list[i].price).toBigDecimal()) * ((list[i].unitcount).toBigDecimal()))
        }
        var scale = fee.setScale(2, BigDecimal.ROUND_HALF_UP)
        val format1 = format.format(scale)
        bills.fee = format1
        recipeModlex.bills = bills

        IpUrl.getInstance()!!.Kaifang(ConstantViewMolde.getToken(), recipeModlex)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<HoleResponse<RecipeModlex>> {
                    override fun onError(e: Throwable?) {
                        rx.dismiss()
                    }
                    override fun onNext(t: HoleResponse<RecipeModlex>?) {
                        if (t?.code == 0) {
                            RxToast.normal("开方成功")
                            finish()
                        } else {
                            RxToast.normal(t!!.msg ?: "网络异常，请稍后再试！")
                        }
                    }
                    override fun onCompleted() {
                        rx.dismiss()
                    }
                })
    }
}