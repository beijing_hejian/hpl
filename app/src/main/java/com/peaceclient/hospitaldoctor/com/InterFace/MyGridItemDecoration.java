/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.InterFace;

import android.graphics.Rect;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import com.peaceclient.hospitaldoctor.com.Utils.DensityUtil;


/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * <p>
 * ━━━━━━感觉萌萌哒━━━━━━
 * 神兽保佑,代码无bug
 *
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.InterFace
 * @class describe
 * @anthor admin
 * @time 2021/7/30 9:55
 * @change
 * @chang time
 * @class describe
 */

public class MyGridItemDecoration extends RecyclerView.ItemDecoration {
      int dim = DensityUtil.dip2px(5f);

      @Override
      public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            if (parent.getChildAdapterPosition(view) % 3 == 0) {
                  outRect.left = 0;
                  outRect.top = 0;
                  outRect.bottom = DensityUtil.dip2px(10f);
                  outRect.right = DensityUtil.dip2px(5f);
            } else if (parent.getChildAdapterPosition(view) % 3 == 1) {
                  outRect.left = DensityUtil.dip2px(5f);
                  outRect.top = 0;
                  outRect.bottom = DensityUtil.dip2px(10f);
                  outRect.right = DensityUtil.dip2px(5f);
            } else if (parent.getChildAdapterPosition(view) % 3 == 2) {
                  outRect.left = DensityUtil.dip2px(5f);
                  outRect.right = 0;
                  outRect.top = 0;
                  outRect.bottom = DensityUtil.dip2px(10f);
            }


      }
}
