package com.peaceclient.hospitaldoctor.com.Activity

import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import com.chad.library.adapter.base.BaseViewHolder
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.android.CaptureActivity
import com.peaceclient.hospitaldoctor.com.common.Constant
import com.peaceclient.hospitaldoctor.com.modle.ConstantViewMolde
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.PeisongBean
import kotlinx.android.synthetic.main.drugsear_act.arrow_back
import kotlinx.android.synthetic.main.drugsear_act.radio
import kotlinx.android.synthetic.main.drugsear_act.recycle
import kotlinx.android.synthetic.main.drugsear_act.scan
import kotlinx.android.synthetic.main.drugsear_act.search_text
import kotlinx.android.synthetic.main.drugsear_act.smart
import kotlinx.android.synthetic.main.mydrug_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name HospitalDoctor222
 * @class name：com.peaceclient.hospitaldoctor.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/11/8 17:16
 * @change
 * @chang time
 * @class describe 我的开方页面
 */

class MyDrugAct : HoleBaseActivity() {
    var adapter: PeopleAdapter? = null
    var adapter1: PeopleAdapter? = null
    var adapter2: PeopleAdapter? = null
    var list: ArrayList<PeisongBean> = arrayListOf()
    var list1: ArrayList<PeisongBean> = arrayListOf()
    var list2: ArrayList<PeisongBean> = arrayListOf()
    var currentId: String = "0"
    var number: String = "0"
    var currLayout: RelativeLayout? = null
    var kong: View? = null
    var kong1: View? = null
    var kong2: View? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.mydrug_act)
        kong = View.inflate(Myapplication.mcontext, R.layout.empty_view, null)
        kong1 = View.inflate(Myapplication.mcontext, R.layout.empty_view, null)
        kong2 = View.inflate(Myapplication.mcontext, R.layout.empty_view, null)
        var manager: LinearLayoutManager = LinearLayoutManager(this)
        recycle.layoutManager = manager
        var manager1: LinearLayoutManager = LinearLayoutManager(this)
        recycle1.layoutManager = manager1
        var manager2: LinearLayoutManager = LinearLayoutManager(this)
        recycle2.layoutManager = manager2


        adapter = PeopleAdapter(R.layout.my_drug_item, list)
        adapter1 = PeopleAdapter(R.layout.my_drug_item, list1)
        adapter2 = PeopleAdapter(R.layout.my_drug_item, list2)
        adapter?.bindToRecyclerView(recycle)
        adapter?.setEmptyView(kong)
        adapter1?.bindToRecyclerView(recycle1)
        adapter1?.setEmptyView(kong1)
        adapter2?.bindToRecyclerView(recycle2)
        adapter2?.setEmptyView(kong2)
        recycle.adapter = adapter
        recycle1.adapter = adapter1
        recycle2.adapter = adapter2
        smart.setOnRefreshListener {
            list.clear()
            list1.clear()
            list2.clear()
            getRecipList()
        }
        arrow_back.setOnClickListener {
            finish()
        }
        scan.setOnClickListener {
            var intent: Intent = Intent(this@MyDrugAct, CaptureActivity::class.java)
            startActivityForResult(intent, 1000)
        }

//        radio.set
        radio.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.first -> {
                    currentId = "0"
                    //待审核
                    list.clear()
                    list1.clear(

                    )
                    list2.clear()
                    recycle1.visibility = View.GONE
                    recycle2.visibility = View.GONE
                    recycle.visibility = View.VISIBLE
                    adapter?.notifyDataSetChanged()
                    adapter1?.notifyDataSetChanged()
                    adapter2?.notifyDataSetChanged()
                   // adapter?.notifyDataSetChanged()
                    if(smart.isRefreshing){
                        smart.finishRefresh()
                    }
                    smart.autoRefresh()
                }
                R.id.secend -> {
                    //未通过
                    currentId = "2"
                    list.clear()
                    list1.clear()
                    list2.clear()
                    recycle.visibility = View.GONE
                    recycle1.visibility = View.GONE
                    recycle2.visibility = View.VISIBLE
                    adapter?.notifyDataSetChanged()
                    adapter1?.notifyDataSetChanged()
                    adapter2?.notifyDataSetChanged()
                    //adapter?.notifyDataSetChanged()
                    if(smart.isRefreshing){
                        smart.finishRefresh()
                    }
                    smart.autoRefresh()
                }
                R.id.third -> {
                    //已通过
                    currentId = "1"
                    list.clear()
                    list1.clear()
                    list2.clear()
                    recycle.visibility = View.GONE
                    recycle2.visibility = View.GONE
                    recycle1.visibility = View.VISIBLE
                    adapter?.notifyDataSetChanged()
                    adapter1?.notifyDataSetChanged()
                    adapter2?.notifyDataSetChanged()
                   // adapter?.notifyDataSetChanged()
                    if(smart.isRefreshing){
                        smart.finishRefresh()
                    }
                    smart.autoRefresh()
                }
            }
            adapter?.notifyDataSetChanged()
            adapter1?.notifyDataSetChanged()
            adapter2?.notifyDataSetChanged()
        }
        adapter?.setOnItemClickListener { adapter, view, position ->
            if (currentId == "0") {
                //待审核 可以修改
                var intent: Intent = Intent(this@MyDrugAct, DiagonsisDetialAct::class.java)
                intent.putExtra("id", list.get(position).id)
                intent.putExtra("appid", list.get(position).appointid)
                startActivity(intent)
            } else if (currentId == "1") {
                //审核通过
                var intent: Intent = Intent(this@MyDrugAct, DiagonsisDetialActs::class.java)
                intent.putExtra("id", list.get(position).id)
                intent.putExtra("appid", list.get(position).appointid)
                startActivity(intent)

            } else if (currentId == "2") {
                //审核不通过 ，可以修改
                var intent: Intent = Intent(this@MyDrugAct, DiagonsisDetialAct::class.java)
                intent.putExtra("id", list.get(position).id)
                intent.putExtra("appid", list.get(position).appointid)
                startActivity(intent)
            }
        }
        adapter1?.setOnItemClickListener { adapter, view, position ->
            if (currentId == "0") {
                //待审核 可以修改
                var intent: Intent = Intent(this@MyDrugAct, DiagonsisDetialAct::class.java)
                intent.putExtra("id", list1.get(position).id)
                intent.putExtra("appid", list1.get(position).appointid)
                startActivity(intent)
            } else if (currentId == "1") {
                //审核通过
                var intent: Intent = Intent(this@MyDrugAct, DiagonsisDetialActs::class.java)
                intent.putExtra("id", list1.get(position).id)
                intent.putExtra("appid", list1.get(position).appointid)
                startActivity(intent)

            } else if (currentId == "2") {
                //审核不通过 ，可以修改
                var intent: Intent = Intent(this@MyDrugAct, DiagonsisDetialAct::class.java)
                intent.putExtra("id", list1.get(position).id)
                intent.putExtra("appid", list1.get(position).appointid)
                startActivity(intent)
            }
        }

        adapter2?.setOnItemClickListener { adapter, view, position ->
            if (currentId == "0") {
                //待审核 可以修改
                var intent: Intent = Intent(this@MyDrugAct, DiagonsisDetialAct::class.java)
                intent.putExtra("id", list2.get(position).id)
                intent.putExtra("appid", list2.get(position).appointid)
                startActivity(intent)
            } else if (currentId == "1") {
                //审核通过
                var intent: Intent = Intent(this@MyDrugAct, DiagonsisDetialActs::class.java)
                intent.putExtra("id", list2.get(position).id)
                intent.putExtra("appid", list2.get(position).appointid)
                startActivity(intent)

            } else if (currentId == "2") {
                //审核不通过 ，可以修改
                var intent: Intent = Intent(this@MyDrugAct, DiagonsisDetialAct::class.java)
                intent.putExtra("id", list2.get(position).id)
                intent.putExtra("appid", list2.get(position).appointid)
                startActivity(intent)
            }
        }


    }


    inner class PeopleAdapter(layoutResId: Int, data: ArrayList<PeisongBean>?) : com.chad.library.adapter.base.BaseQuickAdapter<PeisongBean, BaseViewHolder>(layoutResId, data) {
        override fun convert(helper: BaseViewHolder, item: PeisongBean) {

            helper.getView<TextView>(R.id.name).setText(item?.uname ?: "")
            helper.setText(R.id.time, item?.billtime ?: "")
                    .setText(R.id.keshi, item?.regname ?: "")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1000) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                var resu: String = data?.getStringExtra(Constant.CODED_CONTENT)!!
                search_text.setText(resu ?: "")
            }
        }
    }

    /***
     * 处方结算列表 分为待审核、未通过、已通过
     */
    fun getRecipList() {
//        IpUrl.getInstance()!!.repclist(ConstantViewMolde.getToken(), string)
        IpUrl.getInstance()!!.repclist(ConstantViewMolde.getToken(), currentId)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<java.util.ArrayList<PeisongBean>>> {
                    override fun onError(e: Throwable?) {
                        smart.finishRefresh()
                    }

                    override fun onNext(t: HoleResponse<java.util.ArrayList<PeisongBean>>?) {
                        if (t != null) {
                            when (currentId) {
                                "0" -> {

                                    list?.addAll(t?.data!!)
                                    adapter?.notifyDataSetChanged()
                                }
                                "1" -> {
                                    list1?.addAll(t?.data!!)
                                    adapter1?.notifyDataSetChanged()
                                }
                                "2" -> {
                                    list2?.addAll(t?.data!!)
                                    adapter2?.notifyDataSetChanged()
                                }
                            }

                        }
                    }

                    override fun onCompleted() {
                        smart.finishRefresh()
                    }
                })
    }

    override fun onResume() {
        super.onResume()
        smart.autoRefresh()
    }
}