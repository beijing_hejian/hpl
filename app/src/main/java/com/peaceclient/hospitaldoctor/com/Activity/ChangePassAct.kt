/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Activity


import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import com.jakewharton.rxbinding4.view.clicks
import com.peaceclient.hospitaldoctor.com.Base.BaseUrlUtil
import com.peaceclient.hospitaldoctor.com.Base.HoleBaseActivity
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.View.MyDialog
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.vondear.rxtools.view.RxToast
import kotlinx.android.synthetic.main.change_act.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/8/21 17:37
 * @change
 * @chang time
 * @class describe 修改密码（废弃）
 */

class ChangePassAct : HoleBaseActivity() {
    var oldeye: Boolean = false
    var neweye: Boolean = false
    var confirmeye: Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.change_act)
        eyesClick()
        change.setOnClickListener {
            if (indefy()) {
                ChangePaw(old_password!!.text!!.toString(),new_password!!.text!!.toString())
            }
        }
        arrow_back.clicks().throttleFirst(2000,TimeUnit.MICROSECONDS).subscribeOn(io.reactivex.rxjava3.android.schedulers.AndroidSchedulers.mainThread())
                .subscribe {
                    finish()
                }
    }

    private fun eyesClick() {
        old_eye.setOnClickListener {
            // 不可见密码
            if (!oldeye) {
                old_eye.setImageResource(R.drawable.passord_open)
                old_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                old_password.setSelection(old_password.text.length)
            } else // 可见的密码
            {
                old_eye.setImageResource(R.drawable.password_close)
                old_password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD or InputType.TYPE_CLASS_TEXT)
                old_password.setSelection(old_password.text.length)
            }
            oldeye = !oldeye
        }
        new_eye.setOnClickListener {
            // 不可见密码
            if (!neweye) {
                new_eye.setImageResource(R.drawable.passord_open)
                new_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                new_password.setSelection(new_password.text.length)
            } else
            // 可见的密码
            {
                new_eye.setImageResource(R.drawable.password_close)
                new_password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD or InputType.TYPE_CLASS_TEXT)
                new_password.setSelection(new_password.text.length)
            }
            neweye = !neweye
        }
        confirm_eye.setOnClickListener {
            // 不可见密码
            if (!confirmeye) {
                confirm_eye.setImageResource(R.drawable.passord_open)
                confirm_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                confirm_password.setSelection(confirm_password.text.length)
            } else
            // 可见的密码
            {
                confirm_eye.setImageResource(R.drawable.password_close)
                confirm_password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD or InputType.TYPE_CLASS_TEXT)
                confirm_password.setSelection(confirm_password.text.length)
            }
            confirmeye = !confirmeye
        }
    }

    /***
     * 修改密码页面
     * @param password String
     * @param oldpass String
     */
    private fun ChangePaw(password: String, oldpass: String) {
        var loading: MyDialog = MyDialog(this,R.layout.dialog_loading,false,false)
        loading.show()
        var baseUrlUtil: BaseUrlUtil = BaseUrlUtil()
        baseUrlUtil.getInstance().changePaw(password, oldpass)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<HoleResponse<Int>> {
                    override fun onError(e: Throwable?) {
                     loading.dismiss()
                    }
                    override fun onNext(t: HoleResponse<Int>?) {
                        when (t?.code) {
                            0 -> {
                                RxToast.normal(t?.msg?:"")
                                finish()
                            }
                            1 -> RxToast.normal(t?.msg?:"")
                            else
                            -> {RxToast.normal(t?.msg?:"")
                                loading.dismiss()}
                        }
                    }
                    override fun onCompleted() {
                    loading.dismiss()
                    }
                })
    }

    /***
     * 校验新旧密码1.密码格式2.新旧密码是否相同
     * @return Boolean
     */
    fun indefy(): Boolean {
        if (TextUtils.isEmpty(old_password!!.text)) {
            RxToast.normal("请输入原密码")
            return false
        }
        if (TextUtils.isEmpty(new_password!!.text)) {
            RxToast.normal("请输入新密码")
            return false
        } else {
            if (!com.peaceclient.hospitaldoctor.com.Utils.Utils.isPasswordss(new_password.text.toString())) {
                RxToast.normal("新密码格式不正确")
                return false
            }
        }
        if (TextUtils.isEmpty(confirm_password!!.text)) {
            RxToast.normal("请输入确认密码")
            return false
        } else {
            if (!(new_password.text!!.toString() == (confirm_password.text!!.toString()))) {
                RxToast.normal("新密码和确认密码不相同")
                println(new_password.text.toString() +"YUAN"+confirm_password.text.toString())
                return false
            }
        }
        return true
    }
}