/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.peaceclient.hospitaldoctor.com.Fragment


import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Message
import androidx.viewpager.widget.ViewPager
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.paradoxie.autoscrolltextview.VerticalTextview
import com.peaceclient.hospitaldoctor.com.Activity.*
import com.peaceclient.hospitaldoctor.com.Base.IpUrl
import com.peaceclient.hospitaldoctor.com.Base.Myapplication
import com.peaceclient.hospitaldoctor.com.InterFace.*
import com.peaceclient.hospitaldoctor.com.R
import com.peaceclient.hospitaldoctor.com.View.AutoImageView
import com.peaceclient.hospitaldoctor.com.adapter.HomeBannerAdapter
import com.peaceclient.hospitaldoctor.com.modle.HoleResponse
import com.peaceclient.hospitaldoctor.com.modle.KnowleModle
import com.peaceclient.hospitaldoctor.com.modle.LevelBean
import com.vondear.rxtools.view.dialog.RxDialogSure
import kotlinx.android.synthetic.main.homes_fragment.*
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.lang.reflect.Proxy

/**
 *
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 * ━━━━━━感觉萌萌哒━━━━━━
 *       神兽保佑,代码无bug
 * @name hpl-hospital-android
 * @class name：com.peaceclient.com.Activity
 * @class describe
 * @anthor admin
 * @time 2021/7/12 15:59
 * @change
 * @chang time
 * @class describe 首页正在使用
 */

class HomeFragemnts : BaseLazyFragment(), ILaunchManagerService, OnBannerClicker, CancleBack {
    override fun StateChange(s: Int) {}
    override fun loginsuccess(intent: Intent?) {
        startActivity(intent)
    }

    override fun onImageclick(view: View?, position: Int) {
        var intent: Intent = Intent(Myapplication.mcontext, NewsDetailAct::class.java)
        intent.putExtra("id", listBanner?.get(position)?.id?.toString() ?: "")
        startActivity(intent)
    }

    private var rootview: View? = null

    private var viewFlipper: ViewFlipper? = null
    private var homeadapter: HomeBannerAdapter? = null
    private var launchManagerService: ILaunchManagerService? = null
    private var verticalTextview2: VerticalTextview? = null
    private var verticalTextview1: VerticalTextview? = null
    val mHadler: Handler = object : Handler() {
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            when (msg?.what) {
                0 -> {
                    if (home_banner == null) {

                    } else {
                        if (home_banner?.currentItem == Int.MAX_VALUE) {
                            home_banner?.currentItem = 0
                        } else {
                            home_banner?.currentItem = home_banner!!.currentItem + 1
                        }
                    }
                  //  sendEmptyMessageDelayed(0, 2500)
                }

            }
        }
    }
    var listImage: MutableList<Int>? = mutableListOf()
    var listImages: Array<Int?>? = arrayOfNulls<Int>(5)
    var listBanner: MutableList<KnowleModle>? = mutableListOf()
    var listZIXUN: ArrayList<KnowleModle>? = arrayListOf()

    //    var array = arrayOf(R.drawable.home_1, R.drawable.home_2, R.drawable.home_3, R.drawable.home_4, R.drawable.home_5, R.drawable.home_6, R.drawable.home_7, R.drawable.home_8)
    var arraye = arrayOf(R.drawable.home_chufang, R.drawable.home_transport, R.drawable.home_date, R.drawable.chufang_icon)
    // var arrays = arrayOf("智能导诊", "就诊指南", "医院介绍", "科室介绍", "电子就诊卡", "候诊叫号", "报告查询", "费用查询")
    var arrayet = arrayOf( "药品配送", "预约随访", "患者评价","我的开方")
    val fragment: HomeFragemnts? = null
    override fun lazyLoad() {}
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootview = layoutInflater.inflate(R.layout.home_fragments, null)
        var list: MutableList<LevelBean>? = mutableListOf()
        var listOne: MutableList<LevelBean>? = mutableListOf()
        var homebanner = rootview?.findViewById(R.id.home_banner) as ViewPager
        var health: LinearLayout? = rootview?.findViewById<LinearLayout>(R.id.health_layout)
        var fengcai: LinearLayout? = rootview?.findViewById<LinearLayout>(R.id.fengcai_layout)
        launchManagerService = Proxy.newProxyInstance(javaClass.classLoader, arrayOf<Class<*>>(ILaunchManagerService::class.java), LaunchInvocationHandler(this, Myapplication.mcontext)) as ILaunchManagerService
        CallBackUtils.setCancleBack(this)
        for (intz in 0..3) {
            var bean: LevelBean = LevelBean()
            bean.iconid = arraye[intz]
            bean.desc = arrayet[intz]
            listOne!!.add(bean)
        }
        health!!.setOnClickListener {
           var intent: Intent = Intent(Myapplication.mcontext, RemoteCliAct::class.java)
            //var intent: Intent = Intent(Myapplication.mcontext, DiagnosisAct::class.java)
            startActivity(intent)
        }
        fengcai!!.setOnClickListener {
            var intent: Intent = Intent(Myapplication.mcontext, DiagnosisListAct::class.java)

            startActivity(intent)
        }
        val grid = rootview?.findViewById(R.id.grid) as GridView
        grid.setSelector(R.color.color_transparent)
        var adapter: GideAdapter = GideAdapter(listOne!!)
        grid.adapter = adapter
        adapter.notifyDataSetChanged()
        homeadapter = HomeBannerAdapter(listBanner, Myapplication.mcontext)
        homebanner.adapter = homeadapter
        homeadapter?.notifyDataSetChanged()
        homebanner.setCurrentItem(Int.MAX_VALUE / 2)
        getBanner()
        homeadapter?.setListener(this)
        grid.setOnItemClickListener { parent, view, position, id ->
            when (position) {

                0 -> {
                    startActivity(Intent(Myapplication.mcontext,MedicineListAct::class.java))
                }
                1-> {
                    startActivity(Intent(Myapplication.mcontext,SuifangAct::class.java))
                }
                2 -> {
                   startActivity(Intent(Myapplication.mcontext,EvalueTypeAct::class.java))
                    //startActivity(Intent(Myapplication.mcontext,ChartActivity::class.java))
                }
                3 -> {
                    //startActivity(Intent(Myapplication.mcontext,EvalueTypeAct::class.java))
                    startActivity(Intent(Myapplication.mcontext,MyDrugAct::class.java))
                }
            }

        }
        return rootview
    }
    private fun dialogShow() {
        var dialog: RxDialogSure = RxDialogSure(activity)
        dialog.setTitle("提示")
        dialog.setContent("依据业务科室的需要，暂无线上审方需求。仅保留菜单，等需求明确添加")
        dialog.contentView.gravity = Gravity.LEFT
        dialog.contentView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0f)
        dialog.contentView.setTextColor(Color.parseColor("#050505"))
        dialog.sureView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.sureView.setTextColor(Color.parseColor("#376FE9"))
        dialog.titleView.setTextColor(Color.parseColor("#050505"))
        dialog.titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
        dialog.setSure("我知道了")
        dialog.show()
        dialog.setSureListener({
            dialog.dismiss()
        })
    }
    fun getBanner() {
        IpUrl.getInstance()!!.getBanner()
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<MutableList<KnowleModle>>> {
                    override fun onError(e: Throwable?) {
                    }
                    override fun onNext(t: HoleResponse<MutableList<KnowleModle>>?) {
                        if (t?.code == 0) {
                            println()
                            if (t?.data != null) {
                                listBanner?.addAll(t.data!!)
                                homeadapter?.notifyDataSetChanged()
                            }
                        }
                    }
                    override fun onCompleted() {
                    }
                })
    }

    fun getZIXUN() {
        IpUrl.getInstance()!!.getZixun()
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<HoleResponse<MutableList<KnowleModle>>> {
                    override fun onError(e: Throwable?) {
                        println(e.toString() + "err")
                    }

                    override fun onNext(t: HoleResponse<MutableList<KnowleModle>>?) {
                        if (t?.code == 0) {
                            if (t?.data != null) {
                                listZIXUN?.addAll(t.data!!)
                                for (int in 0..listZIXUN?.size!! - 1) {
                                    val inflate = View.inflate(Myapplication.mcontext, R.layout.one_view, null)
                                    var text1: TextView = inflate.findViewById<TextView>(R.id.text1)
                                    var text2: TextView = inflate.findViewById<TextView>(R.id.text2)
                                    text1.setText(listZIXUN?.get(int)?.title)
                                    if (int + 1 > listZIXUN?.size!! - 1) {
                                        text2.setText(listZIXUN?.get(0)?.title)
                                    } else {
                                        text2.setText(listZIXUN?.get(int + 1)?.title)
                                    }
                                }
                            }
                        }
                    }

                    override fun onCompleted() {

                    }
                })
    }


    override fun onResume() {
        super.onResume()
    }

    override fun onInvisible() {
        super.onInvisible()
        mHadler!!.removeMessages(0)
    }

    override fun onVisible() {
        super.onVisible()
    }


    override fun onPause() {
        super.onPause()
    }
    inner internal class DepartAdapter(layoutResId: Int, data: MutableList<LevelBean>?) : BaseQuickAdapter<LevelBean, BaseViewHolder>(layoutResId, data) {
        override fun convert(holder: BaseViewHolder, item: LevelBean) {
            holder.setText(R.id.name, item!!.desc).setImageResource(R.id.image, item!!.iconid)
        }
    }
    override fun visibleToUser() {
        super.visibleToUser()
        mHadler!!.sendEmptyMessage(0)
    }
    override fun inVisibleToUser() {
        super.inVisibleToUser()
        mHadler!!.removeMessages(0)
    }
    class GideAdapter : BaseAdapter {
        var list: MutableList<LevelBean>
        constructor(arr: MutableList<LevelBean>) {
            this.list = arr
        }
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var view = View.inflate(Myapplication.mcontext, R.layout.gide_item, null)
            var imageview: AutoImageView = view.findViewById(R.id.header)
            var textview: TextView = view.findViewById(R.id.text)
            imageview.setImageResource(list.get(position).iconid)
            textview.setText(list.get(position).desc)
            return view!!
        }

        override fun getItem(position: Int): Any {
            return list!!.get(position)
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return list!!.size
        }
    }
}