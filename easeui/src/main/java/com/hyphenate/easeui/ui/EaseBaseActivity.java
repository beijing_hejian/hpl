package com.hyphenate.easeui.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.hyphenate.easeui.EaseUI;
import com.hyphenate.easeui.utils.NavigationBarUtil;
import com.hyphenate.easeui.utils.StatusBarUtil;

@SuppressLint({"NewApi", "Registered"})
public class EaseBaseActivity extends AppCompatActivity {
      protected InputMethodManager inputMethodManager;
      @Override
      protected void onCreate(Bundle arg0) {
            super.onCreate(arg0);
            //http://stackoverflow.com/questions/4341600/how-to-prevent-multiple-instances-of-an-activity-when-it-is-launched-with-differ/
            // should be in launcher activity, but all app use this can avoid the problem
            StatusBarUtil.setRootViewFitsSystemWindows(this, false);
            StatusBarUtil.setTranslucentStatus(this);
            if (NavigationBarUtil.hasNavigationBar(this)) {
                  NavigationBarUtil.initActivity(findViewById(android.R.id.content));
                  NavigationBarUtil.hasNavigationBar(this);
            }
            if (!isTaskRoot()) {
                  Intent intent = getIntent();
                  String action = intent.getAction();
                  if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && action.equals(Intent.ACTION_MAIN)) {
                        finish();
                        return;
                  }
            }
            inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
      }

      @Override
      protected void onResume() {
            super.onResume();
            // cancel the notification
            EaseUI.getInstance().getNotifier().reset();
      }
      protected void hideSoftKeyboard() {
            if (getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
                  if (getCurrentFocus() != null)
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                                  InputMethodManager.HIDE_NOT_ALWAYS);
            }
      }

      /**
       * back
       *
       * @param view
       */
      public void back(View view) {
            finish();
      }
}
